!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/09/23              !
!===============================!
  
  program driver_program

  use m_1_type_definitions
 
#ifdef _OPENMP
  use omp_lib
#endif

  implicit none

  !======================!
  ! 1a. Interface starts !
  !======================!

  integer(kind=si), parameter :: DIMEN  = 1000_si
  integer(kind=si), parameter :: TRIALS = 100_si

  !====================!
  ! 1b. Interface ends !
  !====================!

  ! 2. local variables

  real(kind=dr), allocatable, dimension(:,:) :: matrix           
  real(kind=dr), allocatable, dimension(:)   :: x
  real(kind=dr), allocatable, dimension(:)   :: y
  real(kind=dr)                              :: t1          
  real(kind=dr)                              :: t2
  integer(kind=si)                           :: i1, i2
  real(kind=dr), parameter                   :: ONE_REAL_PGG  = 1.0_dr
  real(kind=dr), parameter                   :: ZERO_REAL_PGG = 0.0_dr
  integer(kind=si), parameter                :: ONE_INT_PGG   = 1_si
  integer(kind=si), parameter                :: ZERO_INT_PGG  = 0_si

  ! 3. Building the matrix - just an example or real symmetric matrix
  
  write(*,*) ""
  write(*,*) "  1 --> Benchmark, OpenMP"
  write(*,*) "  2 --> Title: DGEMV"
  write(*,*) "  3 --> RAM needed: ", (DIMEN/1024.0_dr**3)*8*DIMEN, " GBytes"
  write(*,*) "  4 --> Please wait ..."

  ! 3a.

  allocate(matrix(1:DIMEN,1:DIMEN))

  ! 3b.

  !$omp parallel       &
  !$omp default(none)  &
  !$omp shared(matrix) 

  !$omp do          &
  !$omp private(i1) &
  !$omp private(i2)

  do i1 = 1, DIMEN
    do i2 = 1, DIMEN

      if (i1 == i2) then
        matrix(i1,i2) = 2.5_dr * (i1+1.0_dr) * (i2+1.0_dr)
      else if (i1 /= i2) then
        matrix(i1,i2) = (i1+1.0_dr) * (i2+1.0_dr) / 3.0_dr
      end if
  
    end do
  end do
 
  !$omp end do
  !$omp end parallel
      
  ! 4. building the vectors
  
  ! 4a.

  allocate(x(1:DIMEN))
  allocate(y(1:DIMEN))      

  ! 4b.  

  do i1 = 1, DIMEN
 
    x(i1) = dsin(real(i1,kind=dr))
 
  end do

  ! 4c.

  y = 0.0_dr
  
  ! 5. the m-v product

  call cpu_time(t1) 

  do i1 = 1, TRIALS

    call dgemv('N',            &
                DIMEN,         &
                DIMEN,         &
                ONE_REAL_PGG,  &
                matrix,        &
                DIMEN,         &
                x,             &
                ONE_INT_PGG,   &
                ZERO_REAL_PGG, &
                y,             &
                ONE_INT_PGG)

!    write(*,*) i1

  end do

  call cpu_time(t2)

  ! 6. results

  write(*,*) "  5 --> Total CPU time (seconds) = ", (t2-t1)
  write(*,*) "  6 --> Some results: "
  write(*,*) "  7 --> y(1)  = ", y(1)
  write(*,*) "  8 --> y(10) = ", y(10)
  write(*,*) ""

  ! 7. free up RAM

  deallocate(matrix) 
  deallocate(x)
  deallocate(y)

  end program driver_program

!======!
! FINI !
!======!
