!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/09/24              !
!===============================!

  module m_2_coo_half_disk_direct

  use m_1_type_definitions

#ifdef _OPENMP
  use omp_lib
#endif

  implicit none

  contains

  subroutine coo_half_direct_to_ram(matrix_file,            &
                                    type_of_format,         & 
                                    matrix_dimen,           &
                                    non_zero_ham_elem_half, &
                                    row_index_array,        &            
                                    column_index_array,     &
                                    ham_elem_array)
                                    

  implicit none

  !  1. interface variables

  character(len=200), intent(in)                           :: matrix_file
  character(len=200), intent(in)                           :: type_of_format
  integer(kind=si), dimension(:), allocatable, intent(out) :: row_index_array
  integer(kind=si), dimension(:), allocatable, intent(out) :: column_index_array
  real(kind=dr)   , dimension(:), allocatable, intent(out) :: ham_elem_array
  integer(kind=si), intent(out)                            :: matrix_dimen
  integer(kind=di), intent(out)                            :: non_zero_ham_elem_half

  !  2. local variables

  real(kind=dr)               :: row_tmp
  real(kind=dr)               :: col_tmp
  real(kind=dr)               :: matrix_dimen_tmp
  real(kind=dr)               :: non_tmp
  real(kind=dr), dimension(3) :: dimen_line
  integer(kind=si)            :: rec_len
  integer(kind=di)            :: i1
  integer(kind=si)            :: my_rank
  integer(kind=si), parameter :: BAUN = 200_si

  !  3. open unit to read the data file
  
  inquire(iolength = rec_len) dimen_line ! evaluating the record length

! NOTE: CAN NOT READ IN PARALLEL USING OPENMP FROM A SINGLE FILE
!       THE I/O IT IS VERY SLOW
!       SO, I WILL OPEN THE SAME FILE FROM MULTIPLE THREADS.
!       THAT WORKS FOR INTEL COMPILER BUT NOT FOR GNU and OPEN64
!       SINCE THE SAME FILE IS CONNECTED TO MULTIPLY UNITS.
!       THE LATTER IS AGAINS TO THE FORTRAN 2003 STANDARD.
!       MAYBE THERE IS A WAY TO ADD A FLAG TO AVOID IT.
!       THERE IS NO SUCJ FLAGS.
!       EVEN IF THE SAME FILE IS POINTED TO BY SYMBOLIC LINKS
!       GNU AND OPEN64 DETECTS AS THE SAME FILE.
!       CONCLUSION: NO WAY TO READ THE SAME FILE FROM MULTIPLE THREADS
!                   AT THE SAME TIME USING OPENMP AND FORTRAN.
!                   WORKS ONLY FOR IFORT. SO UNCCOMENT THE OMP FOR PARALLEL I/O

!  !$omp parallel               &
!  !$omp default(none)          &
!  !$omp shared(type_of_format) &
!  !$omp shared(matrix_file)    &
!  !$omp shared(rec_len)        &
!  !$omp private(my_rank)

#ifdef _OPENMP
  my_rank = omp_get_thread_num()
#else 
  my_rank = 0_si
#endif

  open(unit = BAUN+my_rank,   &
       form = type_of_format, &
       file = matrix_file,    &
       status = "old",        &
       action = "read",       &
       access = "direct",     &
       recl = rec_len)  

!  !$omp end parallel

  !  4. read the data file, part 1/2

  if (type_of_format == "unformatted") then 

    read(unit=BAUN, rec=1) dimen_line(1), &
                           dimen_line(2), &
                           dimen_line(3)

    matrix_dimen_tmp = dimen_line(1)
    matrix_dimen_tmp = dimen_line(2)
    non_tmp          = dimen_line(3)

  else 

    write(*,*)  " error in the chosen format: ", type_of_format
    write(*,*)  " the chosen format must be 'unformatted'." 
    stop
 
  end if

  matrix_dimen = int(matrix_dimen_tmp, kind=si)
  non_zero_ham_elem_half = int(non_tmp, kind=di)

  !  5. allocate RAM

  allocate(row_index_array(1:non_zero_ham_elem_half))
  allocate(column_index_array(1:non_zero_ham_elem_half))
  allocate(ham_elem_array(1:non_zero_ham_elem_half))
  
  !  6. read the data file, part 2/2

! NOTE: CAN NOT READ IN PARALLEL USING OPENMP FROM A SINGLE FILE
!       THE I/O IT IS VERY SLOW
 
!  !$omp parallel                       &
!  !$omp default(none)                  &
!  !$omp shared(ham_elem_array)         &
!  !$omp shared(row_index_array)        &
!  !$omp shared(column_index_array)     &
!  !$omp shared(non_zero_ham_elem_half) &
!  !$omp shared(my_rank)     
 
#ifdef _OPENMP
  my_rank = omp_get_thread_num()
#else 
  my_rank = 0_si
#endif

!  !$omp do                    &
!  !$omp private(i1)           &
!  !$omp private(row_tmp)      &
!  !$omp private(col_tmp)      &
!  !$omp private(dimen_line)   &
!  !$omp firstprivate(my_rank)

  do i1 = 1, non_zero_ham_elem_half

    read(unit=BAUN+my_rank, rec=i1+1) dimen_line(1), &
                                      dimen_line(2), &
                                      dimen_line(3)

    row_tmp            = dimen_line(1)
    col_tmp            = dimen_line(2)
    ham_elem_array(i1) = dimen_line(3)
    
    row_index_array(i1)    = int(row_tmp, kind=si)
    column_index_array(i1) = int(col_tmp, kind=si)

  end do 

!  !$omp end do
!  !$omp end parallel

  !  7. close unit and keep the data file 
 
  close(unit=11, status="keep")

  end subroutine coo_half_direct_to_ram

  end module m_2_coo_half_disk_direct

!======!
! FINI !
!======!
