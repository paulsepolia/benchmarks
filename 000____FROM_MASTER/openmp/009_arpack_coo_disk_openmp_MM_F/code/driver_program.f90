!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2011/09/24              !
!===============================!

  program driver_program

  use m_1_type_definitions
  use m_2_coo_half_disk_direct
  use m_3_arp_coo_half_omp

  implicit none

  ! 1. variables only for the subroutine:
  !    "matrix_coo_official_to_ram_half"

  character(len=200) :: matrix_file_name
  character(len=200) :: type_of_format_for_matrix

  ! 2. variables common to the subroutines:
  !    "matrix_coo_official_to_ram_half"
  !        and
  !    "arpack_coo_half"

  integer(kind=si) :: matrix_dimen
  integer(kind=di) :: non_zero_ham_elem
  integer(kind=si), allocatable, dimension(:) :: row_index_array
  integer(kind=si), allocatable, dimension(:) :: column_index_array
  real(kind=dr)   , allocatable, dimension(:) :: ham_elem_array

  ! 3. variables only for the subroutine: "arpack_coo_half"

  real(kind=dr), allocatable, dimension(:,:) :: vectors_array
  real(kind=dr), allocatable, dimension(:,:) :: values_array
  integer(kind=si) :: nev
  integer(kind=si) :: maxitr
  real(kind=dr)    :: tol
  character(len=2) :: which
  integer(kind=si) :: ncv_coeff

  ! 4. local variables for outputting the produced eigensystem

  character(len=200) :: type_of_format_for_output
  character(len=200) :: file_for_eigenvalues
  character(len=200) :: file_for_eigenvectors
 
  ! 5. local variables and parameters
  
  real(kind=dr), parameter :: ZERO = 0.0_dr
  integer(kind=si) :: i1
  integer(kind=si) :: i2

  !======================!
  ! 6a. Interface starts !
  !======================!

  ! 6-1. Name of the matrix file in hard disk

  matrix_file_name = "matrix_direct" 

  ! 6-2. Type of format of the matrix file

  type_of_format_for_matrix = "unformatted"

  ! 6-3. Type of format of the output eigensystem

  type_of_format_for_output = "formatted"

  ! 6-4. Name of the file for eigenvalues

  file_for_eigenvalues = "___va_omp"

  ! 6-5. Name of the file for eigenvectors

  file_for_eigenvectors = "___ve_omp"

  ! 6-6. Number of eigenvalues/eigenvectors to get back

  nev = 300_si

  ! 6-7. Maximum arnoldi restarts

  maxitr = 3000000_si

  ! 6-8. The convergence criterion

  tol = ZERO
  
  ! 6-9. The part of the spectrum

  which = 'SA'

  ! 6-10. The part 'ncv_coeff'

  ncv_coeff = 2_si

  !====================!
  ! 6b. Interface ends ! 
  !====================!

  ! 7. calling the subroutine which reads form the hard disk the matrix: 'matrix_file_name'
  !    and writes in to the ram the variables scalar/arrays:
  !
  !    a. dimension of the matrix                     : "matrix_dimen"
  !    b. the number of non zero hamiltonian elements : "non_zero_ham_elem"
  !    c. the indexes of rows                         : "row_index_array"
  !    d. the indexes of columns                      : "column_index_array"
  !    e. the corresponing hamiltonian elements       : "ham_elm_array"

  call coo_half_direct_to_ram(matrix_file_name,          &
                              type_of_format_for_matrix, &
                              matrix_dimen,              &
                              non_zero_ham_elem,         &
                              row_index_array,           &
                              column_index_array,        &
                              ham_elem_array)

  ! 8. testing. outputting some characteristics of the matrix: "matrix_file_name"

  write(*,*) "================================================================="
  write(*,*) " Inside driver program"
  write(*,*) 
  write(*,*) " Some characteristics values of the matrix under diagonalization"
  write(*,*) 
  write(*,*) " matrix_dimen --> ", matrix_dimen
  write(*,*) " non_zero_ham_elem --> ", non_zero_ham_elem
  write(*,*) " row_index_array(100) --> ", row_index_array(100)
  write(*,*) " column_index_array(100) --> ", column_index_array(100)
  write(*,*) " ham_elem_array(100) --> ", ham_elem_array(100)
  write(*,*) 
  write(*,*) "================================================================="

  ! 9. calling the subroutine which diagonalizes the matrix: "matrix_file_name"

  call arp_coo_half_omp(row_index_array,    &  !  1. input & deallocated
                        column_index_array, &  !  2. input & deallocated
                        ham_elem_array,     &  !  3. input & deallocated
                        matrix_dimen,       &  !  4. input & output
                        non_zero_ham_elem,  &  !  5. input & output
                        which,              &  !  6. input
                        ncv_coeff,          &  !  7. input
                        nev,                &  !  8. input & output
                        maxitr,             &  !  9. input & output
                        tol,                &  ! 10. input & output
                        vectors_array,      &  ! 11. output
                        values_array)          ! 12. output

  ! 10. writing to the hard disk the eigensystem

  if (type_of_format_for_output == "unformatted") then

    ! 10-1. eigenvalues area. unformatted output

    open(unit=11,                   &
         file=file_for_eigenvalues, &
         status="new",              &
         action="write",            &
         form=type_of_format_for_output)

    do i1 = 1, nev

      write(unit=11) values_array(i1,1)

    end do
  
    close(unit=11, status="keep")

    ! 10-2. eigenvectors area. unformatted output

    open(unit=11,                        &
         file=file_for_eigenvectors,     &
         status="new",                   &
         action="write",                 &
         form=type_of_format_for_output)


    do i1 = 1, nev
      do i2 = 1, matrix_dimen

        write(unit=11) vectors_array(i2,i1)

      end do
    end do

    close(unit=11, status="keep")

  else if (type_of_format_for_output == "formatted") then

    ! 10-3. eigenvalues area. formatmatted output

    open(unit=11,                        &
         file=file_for_eigenvalues,      &
         status="new",                   &
         action="write",                 &
         form=type_of_format_for_output)

    do i1 = 1, nev

      write(unit=11,fmt=*) values_array(i1,1)

    end do

    close(unit=11, status="keep")

    ! 10-4. eigenvectors area. formatted output

    open(unit=11,                        &
         file=file_for_eigenvectors,     &
         status="new",                   &
         action="write",                 &
         form=type_of_format_for_output)


    do i1 = 1, nev
      do i2 = 1, matrix_dimen

        write(unit=11,fmt=*) vectors_array(i2,i1)

      end do
    end do

  else 

    write(*,*) " error: type of format must be 'formatted' of 'unformatted' "
    stop   

  end if

  ! 11. deallocations

  deallocate(row_index_array)
  deallocate(column_index_array)
  deallocate(ham_elem_array)
  deallocate(values_array)
  deallocate(vectors_array)

  end program driver_program

!======!
! FINI !
!======!
