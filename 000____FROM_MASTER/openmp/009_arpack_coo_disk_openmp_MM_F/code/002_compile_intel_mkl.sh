#!/bin/bash

# 1. compile

  ifort -e08                          \
        -warn all                     \
        -cpp                          \
        -qopenmp                      \
        -assume buffered_io           \
        m_1_type_definitions.f90      \
        m_2_coo_half_disk_direct.f90  \
        m_3_arp_coo_half_omp.f90      \
        driver_program.f90            \
        /opt/arpack/lib/libarpack_intel_ext.a \
        -mkl=parallel                 \
        -o x_intel_mkl

# 2. clean

  rm *.mod
