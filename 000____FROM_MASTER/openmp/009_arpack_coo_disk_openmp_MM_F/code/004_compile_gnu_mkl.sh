#!/bin/bash

# 1. compile

  gfortran     -std=f2008                   \
               -Wall                        \
               -O3                          \
               -cpp                         \
               -fopenmp                     \
               m_1_type_definitions.f90     \
               m_2_coo_half_disk_direct.f90 \
               m_3_arp_coo_half_omp.f90     \
               driver_program.f90           \
               /opt/arpack/lib_2015/libarpack_gnu_492_ext.a             \
               /opt/mkl/libs_pgg/libmkl_core_thread_lp64_gnu_2015_pgg.a \
               -ldl \
               -o x_gnu_mkl

# 2. clean

  rm *.mod

# 3. exit
