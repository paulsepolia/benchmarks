#!/bin/bash

# 1. compile

  gfortran     -std=legacy                  \
               -Wall                        \
               -O3                          \
               -cpp                         \
               -fopenmp                     \
               m_1_type_definitions.f90     \
               m_2_coo_half_disk_direct.f90 \
               m_3_arp_coo_half_omp.f90     \
               driver_program.f90           \
               /opt/arpack/lib/libarpack_gnu.a \
               -o x_gnu

# 2. clean

  rm *.mod

# 3. exit
