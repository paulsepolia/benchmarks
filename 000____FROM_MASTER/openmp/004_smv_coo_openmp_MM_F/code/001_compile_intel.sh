#!/bin/bash

# 1. compile

  ifort -e03                     \
        -warn all                \
        -O3                      \
        -cpp                     \
        -qopenmp                 \
        m_1_type_definitions.f90 \
        m_2_coo_ram.f90          \
        m_3_smv_omp_ram.f90      \
        driver_program.f90       \
        -o x_intel

# 2. clean

  rm *.mod
