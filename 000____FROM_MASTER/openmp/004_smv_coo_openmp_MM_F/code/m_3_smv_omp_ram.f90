!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/09/23              !
!===============================!

  module m_3_smv_omp_ram

  use m_1_type_definitions

#ifdef _OPENMP
  use omp_lib
#endif

  implicit none

  contains

  subroutine smv_coo_omp_fortran(row_index_array,    &  
                                 column_index_array, &  
                                 ham_elem_array,     &  
                                 matrix_dimen,       &     
                                 non_zero_ham_elem,  &
                                 x,                  &
                                 y,                  &
                                 nt)
              

  implicit none

  ! 1. variables

  integer(kind=si), intent(in),  allocatable, dimension(:) :: row_index_array        
  integer(kind=si), intent(in),  allocatable, dimension(:) :: column_index_array     
  real(kind=dr),    intent(in),  allocatable, dimension(:) :: ham_elem_array         
  real(kind=dr),    intent(in),  allocatable, dimension(:) :: x
  real(kind=dr),    intent(out), allocatable, dimension(:) :: y  
  integer(kind=si), intent(in)                             :: matrix_dimen
  integer(kind=di), intent(in)                             :: non_zero_ham_elem
  integer(kind=si), intent(in)                             :: nt

  ! 2. openmp help variables. openmp part 1

  real(kind=dr), allocatable, dimension(:,:) ::  x1
  real(kind=dr), allocatable, dimension(:,:) ::  y1
  integer(kind=di) :: i2
  integer(kind=si) :: i1
  integer(kind=si), allocatable ,dimension(:) :: irow, icolumn

  allocate(y(1:matrix_dimen))
  allocate(x1(1:matrix_dimen,1:nt)) ! x1
  allocate(y1(1:matrix_dimen,1:nt)) ! y1
  allocate(irow(1:nt))              ! irow
  allocate(icolumn(1:nt))           ! icolumn
 
    do i1 = 1, nt
      x1(:,i1) = x
    end do

    y1 = 0.0_dr

  ! 3. OpenMP do-loop

  !$omp parallel do                &
  !$omp default(none)              &
  !$omp private(i1)                & 
  !$omp private(i2)                &
  !$omp shared(non_zero_ham_elem)  &
  !$omp shared(nt)                 &
  !$omp shared(irow)               &
  !$omp shared(icolumn)            &
  !$omp shared(y1)                 &
  !$omp shared(x1)                 &
  !$omp shared(ham_elem_array)     &
  !$omp shared(row_index_array)    &
  !$omp shared(column_index_array)

    do i1 = 1, nt

      do i2 = (i1-1)*non_zero_ham_elem/nt + 1, (i1)*non_zero_ham_elem/nt     
 
        irow(i1) = row_index_array(i2)
 
        icolumn(i1) = column_index_array(i2)
 
        y1(irow(i1),i1) = y1(irow(i1),i1) + ham_elem_array(i2)*x1(icolumn(i1),i1)

      end do
 
    end do 

  !$omp end parallel do

    !# WARNING : DANGER OF ROUND-OFF ERRORS AT THE FOLLOWING STATEMENT.

    y = 0.0_dr

    do i1 = 1, nt

      y = y + y1(:,i1) 

    end do      

  end subroutine smv_coo_omp_fortran

  end module m_3_smv_omp_ram

!======!
! FINI !
!======!
