!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/09/23              !
!===============================!

  module m_2_coo_ram

  use m_1_type_definitions

#ifdef _OPENMP
  use omp_lib
#endif

  implicit none

  contains

  subroutine matrix_coo_ram(matrix_dimen_sparse, &
                            non_zero_ham_elem,   &
                            row_index_array,     &            
                            column_index_array,  &
                            ham_elem_array,      &
                            sparsity)
                                    

  implicit none

  ! 1. interface variables

  integer(kind=si), dimension(:), allocatable, intent(out) :: row_index_array
  integer(kind=si), dimension(:), allocatable, intent(out) :: column_index_array
  real(kind=dr)   , dimension(:), allocatable, intent(out) :: ham_elem_array
  integer(kind=si), intent(in) :: matrix_dimen_sparse
  integer(kind=di), intent(out) :: non_zero_ham_elem
  real(kind=dr), intent(in) :: sparsity 

  ! 2. local variables
   
  integer(kind=si) :: matrix_dimen_dense
  integer(kind=di) :: non_zero_ham_elem_help
  integer(kind=di) :: i1
  integer(kind=di) :: i2

  ! 3. allocate RAM space

  non_zero_ham_elem_help = &
    int((real(matrix_dimen_sparse,kind=dr)**2) * (100_si-sparsity)/100_si, kind=di)

  matrix_dimen_dense = &
    int(dsqrt(real( non_zero_ham_elem_help, kind=dr)), kind=si)

  non_zero_ham_elem = &
    int(real(matrix_dimen_dense, kind=dr) * real (matrix_dimen_dense, kind=dr), kind=di)

  allocate(row_index_array   (1:non_zero_ham_elem))
  allocate(column_index_array(1:non_zero_ham_elem))
  allocate(ham_elem_array    (1:non_zero_ham_elem))
 
  ! 4. building the matrix 
  !    the matrix can be real symmetric or non-symmetric

  !$omp parallel                   &
  !$omp default(none)              &
  !$omp shared(ham_elem_array)     &
  !$omp shared(matrix_dimen_dense)  

  !$omp do          &
  !$omp private(i1) &
  !$omp private(i2)

  do i1 = 1, matrix_dimen_dense
    do i2 = 1, matrix_dimen_dense

      ham_elem_array(i2+(i1-1)*matrix_dimen_dense) = &
        dcos(real(i1,kind=dr)*real(i2,kind=dr))

    end do
  end do

  !$omp end do
  !$omp end parallel

  ! 5. building the row_index_array & column_index_array

  !$omp parallel                   &
  !$omp default(none)              &
  !$omp shared(matrix_dimen_dense) &
  !$omp shared(row_index_array)    &
  !$omp shared(column_index_array)

  !$omp do          &
  !$omp private(i1) &
  !$omp private(i2)

   do i1 = 1, matrix_dimen_dense
    do i2 = 1, matrix_dimen_dense

      row_index_array(i2+(i1-1)*matrix_dimen_dense) = int(i1,kind=si)
      column_index_array(i2+(i1-1)*matrix_dimen_dense) = int(i2, kind=si)     

    end do 
  end do

  !$omp end do
  !$omp end parallel

  end subroutine matrix_coo_ram

  end module m_2_coo_ram

!======!
! FINI !
!======!
