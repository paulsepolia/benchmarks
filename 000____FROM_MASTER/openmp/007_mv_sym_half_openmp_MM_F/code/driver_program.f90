!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/09/24              !
!===============================!

  program driver_program

  use m_1_type_definitions
  use m_2_mv_sym_half

#ifdef _OPENMP
  use omp_lib
#endif

  implicit none

  !======================!
  ! 1a. Interface starts !
  !======================!

  integer(kind=si), parameter :: DIMEN  = 10000_si
  integer(kind=si), parameter :: TRIALS = 50_si

  !====================!
  ! 1b. Interface ends !
  !====================!

  ! 2. local variables
  
  real(kind=dr), allocatable, dimension(:) :: matrix
  real(kind=dr), allocatable, dimension(:) :: x
  real(kind=dr), allocatable, dimension(:) :: y
  integer(kind=di) :: dim_total
  integer(kind=di) :: indx
  integer(kind=si) :: i1
  integer(kind=si) :: i2
  real(kind=dr)    :: t1
  real(kind=dr)    :: t2

  ! 3. output

  write(*,*) ""
  write(*,*) "  1 --> Benchmark, OpenMP "
  write(*,*) "  2 --> Title: Matrix Dense Symmetric Unique - Vector Product "
  write(*,*) "  3 --> RAM needed = ",  &
             (DIMEN/(1024.0_dr**3.0_dr))*DIMEN*4_si, " GBytes "
  write(*,*) "  4 --> Please wait ... "                                         

  ! 4. Building the matrix - an example or real symmetric matrix

  dim_total = (int(DIMEN,kind=di)**2 + DIMEN)/2_si 

  ! 4a.

  allocate(matrix(1:dim_total))
  indx = 0_di

  ! 4b.

  do i1 = 1,  DIMEN
    do i2 = i1, DIMEN
    
      indx = indx + 1_di

      if (i1 == i2) then
        matrix(indx) = 2.5_dr * (i1+1.0_dr) * (i2+1.0_dr)
      else if (i1 /= i2) then
        matrix(indx) = (i1+1.0_dr) * (i2+1.0_dr) / 3.0_dr
      end if

    end do
  end do

  ! 5. Building the random "x" vector and initialization of "y"

  ! 5a.

  allocate(x(1:DIMEN))

  ! 5b.

  do i1 = 1, DIMEN
    x(i1) = sin(real(i1,kind=dr))
  end do

  ! 6. Calling the dense matrix symmetric unique - vector product subroutine

  call cpu_time(t1)

  do i1 = 1, TRIALS

    call mv_sym_half(matrix, &
                     DIMEN,  &
                     x,      &
                     y)

  end do
  
  call cpu_time(t2)

  ! 7. Output

  write(*,*) "  5 --> Total CPU time (seconds) = ", (t2-t1)
  write(*,*) ""

  end program driver_program

!======!
! FINI !
!======!
