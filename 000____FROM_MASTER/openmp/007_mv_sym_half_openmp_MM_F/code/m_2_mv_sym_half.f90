!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/09/24              !
!===============================!

  module m_2_mv_sym_half

  use m_1_type_definitions

  implicit none

  contains

!============================================================!
!                                                            !
! 1a. Subroutine: mv_sym_half(matrix, &                      !
!                             dimen,  &                      !
!                             x,      &                      !
!                             y)                             !
!                                                            !
! 1b. This subroutine is an impementation of a               !
!     dense-symmetric-matrix-vector product.                 !
!     The dense matrix is stored as a vector                 !
!     which contains only the half elements                  !
!     (lower triangle or upper triangle).                    !
!     The purpose of this impementation is to save RAM space !
!     and also to speed up the dense-matrix-vector product.  !
!                                                            !
! 1c. The following code developed using Mathematica.        !
!     Strictly speaking is a copy-paste Mathematica code.    !
!                                                            !
!============================================================!

  subroutine mv_sym_half(matrix, & 
                         dimen,  &
                         x,      &
                         y)

  implicit none

  ! 1. input variables

  real(kind=dr), intent(in), allocatable, dimension(:)  :: matrix
  integer(kind=si), intent(in)                          :: dimen
  real(kind=dr), intent(in), allocatable, dimension(:)  :: x
  real(kind=dr), intent(out), allocatable, dimension(:) :: y

  ! 2. local variables

  integer(kind=di) :: i1
  integer(kind=di) :: i2
  integer(kind=di) :: i3
  integer(kind=di), allocatable, dimension(:) :: peakF
  integer(kind=di), allocatable, dimension(:) :: downF
  real(kind=dr)   , allocatable, dimension(:) :: mat_vec

  ! 3. dynamic allocations

  allocate(mat_vec(1:dimen))
  allocate(peakF(1:dimen))
  allocate(downF(1:dimen))
  allocate(y(1:dimen))
 
  y = 0.0_dr

  ! 4. main algorithm
  
  ! 4a.

  do i1 = 1, dimen
  
    peakF(i1) = ((-i1+3)*i1)/2 + dimen * (i1-1)

  end do

  ! 4b.

  do i1 = 1, dimen
  
    downF(i1) = ((1-i1)*i1)/2 + dimen * (i1)

  end do

  ! 4c.

  do i1 = 1, dimen

    mat_vec(1) = matrix(i1)

    ! 4c1.
    do i3 = 1, i1-2
      mat_vec(i3+1) = matrix(i1+(-i3+2*dimen*i3-i3**2)/2)
    end do

    ! 4c2.
    mat_vec(i1:dimen) = matrix(peakF(i1):downF(i1))
 
    ! 4c3.
    do i2 = 1, dimen
      y(i1) = y(i1) + mat_vec(i2)*x(i2)
    end do

  end do

  end subroutine mv_sym_half

  end module m_2_mv_sym_half

!======!
! FINI !
!======!
