!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/09/24              !
!===============================!

  program driver_program
 
  use m_1_type_definitions

#ifdef _OPENMP
  use omp_lib
#endif 

  implicit none
 
  !=======================!
  !  1a. Interface starts !
  !=======================! 

  integer(kind=si), parameter :: DIMEN  = 10000_si
  integer(kind=si), parameter :: TRIALS = 100_si

  !=====================!
  !  1b. Interface ends !
  !=====================!

  real(kind=dr), parameter :: ZERO = 0.0_dr
  real(kind=dr)    :: t1
  real(kind=dr)    :: t2
  integer(kind=si) :: i1
  integer(kind=si) :: i2
  integer(kind=si) :: i3
  real(kind=dr), allocatable, dimension(:,:) :: array1
  real(kind=dr), allocatable, dimension(:,:) :: array2
 
  !  2. output
 
  write(*,*) " "
  write(*,*) "==============================="
  write(*,*) "The 'where' construct benchmark"
  write(*,*) "==============================="
 
  !  3. allocate RAM
 
  allocate(array1(1:DIMEN,1:DIMEN))
  allocate(array2(1:DIMEN,1:DIMEN))
 
  !  4. building the arrays
 
  call cpu_time(t1)

  !$omp parallel       &
  !$omp default(none)  &
  !$omp shared(array1) 
 
  !$omp do          &
  !$omp private(i1) &
  !$omp private(i2)

  do i1 = 1, DIMEN
    do i2 = 1, DIMEN
   
      array1(i2,i1) = dsin(real(i1+i2,kind=dr))
   
    end do
  end do

  !$omp end do
  !$omp end parallel
 
  call cpu_time(t2)
 
  !  5. output
 
  write(*,*) " "
  write(*,*) " 1. "
  write(*,*) " "
  write(*,*) " cpu time for initialization = ", (t2-t1)
  write(*,*) " array1(1,1)                 = ", array1(1,1)
  write(*,*) " array1(dim,dim)             = ", array1(DIMEN,DIMEN)
  write(*,*) " dimension                   = ", DIMEN 
 
  !  6. the "where" construct
 
  call cpu_time(t1)
 
  do i3 = 1, TRIALS

    !$omp parallel workshare
 
    where(array1 > ZERO)
   
      array2 = dlog(array1)
 
    else where
   
      array2 = -9.9999999999
 
    end where
 
    !$omp end parallel workshare

  end do
 
  call cpu_time(t2)
 
  write(*,*) " "
  write(*,*) " 2. "
  write(*,*) " "
  write(*,*) " cpu time for 'where' construct = ", (t2-t1)
  write(*,*) " array1(1,1)                    = ", array1(1,1)
  write(*,*) " array1(dim,dim)                = ", array1(DIMEN,DIMEN)
  write(*,*) " array2(1,1)                    = ", array2(1,1)
  write(*,*) " array2(dim,dim)                = ", array2(DIMEN,DIMEN)
 
  !  7.
 
  call cpu_time(t1)
 
  do i3 = 1, TRIALS
 
    !$omp parallel       &
    !$omp default(none)  &
    !$omp shared(array1) &
    !$omp shared(array2)
  
    !$omp do          &
    !$omp private(i1) &
    !$omp private(i2)

    do i1 = 1, DIMEN
      do i2 = 1, DIMEN
   
        if (array1(i2,i1) > ZERO) then
   
          array2(i2,i1) = dlog(array1(i2,i1))
 
        else
   
          array2(i2,i1) = -9.9999999999_dr
    
        end if
   
      end do
    end do

    !$omp end do
    !$omp end parallel

  end do
 
  call cpu_time(t2)
 
  write(*,*) " "
  write(*,*) " 3. "
  write(*,*) " "
  write(*,*) " cpu time for 'do-loop' = ", (t2-t1)
  write(*,*) " array1(1,1)            = ", array1(1,1)
  write(*,*) " array1(dim,dim)        = ", array1(DIMEN,DIMEN)
  write(*,*) " array2(1,1)            = ", array2(1,1)
  write(*,*) " array2(dim,dim)        = ", array2(DIMEN,DIMEN)
  write(*,*) ""
 
  end program driver_program

!======!
! FINI !
!======!
