!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/09/24              !
!===============================!

  program driver_program

  use m_1_type_definitions
  use m_2_matrix_coo_fake_ram
  use m_3_arpack_coo_omp_ram

#ifdef _OPENMP
  use omp_lib
#endif

  implicit none

  !  1. input variables

  integer(kind=si), parameter :: matrix_dimen = 1000_si

  !  2. local variables

  integer(kind=di), parameter :: non_zero_ham_elem = &
    int(matrix_dimen, kind=di) * matrix_dimen

  integer(kind=si), allocatable, dimension(:) :: row_index_array
  integer(kind=si), allocatable, dimension(:) :: column_index_array
  real(kind=dr), allocatable, dimension(:)    :: ham_elem_array
  real(kind=dr), allocatable, dimension(:,:)  :: vectors_array
  real(kind=dr), allocatable, dimension(:,:)  :: values_array
  integer(kind=si) :: nev
  integer(kind=si) :: maxitr
  real(kind=dr)    :: tol
  character(len=2) :: which
  integer(kind=si) :: ncv_coeff
  integer(kind=si), parameter :: UNIT_RW = 12

  !  3. local variables for outputting the produced eigensystem

  character(len=200) :: type_of_format_for_output
  character(len=200) :: file_for_eigenvalues
  character(len=200) :: file_for_eigenvectors
 
  !  4. local variables and parameters
  
  real(kind=dr), parameter :: ZERO = 0.0_dr
  integer(kind=si) :: i1
  integer(kind=si) :: i2

  !============================================================================!
  ! Interface starts                                                           !
  !============================================================================!
  !  5. All the necessary input for the driver

  !  5-1. type of format of the output eigensystem

  type_of_format_for_output = "formatted"

  !  5-2. name of the file for eigenvalues

  file_for_eigenvalues = "___va"

  !  5-3. name of the file for eigenvectors

  file_for_eigenvectors = "___ve"

  !  5-4. number of eigenvalues/eigenvectors to get back

  nev = 100_si

  !  5-5. maximum arnoldi restarts

  maxitr = 3000000_si

  !  5-6. the convergence criterion

  tol = ZERO

  !  5-7. the part of the spectrum

  which = 'SA'

  !  5-8. the part 'ncv_coeff'

  ncv_coeff = 2_si

  !============================================================================!
  ! Interface ends                                                             !
  !============================================================================!

  !  6. calling the subroutine which reads
  !     from the hard disk the matrix: 'matrix_file_name'
  !     and writes in to the ram the variables scalar/arrays:
  !
  !  a. dimension of the matrix                     : "matrix_dimen"
  !  b. the number of non zero hamiltonian elements : "non_zero_ham_elem"
  !  c. the indexes of rows                         : "row_index_array"
  !  d. the indexes of columns                      : "column_index_array"
  !  e. the corresponing hamiltonian elements       : "ham_elm_array"  

  call matrix_coo_fake_ram(matrix_dimen,       &
                           non_zero_ham_elem,  &
                           row_index_array,    &
                           column_index_array, &
                           ham_elem_array)

  !  7. testing. outputting some characteristics
  !     of the matrix: "matrix_file_name"

  write(*,*) "==========================================================================="
  write(*,*) " Inside driver program"
  write(*,*) 
  write(*,*) " Some characteristics values of the matrix under diagonalization"
  write(*,*) 
  write(*,*) " matrix_dimen --> ", matrix_dimen
  write(*,*) " non_zero_ham_elem --> ", non_zero_ham_elem
  write(*,*) " row_index_array(100) --> ", row_index_array(100)
  write(*,*) " column_index_array(100) --> ", column_index_array(100)
  write(*,*) " ham_elem_array(100) --> ", ham_elem_array(100)
  write(*,*) 
  write(*,*) "==========================================================================="


  !  8. calling the subroutine which diagonalizes the matrix

  call arp_coo_omp(row_index_array,    &  !   1. input & deallocated
                   column_index_array, &  !   2. input & deallocated
                   ham_elem_array,     &  !   3. input & deallocated
                   matrix_dimen,       &  !   4. input & output
                   non_zero_ham_elem,  &  !   5. input & output
                   which,              &  !   6. input
                   ncv_coeff,          &  !   7. input
                   nev,                &  !   8. input & output
                   maxitr,             &  !   9. input & output
                   tol,                &  !  10. input & output
                   vectors_array,      &  !  11. output
                   values_array)          !  12. output

  !  9. writing to the hard disk the eigensystem

  if (type_of_format_for_output == "unformatted") then

    !  9a. eigenvalues area. unformatted output

    open(unit=UNIT_RW,              &
         file=file_for_eigenvalues, &
         status="new",              &
         action="write",            &
         form=type_of_format_for_output)

    do i1 = 1, nev

      write(unit=UNIT_RW) values_array(i1,1)

    end do
  
    close(unit=UNIT_RW, status="keep")

    !  9b. eigenvectors area. unformatted output

    open(unit=UNIT_RW,               &
         file=file_for_eigenvectors, &
         status="new",               &
         action="write",             &
         form=type_of_format_for_output)


    do i1 = 1, nev
      do i2 = 1, matrix_dimen

        write(unit=UNIT_RW) vectors_array(i2,i1)

      end do
    end do

    close(unit=UNIT_RW, status="keep")

  else if (type_of_format_for_output == "formatted") then

    ! 9c. eigenvalues area. formatmatted output

    open(unit=UNIT_RW,              &
         file=file_for_eigenvalues, &
         status="new",              &
         action="write",            &
         form=type_of_format_for_output)

    do i1 = 1, nev

      write(unit=UNIT_RW,fmt=*) values_array(i1,1)

    end do

    close(unit=UNIT_RW, status="keep")

    ! 9d. eigenvectors area. formatted output

    open(unit=UNIT_RW,               &
         file=file_for_eigenvectors, &
         status="new",               &
         action="write",             &
         form=type_of_format_for_output)


    do i1 = 1, nev
      do i2 = 1, matrix_dimen

        write(unit=11,fmt=*) vectors_array(i2,i1)

      end do
    end do

  else 

    write(*,*) " error: type of format must be 'formatted' of 'unformatted' "
    stop   

  end if

  ! 10. deallocations

  deallocate(row_index_array)
  deallocate(column_index_array)
  deallocate(ham_elem_array)
  deallocate(values_array)
  deallocate(vectors_array)

  end program driver_program

!======!
! FINI !
!======!
