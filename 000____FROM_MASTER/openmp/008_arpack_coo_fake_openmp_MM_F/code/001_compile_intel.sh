#!/bin/bash

# 1. compile

  ifort -e08                        \
        -warn all                   \
        -cpp                        \
        -qopenmp                    \
        m_1_type_definitions.f90    \
        m_2_matrix_coo_fake_ram.f90 \
        m_3_arpack_coo_omp_ram.f90  \
        driver_program.f90          \
        /opt/arpack/lib/libarpack_intel.a   \
        -o x_intel

# 2. clean

  rm *.mod
