#!/bin/bash

# 1. compile

  ifort -e03                     \
        -warn all                \
        -O3                      \
        -static                  \
        -parallel                \
        -par-threshold0          \
        -cpp                     \
        m_1_type_definitions.f90 \
        m_2_mat_mat.f90          \
        driver_program.f90       \
        -o x_intel

# 2. clean

  rm *.mod
  rm *.o
