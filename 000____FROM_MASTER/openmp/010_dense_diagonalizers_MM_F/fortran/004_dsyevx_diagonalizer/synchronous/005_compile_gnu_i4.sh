#!/bin/bash

# 1. compile

  gfortran  -std=f2008                  \
            -O3                         \
            -fopenmp                    \
            m_1_type_definitions.f90    \
            m_2_matrix_function.f90     \
            m_3_write_openmp.f90        \
            m_4_matrix_via_function.f90 \
            m_5_dsyevx_pgg.f90          \
            driver_dsyevx.f90           \
            /opt/lapack/lib_2015/liblapack_gnu.a \
            /opt/blas/lib_2015/libblas_gnu.a     \
            -o x_gnu_i4

# 2. clean

  rm *.mod

# 3. exit
