#!/bin/bash

# 1. compile

  ifort  -e08                           \
         -warn all                      \
         -O3                            \
         -parallel                      \
         -par-threshold0                \
         -openmp                        \
         -assume buffered_io            \
         m_1_type_definitions.f90       \
         m_2_matrix_function.f90        \
         m_3_write_openmp.f90           \
         m_4_matrix_via_function.f90    \
         m_5_dsyevx_pgg.f90             \
         driver_dsyevx.f90              \
         -L$MKLPATH                     \
         -I$MKLINCLUDE                  \
         -L$MKLPATH/lmkl_solver_lp64.a  \
         -Wl,--start-group              \
         $MKLPATH/libmkl_intel_lp64.a   \
         $MKLPATH/libmkl_intel_thread.a \
         $MKLPATH/libmkl_core.a         \
         -Wl,--end-group                \
         -o x_intel_mkl_i4

# 2. clean

  rm *.mod

# 3. exit 
