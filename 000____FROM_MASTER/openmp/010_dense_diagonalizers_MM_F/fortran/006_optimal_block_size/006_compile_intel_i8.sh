#!/bin/bash

# 1. compile

  ifort  -e03                     \
         -warn all                \
         -O3                      \
         -parallel                \
         -par-threshold0          \
         -openmp                  \
         m_1_type_definitions.f90 \
         optimal_block_size.f90   \
         /opt/lapack/lib_2015/liblapack_intel_i8.a \
         /opt/blas/lib_2015/libblas_intel_i8.a     \
         -o x_intel_i8

# 2. clean

  rm *.mod

# 3. exit
