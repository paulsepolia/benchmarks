#!/bin/bash

# 1. compile

  gfortran  -std=f2008                     \
            -Wall                          \
            -O3                            \
            -fopenmp                       \
            m_1_type_definitions.f90       \
            optimal_block_size.f90         \
            -L$MKLPATH                     \
            -I$MKLINCLUDE                  \
            -L$MKLPATH/lmkl_solver_lp64.a  \
            -Wl,--start-group              \
            $MKLPATH/libmkl_intel_lp64.a   \
            $MKLPATH/libmkl_intel_thread.a \
            $MKLPATH/libmkl_core.a         \
            -Wl,--end-group                \
            -liomp5                        \
            -lpthread                      \
            -lm                            \
            -o x_gnu_mkl_i4

# 2. clean

  rm *.mod

# 3. exit
