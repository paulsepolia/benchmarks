!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2014/12/12              !
!===============================!

  module m_2_matrix_function

  use m_1_type_definitions, only: si, dr

  implicit none

  contains

!====================================================================!
!====================================================================!

  pure function matrix_function(i,j)
  
  implicit none  

  real(kind=dr)                :: matrix_function
  integer(kind=si), intent(in) :: i
  integer(kind=si), intent(in) :: j 

    matrix_function = 0.0_dr
  
    if (abs(i-j) < 5_si) then
      matrix_function = i+j
    else if (abs(i-j) >= 5_si) then
      matrix_function = 0.0_dr
    end if

  end function matrix_function

!====================================================================!
!====================================================================!

  end module m_2_matrix_function

! FINI 
