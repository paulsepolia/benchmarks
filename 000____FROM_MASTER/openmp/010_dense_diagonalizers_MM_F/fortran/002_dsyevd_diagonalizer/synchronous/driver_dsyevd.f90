!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2014/12/13              !
!===============================!

  program driver_dsyevd

  use m_1_type_definitions,    only: si, dr, fi
  use m_2_matrix_function,     only: matrix_function
  use m_3_write_openmp,        only: matrix_write_openmp, vector_write_openmp
  use m_4_matrix_via_function, only: matrix_via_function
  use m_5_dsyevd_pgg,          only: dsyevd_pgg

  implicit none

  !  1. Interface variables

  character                                  :: jobz  ! 'V' or 'N' for eigenpairs or eigenvalues
  character                                  :: uplo  ! 'U' or 'L' for lower or upper triangle
  integer(kind=fi)                           :: info  ! on exit if '0' then success
  integer(kind=fi)                           :: n     ! matrix dimension
  real(kind=dr), allocatable, dimension(:,:) :: a     ! matrix on entran - eigenvectors on exit
  real(kind=dr), allocatable, dimension(:)   :: w     ! eigenvalues vector

  !  2. Local variables

  integer(kind=si)   :: i
  integer(kind=fi)   :: m
  character(len=200) :: file_system  ! "lustre_off" or "lustre_on"
  character(len=200) :: va_file      ! file name for eigenvalues
  character(len=200) :: ve_file      ! file name for eigenvectors
  character(len=200) :: out_format   ! "by_column" or "by_row"

  !  3. Initialization of the interface variables

  jobz        = 'V'
  uplo        = 'U'
  n           = 100_fi
  m           = n                ! symmetrix matrix
  va_file     = "___e_values"
  ve_file     = "___e_vectors"
  file_system = "lustre_on"      ! "lustre_off" or "lustre_on" 
  out_format  = "by_column"      ! "by_column" or "by_row"

  !  4. Allocation of the workspace

  allocate(a(1:m,1:n))
  allocate(w(1:m))

  !  5. Build the matrix

  call matrix_via_function(m,                &  !  1
                           n,                &  !  2
                           a,                &  !  3
                           matrix_function)     !  4

  !  6. The main diagonalization step

  call dsyevd_pgg(jobz,  &  !  1
                  uplo,  &  !  2
                  n,     &  !  3
                  a,     &  !  4
                  w,     &  !  5
                  info)     !  6

  !============================================================================!
  ! TEST
  ! Write down info, if the job was successful or not
  ! Write in to the hard disk the eigevalues in asceding order

  open(unit=11,           &
       file="___e_test",  &
       status="replace",  &
       action="write",    &
       form="formatted")

  write(unit=11,fmt=*) " info = ", info

  do i = 1, int(m, kind=si)

    write(unit=11, fmt=*)  i, w(i)

  end do

  close(unit=11, status="keep")

  ! END TEST
  !============================================================================!

  !  7. Success and continue or stop

  if (info /= 0_fi) then
 
    write(*,*) "Stop. info = ", info
    
    stop
   
  end if

  !  8. Write the eigenvalues

  call vector_write_openmp(m,            &  !  1
                           w,            &  !  2
                           file_system,  &  !  3
                           va_file)         !  4
  
  !  9. Write the eigenvectors

  call matrix_write_openmp(m,            &  !  1
                           n,            &  !  2
                           a,            &  !  3
                           file_system,  &  !  4
                           ve_file,      &  !  5
                           out_format)      !  6

  ! 10. Local deallocations

  deallocate(a)
  deallocate(w)

  end program driver_dsyevd

! FINI
