!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/02/22              !
!===============================!

  module m_4_write_openmp

  implicit none

  contains

!========================================!
! 1. Subroutine: matrix_write_openmp_all !
!========================================!

  subroutine matrix_write_openmp_all(m,           & !  1.
                                     n,           & !  2.
                                     a,           & !  3.
                                     file_system, & !  4.
                                     matrix_file, & !  5.
                                     out_format,  & !  6.
                                     num_files,   & !  7.
                                     stat_open,   & !  8.
                                     stat_close)    !  9.

  use m_1_parameters, only: si, di, dr, len_a, &
                            FILE_SYS_LU_ON,    &
                            FILE_SYS_LU_OFF,   &
                            OUT_FORMAT_COL,    &
                            OUT_FORMAT_ROW,    &
                            NUM_FILES_ONE,     &
                            NUM_FILES_MANY

  implicit none

  !  1. Interface variables

  integer(kind=si), intent(in)                           :: m
  integer(kind=si), intent(in)                           :: n
  real(kind=dr), allocatable, dimension(:,:), intent(in) :: a
  character(len=len_a), intent(in)                       :: file_system
  character(len=len_a), intent(in)                       :: matrix_file
  character(len=len_a), intent(in)                       :: out_format
  character(len=len_a), intent(in)                       :: num_files
  character(len=len_a), intent(in)                       :: stat_open
  character(len=len_a), intent(in)                       :: stat_close

  !  2. Local variables and parameters

  integer(kind=si)                :: i
  integer(kind=si)                :: j
  integer(kind=si)                :: chunk
  integer(kind=si)                :: low_ba
  integer(kind=si)                :: up_ba
  integer(kind=si)                :: i_am
  integer(kind=si)                :: n_threads
  integer(kind=si)                :: nt
  integer(kind=si)                :: rec_len
  integer(kind=si)                :: omp_get_num_threads
  integer(kind=si)                :: omp_get_thread_num
  character(len=len_a)            :: file_id
  real(kind=dr)                   :: dimen_line
  integer(kind=si), parameter     :: basic_unit = 18_si
  character(len=len_a), parameter :: fmt_a      = "(I10)"

  !  3. Write the eigenvectors

  !=============!
  ! case: ALPHA !
  !=============!

  ! num_files = "one"

  if (num_files == NUM_FILES_ONE) then

    !  3-1. Set the common record length

    dimen_line = 0.123456789_dr ! arbitrary value
    inquire(iolength = rec_len) dimen_line

    !  3-2. Open the unit to write

    open(unit=basic_unit,        &
         form="unformatted",     &
         file=trim(matrix_file), &
         status=stat_open,       &
         action="write",         &
         access="direct",        &
         recl=rec_len)

    !  3-3. Write the dimensions

    write(unit=basic_unit, rec=1_si) real(1_si, kind=dr) ! columns
    write(unit=basic_unit, rec=2_si) real(m, kind=dr)    ! columns
    write(unit=basic_unit, rec=3_si) real(1_si, kind=dr) ! rows
    write(unit=basic_unit, rec=4_si) real(n, kind=dr)    ! rows

    !  3-4. Set the file system

    if (file_system == FILE_SYS_LU_OFF) then

      !$omp parallel      &
      !$omp default(none) &
      !$omp shared(nt)

      nt = 1_si
      call omp_set_num_threads(nt)

      !$omp end parallel

    else if (file_system == FILE_SYS_LU_ON) then
 
      !$omp parallel      &
      !$omp default(none) &
      !$omp shared(nt)

      nt = omp_get_num_threads()
      call omp_set_num_threads(nt)

      !$omp end parallel

    end if

    !  3-5. Write now in parallel if "lustre_on" is set
    !       otherwise in serial

    !$omp parallel           &
    !$omp default(none)      &
    !$omp private(i)         &
    !$omp private(j)         &
    !$omp private(chunk)     &
    !$omp private(i_am)      &
    !$omp private(low_ba)    &
    !$omp private(up_ba)     &
    !$omp shared(n)          &
    !$omp shared(m)          &
    !$omp shared(a)          &
    !$omp shared(n_threads)  &
    !$omp shared(out_format) &
    !$omp shared(nt)         &
    !$omp num_threads(nt)

    ! a.

    i_am = omp_get_thread_num()
    n_threads = omp_get_num_threads()
    chunk = int(n, kind=si) / n_threads
    low_ba = i_am * chunk + 1_si
    up_ba = (i_am + 1_si) * chunk

    ! b.

    if (i_am == n_threads-1) then
      up_ba = int(n, kind=si)
    end if

    ! c. out_format: "by_column" or "by_row"
  
    if (out_format == OUT_FORMAT_COL) then

      do i = low_ba, up_ba
        do j = 1, int(m, kind=si)

          write(unit=basic_unit, rec=4_di+j+(i-1)*int(n, kind=di))  a(j,i)

        end do
      end do

    else if (out_format == OUT_FORMAT_ROW) then

      do i = low_ba, up_ba
        do j = 1, int(m, kind=si)

          write(unit=basic_unit, rec=4_di+i+(j-1)*int(n, kind=di))  a(j,i)

        end do
      end do

    end if

    !$omp end parallel

    !  3-6. Close the unit and keep the file

    close(unit=basic_unit, status=stat_close)

  end if

  !====================!
  ! End of case: ALPHA !
  !====================!

  !  4. Write the eigenvectors

  !============!
  ! case: BETA !
  !============!

  ! num_files == "many"

  if (num_files == NUM_FILES_MANY) then

    !  4-1. Set the common record length

    dimen_line = 0.123456789_dr
    inquire(iolength = rec_len) dimen_line

    !  4-2. Set the file system

    if (file_system == FILE_SYS_LU_OFF) then

      !$omp parallel      &
      !$omp default(none) &
      !$omp shared(nt)

      nt = 1_si
      call omp_set_num_threads(nt)

      !$omp end parallel

    else if (file_system == FILE_SYS_LU_ON) then
 
      !$omp parallel      &
      !$omp default(none) &
      !$omp shared(nt)

      nt = omp_get_num_threads()
      call omp_set_num_threads(nt)

      !$omp end parallel

    end if

    !  4-3. Write now in parallel if "lustre_on" is set
    !       otherwise in serial

    !$omp parallel            &
    !$omp default(none)       &
    !$omp private(i)          &
    !$omp private(j)          &
    !$omp private(chunk)      &
    !$omp private(i_am)       &
    !$omp private(low_ba)     &
    !$omp private(up_ba)      &
    !$omp shared(n)           &
    !$omp shared(m)           &
    !$omp shared(a)           &
    !$omp shared(n_threads)   &
    !$omp shared(out_format)  &
    !$omp shared(nt)          &
    !$omp num_threads(nt)     &
    !$omp private(file_id)    & 
    !$omp shared(matrix_file) &
    !$omp shared(rec_len)     &
    !$omp shared(stat_open)   &
    !$omp shared(stat_close)

    ! a.

    i_am = omp_get_thread_num()
    write(file_id, fmt_a) i_am
    file_id = adjustl(file_id)
    file_id = trim(file_id)

    open(unit=basic_unit+i_am,            &
         form="unformatted",              &
         file=trim(matrix_file)//file_id, &
         status=stat_open,                &
         action="write",                  &
         access="direct",                 &
         recl=rec_len)

    ! b. out_format: "by_column" or "by_row"
  
    if (out_format == OUT_FORMAT_COL) then
      
      ! b-1.

      n_threads = omp_get_num_threads()
      chunk = int(n, kind=si) / n_threads
      low_ba = i_am * chunk + 1_si
      up_ba = (i_am + 1_si) * chunk

      ! b-2.

      if (i_am == n_threads-1) then
        up_ba = int(n, kind=si)
      end if

      ! b-3.

      write(unit=basic_unit+i_am, rec=1_si) real(1_si, kind=dr)   ! rows
      write(unit=basic_unit+i_am, rec=2_si) real(m, kind=dr)      ! rows
      write(unit=basic_unit+i_am, rec=3_si) real(low_ba, kind=dr) ! columns: low_ba
      write(unit=basic_unit+i_am, rec=4_si) real(up_ba, kind=dr)  ! columns: up_ba

      ! b-4.

      do i = low_ba, up_ba
        do j = 1, int(m, kind=si)

          write(unit=basic_unit+i_am, rec=4_di+j+(i-1)*int(n, kind=di)- &
                                i_am*chunk*int(n, kind=di)) a(j,i)

        end do
      end do

    else if (out_format == OUT_FORMAT_ROW) then

      ! b-5.

      n_threads = omp_get_num_threads()
      chunk = int(m, kind=si) / n_threads
      low_ba = i_am * chunk + 1_si
      up_ba = (i_am + 1_si) * chunk

      ! b-6.

      if (i_am == n_threads-1) then
        up_ba = int(m, kind=si)
      end if

      ! b-7.

      write(unit=basic_unit+i_am, rec=1_si) real(low_ba, kind=dr) ! rows: low_ba
      write(unit=basic_unit+i_am, rec=2_si) real(up_ba, kind=dr)  ! rows: up_ba
      write(unit=basic_unit+i_am, rec=3_si) real(1_si, kind=dr)   ! columns
      write(unit=basic_unit+i_am, rec=4_si) real(n, kind=dr)      ! columns

      ! b-8.

      do j = low_ba, up_ba
        do i = 1, int(n, kind=si)

          write(unit=basic_unit+i_am, rec=4_di+i+(j-1)*int(n, kind=di)- &
                                i_am*chunk*int(n, kind=di)) a(j,i)

        end do
      end do

    end if

    close(unit=basic_unit+i_am, status=stat_close)
 
    !$omp end parallel

  end if

  !===================!
  ! End of case: BETA !
  !===================!

  end subroutine matrix_write_openmp_all

!============================================!
! 2. Subroutine: matrix_write_openmp_by_elem !
!============================================!

  subroutine matrix_write_openmp_by_elem(m,               & !  1.
                                         n,               & !  2.
                                         matrix_function, & !  3.
                                         file_system,     & !  4.
                                         matrix_file,     & !  5.
                                         out_format,      & !  6.
                                         num_files,       & !  7.
                                         stat_open,       & !  8.
                                         stat_close)        !  9.

  use m_1_parameters, only: si, di, dr, len_a, &
                            FILE_SYS_LU_ON,    &
                            FILE_SYS_LU_OFF,   &
                            OUT_FORMAT_COL,    &
                            OUT_FORMAT_ROW,    &
                            NUM_FILES_ONE,     &
                            NUM_FILES_MANY

  implicit none

  !  1. Interface variables

  integer(kind=si), intent(in)     :: m
  integer(kind=si), intent(in)     :: n
  real(kind=dr), external          :: matrix_function
  character(len=len_a), intent(in) :: file_system
  character(len=len_a), intent(in) :: matrix_file
  character(len=len_a), intent(in) :: out_format
  character(len=len_a), intent(in) :: num_files
  character(len=len_a), intent(in) :: stat_open
  character(len=len_a), intent(in) :: stat_close 
 
  !  2. Local variables and parameters

  integer(kind=si)                :: i
  integer(kind=si)                :: j
  integer(kind=si)                :: chunk
  integer(kind=si)                :: low_ba
  integer(kind=si)                :: up_ba
  integer(kind=si)                :: i_am
  integer(kind=si)                :: n_threads
  integer(kind=si)                :: nt
  integer(kind=si)                :: rec_len
  integer(kind=si)                :: omp_get_num_threads
  integer(kind=si)                :: omp_get_thread_num
  character(len=len_a)            :: file_id
  real(kind=dr)                   :: dimen_line
  integer(kind=si), parameter     :: basic_unit = 18_si
  character(len=len_a), parameter :: fmt_a      = "(I10)"

  !  3. Write the eigenvectors

  !=============!
  ! case: ALPHA !
  !=============!

  ! num_files = "one"

  if (num_files == NUM_FILES_ONE) then

    !  3-1. Set the common record length

    dimen_line = 0.123456789_dr
    inquire(iolength = rec_len) dimen_line

    !  3-2. Open the unit to write

    open(unit=basic_unit,        &
         form="unformatted",     &
         file=trim(matrix_file), &
         status=stat_open,       &
         action="write",         &
         access="direct",        &
         recl=rec_len)

    !  3-3. Write the dimensions

    write(unit=basic_unit, rec=1_si) real(1_si, kind=dr) ! rows
    write(unit=basic_unit, rec=2_si) real(m, kind=dr)    ! columns
    write(unit=basic_unit, rec=3_si) real(1_si, kind=dr) ! rows
    write(unit=basic_unit, rec=4_si) real(n, kind=dr)    ! columns

    !  3-4. Set the file system

    if (file_system == FILE_SYS_LU_OFF) then

      !$omp parallel      &
      !$omp default(none) &
      !$omp shared(nt)

      nt = 1_si
      call omp_set_num_threads(nt)

      !$omp end parallel

    else if (file_system == FILE_SYS_LU_ON) then
 
      !$omp parallel      &
      !$omp default(none) &
      !$omp shared(nt)

      nt = omp_get_num_threads()
      call omp_set_num_threads(nt)

      !$omp end parallel

    end if

    !  3-5. Write now in parallel if "lustre_on" is set
    !       otherwise in serial

    !$omp parallel           &
    !$omp private(i)         &
    !$omp private(j)         &
    !$omp private(chunk)     &
    !$omp private(i_am)      &
    !$omp private(low_ba)    &
    !$omp private(up_ba)     &           
    !$omp shared(n)          &
    !$omp shared(m)          &
    !$omp shared(n_threads)  &
    !$omp shared(out_format) &
    !$omp shared(nt)         &
    !$omp num_threads(nt)

    ! a.

    i_am = omp_get_thread_num()
    n_threads = omp_get_num_threads()
    chunk = int(n, kind=si) / n_threads
    low_ba = i_am * chunk + 1_si
    up_ba = (i_am + 1_si) * chunk

    ! b.

    if (i_am == n_threads-1) then
      up_ba = int(n, kind=si)
    end if

    ! c. out_format: "by_column" or "by_row"
  
    if (out_format == OUT_FORMAT_COL) then
 
      do i = low_ba, up_ba
        do j = 1, int(m, kind=si)

          write(unit=basic_unit, rec=4_di+j+(i-1)*int(n, kind=di)) matrix_function(j,i)

        end do
      end do 

    else if (out_format == OUT_FORMAT_ROW) then

      do i = low_ba, up_ba
        do j = 1, int(m, kind=si)

          write(unit=basic_unit, rec=4_di+i+(j-1)*int(n, kind=di)) matrix_function(j,i) 
 
        end do
      end do

    end if

    !$omp end parallel

    !  3-6. Close the unit and keep the file

    close(unit=basic_unit, status=stat_close)

  end if

  !====================!
  ! End of case: ALPHA !
  !====================!

  !  4. Write the eigenvectors

  !============!
  ! case: BETA !
  !============!

  ! num_files == "many"

  if (num_files == NUM_FILES_MANY) then

    !  4-1. Set the common record length

    dimen_line = 0.123456789_dr
    inquire(iolength = rec_len) dimen_line

    !  4-2. Set the file system

    if (file_system == FILE_SYS_LU_OFF) then

      !$omp parallel      &
      !$omp default(none) &
      !$omp shared(nt)

      nt = 1_si
      call omp_set_num_threads(nt)

      !$omp end parallel

    else if (file_system == FILE_SYS_LU_ON) then
 
      !$omp parallel      &
      !$omp default(none) &
      !$omp shared(nt)

      nt = omp_get_num_threads()  
      call omp_set_num_threads(nt)

      !$omp end parallel

    end if

    !  4-3. Write now in parallel if "lustre_on" is set
    !       otherwise in serial

    !$omp parallel            &
    !$omp private(i)          &
    !$omp private(j)          &
    !$omp private(chunk)      &
    !$omp private(i_am)       &
    !$omp private(low_ba)     &
    !$omp private(up_ba)      &           
    !$omp shared(n)           &
    !$omp shared(m)           &
    !$omp shared(n_threads)   &
    !$omp shared(out_format)  &
    !$omp shared(nt)          &
    !$omp num_threads(nt)     &
    !$omp private(file_id)    & 
    !$omp shared(matrix_file) &
    !$omp shared(rec_len)     &
    !$omp shared(stat_open)   &
    !$omp shared(stat_close)

    ! a.

    i_am = omp_get_thread_num()
    write(file_id, fmt_a) i_am
    file_id = adjustl(file_id)
    file_id = trim(file_id)

    open(unit=basic_unit+i_am,            &
         form="unformatted",              &
         file=trim(matrix_file)//file_id, &
         status=stat_open,                &
         action="write",                  &
         access="direct",                 &
         recl=rec_len)

    ! b. out_format: "by_column" or "by_row"
  
    if (out_format == OUT_FORMAT_COL) then
      
      ! b-1.

      n_threads = omp_get_num_threads()
      chunk = int(n, kind=si) / n_threads
      low_ba = i_am * chunk + 1_si
      up_ba = (i_am + 1_si) * chunk

      ! b-2.

      if (i_am == n_threads-1) then
        up_ba = int(n, kind=si)
      end if
 
      ! b-3.

      write(unit=basic_unit+i_am, rec=1_si) real(1_si, kind=dr)   ! rows
      write(unit=basic_unit+i_am, rec=2_si) real(m, kind=dr)      ! rows
      write(unit=basic_unit+i_am, rec=3_si) real(low_ba, kind=dr) ! columns: low_ba
      write(unit=basic_unit+i_am, rec=4_si) real(up_ba, kind=dr)  ! columns: up_ba

      ! b-4.

      do i = low_ba, up_ba
        do j = 1, int(m, kind=si)

          write(unit=basic_unit+i_am, rec=4_di+j+(i-1)*int(n, kind=di)- &
                                i_am*chunk*int(n, kind=di)) matrix_function(j,i)

        end do
      end do

    else if (out_format == OUT_FORMAT_ROW) then

      ! b-5.

      n_threads = omp_get_num_threads()
      chunk = int(m, kind=si) / n_threads
      low_ba = i_am * chunk + 1_si
      up_ba = (i_am + 1_si) * chunk

      ! b-6.

      if (i_am == n_threads-1) then
        up_ba = int(m, kind=si)
      end if

      ! b-7.

      write(unit=basic_unit+i_am, rec=1_si) real(low_ba, kind=dr) ! rows: low_ba
      write(unit=basic_unit+i_am, rec=2_si) real(up_ba, kind=dr)  ! rows: up_ba
      write(unit=basic_unit+i_am, rec=3_si) real(1_si, kind=dr)   ! columns
      write(unit=basic_unit+i_am, rec=4_si) real(n, kind=dr)      ! columns

      ! b-8.

      do j = low_ba, up_ba
        do i = 1, int(n, kind=si)

          write(unit=basic_unit+i_am, rec=4_di+i+(j-1)*int(n, kind=di)- &
                                i_am*chunk*int(n, kind=di)) matrix_function(j,i)

        end do
      end do

    end if

    close(unit=basic_unit+i_am, status=stat_close)
    
    !$omp end parallel

  end if

  end subroutine matrix_write_openmp_by_elem

!========================================!
! 3. Subroutine: vector_write_openmp_all !
!========================================!

  subroutine vector_write_openmp_all(n,           & !  1.
                                     w,           & !  2.
                                     file_system, & !  3.
                                     vector_file, & !  4.
                                     num_files,   & !  5.
                                     stat_open,   & !  6.
                                     stat_close)    !  7.

  use m_1_parameters, only: si, di, dr, len_a, &
                            FILE_SYS_LU_ON,    &
                            FILE_SYS_LU_OFF,   &
                            NUM_FILES_ONE,     &
                            NUM_FILES_MANY

  implicit none

  !  1. Interface variables

  integer(kind=di), intent(in)                         :: n
  real(kind=dr), allocatable, dimension(:), intent(in) :: w
  character(len=len_a), intent(in)                     :: file_system
  character(len=len_a), intent(in)                     :: vector_file
  character(len=len_a), intent(in)                     :: num_files
  character(len=len_a), intent(in)                     :: stat_open
  character(len=len_a), intent(in)                     :: stat_close

  !  2. Local variables and parameters

  integer(kind=di)                :: i
  integer(kind=di)                :: chunk
  integer(kind=di)                :: low_ba
  integer(kind=di)                :: up_ba
  integer(kind=si)                :: i_am
  integer(kind=si)                :: n_threads
  integer(kind=si)                :: nt
  integer(kind=si)                :: rec_len
  integer(kind=si)                :: omp_get_num_threads
  integer(kind=si)                :: omp_get_thread_num
  character(len=len_a)            :: file_id
  real(kind=dr)                   :: dimen_line
  integer(kind=si), parameter     :: basic_unit = 18_si
  character(len=len_a), parameter :: fmt_a      = "(I10)"

  !  3. Write the vector

  !=============!
  ! case: ALPHA !
  !=============!

  ! num_files = "one"

  if (num_files == NUM_FILES_ONE) then

    !  3-1. Set the common record length

    dimen_line = 0.123456789_dr ! arbitrary value
    inquire(iolength = rec_len) dimen_line

    !  3-2. Open the unit to write

    open(unit=basic_unit,        &
         form="unformatted",     &
         file=trim(vector_file), &
         status=stat_open,       &
         action="write",         &
         access="direct",        &
         recl=rec_len)

    !  3-3. Write the dimensions

    write(unit=basic_unit, rec=1_di) real(1_di, kind=dr) ! rows
    write(unit=basic_unit, rec=2_di) real(n, kind=dr)    ! rows

    !  3-4. Set the file system

    if (file_system == FILE_SYS_LU_OFF) then

      !$omp parallel      &
      !$omp default(none) &
      !$omp shared(nt)

      nt = 1_si
      call omp_set_num_threads(nt)

      !$omp end parallel

    else if (file_system == FILE_SYS_LU_ON) then
 
      !$omp parallel      &
      !$omp default(none) &
      !$omp shared(nt)

      nt = omp_get_num_threads()
      call omp_set_num_threads(nt)

      !$omp end parallel

    end if

    !  3-5. Write now in parallel if "lustre_on" is set
    !       otherwise in serial

    !$omp parallel          &
    !$omp default(none)     &
    !$omp private(i)        &
    !$omp private(chunk)    &
    !$omp private(i_am)     &
    !$omp private(low_ba)   &
    !$omp private(up_ba)    &
    !$omp shared(n)         &
    !$omp shared(w)         &
    !$omp shared(n_threads) &
    !$omp shared(nt)        &
    !$omp num_threads(nt)

    ! a.

    i_am = omp_get_thread_num()
    n_threads = omp_get_num_threads()
    chunk = int(n, kind=di) / n_threads
    low_ba = i_am * chunk + 1_di
    up_ba = (i_am + 1_di) * chunk

    ! b.

    if (i_am == n_threads-1) then
      up_ba = int(n, kind=di)
    end if

    ! c.

    do i = low_ba, up_ba

      write(unit=basic_unit, rec=2_di+i) w(i)

    end do

    !$omp end parallel

    !  3-6. Close the unit and keep the file

    close(unit=basic_unit, status=stat_close)

  end if

  !====================!
  ! End of case: ALPHA !
  !====================!

  !  4. Write the vector

  !============!
  ! case: BETA !
  !============!

  ! num_files == "many"

  if (num_files == NUM_FILES_MANY) then

    !  4-1. Set the common record length

    dimen_line = 0.123456789_dr
    inquire(iolength = rec_len) dimen_line

    !  4-2. Set the file system

    if (file_system == FILE_SYS_LU_OFF) then

      !$omp parallel      &
      !$omp default(none) &
      !$omp shared(nt)

      nt = 1_si
      call omp_set_num_threads(nt)

      !$omp end parallel

    else if (file_system == FILE_SYS_LU_ON) then
 
      !$omp parallel      &
      !$omp default(none) &
      !$omp shared(nt)

      nt = omp_get_num_threads()
      call omp_set_num_threads(nt)

      !$omp end parallel

    end if

    !  4-3. Write now in parallel if "lustre_on" is set
    !       otherwise in serial

    !$omp parallel            &
    !$omp default(none)       &
    !$omp private(i)          &
    !$omp private(chunk)      &
    !$omp private(i_am)       &
    !$omp private(low_ba)     &
    !$omp private(up_ba)      &
    !$omp shared(n)           &
    !$omp shared(w)           &
    !$omp shared(n_threads)   &
    !$omp shared(nt)          &
    !$omp num_threads(nt)     &
    !$omp private(file_id)    & 
    !$omp shared(vector_file) &
    !$omp shared(rec_len)     &
    !$omp shared(stat_open)   &
    !$omp shared(stat_close)

    ! a.

    i_am = omp_get_thread_num()
    write(file_id, fmt_a) i_am
    file_id = adjustl(file_id)
    file_id = trim(file_id)

    open(unit=basic_unit+i_am,            &
         form="unformatted",              &
         file=trim(vector_file)//file_id, &
         status=stat_open,                &
         action="write",                  &
         access="direct",                 &
         recl=rec_len)

    ! b-1.

    n_threads = omp_get_num_threads()
    chunk = int(n, kind=di) / n_threads
    low_ba = i_am * chunk + 1_di
    up_ba = (i_am + 1_di) * chunk

    ! b-2.

    if (i_am == n_threads-1) then
      up_ba = int(n, kind=di)
    end if

    ! b-3.

    write(unit=basic_unit+i_am, rec=1_di) real(low_ba, kind=dr) ! rows: low_ba
    write(unit=basic_unit+i_am, rec=2_di) real(up_ba, kind=dr)  ! rows: up_ba

    ! b-4.

    do i = low_ba, up_ba

      write(unit=basic_unit+i_am, rec=2_di+i-i_am*chunk) w(i)

    end do
 
    ! b-5.

    close(unit=basic_unit+i_am, status=stat_close)
    
    !$omp end parallel

  end if

  !===================!
  ! End of case: BETA !
  !===================!

  end subroutine vector_write_openmp_all

!============================================!
! 4. Subroutine: vector_write_openmp_by_elem !
!============================================!

  subroutine vector_write_openmp_by_elem(n,               & !  1.
                                         vector_function, & !  2.
                                         file_system,     & !  3.
                                         vector_file,     & !  4.
                                         num_files,       & !  5.
                                         stat_open,       & !  6.
                                         stat_close)        !  7.

  use m_1_parameters, only: si, di, dr, len_a, &
                            FILE_SYS_LU_ON,    &
                            FILE_SYS_LU_OFF,   &
                            NUM_FILES_ONE,     &
                            NUM_FILES_MANY

  implicit none

  !  1. Interface variables

  integer(kind=di), intent(in)     :: n
  real(kind=dr), external          :: vector_function
  character(len=len_a), intent(in) :: file_system
  character(len=len_a), intent(in) :: vector_file
  character(len=len_a), intent(in) :: num_files
  character(len=len_a), intent(in) :: stat_open
  character(len=len_a), intent(in) :: stat_close 
 
  !  2. Local variables and parameters

  integer(kind=di)                :: i
  integer(kind=di)                :: chunk
  integer(kind=di)                :: low_ba
  integer(kind=di)                :: up_ba
  integer(kind=si)                :: i_am
  integer(kind=si)                :: n_threads
  integer(kind=si)                :: nt
  integer(kind=si)                :: rec_len
  integer(kind=si)                :: omp_get_num_threads
  integer(kind=si)                :: omp_get_thread_num
  character(len=len_a)            :: file_id
  real(kind=dr)                   :: dimen_line
  integer(kind=si), parameter     :: basic_unit = 18_si
  character(len=len_a), parameter :: fmt_a      = "(I10)"

  !  3. Write the eigenvectors

  !=============!
  ! case: ALPHA !
  !=============!

  ! num_files = "one"

  if (num_files == NUM_FILES_ONE) then

    !  3-1. Set the common record length

    dimen_line = 0.123456789_dr
    inquire(iolength = rec_len) dimen_line

    !  3-2. Open the unit to write

    open(unit=basic_unit,        &
         form="unformatted",     &
         file=trim(vector_file), &
         status=stat_open,       &
         action="write",         &
         access="direct",        &
         recl=rec_len)

    !  3-3. Write the dimensions

    write(unit=basic_unit, rec=1_si) real(1_si, kind=dr) ! rows
    write(unit=basic_unit, rec=2_si) real(n, kind=dr)    ! rows

    !  3-4. Set the file system

    if (file_system == FILE_SYS_LU_OFF) then

      !$omp parallel      &
      !$omp default(none) &
      !$omp shared(nt)

      nt = 1_si
      call omp_set_num_threads(nt)

      !$omp end parallel

    else if (file_system == FILE_SYS_LU_ON) then
 
      !$omp parallel      &
      !$omp default(none) &
      !$omp shared(nt)

      nt = omp_get_num_threads()
      call omp_set_num_threads(nt)

      !$omp end parallel

    end if

    !  3-5. Write now in parallel if "lustre_on" is set
    !       otherwise in serial

    !$omp parallel          &
    !$omp private(i)        &
    !$omp private(chunk)    &
    !$omp private(i_am)     &
    !$omp private(low_ba)   &
    !$omp private(up_ba)    &           
    !$omp shared(n)         &
    !$omp shared(n_threads) &
    !$omp shared(nt)        &
    !$omp num_threads(nt)

    ! a.

    i_am = omp_get_thread_num()
    n_threads = omp_get_num_threads()
    chunk = int(n, kind=di) / n_threads
    low_ba = i_am * chunk + 1_di
    up_ba = (i_am + 1_di) * chunk

    ! b.

    if (i_am == n_threads-1) then
      up_ba = int(n, kind=si)
    end if

    ! c.
 
    do i = low_ba, up_ba

      write(unit=basic_unit, rec=2_di+i) vector_function(i)

    end do 

    !$omp end parallel

    !  3-6. Close the unit and keep the file

    close(unit=basic_unit, status=stat_close)

  end if

  !====================!
  ! End of case: ALPHA !
  !====================!

  !  4. Write the eigenvectors

  !============!
  ! case: BETA !
  !============!

  ! num_files == "many"

  if (num_files == NUM_FILES_MANY) then

    !  4-1. Set the common record length

    dimen_line = 0.123456789_dr
    inquire(iolength = rec_len) dimen_line

    !  4-2. Set the file system

    if (file_system == FILE_SYS_LU_OFF) then

      !$omp parallel      &
      !$omp default(none) &
      !$omp shared(nt)

      nt = 1_si
      call omp_set_num_threads(nt)

      !$omp end parallel

    else if (file_system == FILE_SYS_LU_ON) then
 
      !$omp parallel      &
      !$omp default(none) &
      !$omp shared(nt)

      nt = omp_get_num_threads()  
      call omp_set_num_threads(nt)

      !$omp end parallel

    end if

    !  4-3. Write now in parallel if "lustre_on" is set
    !       otherwise in serial

    !$omp parallel            &
    !$omp private(i)          &
    !$omp private(chunk)      &
    !$omp private(i_am)       &
    !$omp private(low_ba)     &
    !$omp private(up_ba)      &
    !$omp shared(n)           &
    !$omp shared(n_threads)   &
    !$omp shared(nt)          &
    !$omp num_threads(nt)     &
    !$omp private(file_id)    &
    !$omp shared(vector_file) &
    !$omp shared(rec_len)     &
    !$omp shared(stat_open)   &
    !$omp shared(stat_close)

    ! a.

    i_am = omp_get_thread_num()
    write(file_id, fmt_a) i_am
    file_id = adjustl(file_id)
    file_id = trim(file_id)

    open(unit=basic_unit+i_am,            &
         form="unformatted",              &
         file=trim(vector_file)//file_id, &
         status=stat_open,                &
         action="write",                  &
         access="direct",                 &
         recl=rec_len)

    ! b-1.

    n_threads = omp_get_num_threads()
    chunk = int(n, kind=di) / n_threads
    low_ba = i_am * chunk + 1_di
    up_ba = (i_am + 1_di) * chunk

    ! b-2.

    if (i_am == n_threads-1) then
      up_ba = int(n, kind=di)
    end if
 
    ! b-3.

    write(unit=basic_unit+i_am, rec=1_si) real(low_ba, kind=dr) ! rows: low_ba
    write(unit=basic_unit+i_am, rec=2_si) real(up_ba, kind=dr)  ! rows: up_ba

    ! b-4.

    do i = low_ba, up_ba

      write(unit=basic_unit+i_am, rec=2_di+i-i_am*chunk) vector_function(i)

    end do

    ! b-5.

    close(unit=basic_unit+i_am, status=stat_close)
    
    !$omp end parallel

  end if

  end subroutine vector_write_openmp_by_elem

  end module m_4_write_openmp

!======!
! FINI !
!======!
