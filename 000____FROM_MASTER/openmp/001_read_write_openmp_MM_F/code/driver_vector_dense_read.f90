!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/02/22              !
!===============================!

  program driver_vector_dense_read

  use m_1_parameters,  only: si, di, dr, len_a
  use m_7_read_subs,   only: vector_read_sub
  use m_8_error_check, only: error_check_vector_read

  implicit none

  !  1. Interface variables

  integer(kind=di)                         :: n              ! rows
  integer(kind=si)                         :: num_files      ! number of files
  real(kind=dr), allocatable, dimension(:) :: w              ! vector
  character(len=len_a)                     :: file_system    ! "lustre_off", "lustre_on"
  character(len=len_a)                     :: head_file_name !  head of vector file name
  integer(kind=si)                         :: trials         ! times ot read
  character(len=len_a)                     :: output_yes_no  ! "yes", "no"
  character(len=len_a)                     :: test_yes_no    ! "yes", "no"
  character(len=len_a)                     :: stat_close     ! "keep", "delete"

!===================!
! Drivers interface !
!===================!

  !  2. Initialization of the interface variables

  head_file_name = "vector_direct"
  file_system    = "lustre_on"
  num_files      = 4_si
  output_yes_no  = "yes"
  test_yes_no    = "yes"
  trials         = 10_si
  stat_close     = "keep"

!===========================!
! End of driver's interface !
!===========================!

  !  3. Check for input errors

  call error_check_vector_read(file_system,   & !  1.
                               output_yes_no, & !  2.
                               test_yes_no,   & !  3.
                               stat_close)      !  4.

  !  4. Vector OpenMP write
  
  call vector_read_sub(n,              & !  1.
                       num_files,      & !  2.
                       w,              & !  3.
                       file_system,    & !  4.
                       head_file_name, & !  5.
                       trials,         & !  6.
                       output_yes_no,  & !  7.
                       test_yes_no,    & !  8.
                       stat_close)       !  9. 

  end program driver_vector_dense_read

!======!
! FINI !
!======!
