#!/bin/bash

# 1. compile

  gfortran                     \
  -std=f2008                   \
  -Wall                        \
  -O3                          \
  -fopenmp                     \
  -static                      \
  -static-libgfortran          \
  m_1_parameters.f90           \
  m_2_functions.f90            \
  m_3_build_via_function.f90   \
  m_6_read_openmp.f90          \
  m_7_read_subs.f90            \
  m_8_error_check.f90          \
  driver_vector_dense_read.f90 \
  -liomp5                      \
  -o x_gf_r_vec

# 2. clean

  rm *.mod

# 3. exit
