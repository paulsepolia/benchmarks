!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/02/22              !
!===============================!

  program driver_matrix_dense_read_write

  use m_1_parameters,  only: si, dr, len_a
  use m_2_functions,   only: matrix_function
  use m_5_write_subs,  only: matrix_write_sub
  use m_7_read_subs,   only: matrix_read_sub
  use m_8_error_check, only: error_check_matrix_write, &
                             error_check_matrix_read

  implicit none

  !  1. Interface variables

  integer(kind=si)                           :: m                   ! rows
  integer(kind=si)                           :: n                   ! columns
  real(kind=dr), allocatable, dimension(:,:) :: a                   ! matrix
  character(len=len_a)                       :: file_system_write   ! "lustre_off", "lustre_on"
  character(len=len_a)                       :: file_system_read    ! "luster_off", "lustre_on" 
  character(len=len_a)                       :: file_name_write     ! file name for matrix
  character(len=len_a)                       :: file_name_read      ! head of matrix file name 
  character(len=len_a)                       :: out_format_read     ! "by_column", "by_row"
  character(len=len_a)                       :: out_format_write    ! "by_column", "by_row"
  character(len=len_a)                       :: num_files_write     ! "one", "many"
  integer(kind=si)                           :: num_files_read      ! 1,2,3,..
  character(len=len_a)                       :: type_of_build_write ! "via_matrix", "via_function"
  integer(kind=si)                           :: trials_write        ! times to write
  integer(kind=si)                           :: trials_read         ! times to read
  character(len=len_a)                       :: output_yes_no_write ! "yes", "no"
  character(len=len_a)                       :: output_yes_no_read  ! "yes", "no"
  character(len=len_a)                       :: test_yes_no_read    ! "yes", "no"
  character(len=len_a)                       :: stat_open_write     ! "new", "replace"
  character(len=len_a)                       :: stat_close_write    ! "keep", "delete"
  character(len=len_a)                       :: stat_close_read     ! "keep", "delete"

  !  2. Local variables

  integer(kind=si)                :: i
  integer(kind=si), parameter     :: MAX_RW        = 10_si
  character(len=len_a), parameter :: output_driver = "yes"

!===================!
! Drivers interface !
!===================!

  !  3. Initialization of the interface variables

  m                   = 8000_si
  n                   = m
  file_name_write     = "matrix_direct"
  file_name_read      = "matrix_direct"
  file_system_write   = "lustre_on"
  file_system_read    = "lustre_on"
  out_format_write    = "by_column"
  out_format_read     = "by_column"
  num_files_write     = "many"
  num_files_read      = 4
  type_of_build_write = "via_matrix"
  trials_write        = 2
  trials_read         = 2
  output_yes_no_write = "yes"
  output_yes_no_read  = "yes"
  test_yes_no_read    = "yes"
  stat_open_write     = "replace"
  stat_close_write    = "keep"
  stat_close_read     = "keep"

!===========================!
! End of driver's interface !
!===========================!

  !  4. Check for input errors - write case 

  call error_check_matrix_write(file_system_write,   & !  1.
                                type_of_build_write, & !  2.
                                out_format_write,    & !  3.
                                num_files_write,     & !  4.
                                output_yes_no_write, & !  5.
                                stat_open_write,     & !  6.
                                stat_close_write)      !  7.

  !  5. Check for input errors - read case

  call error_check_matrix_read(file_system_read,   & !  1.
                               out_format_read,    & !  2.
                               output_yes_no_read, & !  3.
                               test_yes_no_read,   & !  4.
                               stat_close_read)      !  5.

  do i = 1, MAX_RW

    !  6. Counter
 
    if (output_driver == "yes") then
      write(*,*) "<<===================================================>>"
      write(*,*) "    Benchmark Countdown ----------> ", MAX_RW-i+1_si
      write(*,*) "<<===================================================>>"
    end if

    !  7. Matrix OpenMP write

    call matrix_write_sub(m,                   & !  1.
                          n,                   & !  2.
                          a,                   & !  3.
                          matrix_function,     & !  4.
                          file_system_write,   & !  5.
                          file_name_write,     & !  6.
                          type_of_build_write, & !  7.
                          out_format_write,    & !  8.
                          num_files_write,     & !  9.
                          trials_write,        & ! 10.
                          output_yes_no_write, & ! 11.
                          stat_open_write,     & ! 12.
                          stat_close_write)      ! 13.        

    !  8. Matrix OpenMP read

    call matrix_read_sub(m,                  & !  1.
                         n,                  & !  2.
                         num_files_read,     & !  3.
                         a,                  & !  4.
                         file_system_read,   & !  5.
                         file_name_read,     & !  6.
                         out_format_read,    & !  7.
                         trials_read,        & !  8.
                         output_yes_no_read, & !  9.
                         test_yes_no_read,   & ! 10.
                         stat_close_read)      ! 11.

    !  9. Local deallocation

    deallocate(a)
 
  end do

  end program driver_matrix_dense_read_write

!======!
! FINI !
!======!
