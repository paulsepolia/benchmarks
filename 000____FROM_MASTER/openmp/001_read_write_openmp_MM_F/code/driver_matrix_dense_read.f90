!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/02/22              !
!===============================!

  program driver_matrix_dense_read

  use m_1_parameters,  only: si, dr, len_a
  use m_7_read_subs,   only: matrix_read_sub
  use m_8_error_check, only: error_check_matrix_read

  implicit none

  !  1. Interface variables

  integer(kind=si)                           :: m              ! rows
  integer(kind=si)                           :: n              ! columns
  integer(kind=si)                           :: num_files      ! number of files
  real(kind=dr), allocatable, dimension(:,:) :: a              ! matrix
  character(len=len_a)                       :: file_system    ! "lustre_off", "lustre_on"
  character(len=len_a)                       :: head_file_name !  head of matrix file name
  character(len=len_a)                       :: out_format     ! "by_column", "by_row"
  integer(kind=si)                           :: trials         ! times ot read
  character(len=len_a)                       :: output_yes_no  ! "yes", "no"
  character(len=len_a)                       :: test_yes_no    ! "yes", "no"
  character(len=len_a)                       :: stat_close     ! "keep", "delete"

!===================!
! Drivers interface !
!===================!

  !  2. Initialization of the interface variables

  head_file_name = "matrix_direct"
  file_system    = "lustre_on"
  out_format     = "by_column"
  num_files      = 4_si
  output_yes_no  = "yes"
  test_yes_no    = "yes"
  trials         = 10_si
  stat_close     = "keep"

!===========================!
! End of driver's interface !
!===========================!

  !  3. Check for input errors

  call error_check_matrix_read(file_system,   & !  1.
                               out_format,    & !  2.
                               output_yes_no, & !  3.
                               test_yes_no,   & !  4.
                               stat_close)

  !  4. Matrix OpenMP write
  
  call matrix_read_sub(m,              & !  1.
                       n,              & !  2.
                       num_files,      & !  3.
                       a,              & !  4.
                       file_system,    & !  5.
                       head_file_name, & !  6.
                       out_format,     & !  7.
                       trials,         & !  8.
                       output_yes_no,  & !  9.
                       test_yes_no,    & ! 10.
                       stat_close)       ! 11. 

  end program driver_matrix_dense_read

!======!
! FINI !
!======!
