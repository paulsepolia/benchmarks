//==============//
// parallel all //
//==============//

#include "functors.h"
#include "functions.h"

#include <iostream>
#include <vector>
#include <iterator>
#include <cmath>
#include <ctime>
#include <iomanip>
#include <chrono>
#include <functional>
#include <utility>
#include <string>
#include <cstdlib>

#include <parallel/algorithm>
#include <parallel/numeric>
#include <parallel/settings.h>

using std::cout;
using std::endl;
using std::vector;
using std::pow;
using std::fixed;
using std::setprecision;
using namespace std::chrono;
using std::boolalpha;
using std::pair;
using std::string;
using std::rand;

// type definition

typedef unsigned long int uli;

// the main function

int main()
{
    cout << fixed;
    cout << setprecision(5);
    cout << boolalpha;

    //===================//
    // parallel settings //
    //===================//

    //==========================================================//
    // FORCE ALL POSSIBLE ALGORITHMS TO BE EXECUTED IN PARALLEL //
    //==========================================================//

    __gnu_parallel::_Settings s;
    s.algorithm_strategy = __gnu_parallel::force_parallel;
    __gnu_parallel::_Settings::set(s);

    //==========================//
    // end of parallel settings //
    //==========================//

    const uli DIM1 = 2 * uli(pow(10.0, 7.0));
    const uli TRIALS1 =  5 * uli(pow(10.0, 0.0)); // HEAVY
    const uli TRIALS2 =  4 * uli(pow(10.0, 1.0)); // LESS HEAVY
    const uli TRIALS3 =  1 * uli(pow(10.0, 3.0)); // LIGHT

    const double ELEM1 = 1.0;
    const double INIT = 0.0;


    double time_total = 0.0;
    double time_sort = 0.0;
    double time_shuffle = 0.0;
    double time_default = 0.0;
    double time_function = 0.0;
    double time_lamda = 0.0;
    double time_functor = 0.0;
    double time_function2 = 0.0;
    double time_lamda2 = 0.0;
    double time_functor2 = 0.0;
    double time_search = 0.0;
    double a1;
    bool res_bool;

    vector<double> v1;
    vector<double> v3;
    pair<vector<double>::iterator, vector<double>::iterator> pairA;
    vector<double>::iterator it1;
    vector<double> * pv4;
    const vector<double> v2(2, -1);

    v1.resize(DIM1);
    v3.resize(DIM1);

    auto t1 = system_clock::now();
    auto t2 = system_clock::now();
    duration<double> time_span;

    // A-1
    // vector constructor

    cout << " --> vector constructor --> non-parallel --------------->  A-1" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        vector<double> v1(DIM1, ELEM1);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();

    // A-2
    // iota

    cout << " --> iota --> non-parallel ----------------------------->  A-2" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        iota(v1.begin(), v1.end(), 0.0);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();

    // 1-1
    // accumulate # 1

    cout << " --> accumulate # 1 --> default ------------------------>  1-1" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        accumulate(v1.begin(), v1.end(), INIT);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_default = time_default + time_span.count();

    // 1-2
    // accumulate # 2

    cout << " --> accumulate # 2 --> function ----------------------->  1-2" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        accumulate(v1.begin(), v1.end(), INIT, &ac_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_function = time_function + time_span.count();

    // 1-3
    // accumulate # 3

    cout << " --> accumulate # 3 --> functor ------------------------>  1-3" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        accumulate(v1.begin(), v1.end(), INIT, ac_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_functor = time_functor + time_span.count();

    // 1-4
    // accumulate # 4

    cout << " --> accumulate # 4 --> lamda function ----------------->  1-4" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        accumulate(v1.begin(), v1.end(), INIT, [](const double & x, const double & y) {
            return (x+y);
        });
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_lamda = time_lamda + time_span.count();

    // 2-1
    // adjacent_difference # 1

    cout << " --> adjacent_difference # 1 --> default --------------->  2-1" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        adjacent_difference(v1.begin(), v1.end(), v3.begin());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_default = time_default + time_span.count();

    // 2-2
    // adjacent_difference # 2

    cout << " --> adjacent_difference # 2 --> function -------------->  2-2" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        adjacent_difference(v1.begin(), v1.end(), v3.begin(), &ac_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_function = time_function + time_span.count();

    // 2-3
    // adjacent_difference # 3

    cout << " --> adjacent_difference # 3 --> functor --------------->  2-3" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        adjacent_difference(v1.begin(), v1.end(), v3.begin(), ac_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_functor = time_functor + time_span.count();

    // 2-4
    // adjacent_difference # 4

    cout << " --> adjacent_difference # 4 --> lamda function -------->  2-4" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        adjacent_difference(v1.begin(), v1.end(), v3.begin(),
        [](const double & x, const double & y) {
            return x+y;
        });
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_lamda = time_lamda + time_span.count();

    // 3-1
    // inner_product # 1

    cout << " --> inner_product # 1 --> default --------------------->  3-1" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        a1 = inner_product(v1.begin(), v1.end(), v3.begin(), INIT);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> inner_product result = " << a1 << endl;

    time_total = time_total + time_span.count();
    time_default = time_default + time_span.count();


    // 3-2
    // inner_product # 2

    cout << " --> inner_product # 2 --> function -------------------->  3-2" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        a1 = inner_product(v1.begin(), v1.end(), v3.begin(),
                           INIT,
                           &ac_fun<double>,
                           &product_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> inner_product result = " << a1 << endl;

    time_total = time_total + time_span.count();
    time_function = time_function + time_span.count();

    // 3-3
    // inner_product # 3

    cout << " --> inner_product # 3 --> functor --------------------->  3-3" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        a1 = inner_product(v1.begin(), v1.end(), v3.begin(),
                           INIT,
                           ac_functor<double>(),
                           product_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> inner_product result = " << a1 << endl;

    time_total = time_total + time_span.count();
    time_functor = time_functor + time_span.count();

    // 3-4
    // inner_product # 4

    cout << " --> inner_product # 4 --> lamda function -------------->  3-4" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        a1 = inner_product(v1.begin(), v1.end(), v3.begin(), INIT,
        [](const double & x, const double & y) {
            return x+y;
        },
        [](const double & x, const double & y) {
            return x*y;
        });
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> inner_product result = " << a1 << endl;

    time_total = time_total + time_span.count();
    time_lamda = time_lamda + time_span.count();

    // 4-1
    // partial_sum # 1

    cout << " --> partial_sum # 1 --> default ----------------------->  4-1" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        partial_sum(v1.begin(), v1.end(), v3.begin());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_default = time_default + time_span.count();

    // 4-2
    // partial_sum # 2

    cout << " --> partial_sum # 2 --> function ---------------------->  4-2" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        partial_sum(v1.begin(), v1.end(), v3.begin(), &ac_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_function = time_function + time_span.count();

    // 4-3
    // partial_sum # 3

    cout << " --> partial_sum # 3 --> functor ----------------------->  4-3" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        partial_sum(v1.begin(), v1.end(), v3.begin(), ac_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_functor = time_functor + time_span.count();

    // 4-4
    // partial_sum # 4

    cout << " --> partial_sum # 4 --> lamda function ---------------->  4-4" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        partial_sum(v1.begin(), v1.end(), v3.begin(),
        [](const double & x, const double & y) {
            return (x+y);
        } );
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_lamda = time_lamda + time_span.count();

    // 5-1
    // adjacent_find # 1

    cout << " --> adjacent_find # 1 --> default --------------------->  5-1" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        adjacent_find(v1.begin(), v1.end());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_default = time_default + time_span.count();

    // 5-2
    // adjacent_find # 2

    cout << " --> adjacent_find # 2 --> function -------------------->  5-2" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        adjacent_find(v1.begin(), v1.end(), &equal_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_function = time_function + time_span.count();

    // 5-3
    // adjacent_find # 3

    cout << " --> adjacent_find # 3 --> functor --------------------->  5-3" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        adjacent_find(v1.begin(), v1.end(), equal_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_functor = time_functor + time_span.count();

    // 5-4
    // adjacent_find # 4

    cout << " --> adjacent_find # 4 --> lamda function -------------->  5-4" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        adjacent_find(v1.begin(), v1.end(),
        [](const double & x, const double & y) {
            return (!(x < y) && !(x > y));
        });
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_lamda = time_lamda + time_span.count();

    // 6-0
    // count # 0

    cout << " --> count # 0 ----------------------------------------->  6-0" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        a1 = count(v1.begin(), v1.end(), -1.0);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> a1 = " << a1 << endl;

    time_total = time_total + time_span.count();

    // 7-1
    // count_if # 1

    cout << " --> count_if # 1 --> function ------------------------->  7-1" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        a1 = count_if(v1.begin(), v1.end(), &isOdd_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> a1 = " << a1 << endl;

    time_total = time_total + time_span.count();
    time_function2 = time_function2 + time_span.count();

    // 7-2
    // count_if # 2

    cout << " --> count_if # 2 --> functor -------------------------->  7-2" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        a1 = count_if(v1.begin(), v1.end(), isOdd_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> a1 = " << a1 << endl;

    time_total = time_total + time_span.count();
    time_functor2 = time_functor2 + time_span.count();

    // 7-3
    // count_if # 3

    cout << " --> count_if # 3 --> lamda function ------------------->  7-3" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        a1 = count_if(v1.begin(), v1.end(), [](const double & x) {
            return ((unsigned long int)(x)%2 == 1);
        });
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> a1 = " << a1 << endl;

    time_total = time_total + time_span.count();
    time_lamda2 = time_lamda2 + time_span.count();

    // 8-1
    // equal # 1

    cout << " --> equal # 1 --> default ----------------------------->  8-1" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    iota(v3.begin(), v3.end(), 0.0);

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        res_bool = equal(v1.begin(), v1.end(), v3.begin());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> equal result = " << res_bool << endl;

    time_total = time_total + time_span.count();
    time_default = time_default + time_span.count();

    // 8-2
    // equal # 2

    cout << " --> equal # 2 --> function ---------------------------->  8-2" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    iota(v3.begin(), v3.end(), 0.0);

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        res_bool = equal(v1.begin(), v1.end(), v3.begin(), &equal_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> equal result = " << res_bool << endl;

    time_total = time_total + time_span.count();
    time_function = time_function + time_span.count();

    // 8-3
    // equal # 3

    cout << " --> equal # 3 --> functor ----------------------------->  8-3" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    iota(v3.begin(), v3.end(), 0.0);

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        res_bool = equal(v1.begin(), v1.end(), v3.begin(), equal_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> equal result = " << res_bool << endl;

    time_total = time_total + time_span.count();
    time_functor = time_functor + time_span.count();

    // 8-4
    // equal # 4

    cout << " --> equal # 4 --> lamda function ---------------------->  8-4" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    iota(v3.begin(), v3.end(), 0.0);

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        res_bool = equal(v1.begin(), v1.end(), v3.begin(),
        [](const double & x, const double & y) {
            return (!(x<y) && !(x>y));
        } );
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> equal result = " << res_bool << endl;

    time_total = time_total + time_span.count();
    time_lamda = time_lamda + time_span.count();

    // 9-0
    // find # 0

    cout << " --> find # 0 ------------------------------------------>  9-0" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        find(v1.begin(), v1.end(), -1.0);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();

    // 10-1
    // find_if # 1

    cout << " --> find_if # 1 --> function --------------------------> 10-1" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        find_if(v1.begin(), v1.end(), &lessThanMinusOne_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_function2 = time_function2 + time_span.count();

    // 10-2
    // find_if # 2

    cout << " --> find_if # 2 --> functor ---------------------------> 10-2" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        find_if(v1.begin(), v1.end(), lessThanMinusOne_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_functor2 = time_functor2 + time_span.count();

    // 10-3
    // find_if # 3

    cout << " --> find_if # 3 --> lamda function --------------------> 10-3" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        find_if(v1.begin(), v1.end(), [](const double & i) {
            return (i < -1.0);
        });
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_lamda2 = time_lamda2 + time_span.count();

    //===============//
    // 11-0          //
    // find_first_of //
    //===============//

    // 12-1
    // for_each # 1

    cout << " --> for_each # 1 --> function -------------------------> 12-1" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        for_each(v1.begin(), v1.end(), &double_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_function2 = time_function2 + time_span.count();


    // 12-2
    // for_each # 2

    cout << " --> for_each # 2 --> functor --------------------------> 12-2" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        for_each(v1.begin(), v1.end(), Sum_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_functor2 = time_functor2 + time_span.count();

    // 12-3
    // for_each # 3

    cout << " --> for_each # 3 --> lamda function -------------------> 12-3" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        for_each(v1.begin(), v1.end(), [](const double & i) {
            return 2*i;
        });
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_lamda2 = time_lamda2 + time_span.count();

    // 13-1
    // generate # 1

    cout << " --> generate # 1 --> function -------------------------> 13-1" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        generate(v1.begin(), v1.end(), &UniqueNumber_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_function2 = time_function2 + time_span.count();

    // 13-2
    // generate # 2

    cout << " --> generate # 2 --> functor --------------------------> 13-2" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        generate(v1.begin(), v1.end(), UniqueNumber_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_functor2 = time_functor2 + time_span.count();

    // 13-3
    // generate # 3

    cout << " --> generate # 3 --> lamda function -------------------> 13-3" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        generate(v1.begin(), v1.end(), []() {
            return 123.4;
        });
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_lamda2 = time_lamda2 + time_span.count();

    // 14-1
    // generate_n # 1

    cout << " --> generate_n # 1 --> function -----------------------> 14-1" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        generate_n(v1.begin(), DIM1, &UniqueNumber_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_function2 = time_function2 + time_span.count();

    // 14-2
    // generate_n # 2

    cout << " --> generate_n # 2 --> functor ------------------------> 14-2" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        generate_n(v1.begin(), DIM1, UniqueNumber_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_functor2 = time_functor2 + time_span.count();

    // 14-3
    // generate_n # 3

    cout << " --> generate_n # 3 --> lamda function -----------------> 14-3" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        generate_n(v1.begin(), DIM1, []() {
            return 123.4;
        });
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_lamda2 = time_lamda2 + time_span.count();

    // 15-1
    // lexicographical_compare # 1

    vector<char> * pvA = new vector<char>(DIM1, 'a');
    vector<char> * pvB = new vector<char>(DIM1, 'a');

    cout << " --> lexicographical_compare # 1 --> default -----------> 15-1" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        res_bool = lexicographical_compare(pvA->begin(), pvA->end(),
                                           pvB->begin(), pvB->end());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> res_bool = " << res_bool << endl;

    time_total = time_total + time_span.count();
    time_default = time_default + time_span.count();

    // 15-2
    // lexicographical_compare # 2

    cout << " --> lexicographical_compare # 2 --> function ----------> 15-2" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        res_bool = lexicographical_compare(pvA->begin(), pvA->end(),
                                           pvB->begin(), pvB->end(),
                                           &strComp_fun<char>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> res_bool = " << res_bool << endl;

    time_total = time_total + time_span.count();
    time_function = time_function + time_span.count();

    // 15-3
    // lexicographical_compare # 3

    cout << " --> lexicographical_compare # 3 --> functor -----------> 15-3" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        res_bool = lexicographical_compare(pvA->begin(), pvA->end(),
                                           pvB->begin(), pvB->end(),
                                           strComp_functor<char>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> res_bool = " << res_bool << endl;

    time_total = time_total + time_span.count();
    time_functor = time_functor + time_span.count();

    // 15-4
    // lexicographical_compare # 4

    cout << " --> lexicographical_compare # 4 --> lamda function ----> 15-4" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        res_bool = lexicographical_compare(pvA->begin(), pvA->end(),
                                           pvB->begin(), pvB->end(),
        [](const char & x, const char & y) {
            return (x < y);
        });
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> res_bool = " << res_bool << endl;

    time_total = time_total + time_span.count();
    time_lamda = time_lamda + time_span.count();

    // delete the local char vector pointer

    delete pvA;
    delete pvB;

    // 16-1
    // mismatch # 1

    iota(v1.begin(), v1.end(), 0.0);
    iota(v3.begin(), v3.end(), 0.0);

    cout << " --> mismatch # 1 --> default --------------------------> 16-1" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        pairA = mismatch(v1.begin(), v1.end(), v3.begin());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> *pairA.first = " << *pairA.first << endl;
    cout << " --> *pairA.second = " << *pairA.second << endl;

    time_total = time_total + time_span.count();
    time_default = time_default + time_span.count();

    // 16-2
    // mismatch # 2

    cout << " --> mismatch # 2 --> function -------------------------> 16-2" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        pairA = mismatch(v1.begin(), v1.end(), v3.begin(), &equal_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> *pairA.first = " << *pairA.first << endl;
    cout << " --> *pairA.second = " << *pairA.second << endl;

    time_total = time_total + time_span.count();
    time_function = time_function + time_span.count();

    // 16-3
    // mismatch # 3

    cout << " --> mismatch # 3 --> functor --------------------------> 16-3" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        pairA = mismatch(v1.begin(), v1.end(), v3.begin(), equal_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> *pairA.first = " << *pairA.first << endl;
    cout << " --> *pairA.second = " << *pairA.second << endl;

    time_total = time_total + time_span.count();
    time_functor = time_functor + time_span.count();

    // 16-4
    // mismatch # 4

    cout << " --> mismatch # 4 --> lamda function -------------------> 16-4" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        pairA = mismatch(v1.begin(), v1.end(), v3.begin(),
        [](const double & x, const double & y) {
            return (x == y);
        });
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> *pairA.first = " << *pairA.first << endl;
    cout << " --> *pairA.second = " << *pairA.second << endl;

    time_total = time_total + time_span.count();
    time_lamda = time_lamda + time_span.count();

    // 17-1
    // search # 1

    cout << " --> search # 1 --> default ----------------------------> 17-1" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        it1 = search(v1.begin(), v1.end(), v2.begin(), v2.end());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> *it1 = " << *it1 << endl;

    time_total = time_total + time_span.count();
    time_default = time_default + time_span.count();
    time_search = time_search + time_span.count();

    // 17-2
    // search # 2

    cout << " --> search # 2 --> function ---------------------------> 17-2" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        it1 = search(v1.begin(), v1.end(), v2.begin(), v2.end(), &equal_fun<double>);
        // THIS DOES NOT WORK PROPERLY WITH INTEL COMPILER AND -O3 and -O2, ONLY -O1
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> *it1 = " << *it1 << endl;

    time_total = time_total + time_span.count();
    time_function = time_function + time_span.count();
    time_search = time_search + time_span.count();

    // 17-3
    // search # 3

    cout << " --> search # 3 --> functor ----------------------------> 17-3" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        it1 = search(v1.begin(), v1.end(), v2.begin(), v2.end(), equal_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> *it1 = " << *it1 << endl;

    time_total = time_total + time_span.count();
    time_functor = time_functor + time_span.count();
    time_search = time_search + time_span.count();

    // 17-4
    // search # 4

    cout << " --> search # 4 --> lamda function ---------------------> 17-4" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        it1 = search(v1.begin(), v1.end(), v2.begin(), v2.end(),
        [](const double & x, const double & y) {
            return (x == y);
        });
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> *it1 = " << *it1 << endl;

    time_total = time_total + time_span.count();
    time_lamda = time_lamda + time_span.count();
    time_search = time_search + time_span.count();

    // 18-1
    // search_n # 1

    cout << " --> search_n # 1 --> default --------------------------> 18-1" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        search_n(v1.begin(), v1.end(), 2, -11.22);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_default = time_default + time_span.count();
    time_search = time_search + time_span.count();

    // 18-2
    // search_n # 2

    cout << " --> search_n # 2 --> function -------------------------> 18-2" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        search_n(v1.begin(), v1.end(), 2, -11.22, &equal_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_function = time_function + time_span.count();
    time_search = time_search + time_span.count();

    // 18-3
    // search_n # 3

    cout << " --> search_n # 3 --> functor --------------------------> 18-3" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        search_n(v1.begin(), v1.end(), 2, -11.22, equal_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_functor = time_functor + time_span.count();
    time_search = time_search + time_span.count();

    // 18-4
    // search_n # 4

    cout << " --> search_n # 4 --> lamda function -------------------> 18-4" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        search_n(v1.begin(), v1.end(), 2, -11.22,
        [](const double & x, const double & y) {
            return (x == y);
        });
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_lamda = time_lamda + time_span.count();
    time_search = time_search + time_span.count();

    // 19-1
    // transform # 1

    iota(v1.begin(), v1.end(), 0.0);

    cout << " --> transform # 1 --> function ------------------------> 19-1" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        transform(v1.begin(), v1.end(), v1.begin(), &fun_pp<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_function2 = time_function2 + time_span.count();

    // 19-2
    // transform # 2

    iota(v1.begin(), v1.end(), 0.0);

    cout << " --> transform # 2 --> functor -------------------------> 19-2" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        transform(v1.begin(), v1.end(), v1.begin(), functor_pp<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_functor2 = time_functor2 + time_span.count();

    // 19-3
    // transform # 3

    iota(v1.begin(), v1.end(), 0.0);

    cout << " --> transform # 3--> lamda function -------------------> 19-3" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        transform(v1.begin(), v1.end(), v1.begin(), [](double & i) {
            return i++;
        });
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_lamda2 = time_lamda2 + time_span.count();

    // 20-0
    // replace # 0

    string * ps1 = new string();

    for (uli i = 0; i != DIM1; i++) {
        ps1->push_back('a');
    }

    cout << " --> replace # 0 ---------------------------------------> 20-0" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS3; i++) {
        ps1->replace(ps1->begin(), ps1->end(), DIM1, 'b');
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    cout << " --> (*ps1)[0] = " <<  (*ps1)[0] << endl;
    cout << " --> (*ps1)[1] = " <<  (*ps1)[1] << endl;
    cout << " --> (*ps1)[DIM1-1] = " <<  (*ps1)[DIM1-1] << endl;

    delete ps1;

    time_total = time_total + time_span.count();

    // 21-1
    // replace_if # 1

    iota(v1.begin(), v1.end(), 0.0);

    cout << " --> replace_if # 1 --> function -----------------------> 21-1" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        replace_if(v1.begin(), v1.end(), &isOdd_fun<double>, -1.0);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_function2 = time_function2 + time_span.count();

    // 21-2
    // replace_if # 2

    iota(v1.begin(), v1.end(), 0.0);

    cout << " --> replace_if # 2 --> functor ------------------------> 21-2" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        replace_if(v1.begin(), v1.end(), isOdd_functor<double>(), -1.0);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_functor2 = time_functor2 + time_span.count();

    // 21-3
    // replace_if # 3

    iota(v1.begin(), v1.end(), 0.0);

    cout << " --> replace_if # 3 --> lamda function -----------------> 21-3" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        replace_if(v1.begin(), v1.end(), [](double & x) {
            return ((static_cast<uli>(x)%2) == 1);
        }, -1.0);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_lamda2 = time_lamda2 + time_span.count();

    // 22-1
    // max_element # 1

    iota(v1.begin(), v1.end(), 0.0);

    cout << " --> max_element # 1 --> default -----------------------> 22-1" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        a1 = *max_element(v1.begin(), v1.end());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> max_elem = " << a1 << endl;

    time_total = time_total + time_span.count();
    time_default = time_default + time_span.count();

    // 22-2
    // max_element # 2

    iota(v1.begin(), v1.end(), 0.0);

    cout << " --> max_element # 2 --> function ----------------------> 22-2" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        a1 = *max_element(v1.begin(), v1.end(), &lessThan_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> max_elem = " << a1 << endl;

    time_total = time_total + time_span.count();
    time_function = time_function + time_span.count();

    // 22-3
    // max_element # 3

    iota(v1.begin(), v1.end(), 0.0);

    cout << " --> max_element # 3 --> functor -----------------------> 22-3" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        a1 = *max_element(v1.begin(), v1.end(), lessThan_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> max_elem = " << a1 << endl;

    time_total = time_total + time_span.count();
    time_functor = time_functor + time_span.count();

    // 22-4
    // max_element # 4

    iota(v1.begin(), v1.end(), 0.0);

    cout << " --> max_element # 4 --> lamda function ----------------> 22-4" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        a1 = *max_element(v1.begin(), v1.end(), [] (const double & x, const double & y) {
            return (x < y);
        });
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> max_elem = " << a1 << endl;

    time_total = time_total + time_span.count();
    time_lamda = time_lamda + time_span.count();

    //=======//
    // 23-0  //
    // merge //
    // ======//

    // 24-1
    // min_element # 1

    iota(v1.begin(), v1.end(), 0.0);

    cout << " --> min_element # 1 --> default -----------------------> 24-1" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        a1 = *min_element(v1.begin(), v1.end());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> min_elem = " << a1 << endl;

    time_total = time_total + time_span.count();
    time_default = time_default + time_span.count();

    // 24-2
    // min_element # 2

    iota(v1.begin(), v1.end(), 0.0);

    cout << " --> min_element # 2 --> function ----------------------> 24-2" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        a1 = *min_element(v1.begin(), v1.end(), &lessThan_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> min_elem = " << a1 << endl;

    time_total = time_total + time_span.count();
    time_function = time_function + time_span.count();

    // 24-3
    // min_element # 3

    iota(v1.begin(), v1.end(), 0.0);

    cout << " --> min_element # 3 --> functor -----------------------> 24-3" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        a1 = *min_element(v1.begin(), v1.end(), lessThan_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> min_elem = " << a1 << endl;

    time_total = time_total + time_span.count();
    time_functor = time_functor + time_span.count();

    // 24-4
    // min_element # 4

    iota(v1.begin(), v1.end(), 0.0);

    cout << " --> min_element # 4 --> lamda function ----------------> 24-4" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        a1 = *min_element(v1.begin(), v1.end(), [] (const double & x, const double & y) {
            return (x < y);
        });
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> min_elem = " << a1 << endl;

    time_total = time_total + time_span.count();
    time_lamda = time_lamda + time_span.count();

    // 25-1
    // nth_element # 1

    cout << " --> nth_element # 1 --> default -----------------------> 25-1" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    random_shuffle(v1.begin(), v1.end());

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        nth_element(v1.begin(), v1.begin()+i, v1.end());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_default = time_default + time_span.count();

    // 25-2
    // nth_element # 2

    cout << " --> nth_element # 2 --> function ----------------------> 25-2" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    random_shuffle(v1.begin(), v1.end());

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        nth_element(v1.begin(), v1.begin()+i, v1.end(), &lessThan_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_function = time_function + time_span.count();

    // 25-3
    // nth_element # 3

    cout << " --> nth_element # 3 --> functor -----------------------> 25-3" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    random_shuffle(v1.begin(), v1.end());

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        nth_element(v1.begin(), v1.begin()+i, v1.end(), lessThan_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_functor = time_functor + time_span.count();

    // 25-4
    // nth_element # 4

    cout << " --> nth_element # 4 --> lamda function ----------------> 25-4" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    random_shuffle(v1.begin(), v1.end());

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        nth_element(v1.begin(), v1.begin()+i, v1.end(),
        [] (const double & x, const double & y) {
            return (x < y);
        });
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_lamda = time_lamda + time_span.count();

    // 26-1
    // partial_sort # 1

    cout << " --> partial_sort # 1 --> default ----------------------> 26-1" << endl;

    random_shuffle(v1.begin(), v1.end());

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        partial_sort(v1.begin(), v1.end(), v1.end());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_sort = time_sort + time_span.count();
    time_default = time_default + time_span.count();

    // 26-2
    // partial_sort # 2

    cout << " --> partial_sort # 2 --> function ---------------------> 26-2" << endl;

    random_shuffle(v1.begin(), v1.end());

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        partial_sort(v1.begin(), v1.end(), v1.end(), &lessThan_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_sort = time_sort + time_span.count();
    time_function = time_function + time_span.count();

    // 26-3
    // partial_sort # 3

    cout << " --> partial_sort # 3 --> functor ----------------------> 26-3" << endl;

    random_shuffle(v1.begin(), v1.end());

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        partial_sort(v1.begin(), v1.end(), v1.end(), lessThan_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_sort = time_sort + time_span.count();
    time_functor = time_functor + time_span.count();

    // 26-4
    // partial_sort # 4

    cout << " --> partial_sort # 4 --> lamda function ---------------> 26-4" << endl;

    random_shuffle(v1.begin(), v1.end());

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        partial_sort(v1.begin(), v1.end(), v1.end(), [](const double & x, const double & y) {
            return (x < y);
        });
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_sort = time_sort + time_span.count();
    time_lamda = time_lamda + time_span.count();

    // 27-1
    // partition # 1

    cout << " --> partition # 1 --> function ------------------------> 27-1" << endl;

    random_shuffle(v1.begin(), v1.end());
    sort(v1.begin(), v1.end());

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        it1 = partition(v1.begin(), v1.end(), &isOdd_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> bound = " << *it1 << endl;

    time_total = time_total + time_span.count();
    time_function2 = time_function2 + time_span.count();

    // 27-2
    // partition # 2

    cout << " --> partition # 2 --> functor -------------------------> 27-2" << endl;

    random_shuffle(v1.begin(), v1.end());
    sort(v1.begin(), v1.end());

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        it1 = partition(v1.begin(), v1.end(), isOdd_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> bound = " << *it1 << endl;

    time_total = time_total + time_span.count();
    time_functor2 = time_functor2 + time_span.count();

    // 27-3
    // partition # 3

    cout << " --> partition # 3 --> lamda function ------------------> 27-3" << endl;

    random_shuffle(v1.begin(), v1.end());
    sort(v1.begin(), v1.end());

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        it1 = partition(v1.begin(), v1.end(), [](double & x) {
            return ((static_cast<uli>(x)%2) == 1);
        });
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> bound = " << *it1 << endl;

    time_total = time_total + time_span.count();
    time_lamda2 = time_lamda2 + time_span.count();

    // 28-1
    // random_shuffle # 1

    cout << " --> random_shuffle # 1 --> default --------------------> 28-1" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        random_shuffle(v1.begin(), v1.end());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_shuffle = time_shuffle + time_span.count();
    time_default = time_default + time_span.count();

    // 28-2
    // random_shuffle # 2

    cout << " --> random_shuffle # 2 --> function -------------------> 28-2" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        random_shuffle(v1.begin(), v1.end(), &rand_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_shuffle = time_shuffle + time_span.count();
    time_function = time_function + time_span.count();

    // 28-3
    // random_shuffle # 3

    cout << " --> random_shuffle # 3 --> functor --------------------> 28-3" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        random_shuffle(v1.begin(), v1.end(), rand_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_shuffle = time_shuffle + time_span.count();
    time_functor = time_functor + time_span.count();

    // 28-4
    // random_shuffle # 4

    cout << " --> random_shuffle # 4 --> lamda function -------------> 28-4" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        random_shuffle(v1.begin(), v1.end(), [](const double & i) {
            return (rand()%uli(i));
        });
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_shuffle = time_shuffle + time_span.count();
    time_lamda = time_lamda + time_span.count();

    // 29-1
    // set_union # 1

    cout << " --> set_union # 1 --> default -------------------------> 29-1" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    iota(v3.begin(), v3.end(), 0.0);
    pv4 = new vector<double>;
    pv4->resize(DIM1);

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        set_union(v1.begin(), v1.end(),
                  v3.begin(), v3.end(), pv4->begin());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    delete pv4;
    pv4 = 0;

    time_total = time_total + time_span.count();
    time_default = time_default + time_span.count();

    // 29-2
    // set_union # 2

    cout << " --> set_union # 2 --> function ------------------------> 29-2" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    iota(v3.begin(), v3.end(), 0.0);
    pv4 = new vector<double>;
    pv4->resize(DIM1);

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        set_union(v1.begin(), v1.end(),
                  v3.begin(), v3.end(), pv4->begin(), &lessThan_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    delete pv4;

    time_total = time_total + time_span.count();
    time_function = time_function + time_span.count();

    // 29-3
    // set_union # 3

    cout << " --> set_union # 3 --> functor -------------------------> 29-3" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    iota(v3.begin(), v3.end(), 0.0);
    pv4 = new vector<double>;
    pv4->resize(DIM1);

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        set_union(v1.begin(), v1.end(),
                  v3.begin(), v3.end(), pv4->begin(), lessThan_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    delete pv4;

    time_total = time_total + time_span.count();
    time_functor = time_functor + time_span.count();

    // 29-4
    // set_union # 4

    cout << " --> set_union # 4 --> lamda function ------------------> 29-4" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    iota(v3.begin(), v3.end(), 0.0);
    pv4 = new vector<double>;
    pv4->resize(DIM1);

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        set_union(v1.begin(), v1.end(),
                  v3.begin(), v3.end(), pv4->begin(),
        [](const double & x, const double & y) {
            return (x < y);
        });
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    delete pv4;
    pv4 = 0;

    time_total = time_total + time_span.count();
    time_lamda = time_lamda + time_span.count();

    // 30-1
    // set_intersection # 1

    cout << " --> set_intersection # 1 --> default ------------------> 30-1" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    iota(v3.begin(), v3.end(), 0.0);

    sort(v1.begin(), v1.end());
    sort(v3.begin(), v3.end());

    pv4 = new vector<double>;
    pv4->resize(2*DIM1);

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        it1 = set_intersection(v1.begin(), v1.end(),
                               v3.begin(), v3.end(), pv4->begin());
    }

    t2 = system_clock::now();

    pv4->resize(it1 - pv4->begin());

    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> *it1 = " << *it1 << endl;
    cout << " --> pv4->size() = " << pv4->size() << endl;

    delete pv4;
    pv4 = 0;

    time_total = time_total + time_span.count();
    time_default = time_default + time_span.count();

    // 30-2
    // set_intersection # 2

    cout << " --> set_intersection # 2 --> function -----------------> 30-2" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    iota(v3.begin(), v3.end(), 0.0);

    sort(v1.begin(), v1.end());
    sort(v3.begin(), v3.end());

    pv4 = new vector<double>;
    pv4->resize(2*DIM1);

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        it1 = set_intersection(v1.begin(), v1.end(),
                               v3.begin(), v3.end(), pv4->begin(), &lessThan_fun<double>);
    }

    t2 = system_clock::now();

    pv4->resize(it1 - pv4->begin());

    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> *it1 = " << *it1 << endl;
    cout << " --> pv4->size() = " << pv4->size() << endl;

    delete pv4;
    pv4 = 0;

    time_total = time_total + time_span.count();
    time_function = time_function + time_span.count();

    // 30-3
    // set_intersection # 3

    cout << " --> set_intersection # 3 --> functor ------------------> 30-3" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    iota(v3.begin(), v3.end(), 0.0);

    sort(v1.begin(), v1.end());
    sort(v3.begin(), v3.end());

    pv4 = new vector<double>;
    pv4->resize(2*DIM1);

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        it1 = set_intersection(v1.begin(), v1.end(),
                               v3.begin(), v3.end(), pv4->begin(), lessThan_functor<double>());
    }

    t2 = system_clock::now();

    pv4->resize(it1 - pv4->begin());

    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> *it1 = " << *it1 << endl;
    cout << " --> pv4->size() = " << pv4->size() << endl;

    delete pv4;
    pv4 = 0;

    time_total = time_total + time_span.count();
    time_functor = time_functor + time_span.count();

    // 30-4
    // set_intersection # 4

    cout << " --> set_intersection # 4 --> lamda function -----------> 30-4" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    iota(v3.begin(), v3.end(), 0.0);

    sort(v1.begin(), v1.end());
    sort(v3.begin(), v3.end());

    pv4 = new vector<double>;
    pv4->resize(2*DIM1);

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        it1 = set_intersection(v1.begin(), v1.end(),
                               v3.begin(), v3.end(), pv4->begin(),
        [](const double & x, const double & y) {
            return (x < y);
        } );
    }

    t2 = system_clock::now();

    pv4->resize(it1 - pv4->begin());

    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> *it1 = " << *it1 << endl;
    cout << " --> pv4->size() = " << pv4->size() << endl;

    delete pv4;
    pv4 = 0;

    time_total = time_total + time_span.count();
    time_lamda = time_lamda + time_span.count();

    // 31-1
    // set_symmetric_difference # 1

    cout << " --> set_symmetric_difference # 1 --> default ----------> 31-1" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    iota(v3.begin(), v3.end(), 0.0);

    sort(v1.begin(), v1.end());
    sort(v3.begin(), v3.end());

    pv4 = new vector<double>;
    pv4->resize(2*DIM1);

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        it1 = set_symmetric_difference(v1.begin(), v1.end(),
                                       v3.begin(), v3.end(), pv4->begin());
    }

    t2 = system_clock::now();

    pv4->resize(it1-pv4->begin());

    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> *it1 = " << *it1 << endl;
    cout << " --> pv4->size() = " << pv4->size() << endl;

    delete pv4;
    pv4 = 0;

    time_total = time_total + time_span.count();
    time_default = time_default + time_span.count();

    // 31-2
    // set_symmetric_difference # 2

    cout << " --> set_symmetric_difference # 2 --> function ---------> 31-2" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    iota(v3.begin(), v3.end(), 0.0);

    sort(v1.begin(), v1.end());
    sort(v3.begin(), v3.end());

    pv4 = new vector<double>;
    pv4->resize(2*DIM1);

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        it1 = set_symmetric_difference(v1.begin(), v1.end(),
                                       v3.begin(), v3.end(), pv4->begin(),
                                       &lessThan_fun<double>);
    }

    t2 = system_clock::now();

    pv4->resize(it1-pv4->begin());

    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> *it1 = " << *it1 << endl;
    cout << " --> pv4->size() = " << pv4->size() << endl;

    delete pv4;
    pv4 = 0;

    time_total = time_total + time_span.count();
    time_function = time_function + time_span.count();

    // 31-3
    // set_symmetric_difference # 3

    cout << " --> set_symmetric_difference # 3 --> functor ----------> 31-3" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    iota(v3.begin(), v3.end(), 0.0);

    sort(v1.begin(), v1.end());
    sort(v3.begin(), v3.end());

    pv4 = new vector<double>;
    pv4->resize(2*DIM1);

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        it1 = set_symmetric_difference(v1.begin(), v1.end(),
                                       v3.begin(), v3.end(), pv4->begin(),
                                       lessThan_functor<double>());
    }

    t2 = system_clock::now();

    pv4->resize(it1-pv4->begin());

    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> *it1 = " << *it1 << endl;
    cout << " --> pv4->size() = " << pv4->size() << endl;

    delete pv4;
    pv4 = 0;

    time_total = time_total + time_span.count();
    time_functor = time_functor + time_span.count();

    // 31-4
    // set_symmetric_difference # 4

    cout << " --> set_symmetric_difference # 4 --> lamda function ---> 31-4" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    iota(v3.begin(), v3.end(), 0.0);

    sort(v1.begin(), v1.end());
    sort(v3.begin(), v3.end());

    pv4 = new vector<double>;
    pv4->resize(2*DIM1);

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        it1 = set_symmetric_difference(v1.begin(), v1.end(),
                                       v3.begin(), v3.end(), pv4->begin(),
        [](const double & x, const double & y) {
            return (x < y);
        });
    }

    t2 = system_clock::now();

    pv4->resize(it1-pv4->begin());

    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> *it1 = " << *it1 << endl;
    cout << " --> pv4->size() = " << pv4->size() << endl;

    delete pv4;
    pv4 = 0;

    time_total = time_total + time_span.count();
    time_lamda = time_lamda + time_span.count();

    // 32-1
    // set_difference # 1

    cout << " --> set_difference # 1 --> default --------------------> 32-1" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    iota(v3.begin(), v3.end(), 0.0);

    sort(v1.begin(), v1.end());
    sort(v3.begin(), v3.end());

    pv4 = new vector<double>;
    pv4->resize(2*DIM1);

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        it1 = set_difference(v1.begin(), v1.end(),
                             v3.begin(), v3.end(), pv4->begin());
    }

    t2 = system_clock::now();

    pv4->resize(it1-pv4->begin());

    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> *it1 = " << *it1 << endl;
    cout << " --> pv4->size() = " << pv4->size() << endl;

    delete pv4;
    pv4 = 0;

    time_total = time_total + time_span.count();
    time_default = time_default + time_span.count();

    // 32-2
    // set_difference # 2

    cout << " --> set_difference # 2 --> function -------------------> 32-2" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    iota(v3.begin(), v3.end(), 0.0);

    sort(v1.begin(), v1.end());
    sort(v3.begin(), v3.end());

    pv4 = new vector<double>;
    pv4->resize(2*DIM1);

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        it1 = set_difference(v1.begin(), v1.end(),
                             v3.begin(), v3.end(), pv4->begin(),
                             &lessThan_fun<double>);
    }

    t2 = system_clock::now();

    pv4->resize(it1-pv4->begin());

    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> *it1 = " << *it1 << endl;
    cout << " --> pv4->size() = " << pv4->size() << endl;

    delete pv4;
    pv4 = 0;

    time_total = time_total + time_span.count();
    time_function = time_function + time_span.count();

    // 32-3
    // set_difference # 3

    cout << " --> set_difference # 3 --> functor --------------------> 32-3" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    iota(v3.begin(), v3.end(), 0.0);

    sort(v1.begin(), v1.end());
    sort(v3.begin(), v3.end());

    pv4 = new vector<double>;
    pv4->resize(2*DIM1);

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        it1 = set_difference(v1.begin(), v1.end(),
                             v3.begin(), v3.end(), pv4->begin(),
                             lessThan_functor<double>());
    }

    t2 = system_clock::now();

    pv4->resize(it1-pv4->begin());

    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> *it1 = " << *it1 << endl;
    cout << " --> pv4->size() = " << pv4->size() << endl;

    delete pv4;
    pv4 = 0;

    time_total = time_total + time_span.count();
    time_functor = time_functor + time_span.count();

    // 32-4
    // set_difference # 4

    cout << " --> set_difference # 4 --> lamda function -------------> 32-4" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    iota(v3.begin(), v3.end(), 0.0);

    sort(v1.begin(), v1.end());
    sort(v3.begin(), v3.end());

    pv4 = new vector<double>;
    pv4->resize(2*DIM1);

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        it1 = set_difference(v1.begin(), v1.end(),
                             v3.begin(), v3.end(), pv4->begin(),
        [](const double & x, const double & y) {
            return (x < y);
        });
    }

    t2 = system_clock::now();

    pv4->resize(it1-pv4->begin());

    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> *it1 = " << *it1 << endl;
    cout << " --> pv4->size() = " << pv4->size() << endl;

    delete pv4;
    pv4 = 0;

    time_total = time_total + time_span.count();
    time_lamda = time_lamda + time_span.count();

    // 33-1
    // sort # 1

    cout << " --> sort # 1 --> default ------------------------------> 33-1" << endl;

    random_shuffle(v1.begin(), v1.end());

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        sort(v1.begin(), v1.end());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_sort = time_sort + time_span.count();
    time_default = time_default + time_span.count();

    // 33-2
    // sort # 2

    cout << " --> sort # 2 --> function -----------------------------> 33-2" << endl;

    random_shuffle(v1.begin(), v1.end());

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        sort(v1.begin(), v1.end(), &lessThan_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_sort = time_sort + time_span.count();
    time_function = time_function + time_span.count();

    // 33-3
    // sort # 3

    cout << " --> sort # 3 --> functor ------------------------------> 33-3" << endl;

    random_shuffle(v1.begin(), v1.end());

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        sort(v1.begin(), v1.end(), lessThan_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_sort = time_sort + time_span.count();
    time_functor = time_functor + time_span.count();

    // 33-4
    // sort # 4

    cout << " --> sort # 4 --> lamda function -----------------------> 33-4" << endl;

    random_shuffle(v1.begin(), v1.end());

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        sort(v1.begin(), v1.end(), [](const double & x, const double & y) {
            return (x < y);
        });
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_sort = time_sort + time_span.count();
    time_lamda = time_lamda + time_span.count();

    // 34-1
    // stable_sort # 1

    cout << " --> stable_sort # 1 --> default -----------------------> 34-1" << endl;

    random_shuffle(v1.begin(), v1.end());

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        stable_sort(v1.begin(), v1.end());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_sort = time_sort + time_span.count();
    time_default = time_default + time_span.count();

    // 34-2
    // stable_sort # 2

    cout << " --> stable_sort # 2 --> function ----------------------> 34-2" << endl;

    random_shuffle(v1.begin(), v1.end());

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        stable_sort(v1.begin(), v1.end(), &lessThan_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_sort = time_sort + time_span.count();
    time_function = time_function + time_span.count();

    // 34-3
    // stable_sort # 3

    cout << " --> stable_sort # 3 --> functor -----------------------> 34-3" << endl;

    random_shuffle(v1.begin(), v1.end());

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        stable_sort(v1.begin(), v1.end(), lessThan_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_sort = time_sort + time_span.count();
    time_functor = time_functor + time_span.count();

    // 34-4
    // stable_sort # 4

    cout << " --> stable_sort # 4 --> lamda function ----------------> 34-4" << endl;

    random_shuffle(v1.begin(), v1.end());

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        stable_sort(v1.begin(), v1.end(), [](const double & x, const double & y) {
            return (x < y);
        });
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_sort = time_sort + time_span.count();
    time_lamda = time_lamda + time_span.count();

    // 35-1
    // unique_copy # 1

    cout << " --> unique_copy --> default ---------------------------> 35-1" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    random_shuffle(v1.begin(), v1.end());

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        unique_copy(v1.begin(), v1.end(), v3.begin());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_default = time_default + time_span.count();

    // 35-2
    // unique_copy # 2

    cout << " --> unique_copy --> function --------------------------> 35-2" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    random_shuffle(v1.begin(), v1.end());

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        unique_copy(v1.begin(), v1.end(), v3.begin(), &equal_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_function = time_function + time_span.count();

    // 35-3
    // unique_copy # 3

    cout << " --> unique_copy --> functor ---------------------------> 35-3" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    random_shuffle(v1.begin(), v1.end());

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        unique_copy(v1.begin(), v1.end(), v3.begin(), equal_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_functor = time_functor + time_span.count();

    // 35-4
    // unique_copy # 4

    cout << " --> unique_copy --> lamda function --------------------> 35-4" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    random_shuffle(v1.begin(), v1.end());

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        unique_copy(v1.begin(), v1.end(), v3.begin(), [](const double & x, const double & y) {
            return (x == y) ;
        });
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    time_total = time_total + time_span.count();
    time_lamda = time_lamda + time_span.count();

    cout << "===========================================================" << endl;
    cout << " --> Total time           = " << time_total     << endl;
    cout << " --> Total time sort      = " << time_sort      << endl;
    cout << " --> Total time shuffle   = " << time_shuffle   << endl;
    cout << " --> Total time search    = " << time_search    << endl;
    cout << " --> Total time default   = " << time_default   << endl;
    cout << " --> Total time lamda     = " << time_lamda     << endl;
    cout << " --> Total time function  = " << time_function  << endl;
    cout << " --> Total time functor   = " << time_functor   << endl;
    cout << " --> Total time lamda2    = " << time_lamda2    << endl;
    cout << " --> Total time function2 = " << time_function2 << endl;
    cout << " --> Total time functor2  = " << time_functor2  << endl;
    cout << "===========================================================" << endl;

    return 0;
}

// end
