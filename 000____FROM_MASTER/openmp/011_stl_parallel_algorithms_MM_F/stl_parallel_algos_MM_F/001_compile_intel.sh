#!/bin/bash

  # 1. compile

  icpc  -O1                 \
        -Wall               \
	    -wd1202             \
        -std=c++17          \
	    -qopenmp            \
        -qoverride-limits   \
	    -D_GLIBCXX_PARALLEL \
	    functors.cpp        \
	    functions.cpp       \
        driver_program.cpp  \
        -o x_intel
