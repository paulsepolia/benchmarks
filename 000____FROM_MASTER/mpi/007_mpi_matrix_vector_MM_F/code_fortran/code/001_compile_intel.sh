#!/bin/bash

# 1. compile 

  mpiifort                     \
  -e03                         \
  -warn all                    \
  -O3                          \
  -static_mpi                  \
  -static-intel                \
  m_1_parameters.f90           \
  driver_matrix_vector_mpi.f90 \
  -o x_intel

# 2. clean

  rm *.mod

# 3. exit
