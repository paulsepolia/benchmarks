!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/03/16              !
!===============================!

  program driver_matrix_vector_mpi

!==========================================================================!
! NOTES                                                                    !
!                                                                          !
! (1) An MPI matrix-vector product.                                        !
! (2) It holds ONLY for symmetric matrices.                                !
! (3) The allocation and building of the input vector and the input matrix !
!     is done only in master MPI thread (rank=0).                          !
! (4) I use the non blocking MPI_Isend and MPI_Irecv commands              !
!     for broadcasting parts of the matrix and the whole vector            !
!     to the rest of MPI threads, and the MPI_Wait for synchronization.    !
! (5) LIMITATION: The number of matrix rows                                !
!     must be divisible by p (number of MPI threads).                      !
! (6) All of the started up MPI threads do the matrix-vector product,      !
!     each one its own part.                                               !
!==========================================================================!

  use mpi
  use m_1_parameters

  implicit none 

  !=========================!
  !  1a. Start of interface !                                                           
  !=========================!

  integer(kind=si), parameter :: DIMEN = 10000_di   ! input
  integer(kind=si), parameter :: KMAX  = 1000000_si    ! input

  !=======================!
  !  1b. End of interface !
  !=======================!

  !  2. Local variables

  integer(kind=si) :: ierr 
  integer(kind=si) :: my_rank
  integer(kind=si) :: p
  integer(kind=si) :: comm
  integer(kind=di) :: dimen_local
  integer(kind=di) :: i
  integer(kind=di) :: j
  integer(kind=di) :: k
  real(kind=dr), allocatable, dimension(:)   :: vector
  real(kind=dr), allocatable, dimension(:)   :: vector_help
  real(kind=dr), allocatable, dimension(:)   :: vector_final
  real(kind=dr), allocatable, dimension(:,:) :: matrix
  real(kind=dr) :: sum_local
  real(kind=dr) :: t1
  real(kind=dr) :: t2
  real(kind=dr) :: t1_mpi
  real(kind=dr) :: t2_mpi

  !  3. MPI start up

  call MPI_Init(ierr)

  comm = MPI_COMM_WORLD

  !  4. Getting my process rank

  call MPI_Comm_rank(comm, my_rank, ierr)

  !  5. Find how many MPI processes are being used

  call MPI_Comm_size(comm, p, ierr)

  !  6. Setting the local dimension

  dimen_local = DIMEN/p

  !  7. Allocating and building the vectors

  !  7-1. The common vector

  allocate(vector(1:DIMEN))
  allocate(vector_help(1:DIMEN))

  !  7-2. The final rank 0 vector

  if (my_rank == 0) then

    allocate(vector_final(1:DIMEN))
    vector_final = 0.0_dr

  end if

  !  7-3. Build the common vector

  vector = 0.0_dr

  do i = 1, DIMEN

    vector(i) = dcos(real(i,kind=dr))

  end do

  call MPI_Barrier(comm, ierr)

  !  8. Declaration of matrix for all ranks

  allocate(matrix(1:DIMEN, 1:dimen_local))

  !  9. Definition of matrix for all ranks

  matrix = 0.0_dr

  do j = 1, dimen_local
    do i = 1, DIMEN

      matrix(i,j) = dsin(real(i, kind=dr)) * & 
                    dcos(real(j+dimen_local*my_rank, kind=dr))

    end do
  end do

  call MPI_Barrier(comm, ierr)

  ! 10. Timing. Part --> a

  if (my_rank == 0) then
    call cpu_time(t1) 
  end if

  t1_mpi = MPI_Wtime()

  ! 11. The main matrix-vector product
  !     Benchmarking

  vector_help = 0.0_dr

  do k = 1, KMAX
    do j = 1, dimen_local
      sum_local = 0.0_dr
      do i = 1, DIMEN
        sum_local = sum_local+ matrix(i,j)*vector(i)
      end do
      vector_help(j+my_rank*dimen_local) = sum_local
    end do

    call MPI_Barrier(MPI_COMM_WORLD, ierr)

    call MPI_Reduce(vector_help,          &
                    vector_final,         &
                    DIMEN,                &
                    MPI_DOUBLE_PRECISION, &
                    MPI_SUM,              &
                    0,                    & 
                    MPI_COMM_WORLD,       &
                    ierr)

    call MPI_Barrier(MPI_COMM_WORLD, ierr)

  end do

  ! 12. Timing. Part --> b 

  if (my_rank == 0) then
    call cpu_time(t2)
  end if

  t2_mpi = MPI_Wtime()

  if(my_rank == 0) then
    write(*,*)
    write(*,*) "==================================================================================="
    write(*,*) "  1 --> The vector(1)                  = ", vector_final(1)
    write(*,*) "  2 --> The vector(dimen)              = ", vector_final(DIMEN)
    write(*,*) "  3 --> Real time used (rank 0)        = ", t2-t1, " seconds"
    write(*,*) "  4 --> Real time used (MPI)           = ", t2_mpi-t1_mpi, " seconds"
    write(*,*) "  5 --> Size of the matrix             = ", &
               (DIMEN**2.0_dr) * 8.0_dr/(1024.0_dr**3.0_dr), " GBytes"
    write(*,*) "  6 --> Matrix-Vector products per sec = ", KMAX/(t2_mpi-t1_mpi)
    write(*,*) "  7 --> Matrix dimension (N x N)       = ", DIMEN, " x ", DIMEN
    write(*,*) "  8 --> Total Matrix-Vector products   = ", KMAX
    write(*,*) "  9 --> Total MPI processes            = ", p
    write(*,*) "==================================================================================="
    write(*,*)
  end if

  ! 13. Shut down MPI

  call MPI_Finalize(ierr)

  end program driver_matrix_vector_mpi

!======!
! FINI !
!======!
