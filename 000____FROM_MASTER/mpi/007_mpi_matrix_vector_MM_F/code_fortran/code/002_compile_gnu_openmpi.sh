#!/bin/bash

# 1. compile 

  mpif90.openmpi               \
  -std=f2003                   \
  -Wall                        \
  -O3                          \
  m_1_parameters.f90           \
  driver_matrix_vector_mpi.f90 \
  -o x_gnu_openmpi

# 2. clean

  rm *.mod

# 3. exit
