#!/bin/bash

  # 1. compile
  
  mpif90.mpich  -O3                             \
                -std=f2008                      \
                -Wall                           \
                m_1_type_definitions.f90        \
                m_2_matrix_coo_ram.f90          \
                m_3_smv_coo_half_mpi.f90        \
                driver_program.f90              \
                -lparpack \
                -larpack  \
                -o x_gnu_mpich

  # 2. clean

  rm *.mod
