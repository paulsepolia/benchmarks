!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/09/27              !
!===============================!

  module m_2_matrix_coo_ram

  use mpi
  use m_1_type_definitions

  contains

!===============================!
! A. subroutine: matrix_coo_ram !
!===============================!
  
  subroutine matrix_coo_ram(comm,               &  !  1. 
                            my_rank,            &  !  2.
                            p,                  &  !  3.
                            matrix_dimen,       &  !  4.
                            row_index_array,    &  !  5.
                            column_index_array, &  !  6.
                            ham_elem_array)        !  7.

  implicit none 

  ! 1. interface variables

  integer(kind=si), intent(in)                               :: comm               !  1.
  integer(kind=si), intent(in)                               :: my_rank            !  2.
  integer(kind=si), intent(in)                               :: p                  !  3.
  integer(kind=si), intent(in)                               :: matrix_dimen       !  4.
  integer(kind=si), intent(inout), allocatable, dimension(:) :: row_index_array    !  5.
  integer(kind=si), intent(inout), allocatable, dimension(:) :: column_index_array !  6.
  real(kind=dr),    intent(inout), allocatable, dimension(:) :: ham_elem_array     !  7.

  ! 2. local variables

  integer(kind=di)  :: i1
  integer(kind=di)  :: i2
  integer(kind=si)  :: ierr

  ! 3a. build the local matrices

  do i1 = 1, matrix_dimen/p
    do i2 = 1, matrix_dimen

      ham_elem_array(i2+(i1-1)*matrix_dimen) = &
      dcos(real((i1+my_rank*matrix_dimen/p)*i2, kind=dr))

    end do
  end do

  ! 3b. build the local matrices

  do i1 = 1, matrix_dimen/p
    do i2 = 1, matrix_dimen

      row_index_array(i2+(i1-1)*matrix_dimen) = &
        int(i1+my_rank*(matrix_dimen/p), kind=si)

      column_index_array(i2+(i1-1)*matrix_dimen) = &
        int(i2, kind=si)

    end do 
  end do

  call MPI_Barrier(comm, ierr)

  ! 4. finish

  end subroutine matrix_coo_ram

  end module m_2_matrix_coo_ram  

!======!
! FINI !
!======!
