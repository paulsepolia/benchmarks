!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/09/26              !
!===============================!

  module m_3_smv_coo_half_mpi
 
  use mpi
  use m_1_type_definitions

  implicit none

  contains 

!=================================!
! A. subroutine: smv_coo_half_mpi !
!=================================!

  subroutine smv_coo_half_mpi(comm,               &  !  1.
                              matrix_dimen,       &  !  2.
                              row_index_array,    &  !  3.
                              column_index_array, &  !  4.
                              ham_elem_array,     &  !  5.
                              non_zero_a,         &  !  6.
                              x,                  &  !  7.
                              y,                  &  !  8.
                              y_final)               !  9.

  !  1. declarations of the interface variables

  integer(kind=si), intent(in)                                :: comm                !  1.
  integer(kind=si), intent(in)                                :: matrix_dimen        !  2.
  integer(kind=si), intent(in),    allocatable, dimension(:)  :: row_index_array     !  3.
  integer(kind=si), intent(in),    allocatable, dimension(:)  :: column_index_array  !  4.
  real(kind=dr)   , intent(in),    allocatable, dimension(:)  :: ham_elem_array      !  5.
  integer(kind=di), intent(in)                                :: non_zero_a          !  6.
  real(kind=dr),    intent(in),    allocatable, dimension(:)  :: x                   !  7.
  real(kind=dr),    intent(inout), allocatable, dimension(:)  :: y                   !  8.
  real(kind=dr),    intent(inout), allocatable, dimension(:)  :: y_final             !  9.

  !  2. declarations of the rest variables

  integer(kind=di)  :: i
  integer(kind=si)  :: ierr
  integer(kind=si)  :: irow
  integer(kind=si)  :: icolumn

  !  3. broadcasting the x vector to the rest of mpi processes 

  call MPI_Bcast(x, matrix_dimen, MPI_DOUBLE_PRECISION, 0, comm, ierr)
  call MPI_Barrier(comm, ierr)

  !  4. the mpi sparse matrix - vector product 

  !  4a.

  y = 0.0_dr

  !  4b. 

  do i = 1, non_zero_a
 
    irow = row_index_array(i)
    icolumn = column_index_array(i)
    y(irow) = y(irow) + ham_elem_array(i) * x(icolumn)
!    if (irow.ne.icolumn) y(icolumn) = y(icolumn) + ham_elem_array(i) * x(irow)

  end do

  call MPI_Barrier(comm, ierr)

  ! 4c.

  call MPI_Reduce(y,                    &
                  y_final,              &
                  matrix_dimen,         &
                  MPI_DOUBLE_PRECISION, & 
                  MPI_SUM,              & 
                  0,                    &
                  comm,                 &
                  ierr)

  call MPI_Barrier(comm, ierr)

  end subroutine smv_coo_half_mpi

  end module m_3_smv_coo_half_mpi

!======!
! FINI !
!======!
