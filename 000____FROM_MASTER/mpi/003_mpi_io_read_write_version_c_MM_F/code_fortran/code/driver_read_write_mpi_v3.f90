!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/08/21              !
!===============================!

  program driver_read_write_mpi_v3

  use mpi
  use m_1_type_definitions

  implicit none
   
  !  1. interface parameters

  character(len=200), parameter :: TEST_FILE           = "test_file"
  integer(kind=di), parameter   :: DIMEN               = 1_di * 1000_di * 1000_di
  character(len=200), parameter :: DATA_REPRESENTATION = "native" ! "internal", "external32"
  integer(kind=si), parameter   :: I_DO_MAX            = 4_si
  logical, parameter            :: RUN_TEST            = .false. 
  real(kind=dr)                 :: ERROR_TEST          = 10e-12_dr
  integer(kind=si), parameter   :: IO_MODE             = MPI_MODE_CREATE + &
                                                         MPI_MODE_RDWR   + &
                                                         MPI_MODE_DELETE_ON_CLOSE

  !  2. local variables

  integer(kind=si)                         :: comm
  integer(kind=si)                         :: ierr
  integer(kind=si)                         :: my_rank
  integer(kind=si)                         :: p
  integer(kind=si)                         :: i1
  integer(kind=si)                         :: i2
  integer(kind=si)                         :: the_file
  integer(kind=MPI_OFFSET_KIND)            :: disp
  real(kind=dr), allocatable, dimension(:) :: test_array
  real(kind=dr)                            :: t1_mpi
  real(kind=dr)                            :: t2_mpi
  real(kind=dr)                            :: t_mpi_write
  real(kind=dr)                            :: t_mpi_read
  real(kind=dr)                            :: total_giga_bytes
  real(kind=dr)                            :: total_mega_bytes
  real(kind=dr)                            :: tmp_var

  !  3. mpi starts

  t1_mpi      = 0.0_dr
  t2_mpi      = 0.0_dr
  t_mpi_write = 0.0_dr
  t_mpi_read  = 0.0_dr

  call MPI_Init(ierr)

  comm = MPI_COMM_WORLD

  call MPI_Comm_rank(comm, my_rank, ierr)

  call MPI_Comm_size(comm, p, ierr)

  !  4. build an array local to each MPI process

  allocate(test_array(1:DIMEN))

  do i1 = 1, DIMEN

    test_array(i1) = sin(real(i1+my_rank*p, kind=dr))
   
  end do

  call MPI_Barrier(comm, ierr)

  do i1 = 1, I_DO_MAX

    !  5. timing write starts

    if (i1 == 1) then
      t1_mpi = MPI_Wtime()
    end if

    !  6. all the processes open a single file

    call MPI_File_open(comm,          &
                       TEST_FILE,     &
                       IO_MODE,       &
                       MPI_INFO_NULL, &
                       the_file,      &
                       ierr)

    !  7. set the MPI file view

    disp = my_rank * DIMEN * 8_si ! displacement in bytes

    call MPI_File_set_view(the_file,             &
                           disp,                 &
                           MPI_DOUBLE_PRECISION, &
                           MPI_DOUBLE_PRECISION, &
                           DATA_REPRESENTATION,  &
                           MPI_INFO_NULL,        &
                           ierr)

    !  8. write to the file in parallel

    call MPI_File_write(the_file,             &
                        test_array,           &
                        DIMEN,                &
                        MPI_DOUBLE_PRECISION, &
                        MPI_STATUS_IGNORE,    &
                        ierr)

    !  9. timing write ends

    if (i1 == I_DO_MAX) then
      t2_mpi = MPI_Wtime()
      t_mpi_write = t2_mpi - t1_mpi
    end if

    call MPI_Barrier(comm, ierr)

    ! 10. timing write starts

    if (i1 == 1) then
      t1_mpi = MPI_Wtime()
    end if

    ! 11. read the file in parallel

    call MPI_File_read(the_file,             &
                       test_array,           &
                       DIMEN,                &
                       MPI_DOUBLE_PRECISION, &
                       MPI_STATUS_IGNORE,    &
                       ierr)
 
    ! 12. timing read ends

    if (i1 == I_DO_MAX) then
      t2_mpi = MPI_Wtime()
      t_mpi_read = t2_mpi - t1_mpi
    end if

    call MPI_Barrier(comm, ierr)

    ! 13. do a test what data has been read and written

    if (RUN_TEST) then

      do i2 = 1, DIMEN

        tmp_var = test_array(i2) - sin(real(i2+my_rank*p, kind=dr))

        if(abs(tmp_var) > ERROR_TEST) then

          write(*,*)  " From rank ", &
                      my_rank,       &
                      " --> i2 = ",  &
                      i2,            &
                      abs(tmp_var)
          stop

        end if

      end do

    end if

    call MPI_Barrier(comm, ierr)

    ! 14. close the file and execute the IO_MODE

    call MPI_File_close(the_file, ierr)

  end do

  !  15. report

  if (my_rank == 0) then

    total_giga_bytes = I_DO_MAX*p*DIMEN*8_si/(1024.0_dr)**3
    total_mega_bytes = total_giga_bytes * 1024.0_dr

    write(*,*)
    write(*,*) "================================================================"
    write(*,*) " Author      : Pavlos G. Galiatsatos                            "
    write(*,*) " Description : MPI I/O via 'MPI_File_read' and 'MPI_File_write' "
    write(*,*) " Language    : Fortran 2008                                     "
    write(*,*) " Date        : 2013/08/21                                       "
    write(*,*) " Code name   : BENIOC                                           "
    write(*,*) "================================================================"
    write(*,*)

    write(*,*) "  1. Total real time used to write is    : ", t_mpi_write, " seconds"
    write(*,*) "  2. Total real time used to read  is    : ", t_mpi_read , " seconds"
    write(*,*) "  3. Total GBytes written are            : ", total_giga_bytes
    write(*,*) "  4. Total GBytes read are               : ", total_giga_bytes
    write(*,*) "  5. Speed write (MBytes/sec) is         : ", total_mega_bytes/t_mpi_write
    write(*,*) "  6. Speed read  (MBytes/sec) is         : ", total_mega_bytes/t_mpi_read
    write(*,*) "  7. Speed write (MBytes/sec/process) is : ", total_mega_bytes/t_mpi_write/p
    write(*,*) "  8. Speed read  (MBytes/sec/process) is : ", total_mega_bytes/t_mpi_read/p
    write(*,*) "  9. Total MPI processes used are        : ", p

  end if

  ! 16. free up the RAM and exit the MPI environment

  deallocate(test_array)

  call MPI_Finalize(ierr)

  end program driver_read_write_mpi_v3

!======!
! FINI !
!======!
