#!/bin/bash

# 1. compile

  gfortran-4.8  -std=f2008               \
                -Wall                    \
                -Ofast                   \
                -static                  \
                -static-libgfortran      \
                m_1_type_definitions.f90 \
                driver_program.f90       \
                -o x_gnu_read

# 2. clean

  rm *.mod
