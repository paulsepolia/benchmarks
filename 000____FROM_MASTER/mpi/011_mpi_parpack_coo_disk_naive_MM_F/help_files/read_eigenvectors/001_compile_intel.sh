#!/bin/bash

# 1. compile

  ifort  -e08                     \
         -warn all                \
         -xHost                   \
         -static                  \
         -assume buffered_io      \
         m_1_type_definitions.f90 \
         driver_program.f90       \
         -o x_intel_read

# 2. clean

  rm *.mod
