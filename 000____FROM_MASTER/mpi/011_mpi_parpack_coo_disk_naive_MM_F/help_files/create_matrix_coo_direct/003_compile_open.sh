#!/bin/bash

# 1. compile

  openf95  -fullwarn                \
           -O3                      \
           -static-libgcc           \
           m_1_type_definitions.f90 \
           driver_coo_direct.f90    \
           -o x_open_coo

# 2. clean

  rm *.mod
