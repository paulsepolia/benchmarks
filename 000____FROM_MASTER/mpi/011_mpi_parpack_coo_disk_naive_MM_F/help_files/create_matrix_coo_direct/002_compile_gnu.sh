#!/bin/bash

# 1. compile

  gfortran-4.8  -std=f2008               \
                -Wall                    \
                -Ofast                   \
                -static                  \
                -static-libgfortran      \
                m_1_type_definitions.f90 \
                driver_coo_direct.f90    \
                -o x_gnu_coo

# 2. clean

  rm *.mod
