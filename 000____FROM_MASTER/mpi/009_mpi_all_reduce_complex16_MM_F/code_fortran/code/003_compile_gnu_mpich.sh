#!/bin/bash

  # 1. compile

  mpif90.mpich  -O3                                 \
                -std=f2003                          \
                -Wall                               \
                m_1_parameters.f90                  \
                driver_mpi_all_reduce_complex16.f90 \
                -o x_gnu_mpich

  # 2. clean

  rm *.mod
