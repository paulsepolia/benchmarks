!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/03/16              !
!===============================!

!======================================================!
! MPI Code                                             !
! Testing the MPI_Allreduce subroutine                 !
!                                                      !
! 1. I allocate ram space for a vector.                !
!    This is done for every mpi thread.                !
!                                                      !
! 2. I define the vector only in mpi thread 0.         !
!    So I occupy ram only concerning mpi thread 0.     !
!                                                      !
! 3. I broadcast the above ram/vector                  !
!    to any other mpi existing thread.                 !
!                                                      !
! 4. I do many MPI_Reduce to find the total sum of the !
!    elements of the vector.                           !
!                                                      !
! 5. I am using only the mpi functions/subroutines:    !
!                                                      !
!    mpi_init ( ... )                                  !
!    mpi_comm_rank ( ... )                             !
!    mpi_comm_size ( ... )                             !
!    mpi_bcast ( ... )                                 !
!    mpi_finalize( ... )                               !
!    mpi_allreduce( ... )                              !
!                                                      !
!======================================================!

  program mpi_all_reduce_test

  use mpi
  use m_1_parameters

  implicit none

  !=========================!
  !  1a. Start of interface !
  !=========================!

  integer(kind=di), parameter :: DIMEN = int(4 * (10.0_dr ** 6), kind=di)
  integer(kind=si), parameter :: KMAX  = int(1 * (10.0_dr ** 3), kind=si)

  !=======================!
  !  1b. End of interface !
  !=======================!

  !  2. local variables

  integer(kind=si) :: ierr
  integer(kind=si) :: my_rank
  integer(kind=si) :: p
  integer(kind=di) :: i
  integer(kind=di) :: k
  integer(kind=si) :: comm
  complex(kind=dr), allocatable, dimension(:) :: vector
  complex(kind=dr), allocatable, dimension(:) :: sum_vector
  real(kind=dr) :: t1
  real(kind=dr) :: t2
  real(kind=dr) :: t1_mpi
  real(kind=dr) :: t2_mpi
  real(kind=dr) :: sum_my_real
  real(kind=dr) :: sum_my_imag
  real(kind=dr) :: real_part
  real(kind=dr) :: imag_part

  !  3. mpi start up

  call mpi_init(ierr)

  !  4. get my process rank

  call mpi_comm_rank(MPI_COMM_WORLD, my_rank, ierr)

  comm = MPI_COMM_WORLD

  !  5. find out how many mpi processes are being used

  call mpi_comm_size(comm, p, ierr)

  !  6. interface 

  if (my_rank == 0) then

    write(*,*) " 1. Please wait while the mpi executes ... "

  end if

  call mpi_barrier(comm, ierr)

  !  7. allocating space in each mpi thread
  !     but building the vector only in master mpi thread

  allocate(vector(1:DIMEN))
  allocate(sum_vector(1:DIMEN))

  vector = (0.0_dr, 0.0_dr)
  sum_vector = (0.0_dr, 0.0_dr)

  if (my_rank == 0) then

    do i = 1, DIMEN

      real_part = cos(real(i, kind=dr))
      imag_part = sin(real(i, kind=dr))
      vector(i) = cmplx(real_part, imag_part, kind=dr)

    end do

  end if

  call mpi_barrier(comm, ierr)

  !  8. broadcasting the vector to the rest mpi threads

  call mpi_bcast(vector,        &
                 DIMEN,         &
                 MPI_COMPLEX16, &
                 0,             &
                 comm,          &
                 ierr)


  call mpi_barrier(comm, ierr)  

  !  9. timing

  if (my_rank == 0) then

    call cpu_time(t1)

  end if

  t1_mpi = mpi_wtime()

  ! 10. looping 

  sum_vector  = cmplx(0.0_dr, 0.0_dr, kind=dr)
  sum_my_real = 0.0_dr
  sum_my_imag = 0.0_dr

  do k = 1, KMAX

    sum_my_real = 0.0_dr
    sum_my_imag = 0.0_dr

    call mpi_allreduce(vector,        & 
                       sum_vector,    &
                       DIMEN,         &
                       MPI_COMPLEX16, &
                       MPI_SUM,       & 
                       comm,          & 
                       ierr)

  end do  

  ! 11. final result

  call mpi_barrier(comm, ierr)

  do i = 1, DIMEN

    sum_my_real = sum_my_real + real(sum_vector(i), kind=dr) 
    sum_my_imag = sum_my_imag + aimag(sum_vector(i))

  end do

  sum_my_real = sum_my_real / p
  sum_my_imag = sum_my_imag / p

  ! 12. timing

  call cpu_time(t2)

  t2_mpi = mpi_wtime()

  ! 13. outputs 

  do i = 0, p-1

    if (my_rank == i) then

      write(*,*) "  -----> For the mpi thread --> ", i 

      write(*,*) "  2. Total real time used is --> ", (t2-t1), " seconds. "

      write(*,*) "  3. Total real time used (mpi) is --> ", (t2_mpi-t1_mpi), " seconds. "

      write(*,*) "  4. The broadcasted vector size is --> ", &
                 DIMEN * 8.0_dr /(1024.0_dr ** 3.0_dr), " gigabytes. "

      write(*,*) "  5. The all-reductions per second is --> ", KMAX * p / (t2_mpi-t1_mpi)

      write(*,*) "  6. The sum_real is --> ", sum_my_real

      write(*,*) "  7. The sum_imag is --> ", sum_my_imag

    end if

  call mpi_barrier(comm, ierr)

  end do

  ! 14. mpi shut down

  call mpi_finalize(ierr)

end program mpi_all_reduce_test

!======!
! FINI !
!======!
