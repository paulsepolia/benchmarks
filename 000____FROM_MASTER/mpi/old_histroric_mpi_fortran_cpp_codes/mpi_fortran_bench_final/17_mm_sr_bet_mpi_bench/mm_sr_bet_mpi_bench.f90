
program mpi_bench_17

! MPI Benchmark 17.

! 1. An MPI matrix-matrix product.

! 2. The allocation and building of the two matrices
!    is done only in master mpi thread ( rank = 0 ).

! 3. I use the blocking MPI_Send and MPI_Recv commands
!    for broadcasting parts of the matrix and the whole vector
!    to the rest of mpi threads.

! 4. The limitation is that the number of 1st matrix rows
!    must be divisible by p ( number of mpi threads)
!    and the number of 2nd matrix columns
!    must be divisible by p ( number of mpi threads ).

! 5. All the started up mpi threads do the matrix-matrix product.
!    Each one its own part.

! 6. I do not use the MPI_Reduce function to collect and sum all
!    the local sums.

! 7. MPI functions/subroutines in use :

!    MPI_Init( ... )
!    MPI_Comm_size( ... )
!    MPI_Comm_rank( ... )
!    MPI_Send( ... )
!    MPI_Recv( ... )

! 8. MPI type declarations in use :

!    MPI_Status

  use mpi
  implicit none

  integer*4 :: ierr, my_rank, p, sentinel
  integer*8 :: dimen, dimen_small, i, j, k, L, Lmax
  real*8, allocatable, dimension(:,:) :: matrix_1, matrix_2
  real*8, allocatable, dimension(:,:) :: matrix_local_1, matrix_local_2
  real*8, allocatable, dimension(:,:) :: matrix_res
  integer*4, allocatable, dimension(:) :: tag
  integer*4 :: status_my(MPI_STATUS_SIZE)
  real*8 :: t1, t2, t1_mpi, t2_mpi
  real*8 :: sum_local

  ! 1. mpi start up
  call MPI_Init( ierr )

  ! 2. get my process rank
  call MPI_Comm_rank( MPI_COMM_WORLD, my_rank, ierr )

  ! 3. find out how many mpi processes are being used
  call MPI_Comm_size( MPI_COMM_WORLD, p, ierr )

  ! 4. setting the dimension of the vectors
  dimen = 2 * 1024 * (10.0 ** 1.0)
  dimen_small = dimen / p

  ! 5. interface
  if ( my_rank == 0 ) then
    write(*,*) "  1. Enter an integer to start the mpi benchmark : "
    read(*,*) sentinel
    write(*,*) "  2. Please wait while the mpi benchmark executes ... "
  end if

  ! 6. allocating of the 1st matrix
  allocate( matrix_1(1:dimen,1:dimen) )

  ! 7. definition of 1st matrix for rank 0
  if ( my_rank == 0 ) then
    do i = 1, dimen
      do j = 1, dimen
        matrix_1(i,j) = dcos(dsin(dcos( real(i+j,kind=8) )))
      end do
    end do
  end if
 
  ! 8. setting up the tags
  allocate( tag(1:p) )
  do k = 1, p
    tag(k) = k + 10
  end do
 
  ! 9. 1st local matrix for the rest of the mpi threads
  allocate( matrix_local_1(1:dimen,1:dimen_small) )
 
  ! 10. send parts of the 1st matrix to the rest of the mpi threads
  if ( my_rank == 0 ) then
    ! 11. define the small matrix for rank 0
    do j = 1, dimen_small
      do i = 1, dimen
        matrix_local_1(i,j) = matrix_1(i,j)
      end do
    end do
  else  ! 12. define the small matrix for the rest of the mpi threads
    do k = 1, p-1
        call MPI_Send( matrix_1(dimen_small*k,dimen_small), dimen_small, &
                       MPI_DOUBLE_PRECISION, k, tag(k), MPI_COMM_WORLD, ierr )
    end do
  end if

  ! 13. receive the sent parts of the 1st matrix
  do k = 1, p-1
    if ( my_rank == k ) then
      do i = 1, dimen
        call MPI_Recv( matrix_local_1(i,:), dimen, MPI_DOUBLE_PRECISION, &
                       0, tag(k), MPI_COMM_WORLD, status_my, ierr )
      end do
    end if
  end do

  ! 14. deleting the matrix_1
  deallocate( matrix_1 )

  ! 15. allocating of the 2nd matrix
  allocate( matrix_2(1:dimen,1:dimen) )

  ! 16. definition of 2nd matrix for rank 0
  if ( my_rank == 0 ) then
    do i = 1, dimen
      do j = 1, dimen
        matrix_2(i,j) = dcos(dsin(dcos( real(i+j,kind=8) ))) 
      end do
    end do
  end if
 
  ! 17. 2nd local matrix for the rest of the mpi threads
  allocate( matrix_local_2(1:dimen,1:dimen_small) )
 
  ! 18. send parts of the 2nd matrix to the rest of the mpi threads
  if ( my_rank == 0 ) then
    ! 19. define the small matrix for rank 0
    do j = 1, dimen_small
      do i = 1, dimen
        matrix_local_2(i,j) = matrix_2(i,j)
      end do
    end do
  else ! 20. define the small matrix for the rest of the mpi threads
    do k = 1, p-1
      do i = 1, dimen
        call MPI_Send( matrix_2(i,dimen_small*k), dimen_small, MPI_DOUBLE_PRECISION, &
                       k, tag(k), MPI_COMM_WORLD, ierr )
      end do
    end do
  end if

  ! 21. receive the sent parts of the 2nd matrix
  do k = 1, p-1
    if ( my_rank == k ) then
      do i = 1, dimen
        call MPI_Recv( matrix_local_2(i,:), dimen, MPI_DOUBLE_PRECISION, &
                       0, tag(k), MPI_COMM_WORLD, status_my, ierr )
      end do
    end if
  end do

  ! 22. deleting the matrix_2
  deallocate( matrix_2 )

  ! 23. timing - part a
  if ( my_rank == 0 ) then
    call cpu_time(t1) 
  end if
  t1_mpi = MPI_Wtime()

  ! 24. the main matrix-matrix product - benchmarking
  Lmax = 1 * (10.0 ** 0.0)  ! benchmark variable

  ! 25. allocating of the resultant matrix
  allocate( matrix_res(1:dimen,1:dimen) )

  do L = 1, Lmax ! 26. benchmarking loop
    do j = 1, dimen_small
      do i = 1, dimen_small
        sum_local = 0.0
        do k = 1, dimen
          sum_local = sum_local + matrix_local_1(k,i) * matrix_local_2(k,j)
          matrix_res(i,j) = sum_local
         end do
      end do
    end do
  end do ! 27. benchmarking loop

  ! 28. timing - part b
  if ( my_rank == 0 ) then
    call cpu_time(t2)
  end if

  t2_mpi = MPI_Wtime()

  ! 29. matrix_local_1 and matrix_local_2 deallocation
   
  deallocate( matrix_local_1 )
  deallocate( matrix_local_2 )

  ! 30. the results
  call MPI_Barrier( MPI_COMM_WORLD, ierr )

  if( my_rank == 0 ) then
    write(*,*) "  3. The matrix_result(1,3)        is ", matrix_res(1,3) 
    write(*,*) "  4. The matrix_result(2,1)        is ", matrix_res(2,1)
    write(*,*) "  5. The matrix_result(1,2)        is ", matrix_res(1,2)
    write(*,*) "  6. The matrix_result(3,1)        is ", matrix_res(3,1)
    write(*,*) "  7. The real time used ( rank 0 ) is ", (t2-t1), " seconds. "
    write(*,*) "  8. The real time used ( mpi )    is ", (t2_mpi-t1_mpi), " seconds. "
    write(*,*) "  9. The size of each matrix       is ", (dimen ** 2.0) * 8.0 / (1024.0 ** 3.0), " gigabytes. " 
    write(*,*) " 10. The dimension of each matrix  is ", dimen
    write(*,*) " 11. We have ", Lmax / (t2_mpi-t1_mpi), " matrix-matrix products per second."
    write(*,*) " 12. Enter an integer to exit : "
    read(*,*) sentinel
  end if

  ! 31. mpi shut down
  call MPI_Finalize( ierr )

end program mpi_bench_17
