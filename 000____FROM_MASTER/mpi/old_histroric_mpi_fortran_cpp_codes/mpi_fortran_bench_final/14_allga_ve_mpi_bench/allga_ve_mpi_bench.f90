
program mpi_bench_14

! MPI Benchmark 14.

! 1. I allocate ram space for vectors.
!    This is done for every mpi thread.
! 2. I define the vectors in any mpi thread.
! 3. I allgather the vectors in master thread (here the benchmark).
! 4. I am using only the mpi functions/subroutines:
!
!    MPI_Init ( ... )
!    MPI_Comm_rank ( ... )
!    MPI_Comm_size ( ... )
!    MPI_Allgather ( ... )
!    MPI_Finalize( ... )

  use mpi
  implicit none

  integer*4 :: ierr, my_rank, p, sentinel
  integer*8 :: i, j, k, dimen, kmax
  real*8, allocatable, dimension(:) :: vector_ga, vector
  real*8 :: t1, t2, t1_mpi, t2_mpi

  ! 1. mpi start up
  call MPI_Init( ierr )

  ! 2. get my process rank
  call MPI_Comm_rank( MPI_COMM_WORLD, my_rank, ierr )

  ! 3. find out how many mpi processes are being used
  call MPI_Comm_size( MPI_COMM_WORLD, p, ierr )
  
  ! 4. setting the dimension of the vectors 
  dimen = 3 * 1024 * (10.0 ** 4.0)

  ! 5. interface
  if ( my_rank == 0 ) then
    write(*,*) "  1. Enter an integer to start the mpi benchmark : "
    read(*,*) sentinel
    write(*,*) "  2. Please wait while the mpi executes ... "
  end if

  ! 6. building the vectors
  allocate( vector_ga( 1 : dimen * p ) )
  allocate( vector ( 1: dimen ) )

  do i = 1, dimen
    vector(i) = dcos(dsin(dcos( real(i+my_rank,kind=8) )))
  end do

  ! 7. timing
  if ( my_rank == 0 ) then
    call cpu_time(t1)
  end if
  t1_mpi = MPI_Wtime()

  ! 8. allgather the vectors in master thread
  kmax = 100
  do k = 1, kmax
    call MPI_Allgather( vector, dimen, MPI_DOUBLE_PRECISION,    &
                        vector_ga, dimen, MPI_DOUBLE_PRECISION, &
                        MPI_COMM_WORLD, ierr )
    if ( k < kmax ) then    
      deallocate ( vector_ga ) 
      allocate ( vector_ga(1:dimen*p) )
    end if
  end do

  ! 9. timing
  call cpu_time(t2)
  t2_mpi = MPI_Wtime()

  ! 10. put a mpi barrier
  call MPI_Barrier( MPI_COMM_WORLD, ierr )

  ! 11. sentineling - just to observe the ram
  if ( my_rank == 0 ) then
    write(*,*) "  3. Total real time used ( rank 0 ) is ", (t2-t1), " seconds. " 
    write(*,*) "  4. Total real time used ( mpi )    is ", (t2_mpi-t1_mpi), " seconds. " 
    write(*,*) "  5. The gathered vector size        is ", dimen*8/(1024.0**3.0), " gigabytes. "
    write(*,*) "  6. The allgathers per second       is ", kmax*p/(t2_mpi-t1_mpi)
    write(*,*) "  7. Enter an integer to exit : "
    read(*,*) sentinel
  end if

  ! 12. mpi shut down
  call MPI_Finalize( ierr )

end program mpi_bench_14
