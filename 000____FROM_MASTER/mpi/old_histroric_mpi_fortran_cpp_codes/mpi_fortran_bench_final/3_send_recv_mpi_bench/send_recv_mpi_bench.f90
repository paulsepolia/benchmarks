
  program mpi_bench_3

! MPI Benchmark 3. 

! 1. I allocate ram space for the variable: vector[dimen].

! 2. I allocate ram space for the 2D vector : vector_local[p][dimen].

! 3. I use the non blocking mpi routines MPI_Recv and MPI_Send 
!    to copy and paste the data of vector[dimen] to the vector_local[p][dimen].

! 4. The 1D variable vector[dimen] is defined only in mpi thread 0.

! 5. The 2D variables vector_local[p][dimen] are defined only in mip threads 1 -> p-1.  

! 6. I do this copy and paste many times.

! 7. This benchmarks resables the MPI Benchmark 1 
!    since i do the same but using the MPI_Bcast routine.

! 8. The mpi routines in use are:
!    MPI_Inint( ... )
!    MPI_Comm_rank ( ... )
!    MPI_Comm_size( ... )
!    MPI_Recv( ... )
!    MPI_Send( ... )

! 9. The mpi type declarations in use are:
!    MPI_Status

  use mpi
  implicit none  

  integer*4 :: ierr
  integer*4 :: my_rank
  integer*4 :: p
  integer*8, parameter :: dimen = 2 * ( 10.0 ** 8.0 )
  integer*4 :: sentinel
  real*8, allocatable, dimension(:) :: vector
  real*8, allocatable, dimension(:,:) :: vector_local
  integer*4 :: tag
  integer*8 :: i
  integer*4 :: status_my( MPI_STATUS_SIZE )
  real*8 :: t1, t2, t1_mpi, t2_mpi 
  integer*8 :: k, kmax

  ! 1. mpi start up
  call MPI_Init( ierr )

  ! 2. get my process rank
  call MPI_Comm_rank( MPI_COMM_WORLD, my_rank, ierr )

  ! 3. find out how many mpi processes are being used
  call MPI_Comm_size( MPI_COMM_WORLD, p, ierr )

  ! 4. interface
  if ( my_rank == 0 ) then
    write(*,*) "  1. Enter an integer to start the mpi benchmark : "
    read(*,*) sentinel
    write(*,*) "  2. Please wait while the mpi benchmark executes ... "
  end if
 
  ! 5. allocating and building the vectors
  !    global vector - only for master thread ( my_rank = 0)
  allocate( vector(1:dimen) )

  if ( my_rank == 0 ) then 
    do i = 1, dimen
      vector(i) = dcos(dsin(dcos( real(i,kind=8) ))) 
    end do
  end if  

  ! 6. sending parts of the vectors 
  !    from the master thread to the rest mpi threads
  !    The tags and mpi defined data types
  tag = 30 

  ! 7. timing
  if ( my_rank == 0 ) then
    call cpu_time(t1)
  end if
  
  t1_mpi = MPI_Wtime()

  ! 8. some constants and looping
  kmax = 5 * ( 10.0 ** 1.0 ) 

  do k = 1, kmax
 
    if ( my_rank == 0 ) then
      call MPI_Send( vector, dimen, MPI_DOUBLE_PRECISION, 1, tag, MPI_COMM_WORLD, ierr )
      call MPI_Recv( vector, dimen, MPI_DOUBLE_PRECISION, p-1 , tag, MPI_COMM_WORLD,  status_my, ierr )
    end if
 
    do i = 1, p - 2
      if ( my_rank == i ) then
        allocate( vector_local(p, dimen) )
        call MPI_Recv( vector_local, dimen, MPI_DOUBLE_PRECISION, i-1, tag, MPI_COMM_WORLD, status_my, ierr )
        call MPI_Send( vector_local, dimen, MPI_DOUBLE_PRECISION, i+1, tag, MPI_COMM_WORLD, ierr )
        deallocate ( vector_local )
      end if 
   
      if ( my_rank == p - 1 ) then
        allocate( vector_local(p,dimen) )
        call MPI_Send( vector_local, dimen, MPI_DOUBLE_PRECISION, 0, tag, MPI_COMM_WORLD, ierr )
        call MPI_Recv( vector_local, dimen, MPI_DOUBLE_PRECISION, p-2, tag, MPI_COMM_WORLD, status_my, ierr )
        deallocate( vector_local )
      end if
    end do

  end do

  ! 9. timing
  if ( my_rank == 0 ) then
    call cpu_time(t2)
  end if

  t2_mpi = MPI_Wtime()

  ! 10. the results
  if( my_rank == 0 ) then
    write(*,*) "  3. The real time used ( rank 0 ) is ", 1.0 * (t2-t1), " seconds. "
    write(*,*) "  4. The real time used ( mpi )    is ", (t2_mpi-t1_mpi), " seconds. "
    write(*,*) "  5. The size of the vector        is ", dimen * 8 / (1024.0d0 ** 3.0), " gigabytes. "
    write(*,*) "  6. Enter an integer to exit : "
    read(*,*) sentinel
    write(*,*) " pgg : program will terminate abnormally."
    stop  ! 11. there is no synchronization (deadlock) at the end so i stop it manually.
  end if

  ! 12. mpi shut down
  call  MPI_Finalize( ierr )

  end program mpi_bench_3
