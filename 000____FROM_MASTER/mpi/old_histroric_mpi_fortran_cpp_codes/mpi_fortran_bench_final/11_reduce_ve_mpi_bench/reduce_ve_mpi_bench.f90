program mpi_bench_11

! MPI Benchmark 11.

! 1. I allocate ram space for a vector.
!    This is done for every mpi thread.
! 2. I define the vector only in mpi thread 0.
!    So I occupy ram only concerning mpi thread 0.
! 3. I broadcast the above ram/vector
!    to any other mpi existing thread.
! 4. I do many MPI_Reduce to find the total sum of the
!    elements of the vector.
! 5. I am using only the mpi function:
!
!    MPI_Init ( ... )
!    MPI_Comm_rank ( ... )
!    MPI_Comm_size ( ... )
!    MPI_Bcast ( ... )
!    MPI_Finalize( ... )
!    MPI_Reduce( ... ) 

  use mpi

  implicit none

  integer*4 :: my_rank, ierr, p, sentinel
  integer*8 :: dimen, i, j, k
  real*8, allocatable, dimension(:) :: vector
  real*8 :: t1, t2, t1_mpi, t2_mpi
  real*8 :: sum_my, sum_tmp
  integer*8 :: kmax 

  ! 1. mpi start up
  call MPI_Init( ierr )

  ! 2. get my process rank
  call MPI_Comm_rank( MPI_COMM_WORLD, my_rank, ierr )

  ! 3. find out how many mpi processes are being used
  call MPI_Comm_size( MPI_COMM_WORLD, p, ierr )
  
  ! 4. setting the dimension of the vectors 
  dimen = 1 * (10.0 ** 8.0)

  ! 5. interface
  if ( my_rank == 0 ) then
    write(*,*) "  1. Enter an integer to start the mpi benchmark : "
    read(*,*) sentinel
    write(*,*) "  2. Please wait while the mpi executes ... "
  end if

  ! 6. allocating space and building the matrix
  allocate( vector(1:dimen) )

  if ( my_rank == 0 ) then
    do i = 1, dimen
      vector(i) = dcos(dsin(dcos( real(i,kind=8) )))
    end do
  end if
  
  ! 7. broadcasting the vector to the rest mpi threads
  call MPI_Bcast( vector, dimen, MPI_DOUBLE_PRECISION, & 
                  0, MPI_COMM_WORLD, ierr )

  ! 8. timing
  if ( my_rank == 0 ) then
    call cpu_time(t1)
  end if
  t1_mpi = MPI_Wtime()

  ! 9. looping 
  sum_my = 0.0
  sum_tmp = 0.0
  kmax = 4 * ( 10.0 ** 0.0 )

  do k = 1, kmax
    sum_my = 0.0
    sum_tmp = 0.0
    do i = 1, dimen
      call MPI_Reduce( vector(i), sum_tmp, 1,              &
                       MPI_DOUBLE_PRECISION,               &
                       MPI_SUM, 0, MPI_COMM_WORLD, ierr )
      sum_my = sum_my + sum_tmp
    end do
  end do 

  ! 10. final result
  sum_my = sum_my / p

  ! 11. timing
  if ( my_rank == 0 ) then
    call cpu_time(t2)
  end if
  t2_mpi = MPI_Wtime()

  ! 12. sentineling - just to observe the ram
  call MPI_Barrier( MPI_COMM_WORLD, ierr )

  if ( my_rank == 0 ) then
    write(*,*) "  3. Total real time used ( rank 0 ) is ", (t2-t1), " seconds. "
    write(*,*) "  4. Total real time used ( mpi )    is ", (t2_mpi-t1_mpi), " seconds. "
    write(*,*) "  5. The broadcasted vector size     is ", dimen * 8.0 /(1024.0d0 ** 3.0), " gigabytes. "
    write(*,*) "  6. The reductions per second       is ", kmax * p / (t2_mpi-t1_mpi)
    write(*,*) "  7. The sum                         is ", sum_my
    write(*,*) "  8. Enter an integer to exit : "
    read(*,*) sentinel
  end if

  ! 13. mpi shut down
  call MPI_Finalize( ierr )

end program mpi_bench_11
