
  program mpi_bench_8

! MPI Benchmark 8.

! 1. An MPI matrix-matrix product.

! 2. The allocation and building of the two matrices
!    is done only in master mpi thread ( rank = 0 ).

! 3. The algorithm is the following:


! 4. The 1st limitation is that the number of rows( columns ) of the 1st matrix
!    must be divisible by p ( number of mpi threads )
!    and the number of 2nd matrix columns( rows )
!    must be divisible by p ( number of mpi threads ).

! 5. The 2nd limitation is that tye following program holds 
!    only for symmetric matrices.

! 6. Both above limitatins can be cancelled and 
!    a full generalization is straightforward.
!    This will be done in a next mpi program.

! 7. All the started-up mpi threads do the matrix-matrix product.
!    Each one its own part.

! 8. MPI function in use :

!    MPI_Init( ... )
!    MPI_Comm_size( ... )
!    MPI_Comm_rank( ... )
!    MPI_Send( ... )
!    MPI_Recv( ... )
!    MPI_Scatter( ... )
!    MPI_Barrier( ... )

! 9. MPI type declarations in use :

!    MPI_Status

  use mpi
  implicit none
  
  integer*4 :: ierr, my_rank, p
  integer*8 :: dimen, dimen_small, i, j, k
  integer*4 :: sentinel, status_my(MPI_STATUS_SIZE)
  real*8, allocatable, dimension(:,:) :: matrix_1, matrix_2
  real*8, allocatable, dimension(:,:) :: matrix_local_1, matrix_local_2
  integer*4, allocatable, dimension(:) :: tag
  real*8 :: sum_local
  integer*8 :: L, Lmax
  real*8 :: t1, t2, t1_mpi, t2_mpi

  ! 1. mpi start up
  call MPI_Init( ierr )

  ! 2. get my process rank
  call MPI_Comm_rank( MPI_COMM_WORLD, my_rank, ierr )

  ! 3. find out how many mpi processes are being used
  call MPI_Comm_size( MPI_COMM_WORLD, p, ierr )

  ! 4. setting the dimension of the matrices
  dimen = 1 * 1024 * (10.0 ** 0.0)
  dimen_small = dimen / p

  ! 5. interface
  if ( my_rank == 0 ) then
    write(*,*) "  1. Enter an integer to start the mpi benchmark : "
    read(*,*) sentinel
    write(*,*) "  2. Please wait while the mpi benchmark executes ... "
  end if

  ! 6. allocation of the 1st matrix
  allocate( matrix_1(1:dimen,1:dimen) )

  ! 7. definition of 1st matrix for rank 0
  if ( my_rank == 0 ) then
    do i = 1, dimen
      do j = 1, dimen
        matrix_1(i,j) = dcos(dsin(dcos( real(i+j,kind=8) )))
      end do
    end do
  end if

  ! 8. put a synchronization barrier
  call MPI_Barrier( MPI_COMM_WORLD, ierr )
 
  ! to be deleted.
  if ( my_rank == 0 ) then
    write(*,*) " matrix_1 has been built. "
    write(*,*) " enter an integer to continue: "
    read(*,*) sentinel 
  end if

  ! 9. allocating of the 2nd matrix
  allocate( matrix_2(1:dimen,1:dimen) )

  ! 10. definition of 2nd matrix for rank 0
  if ( my_rank == 0 ) then
    do i = 1, dimen
      do j = 1, dimen
         matrix_2(i,j) = dsin(dcos(dsin( real(i+j,kind=8) ))) 
      end do
    end do
  end if
 
  ! 11. put a synchronization barrier 
  call MPI_Barrier( MPI_COMM_WORLD, ierr )

  ! to be deleted.
  if ( my_rank == 0 ) then
    write(*,*) " matrix_2 has been built. "
    write(*,*) " enter an integer to continue: "
    read(*,*) sentinel
  end if

  ! 12. setting up some tags to do my job
  allocate( tag(1:p) )
  do k = 1, p
    tag(k) = k+10
  end do
 
  ! 13. matrix_local_1: 
  !     help local small matrix for 
  !     splitting the matrix_1 into p parts 
  !     and sent( scatter ) each part to each mpi thread.
  allocate( matrix_local_1(1:dimen,1:dimen_small) )
 
  ! 14. Scatter the matrix_1 to all the mpi threads.
  !     Each part will be stored to matrix_local_1.
  call MPI_Scatter( matrix_1, dimen*dimen_small, MPI_DOUBLE_PRECISION,        &
                    matrix_local_1, dimen*dimen_small, MPI_DOUBLE_PRECISION,  & 
                    0, MPI_COMM_WORLD, ierr )

  ! 15. put a synchronization barrier 
  call MPI_Barrier( MPI_COMM_WORLD, ierr )
 
  ! to be deleted.
  if ( my_rank == 0 ) then
    write(*,*) " matrix_1 has been scattered to dimen_small x dimen matrices . "
    write(*,*) " the total number of the sub-matrices is p. "
    write(*,*) " the name of the local matrix in each mpi thread is "
    write(*,*) " matrix_local_1. "
    write(*,*) " so now i am allowed to deallocate the matrix_1 in rank 0. "
    write(*,*) " enter an integer to continue: "
    read(*,*) sentinel
  end if

  ! 16. So: Up to now the 1st matrix has been divided in pieces
  !     and each piece has been sent to an mpi thread,
  !     stored as matrix_local_1.
  !     Now, I am allowed to deallocate the matrix_1.  
  deallocate( matrix_1 )
  
  ! 17. put a synchronization barrier 
  call MPI_Barrier( MPI_COMM_WORLD, ierr )

  ! to be deleted
  if ( my_rank == 0 ) then
    write(*,*) " matrix_1 has been deleted. "
    write(*,*) " enter an integer to continue: "
    read(*,*) sentinel
  end if

  ! 18. Now I have to allocate ram space for the small part of matrix 2.
  !     This small part of matrix_2, named matrix_local_2
  !     will be used as a tmp storage space in each mpi thread.
  allocate( matrix_local_2( 1:dimen, 1:dimen_small ) )

  ! 19. now i have to sent the 1st part of matrix_2 to each mpi thread
  !     and to store it to matrix_local_2 

  if ( my_rank == 0 ) then 
    do k = 1, p-1
      call MPI_Send( matrix_2(1,1), dimen*dimen_small,  &
                     MPI_DOUBLE_PRECISION, k, tag(k),   &
                     MPI_COMM_WORLD, ierr ) 
    end do
  end if

  write(*,*) " a1 "

  if ( my_rank /= 0 ) then 
    do k = 1, p-1
      call MPI_Recv( matrix_local_2, dimen*dimen_small,      &
                     MPI_DOUBLE_PRECISION, k, tag(k),        &
                     MPI_COMM_WORLD, status_my, ierr )
    end do
  end if

  write(*,*) " a2 "

  ! 20. put a synchronization barrier 
  call MPI_Barrier( MPI_COMM_WORLD, ierr )

  if ( my_rank == 0 ) then
    write(*,*) " 1st part of matrix_2 has been sent to each thread. "
    read(*,*) sentinel
  end if

  ! 16. Now I have to do the matrix_2-matrix_local_1 products
  !     and I have to store them in matrix_res.
  !     So I have to create ram space.

!  do j = 1, dimen_small
!    do i = 1, dimen_small
!      sum_local = 0.0
!      do k = 1, dimen  
!        sum_local = sum_local + matrix_local_1(k,i) * matrix_2(k,j)
!        matrix_1(i+my_rank*dimen_small,j+my_rank*dimen_small) = sum_local
!      end do
!    end do
!  end do

  ! 25. timing - part b
!  if ( my_rank == 0 ) then
!    call cpu_time(t2)
!  end if
!  t2_mpi = MPI_Wtime()

!  call MPI_Barrier( MPI_COMM_WORLD, ierr )

!  do k = 1, p-1 
!    if ( my_rank == k ) then 
!      call MPI_Send( matrix_1(dimen_small*k+1,dimen_small*k+1), dimen_small*dimen_small, &
!                     MPI_DOUBLE_PRECISION, 0, tag(k), MPI_COMM_WORLD, ierr ) 
!    end if
!  end do

!  if ( my_rank == 0 ) then 
!    do k = 1, p-1
!      call MPI_Recv( matrix_2(dimen_small*k+1,dimen_small*k+1), dimen_small*dimen_small, &
!                     MPI_DOUBLE_PRECISION, k, tag(k), MPI_COMM_WORLD, status_my, ierr )
!    end do
!  end if


!  call MPI_Allgather( matrix_1(my_rank*dimen_small+1,my_rank*dimen_small+1),    &
!                      dimen_small*dimen_small, MPI_DOUBLE_PRECISION,            &
!                      matrix_1(my_rank*dimen_small+1,my_rank*dimen_small+1),    &
!                      dimen_small*dimen_small, MPI_DOUBLE_PRECISION,            &
!                      MPI_COMM_WORLD, ierr )

!  call MPI_Barrier( MPI_COMM_WORLD, ierr )

!  write(*,*) my_rank, matrix_1(my_rank*dimen_small+dimen_small,my_rank*dimen_small+dimen_small)

  ! 26. matrix_local_1 and matrix_local_2 
!  deallocate( matrix_local_1 )
!  deallocate( matrix_local_2 )

  ! 27. the results
!  call MPI_Barrier( MPI_COMM_WORLD, ierr )

!  if( my_rank == 0 ) then
!    write(*,*) "  3. The matrix_result(1,3)           is ", matrix_1(1,3)
!    write(*,*) "  4. The matrix_result(2,1)           is ", matrix_1(2,1)
!    write(*,*) "  5. The matrix_result(1,2)           is ", matrix_1(1,2)
!    write(*,*) "  6. The matrix_result(3,1)           is ", matrix_1(3,1)
!    write(*,*) "  7. The matrix_result(dimen,dimen-1) is ", matrix_1(dimen,dimen-1)
!    write(*,*) "  8. The matrix_result(dimen-1,dimen) is ", matrix_1(dimen-1,dimen)
!    write(*,*) "  9. The real time used ( rank 0 )    is ", (t2-t1), " seconds. "
!    write(*,*) " 10. The real time used ( mpi )       is ", (t2_mpi-t1_mpi), " seconds. "
!    write(*,*) " 11. The size of each matrix          is ", (dimen**2.0)*8.0 / (1024.0**3.0), " gigabytes. "  
!    write(*,*) " 12. We have ", Lmax / (t2_mpi-t1_mpi), " matrix-matrix products per second. "
!    write(*,*) " 13. Enter an integer to exit : "
!    read(*,*) sentinel 
!  end if

  ! 28. mpi shut down
  call MPI_Finalize( ierr )

  end program mpi_bench_8
