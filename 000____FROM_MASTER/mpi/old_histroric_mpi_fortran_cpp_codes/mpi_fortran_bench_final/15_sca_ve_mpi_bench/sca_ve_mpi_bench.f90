
program mpi_bench_15

! MPI Benchmark 15.

! 1. I allocate ram space for a vector.
!    This is done for every mpi thread.
! 2. I define the vector only in mpi thread 0.
!    So I occupy ram only concerning mpi thread 0.
! 3. I scatter the above ram/vector
!    to any other mpi existing thread.
! 4. I am using only the mpi functions/subroutines:
!
!    MPI_Init ( ... )
!    MPI_Comm_rank ( ... )
!    MPI_Comm_size ( ... )
!    MPI_Scatter ( ... )
!    MPI_Finalize( ... )

  use mpi
  implicit none

  integer*4 :: ierr, my_rank, p, sentinel
  integer*8 :: dimen, i, j, k, kmax
  real*8, allocatable, dimension(:) :: vector_local, vector
  real*8 :: t1, t2, t1_mpi, t2_mpi

  ! 1. mpi start up
  call MPI_Init( ierr )

  ! 2. get my process rank
  call MPI_Comm_rank( MPI_COMM_WORLD, my_rank, ierr )

  ! 3. find out how many mpi processes are being used
  call MPI_Comm_size( MPI_COMM_WORLD, p, ierr )
  
  ! 4. setting the dimension of the vector
  dimen = 4 * 1024 * (10.0 ** 5.0)

  ! 5. interface
  if ( my_rank == 0 ) then
    write(*,*) "  1. Enter an integer to start the mpi benchmark : "
    read(*,*) sentinel
    write(*,*) "  2. Please wait while the mpi executes ... "
  end if

  ! 6. building the vector(s)
  allocate( vector_local( dimen / p ) )
  allocate( vector( dimen ) )

  if ( my_rank == 0 ) then
    do i = 1, dimen
      vector(i) = dcos(dsin(dcos( real(i,kind=8) )))
    end do
  end if

  ! 7. scatter the vector to the rest of mpi threads
  ! 8. timing
  if ( my_rank == 0 ) then
    call cpu_time(t1)
  end if
  t1_mpi = MPI_Wtime()

  ! 9. looping 
  kmax = 2 * ( 10.0 ** 2.0 ) 

  do k = 1, kmax  
!   write(*,*) " my_rank --> ", my_rank, " --> ", k
    call MPI_Scatter( vector, dimen/p, MPI_DOUBLE_PRECISION,       &
                      vector_local, dimen/p, MPI_DOUBLE_PRECISION, & 
                      0, MPI_COMM_WORLD, ierr )
    if ( my_rank /= 0 ) then
      if ( k /= kmax )  then
        deallocate ( vector_local )
        allocate( vector_local ( dimen / p ) )
      end if
    end if
  end do

  ! 10. timing
  if ( my_rank == 0 ) then
    call cpu_time(t2)
  end if
  t2_mpi = MPI_Wtime()

  ! 11. put a mpi barrier
  call MPI_Barrier( MPI_COMM_WORLD, ierr )

  ! 12. sentineling - just to observe the ram
  if ( my_rank == 0 ) then   
    write(*,*) "  3. Total real time used ( rank 0 )          is ", (t2-t1), " seconds. "
    write(*,*) "  4. Total real time used ( mpi )             is ", (t2_mpi-t1_mpi), " seconds. "
    write(*,*) "  5. The scattered vector size                is ", dimen*8/(1024.0d0**3.0), " gigabytes. "
    write(*,*) "  6. The scatters per second                  is ", kmax * p / (t2_mpi-t1_mpi)
    write(*,*) "  7. At rank 0, the vector( 1 )               is ", vector(1)
    write(*,*) "  8. At rank 0, the vector_local( 1 )         is ", vector_local(1)
    write(*,*) "  9. At rank 0, the vector( dimen/p + 1 )     is ", vector(dimen/p+1)
    write(*,*) " 10. At rank 0, the vector( 2*dimen/p + 1 )   is ", vector(2*dimen/p+1)
    write(*,*) " 11. At rank 0, the vector( dimen )           is ", vector(dimen)
  end if

  ! 13.
  call MPI_Barrier( MPI_COMM_WORLD, ierr )

  if ( my_rank == 1 ) then
    write(*,*) " 12. At rank ", my_rank, ", the vector_local( 1 )         is ", vector_local(1)
  end if

  ! 14.
  call MPI_Barrier( MPI_COMM_WORLD, ierr )

  if ( my_rank == 2 ) then
    write(*,*) " 13. At rank ", my_rank, ", the vector_local( 1 )         is ", vector_local(1)
  end if

  ! 15.
  call MPI_Barrier( MPI_COMM_WORLD, ierr )

  if ( my_rank == p - 1 ) then
    write(*,*) " 14. At rank ", my_rank, ", the vector_local( dimen/p ) is ", vector_local(dimen/p)
  end if

  ! 16.
  call MPI_Barrier( MPI_COMM_WORLD, ierr )

  if ( my_rank == 0 ) then
    write(*,*) " 15. Enter an integer to exit : "
    read(*,*) sentinel 
  end if
  
  ! 17. mpi shut down
  call MPI_Finalize( ierr )

end program mpi_bench_15
