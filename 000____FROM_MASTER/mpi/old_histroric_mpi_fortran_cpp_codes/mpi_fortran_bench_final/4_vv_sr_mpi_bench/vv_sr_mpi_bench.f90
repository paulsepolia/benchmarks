  program mpi_bench_4

! MPI Benchmark 4.

! 1. An MPI vector-vector product.

! 2. The allocation and building of the 2 vectors
!    is done only in master mpi thread ( rank = 0 ).

! 3. I use the blocking MPI_Send and MPI_Recv commands 
!    for broadcasting the elements of each vector
!    to the rest of mpi threads.

! 4. The limitation is that the lenght of the vector
!    must be divisible with p ( number of mpi threads ).

! 5. All the started up mpi threads do the vector-vector product.
!    Each one its own part.

! 6. I use the MPI_Reduce function to collect and sum all
!    the local sums.

! 7. MPI function in use:

!    MPI_Init( ... )
!    MPI_Comm_size( ... )
!    MPI_Comm_rank( ... )
!    MPI_Send( ... ) 
!    MPI_Recv( ... )
!    MPI_Reduce( ... )

! 8. MPI type declarations in use:

!    MPI_Status

  use mpi
  implicit none

  integer*4 :: ierr
  integer*4 :: my_rank
  integer*4 :: p
  integer*8 :: dimen
  integer*8 :: dimen_small
  integer*4 :: sentinel
  real*8, allocatable, dimension(:) :: vector_1
  real*8, allocatable, dimension(:) :: vector_2
  real*8, allocatable, dimension(:) :: vector_1_local
  real*8, allocatable, dimension(:) :: vector_2_local
  integer*4 :: tag_1, tag_2, i
  integer*4 :: status_my( MPI_STATUS_SIZE )
  real*8 :: t1,t2,t1_mpi,t2_mpi
  real*8 :: sum_my, local_sum
  integer*8 :: k, kmax
  
  ! 1. mpi start up
  call MPI_Init( ierr )

  ! 2. get my process rank
  call MPI_Comm_rank( MPI_COMM_WORLD, my_rank, ierr )

  ! 3. find out how many mpi processes are being used
  call MPI_Comm_size( MPI_COMM_WORLD, p, ierr )
  
  ! 4. setting the dimension of the vectors 
  dimen = 1 * 1024 *  (10.0 ** 5.0)
  dimen_small = dimen / p

  ! 5. interface
  if ( my_rank == 0 ) then
    write(*,*) "  1. Enter an integer to start the mpi benchmark : "
    read(*,*) sentinel
    write(*,*) "  2. Please wait while the mpi benchmark executes ... "
  end if

  ! 6. allocating and building the vectors   
  allocate( vector_1(1:dimen) )
  allocate( vector_2(1:dimen) )

  if ( my_rank == 0 ) then
    do i = 1, dimen
      vector_1( i ) = dcos(dsin(dcos( real(i,kind=8) )))
      vector_2( i ) = dsin(dcos(dsin( real(i,kind=8) ))) 
    end do
  end if

  ! 7. local vectors for the rest ranks
  allocate( vector_1_local(1:dimen_small) )
  allocate( vector_2_local(1:dimen_small) )

  ! 8. sending parts of the vectors 
  !    from the master thread to the rest mpi threads

  ! 9. some variables
  tag_1 = 40
  tag_2 = 41

  ! 10. send and receive - vector_1
  if ( my_rank == 0 ) then
    do i = 1, p-1
      call MPI_Send( vector_1(dimen_small*i+1), dimen_small, &
                     MPI_DOUBLE_PRECISION, i, tag_1,         &
                     MPI_COMM_WORLD, ierr ) 
    end do
  else
    call MPI_Recv( vector_1_local, dimen_small,     &
                   MPI_DOUBLE_PRECISION, 0, tag_1,  &
                   MPI_COMM_WORLD, status_my, ierr )  
  end if

  ! 11. send and receive - vector_2
  if ( my_rank == 0 ) then
    do i = 1, p-1
      call MPI_Send( vector_2(dimen_small*i+1),         &
                     dimen_small, MPI_DOUBLE_PRECISION, &
                     i, tag_2, MPI_COMM_WORLD, ierr )
    end do
  else
    call MPI_Recv( vector_2_local, dimen_small,    & 
                   MPI_DOUBLE_PRECISION, 0, tag_2, & 
                   MPI_COMM_WORLD, status_my, ierr )
  end if

  ! 12. do the dot product.

  ! 13. timing
  if ( my_rank == 0 ) then
    call cpu_time(t1) 
  end if

  t1_mpi = MPI_Wtime()

  ! 14. some variables
  sum_my = 0.0
  kmax = 1 * (10.0 ** 3.0)

  ! 15. main dot product - is done kmax times 
  do k = 1, kmax
    local_sum = 0.0
    if ( my_rank == 0 ) then
      do i = 1, dimen_small
        local_sum = local_sum + vector_1(i) * vector_2(i)
      end do
    end if

    if ( my_rank /= 0 ) then
      do i = 1, dimen_small
        local_sum = local_sum + vector_1_local(i) * vector_2_local(i) 
      end do  
    end if
 
    ! 16. mpi_reduce
    sum_my = 0.0
    call MPI_Reduce( local_sum, sum_my, 1, MPI_DOUBLE_PRECISION, &
                     MPI_SUM, 0, MPI_COMM_WORLD, ierr )
  end do

  ! 17. timing
  if ( my_rank == 0 ) then 
    call cpu_time(t2)
  end if

  t2_mpi = MPI_Wtime()

  ! 18. the results
  call MPI_Barrier( MPI_COMM_WORLD, ierr )

  if( my_rank == 0 ) then
    write(*,*) "  3. The sum                             is ", sum_my
    write(*,*) "  4. The size of each of the two vectors is ", dimen * 8.0 / (1024.0d0 ** 3.0), " gigabytes. "
    write(*,*) "  5. The real time used ( rank 0 )       is ", (t2-t1), " seconds. "
    write(*,*) "  6. The real time used ( mpi )          is ", (t2_mpi-t1_mpi), " seconds. "
    write(*,*) "  7. We have ", kmax / (t2_mpi-t1_mpi), " vector-vector products per second."
    write(*,*) "  8. Enter an integer to exit : "
    read(*,*) sentinel
  end if

  ! 19. mpi shut down
  call MPI_Finalize( ierr )

  end program mpi_bench_4
