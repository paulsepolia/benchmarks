  program mpi_bench_1

! MPI Benchmark 1.

! 1. I allocate ram space for a vector.
!    This is done for every mpi thread.
! 2. I define the vector only in mpi thread 0.
!    So I occupy ram only concerning mpi thread 0.
! 3. I broadcast the above ram/vector
!    to any other mpi existing thread.
! 4. I am using only the mpi function:
!
!    MPI_Init ( ... )
!    MPI_Comm_rank ( ... )
!    MPI_Comm_size ( ... )
!    MPI_Bcast ( ... )
!    MPI_Finalize( ... )

  use mpi
  implicit none

  integer*4 :: ierr
  integer*4 :: my_rank
  integer*4 :: p
  integer*8, parameter :: dimen = 2 * 1024 * ( 10.0 ** 5.0 )
  integer*4 :: sentinel
  real*8, allocatable, dimension(:) :: vector 
  real*8 :: t1, t2, t1_mpi, t2_mpi
  integer*8 :: k, kmax,i

  ! 1. mpi start up
  call MPI_Init( ierr )

  ! 2. get my process rank
  call MPI_Comm_rank( MPI_COMM_WORLD, my_rank, ierr )

  ! 3. find out how many mpi processes are being used
  call MPI_Comm_size( MPI_COMM_WORLD, p, ierr )

  ! 4. interface
  if ( my_rank == 0 ) then
    write(*,*) "  1. Enter an integer to start the mpi benchmark : "
    read(*,*) sentinel
    write(*,*) "  2. Please wait while the mpi executes ... "
  end if

  ! 5. building the vector(s)
  allocate( vector(1:dimen) )

  if ( my_rank == 0 ) then 
    do i = 1, dimen    
      vector(i) = dcos(dsin(dcos( real(i,kind=8) )))
    end do
  end if

  ! 6. timing
  if ( my_rank == 0 ) then
    call cpu_time(t1)
  end if
  t1_mpi = MPI_Wtime()

  ! 7. looping and broadcasting
  kmax = 5 * (10.0 ** 1.0)

  do k = 1, kmax
    call MPI_Bcast( vector, dimen, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr )
    if ( my_rank /= 0 .and. k /= kmax ) then
      deallocate( vector )
      allocate ( vector(1:dimen) )
    end if
  end do
 
  ! 8. timing
  if ( my_rank == 0 ) then
    call cpu_time(t2)
  end if
  t2_mpi = MPI_Wtime()

  ! 9. put a mpi barrier
  call MPI_Barrier( MPI_COMM_WORLD, ierr )

  if ( my_rank == 1 ) then
    write(*,*) "  7-1. The vector(1)     in rank 1   is ", vector(1)
    write(*,*) "  8-1. The vector(dimen) in rank 1   is ", vector(dimen)
  end if

  ! 10. put a mpi barrier
  call MPI_Barrier( MPI_COMM_WORLD, ierr )

  if ( my_rank == 2 ) then
    write(*,*) "  7-2. The vector(1)     in rank 2   is ", vector(1)
    write(*,*) "  8-2. The vector(dimen) in rank 2   is ", vector(dimen)
  end if

  ! 11. put a mpi barrier
  call MPI_Barrier( MPI_COMM_WORLD, ierr )

  ! 12. sentineling - just to observe the ram
  if ( my_rank == 0 ) then
    write(*,*) "  3. Total real time used ( rank 0 ) is ", (t2-t1), " seconds. "
    write(*,*) "  4. Total real time used ( mpi )    is ", (t2_mpi-t1_mpi), " seconds. "
    write(*,*) "  5. The broadcasted vector size     is ", dimen*8/(1024.0d0 ** 3.0), " gigabytes. "
    write(*,*) "  6. The broadcasts per second       is ", kmax * p / (t2_mpi-t1_mpi)
    write(*,*) "  7-0. The vector(1)     in rank 0   is ", vector(1)
    write(*,*) "  8-0. The vector(dimen) in rank 0   is ", vector(dimen)
    write(*,*) "  9. Enter an integer to exit : "
    read(*,*) sentinel
  end if

  ! 13. mpi shut down
  call MPI_Finalize( ierr )

  end program mpi_bench_1
