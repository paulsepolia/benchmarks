
  program mpi_bench_10

! MPI Benchmark 10.

! 1. I allocate ram space for a matrix.
!    This is done for every mpi thread.
! 2. I define the matrix only in mpi thread 0.
!    So I occupy ram only concerning mpi thread 0.
! 3. I broadcast the above ram/matrix
!    to any other mpi existing thread.
! 4. I am using only the mpi function:
!
!    MPI_Init ( ... )
!    MPI_Comm_rank ( ... )
!    MPI_Comm_size ( ... )
!    MPI_Bcast ( ... )
!    MPI_Finalize( ... )

  use mpi
  implicit none

  integer*4 :: ierr, my_rank, p, sentinel
  integer*8 :: dimen, i, j, k, kmax
  real*8, allocatable, dimension(:,:) :: matrix
  real*8 :: t1, t2, t1_mpi, t2_mpi

  ! 1. mpi start up
  call MPI_Init( ierr )

  ! 2. get my process rank
  call MPI_Comm_rank( MPI_COMM_WORLD, my_rank, ierr )

  ! 3. find out how many mpi processes are being used
  call MPI_Comm_size( MPI_COMM_WORLD, p, ierr )
  
  ! 4. setting the dimension of the vectors 
  dimen = 2 * 2 * 4 * (10.0 ** 3.0)

  ! 5. interface
  if ( my_rank == 0 ) then
    write(*,*) "  1. Enter an integer to start the mpi benchmark : "
    read(*,*) sentinel
    write(*,*) "  2. Please wait while the mpi executes ... "
  end if

  ! 6. allocating space and building the matrix
  allocate( matrix( 1:dimen, 1:dimen) )

  if ( my_rank == 0 ) then
    do i = 1, dimen
      do j = 1, dimen
        matrix(j,i) = dcos(dsin(dcos( real(i+j,kind=8) )))
      end do
    end do
  end if
  
  ! 7. broadcasting the matrix to the rest mpi threads
  ! 8. timing
  if ( my_rank == 0 ) then
    call cpu_time(t1)
  end if
  t1_mpi = MPI_Wtime()

  ! 9. looping 
  kmax = 5 * (10.0 ** 1.0)

  do k = 1, kmax
    call MPI_Bcast( matrix(1,1), dimen*dimen,      & ! slow broabcasting method
                    MPI_DOUBLE_PRECISION, 0,       &
                    MPI_COMM_WORLD, ierr )
    if ( my_rank /= 0 .and. k /= kmax ) then
      deallocate( matrix )  
      allocate( matrix(1:dimen,1:dimen))     
    end if
  end do

  ! 10. timing
  if ( my_rank == 0 ) then
    call cpu_time(t2)
  end if
  t2_mpi = MPI_Wtime()

  ! 11. sentineling - just to observe the ram
  call MPI_Barrier( MPI_COMM_WORLD, ierr )

  if ( my_rank == 0 ) then
    write(*,*) "  3. rank 0, matrix(1,1)             is ", matrix(1,1)
    write(*,*) "  4. rank 0, matrix(dimen,dimen)     is ", matrix(dimen,dimen)
  end if

  call MPI_Barrier( MPI_COMM_WORLD, ierr )

  if ( my_rank == 1 ) then
    write(*,*) "  5. rank 1, matrix(1,1)             is ", matrix(1,1)
    write(*,*) "  6. rank 1, matrix(dimen,dimen)     is ", matrix(dimen,dimen)
  end if

  call MPI_Barrier( MPI_COMM_WORLD, ierr )

  if ( my_rank == 2 ) then
    write(*,*) "  7. rank 2, matrix(1,1)             is ", matrix(1,1)
    write(*,*) "  8. rank 2, matrix(dimen,dimen)     is ", matrix(dimen,dimen)
  end if

  call MPI_Barrier( MPI_COMM_WORLD, ierr )

  if ( my_rank == 0 ) then  
    write(*,*) "  9. Total real time used ( rank 0 ) is ", (t2-t1), " seconds. "
    write(*,*) " 10. Total real time used ( mpi )    is ", (t2_mpi-t1_mpi), " seconds. "
    write(*,*) " 11. The broadcasted matrix size     is ", (dimen ** 2.0)*8/ (1024.0 ** 3.0), " gigabytes. " 
    write(*,*) " 12. The broadcasts per second       is ", kmax*p /(t2_mpi-t1_mpi)
    write(*,*) " 13. Enter an integer to exit : "
    read(*,*) sentinel
  end if

  ! 12. mpi shut down
  call MPI_Finalize( ierr )

end program mpi_bench_10
