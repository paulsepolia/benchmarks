
program mpi_bench_16

! MPI Benchmark 16.

! 1. I allocate ram space for a vector.
!    This is done for every mpi thread.
! 2. I define the vector only in mpi thread 0.
!    So I occupy ram only concerning mpi thread 0.
! 3. I pack the above vetcors only to thread master.
! 4. I am using only the mpi functions/subroutines :
!
!    MPI_Init ( ... )
!    MPI_Comm_rank ( ... )
!    MPI_Comm_size ( ... )
!    MPI_Pack ( ... )
!    MPI_Finalize( ... )

  use mpi
  implicit none

  integer*4 :: ierr, my_rank, p, sentinel, position_my
  integer*8 :: dimen, i, k, kmax, size_send
  real*8, allocatable, dimension(:) :: vector
  real*8, allocatable, dimension(:) :: buffer
  real*8 :: t1, t2, t1_mpi, t2_mpi

  ! 1. mpi start up
  call MPI_Init( ierr )

  ! 2. get my process rank
  call MPI_Comm_rank( MPI_COMM_WORLD, my_rank, ierr )

  ! 3. find out how many mpi processes are being used
  call MPI_Comm_size( MPI_COMM_WORLD, p, ierr )
  
  ! 4. setting the dimension of the vectors 
  dimen = 1 * (10.0 ** 9.0)

  ! 5. interface
  if ( my_rank == 0 ) then 
    write(*,*) "  1. Enter an integer to start the mpi benchmark : "
    read(*,*) sentinel
    write(*,*) "  2. Please wait while the mpi benchmark executes ..."
  end if

  ! 6. allocating and building the vector/data
  allocate( vector(1:dimen) )

  call MPI_BARRIER( MPI_COMM_WORLD, ierr ) 

  if( my_rank == 0 ) then
    write(*,*) "  a. i am building the vector now ... "
    write(*,*) "  b. please wait ... "
  end if

  call MPI_BARRIER( MPI_COMM_WORLD, ierr )

  do i = 1, dimen
    vector(i) = dcos(dsin(dcos( real(i,kind=8) )))
  end do

  ! 7. allocating space for the buffer
  allocate( buffer(1:dimen) )

  ! 8. timing
  if ( my_rank == 0 ) then
    call cpu_time(t1)
  end if
  t1_mpi = MPI_Wtime()

  ! 9. packing the vector/data to buffer
  position_my = 1  ! start at the beginning of buffer
                   ! 'position' argument is in/out
  kmax = 4 * (10.0 ** 2.0)
  size_send = 1.0 * dimen/10.0 ! must be 10 times at least smaller than dimen

  call MPI_BARRIER( MPI_COMM_WORLD, ierr ) 

  if( my_rank == 0 ) then
    write(*,*) "  c. i am packing now ... "
    write(*,*) "  d. please wait ... "
  end if

  call MPI_BARRIER( MPI_COMM_WORLD, ierr )

  do k = 1, kmax
    call MPI_Pack( vector, size_send, MPI_DOUBLE_PRECISION, &
                   buffer, dimen, position_my,              & 
                   MPI_COMM_WORLD, ierr ) 
    position_my = 1 
  end do

  call MPI_BARRIER( MPI_COMM_WORLD, ierr )

  ! 10. timing
  if ( my_rank == 0 ) then
    call cpu_time(t2)
  end if
  t2_mpi = MPI_Wtime()

  ! 11. the results
  if( my_rank == 0 ) then
    write(*,*) "  3. The real time used ( rank 0 ) is ", (t2-t1), " seconds. "
    write(*,*) "  4. The real time used ( mpi )    is ", (t2_mpi-t1_mpi), " seconds. "
    write(*,*) "  5. The vector size               is ", dimen * 8 / (1024.0 ** 3.0), " gigabytes. "
    write(*,*) "  6. The packs per second          is ", kmax * size_send / (t2_mpi-t1_mpi)
    write(*,*) "  7. Each size package packed      is ", size_send * 8 / (1024.0 ** 3.0), " gigabytes. "
    write(*,*) "  8. Enter an integer to exit : "
    read(*,*) sentinel
  end if

  ! 12. mpi shut down
  call MPI_Finalize( ierr )

end program mpi_bench_16 
