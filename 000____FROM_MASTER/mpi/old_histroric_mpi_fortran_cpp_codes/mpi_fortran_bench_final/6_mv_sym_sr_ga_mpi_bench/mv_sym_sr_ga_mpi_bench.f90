  program mpi_bench_6

! MPI Benchmark 6.

! 1. An MPI matrix-vector product. 

! 2. This is a matrix-vector mpi program
!    only for symmetrix matrices.

! 3. The allocation and building of the 1 vector and 1 matrix
!    is done only in master mpi thread ( rank = 0 ).

! 4. I use the blocking MPI_Send and MPI_Recv commands
!    for broadcasting parts of the matrix and the whole vector
!    to the rest of mpi threads.

! 5. The limitation is that the number of matrix rows
!    must be divisible by p ( number of mpi threads ).

! 6. All the started up mpi threads do the matrix-vector product.
!    Each one its own part.

! 7. I do not use the MPI_Reduce function to collect and sum all
!    the local sums.

! 8. MPI function in use :

!    MPI_Init( ... )
!    MPI_Comm_size( ... )
!    MPI_Comm_rank( ... )
!    MPI_Send( ... )
!    MPI_Recv( ... )
!    MPI_Gather( ... )

! 9. MPI type declarations in use :

!    MPI_Status

  use mpi
  implicit none

  integer*4 :: ierr, my_rank, p, sentinel, status_my(MPI_STATUS_SIZE)
  integer*8 :: dimen, dimen_small, i, j, k, kmax
  real*8, allocatable, dimension(:) :: vector, vector_local, tag
  real*8, allocatable, dimension(:,:) :: matrix, matrix_local
  real*8 :: t1, t2, t1_mpi, t2_mpi
  real*8 :: sum_local

  ! 1. mpi start up
  call MPI_Init( ierr )

  ! 2. get my process rank
  call MPI_Comm_rank( MPI_COMM_WORLD, my_rank, ierr )

  ! 3. find out how many mpi processes are being used
  call MPI_Comm_size( MPI_COMM_WORLD, p, ierr )

  ! 4. setting the dimension of the vectors
  dimen = 3 * 1024 * ( 10.0 ** 1.0 )
  dimen_small = dimen / p

  ! 5. interface
  if ( my_rank == 0 ) then
    write(*,*) "  1. Enter an integer to start the mpi benchmark : "
    read(*,*) sentinel
    write(*,*) "  2. Please wait while the mpi benchmark executes ... "
  end if

  ! 6. allocating and building the vector
  allocate( vector (1:dimen) )
  allocate( vector_local (1:dimen) )

  if ( my_rank == 0 ) then
    do i = 1, dimen 
      vector(i) = dcos(dsin(dcos( real(i,kind=8) )))
      vector_local(i) = 0.0 
    end do
  end if

  ! 7. declaration of matrix for all ranks
  allocate( matrix(1:dimen,1:dimen) )

  ! 8. definition of matrix for rank 0
  if ( my_rank == 0 ) then
    do j = 1, dimen 
      do i = 1, dimen
        matrix(i,j) = dcos(dsin(dcos( real(i+j,kind=8) )))
      end do
    end do
  end if
 
  ! 9. local matrix for the rest of the mpi ranks
  allocate( matrix_local(1:dimen,1:dimen_small) )

  ! 10. broadcasting the vector
  call MPI_Bcast( vector, dimen, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr )

  ! 11. setting up the tags
  allocate( tag(1:p) )
  do k = 1, p
    tag(k) = k + 11
  end do
 
  ! 12. send parts of the matrix to the rest of the mpi threads
  if ( my_rank == 0 ) then
    ! 13. define the small matrix for rank 0
    do j = 1, dimen_small
      do i = 1, dimen
        matrix_local(i,j) = matrix(i,j)
      end do
    end do
    ! 14. define the small matrix for the rest of the mpi threads
    do k = 1, p-1
      call MPI_Send( matrix(1,dimen_small*k+1), dimen*dimen_small,        &
                     MPI_DOUBLE_PRECISION, k, tag(k),                     &
                     MPI_COMM_WORLD, ierr )
    end do
  end if

  ! 15. receive the sended parts
  do k = 1, p-1
    if ( my_rank == k ) then
      call MPI_Recv( matrix_local, dimen*dimen_small,       &
                     MPI_DOUBLE_PRECISION, 0, tag(k),       &
                     MPI_COMM_WORLD, status_my, ierr )
    end if
  end do

  call MPI_Barrier( MPI_COMM_WORLD, ierr )

  ! 16. deleting the matrix
  deallocate( matrix )

  ! 17. timing - part a
  if ( my_rank == 0 ) then 
    call cpu_time(t1)
  end if
  t1_mpi = MPI_Wtime()

  ! 18. the main matrix-vector product - benchmarking
  kmax = 4 * (10.0 ** 2.0) ! benchmark variable

  do k = 1, kmax
    do j = 1, dimen_small
      sum_local = 0.0
      do i = 1, dimen
        sum_local = sum_local + matrix_local(i,j) * vector(i)
        vector_local( j + my_rank*dimen_small ) = sum_local
     end do
    end do
  end do

  ! 19. timing - part b
  if ( my_rank == 0 ) then
    call cpu_time(t2)
  end if
  t2_mpi = MPI_Wtime()

  ! 20. the results
  call MPI_Barrier( MPI_COMM_WORLD, ierr )

  call MPI_Gather( vector_local(my_rank*dimen_small+1), dimen_small, MPI_DOUBLE_PRECISION, &
                   vector(my_rank*dimen_small+1), dimen_small, MPI_DOUBLE_PRECISION,       & 
                   0, MPI_COMM_WORLD, ierr )

  call MPI_Barrier( MPI_COMM_WORLD, ierr )

  if( my_rank == 0 ) then
    write(*,*) "  3. The vector(1)                 is ", vector(1)
    write(*,*) "  4. The vector(dimen)             is ", vector(dimen)
    write(*,*) "  5. The real time used ( rank 0 ) is ", (t2-t1), " seconds. "
    write(*,*) "  6. The real time used ( mpi )    is ", (t2_mpi-t1_mpi)," seconds. "
    write(*,*) "  7. The size of the matrix        is ", (dimen ** 2.0) * 8.0 / (1024.0d0 ** 3.0), " gigabytes. " 
    write(*,*) "  8. We have ", kmax / (t2_mpi-t1_mpi)," matrix-vector products per second."
    write(*,*) "  9. Enter an integer to exit : "
    read(*,*) sentinel
  end if

  ! 21. mpi shut down
  call MPI_Finalize( ierr )

  end program mpi_bench_6
