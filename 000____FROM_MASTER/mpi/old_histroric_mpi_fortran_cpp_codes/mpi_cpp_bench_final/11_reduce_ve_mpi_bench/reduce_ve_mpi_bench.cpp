
// MPI Benchmark 11.

// 1. I allocate ram space for a vector.
//    This is done for every mpi thread.
// 2. I define the vector only in mpi thread 0.
//    So I occupy ram only concerning mpi thread 0.
// 3. I broadcast the above ram/vector
//    to any other mpi existing thread.
// 4. I do many MPI_Reduce to find the total sum of the
//    elements of the vector.
// 4. I am using only the mpi function:
//
//    MPI_Init ( ... )
//    MPI_Comm_rank ( ... )
//    MPI_Comm_size ( ... )
//    MPI_Bcast ( ... )
//    MPI_Finalize( ... )
//    MPI_Reduce( ... )

#include "mpi.h"
#include <iostream>
#include <cmath>
#include <ctime>
#include <iomanip>

using namespace std;

// main function
int main( int argc, char** argv )
{
    // 1. mpi start up
    MPI_Init( & argc, & argv );

    // 2. get my process rank
    int my_rank;
    MPI_Comm_rank( MPI_COMM_WORLD, & my_rank );

    // 3. find out how many mpi processes are being used
    int p;
    MPI_Comm_size( MPI_COMM_WORLD, & p );

    // 4. setting the dimension of the vectors
    const long int dimen = 1 * static_cast<long int>( pow(10.0, 8.0) );
    long int i,j,k; // common looping variables

    // 5. interface
    int sentinel;

    if ( my_rank == 0 ) {
        cout << "  1. Enter an integer to start the mpi benchmark : ";
        cin  >> sentinel;
        cout << "  2. Please wait while the mpi executes ... " << endl;
    }

    // 6. allocating space and building the matrix
    double * vector = new double [ dimen ];

    if ( my_rank == 0 ) {
        for ( i = 0; i < dimen; i++ ) {
            vector[ i ] = cos(sin(cos( static_cast<double>(i+1) )));
        }
    }

    // 7. broadcasting the vector to the rest mpi threads
    MPI_Bcast( vector, dimen, MPI_DOUBLE,
               0, MPI_COMM_WORLD );

    // 8. timing
    time_t t1;
    if ( my_rank == 0 ) {
        t1 = clock();
    }

    double t1_mpi;
    t1_mpi = MPI_Wtime();

    // 9. looping
    double sum = 0.0;
    double sum_tmp = 0.0;
    const long int kmax = 4 * static_cast<long int>( pow(10.0, 0.0) );

    for ( k = 1; k <= kmax; k++ ) {
        sum = 0.0;
        sum_tmp = 0.0;

        for ( i = 0; i < dimen; i++) {
            MPI_Reduce( & vector[i], & sum_tmp, 1,
                        MPI_DOUBLE,
                        MPI_SUM, 0, MPI_COMM_WORLD );
            sum = sum + sum_tmp;
        }
    }

    // 10. final result
    sum = sum / p;

    // 11. timing
    time_t t2;
    if ( my_rank == 0 ) {
        t2 = clock();
    }

    double t2_mpi;
    t2_mpi = MPI_Wtime();

    // 12. sentineling - just to observe the ram
    MPI_Barrier( MPI_COMM_WORLD );

    if ( my_rank == 0 ) {
        cout << setprecision(15) << fixed;
        cout << "  3. Total real time used ( rank 0 ) is " << 1.0 * (t2-t1)/ CLOCKS_PER_SEC << " seconds. " <<  endl;
        cout << "  4. Total real time used ( mpi )    is " << (t2_mpi-t1_mpi) << " seconds. " << endl;
        cout << "  5. The broadcasted vector size     is " << pow(dimen,1.0)*8/(pow(1024.0,3.0))
             << " gigabytes. " << endl;
        cout << scientific;
        cout << "  6. The reductions per second       is " <<  kmax * p / (t2_mpi-t1_mpi) << endl;
        cout << setprecision(15) << fixed;
        cout << "  7. The sum                         is " << sum << endl;
        cout << "  8. Enter an integer to exit : ";
        cin  >> sentinel;
    }

    // 13. mpi shut down
    MPI_Finalize( );

    // 14. exiting.
    return 0;
}
