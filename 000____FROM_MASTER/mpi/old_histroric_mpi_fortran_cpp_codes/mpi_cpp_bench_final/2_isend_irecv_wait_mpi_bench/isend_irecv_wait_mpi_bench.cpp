
// MPI Benchmark 2.

// 1. I allocate ram space for the variable: vector[dimen].

// 2. I allocate ram space for the 2D vector : vector_local[p][dimen].

// 3. I use the blocking mpi routines MPI_Irecv and MPI_Isend
//    to copy and paste the data of vector[dimen] to the vector_local[p][dimen].

// 4. The 1D variable vector[dimen] is defined only in mpi thread 0.

// 5. The 2D variables vector_loacl[p][dimen] are defined only in mip threads 1 -> p-1.

// 6. I do this copy and paste many times.

// 7. This benchmarks resables the MPI Benchmark 1
//    since i do the same but using the MPI_Bcast routine.

// 8. The mpi routines in use are:
//    MPI_Inint( ... )
//    MPI_Comm_rank ( ... )
//    MPI_Comm_size( ... )
//    MPI_Irecv( ... )
//    MPI_Isend( ... )
//    MPI_Wait( ... )

// 9. The mpi type declarations in use are:
//    MPI_Status
//    MPI_Request

#include "mpi.h"
#include <iostream>
#include <iomanip>
#include <cmath>
#include <ctime>

using namespace std;

// main function
int main( int argc, char** argv )
{
    // 1. mpi start up
    MPI_Init( & argc, & argv );

    // 2. get my process rank
    int my_rank;  // my process rank
    MPI_Comm_rank( MPI_COMM_WORLD, & my_rank );

    // 3. find out how many mpi processes are being used
    int p;  // the number of processes
    MPI_Comm_size( MPI_COMM_WORLD, & p );

    // 4. setting the dimension of the vectors
    long int dimen = 2 * static_cast<long int>( pow(10.0, 8.0) );

    // 5. interface
    int sentinel;
    if ( my_rank == 0 ) {
        cout << "  1. Enter an integer to start the mpi benchmark : ";
        cin  >> sentinel;
        cout << "  2. Please wait while the mpi benchmark executes ... " << endl;
    }

    // 6. allocating and building the vectors
    //    global vector - only for master thread ( my_rank = 0)
    double * vector;
    vector = new double [ dimen ];

    if ( my_rank == 0 ) {
        for ( long int i = 0; i < dimen; i++ ) {
            vector[ i ] = cos(sin(cos( static_cast<double>(i+1) )));
        }
    }

    // 7. local 2D-vector
    double ** vector_local = new double * [ dimen ];

    for ( long int i = 0; i < p; i++ ) {
        vector_local[i] = new double [ dimen ];
    }

    // 8. sending parts of the vectors
    //    from the master thread to the rest mpi threads

    // 9. the tags and mpi defined data types
    int tag = 30;
    int i;

    MPI_Status status;  // for internal tests
    MPI_Request request;  // for interanl tests

    // 10. timing
    time_t t1;
    if ( my_rank == 0 ) {
        t1 = clock();
    }

    double t1_mpi;
    t1_mpi = MPI_Wtime();

    // 11. some constants
    long int k;
    const long int kmax = 5 * static_cast<long int>(pow(10.0, 1.0));

    for ( k = 1; k <= kmax; k++ ) {
        if ( my_rank == 0 ) {
            MPI_Isend( vector, dimen, MPI_DOUBLE, 1, tag, MPI_COMM_WORLD, & request );
            MPI_Wait( & request, & status );
            MPI_Irecv( vector, dimen, MPI_DOUBLE, p-1 , tag, MPI_COMM_WORLD, & request );
            MPI_Wait( & request, & status );
        }

        for ( int i = 1; i < p - 1 ; i++ ) {
            if ( my_rank == i ) {
                vector_local[i] = new double [ dimen ];
                MPI_Irecv( vector_local[i], dimen, MPI_DOUBLE, i-1, tag, MPI_COMM_WORLD, & request );
                MPI_Wait( & request, & status );
                MPI_Isend( vector_local[i], dimen, MPI_DOUBLE, i+1, tag, MPI_COMM_WORLD, & request );
                MPI_Wait( & request, & status );

                delete [] vector_local[i];
            }
        }

        if ( my_rank == p - 1 ) {
            vector_local[my_rank] = new double [ dimen ];
            MPI_Irecv( vector_local[my_rank], dimen, MPI_DOUBLE, p-2, tag, MPI_COMM_WORLD, & request );
            MPI_Wait( & request, & status );
            MPI_Isend(  vector_local[my_rank], dimen, MPI_DOUBLE, 0, tag, MPI_COMM_WORLD, & request );
            MPI_Wait( & request, & status );

            delete [] vector_local[my_rank];
        }
    }

    // 12. timing
    time_t t2;
    if ( my_rank == 0 ) {
        t2 = clock();
    }

    double t2_mpi;
    t2_mpi = MPI_Wtime();

    // 13. the results
    MPI_Barrier( MPI_COMM_WORLD );

    if( my_rank == 0 ) {
        cout << setprecision(15) << fixed;
        cout << "  3. The real time used ( rank 0 ) is " << 1.0 * (t2-t1)/CLOCKS_PER_SEC << " seconds. " << endl;
        cout << "  4. The real time used ( mpi )    is " << (t2_mpi-t1_mpi) << " seconds. " << endl;
        cout << scientific;
        cout << "  5. The size of the vector        is "
             << dimen * 8 / ( pow(1024.0,3.0) ) << " gigabytes. " << endl;
        cout << "  6. Enter an integer to exit : ";
        cin  >> sentinel;
    }

    // 14. mpi shut down
    MPI_Finalize( );

    // 15. exiting.
    return 0;
}
