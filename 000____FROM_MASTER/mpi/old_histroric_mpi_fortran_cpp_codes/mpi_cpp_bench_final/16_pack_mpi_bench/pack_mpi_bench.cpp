
// MPI Benchmark 16.

// 1. I allocate ram space for a vector.
//    This is done for every mpi thread.
// 2. I define the vector only in mpi thread 0.
//    So I occupy ram only concerning mpi thread 0.
// 3. I pack the above vetcors only to thread master.
// 4. I am using only the mpi functions:
//
//    MPI_Init ( ... )
//    MPI_Comm_rank ( ... )
//    MPI_Comm_size ( ... )
//    MPI_Pack ( ... )
//    MPI_Finalize( ... )

#include "mpi.h"
#include <iostream>
#include <cmath>
#include <ctime>
#include <iomanip>

using namespace std;

// main function
int main( int argc, char** argv )
{
    // 1. mpi start up
    MPI_Init( & argc, & argv );

    // 2. get my process rank
    int my_rank;  // my process rank
    MPI_Comm_rank( MPI_COMM_WORLD, & my_rank );

    // 3. find out how many mpi processes are being used
    int p;  // the number of processes
    MPI_Comm_size( MPI_COMM_WORLD, & p );

    // 4. setting the dimension of the vectors
    long int dimen = 1 * static_cast<long int>( pow(10.0, 9.0) );

    // 5. interface
    int sentinel;
    if ( my_rank == 0 ) {
        cout << "  1. Enter an integer to start the mpi benchmark : ";
        cin  >> sentinel;
        cout << "  2. Please wait while the mpi benchmark executes ..." << endl;
    }

    // 6. allocating and building the vector/data
    double * vector;
    vector = new double [ dimen ];

    MPI_Barrier( MPI_COMM_WORLD );

    if ( my_rank == 0 ) {
        cout << "  a. i am building the vector now ... " << endl;
        cout << "  b. please wait ... " << endl;
    }

    MPI_Barrier( MPI_COMM_WORLD );

    for ( long int i = 0; i < dimen; i++ ) {
        vector[ i ] = cos(sin(cos( static_cast<double>(i+1) )));
    }

    // 7. allocating space for the buffer
    double * buffer;
    buffer = new double [ dimen ];

    // 8. timing
    time_t t1;
    if ( my_rank == 0 ) {
        t1 = clock();
    }

    double t1_mpi;
    t1_mpi = MPI_Wtime();

    // 9. packing the vector/data to buffer
    int position;
    position = 0; // start at the beginning of buffer
    // 'position' argument is in/out
    long int k;
    const long int kmax = 4 * pow(10.0, 1.0);
    long int size_send = 1.0 * dimen/10.0; // must be 10 times at least smaller than dimen

    MPI_Barrier( MPI_COMM_WORLD );

    if ( my_rank == 0 ) {
        cout << "  c. i am packing now ... " << endl;
        cout << "  d. please wait ... " << endl;
    }

    MPI_Barrier( MPI_COMM_WORLD );

    if ( my_rank == 0 ) {
        for ( k = 1; k <= kmax; k++ ) {
            MPI_Pack( vector, size_send, MPI_DOUBLE,
                      buffer, dimen, & position,
                      MPI_COMM_WORLD );
            position = 0;
        }
    }

    // 10. timing
    time_t t2;
    if ( my_rank == 0 ) {
        t2 = clock();
    }

    double t2_mpi;
    t2_mpi = MPI_Wtime();

    // 11. the results
    if( my_rank == 0 ) {
        cout << setprecision(15) << fixed;
        cout << "  3. The real time used ( rank 0 ) is " << 1.0 * (t2-t1)/CLOCKS_PER_SEC << " seconds. " << endl;
        cout << "  4. The real time used ( mpi )    is " << (t2_mpi-t1_mpi) << " seconds. " << endl;
        cout << scientific;
        cout << "  5. The vector size               is " << dimen * 8 / pow(1024.0,3.0) << " gigabytes. " << endl;
        cout << "  6. The packs per second          is " << kmax * size_send / (t2_mpi-t1_mpi) << endl;
        cout << "  7. Each size package packed      is " << size_send * 8 / pow(1024.0,3.0) << " gigabytes. " << endl;
        cout << "  8. Enter an integer to exit : ";
        cin  >> sentinel;
    }

    // 12. mpi shut down
    MPI_Finalize( );

    // 13. exiting.
    return 0;
}
