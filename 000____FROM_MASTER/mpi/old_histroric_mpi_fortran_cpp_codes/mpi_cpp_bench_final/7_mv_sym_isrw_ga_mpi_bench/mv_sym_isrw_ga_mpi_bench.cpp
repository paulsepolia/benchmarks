
// MPI Benchmark 7.

// 1. An MPI matrix-vector product.

// 2. The allocation and building of the 1 vector and 1 matrix
//    is done only in master mpi thread ( rank = 0 ).

// 3. I use the non blocking MPI_Isend and MPI_Irecv commands
//    for broadcasting parts of the matrix and the whole vector
//    to the rest of mpi threads, and the MPI_Wait for synchronization.

// 4. The limitation is that the number of matrix rows
//    must be divisible by p ( number of mpi threads ).

// 5. All the started up mpi threads do the matrix-vector product.
//    Each one its own part.

// 6. I do not use the MPI_Reduce function to collect and sum all
//    the local sums.

// 7. MPI function in use :

//    MPI_Init( ... )
//    MPI_Comm_size( ... )
//    MPI_Comm_rank( ... )
//    MPI_Isend( ... )
//    MPI_Irecv( ... )
//    MPI_Wait( ... )

// 8. MPI type declarations in use :

//    MPI_Status
//    MPI_Request

#include "mpi.h"
#include <iostream>
#include <cmath>
#include <ctime>
#include <iomanip>

using namespace std;

// main function
int main( int argc, char** argv )
{
    // 1. mpi start up
    MPI_Init( & argc, & argv );

    // 2. get my process rank
    int my_rank;
    MPI_Comm_rank( MPI_COMM_WORLD, & my_rank );

    // 3. find out how many mpi processes are being used
    int p;
    MPI_Comm_size( MPI_COMM_WORLD, & p );

    // 4. setting the dimension of the vectors
    long int dimen = 3 * 1024 * static_cast<long int>( pow(10.0, 1.0) );
    long int dimen_small = dimen / p;
    long int i,j,k; // common loop variables // ONLY here

    // 5. interface
    int sentinel;

    if ( my_rank == 0 ) {
        cout << "  1. Enter an integer to start the mpi benchmark : ";
        cin  >> sentinel;
        cout << "  2. Please wait while the mpi benchmark executes ... " << endl;
    }

    // 6. allocating and building the vector
    double * vector = new double [ dimen ];
    double * vector_local = new double [ dimen ];

    if ( my_rank == 0 ) {
        for ( i = 0; i < dimen; i++ ) {
            vector[ i ] = cos(sin(cos( static_cast<double>(i+1) )));
            vector_local[ i ] = 0.0;
        }
    }

    // 7. declaration of matrix for all ranks
    double ** matrix = new double * [ dimen ];
    for ( i = 0; i < dimen; i++ ) {
        matrix[i] = new double [ dimen ];
    }

    // 8. definition of matrix for rank 0
    if ( my_rank == 0 ) {
        for( i = 0; i < dimen; i++ ) {
            for ( j = 0; j < dimen; j++ ) {
                matrix[i][j] = cos(sin(cos( static_cast<double>(i+j+2) )));
            }
        }
    }

    // 9. local matrix for the rest of the mpi ranks
    double ** matrix_local = new double * [ dimen_small ];
    for ( i = 0; i < dimen_small; i++ ) {
        matrix_local[i] = new double [ dimen ];
    }

    // 10. broadcasting the vector
    MPI_Bcast( vector, dimen, MPI_DOUBLE, 0, MPI_COMM_WORLD );

    // 11. setting up the tags
    double * tag = new double [ p ];
    for ( k = 0; k < p; k++ ) {
        tag[k] = k + 10 ;
    }

    MPI_Status status;
    MPI_Request request;

    // 12. send parts of the matrix to the rest of the mpi threads
    if ( my_rank == 0 ) {
        // 13. define the small matrix for rank 0
        for ( i = 0; i < dimen_small; i++ ) {
            for ( j = 0; j < dimen; j++ ) {
                matrix_local[i][j] = matrix[i][j];
            }
        }
        // 14. define the small matrix for the rest of the mpi threads
        for ( k = 1; k < p; k++ ) {
            for ( i = 0; i < dimen_small; i++ ) {
                MPI_Isend( matrix[i+dimen_small*k], dimen,
                           MPI_DOUBLE, k, tag[k],
                           MPI_COMM_WORLD, & request );
                MPI_Wait( & request, & status );
            }
        }
    }

    // 15. receive the sended parts
    for ( int k = 1; k < p; k++ ) {
        if ( my_rank == k ) {
            for ( i = 0; i < dimen_small; i++ ) {
                MPI_Irecv( matrix_local[i], dimen,
                           MPI_DOUBLE, 0, tag[k],
                           MPI_COMM_WORLD, & request );
                MPI_Wait( & request, & status );
            }
        }
    }

    MPI_Barrier( MPI_COMM_WORLD );

    // 16. deleting the matrix
    if ( my_rank == 0 ) {
        for( i = 0; i < dimen; i++ ) {
            delete [] matrix[i];
        }
        delete [] matrix;
    }

    // 17. timing - part a
    time_t t1;
    if ( my_rank == 0 ) {
        t1 = clock();
    }

    double t1_mpi;
    t1_mpi = MPI_Wtime();

    // 18. the main matrix-vector product - benchmarking
    double sum_local;
    const long double kmax = 1 * pow(10.0, 2.0); // benchmark variable

    for ( k = 1; k <= kmax; k++ ) {
        for ( j = 0; j < dimen_small; j++ ) {
            sum_local = 0;
            for( i = 0; i < dimen; i++  ) {
                sum_local = sum_local + matrix_local[j][i] * vector[i];
                vector_local[ j +  my_rank * dimen_small ] = sum_local;
            }
        }
    }

    // 19. timing - part b
    time_t t2;
    if ( my_rank == 0 ) {
        t2 = clock();
    }

    double t2_mpi;
    t2_mpi = MPI_Wtime();

    // 20. the results
    MPI_Barrier( MPI_COMM_WORLD );

    MPI_Gather( vector_local+my_rank*dimen_small, dimen_small, MPI_DOUBLE,
                vector+my_rank*dimen_small, dimen_small, MPI_DOUBLE,
                0, MPI_COMM_WORLD );

    MPI_Barrier( MPI_COMM_WORLD );

    if( my_rank == 0 ) {
        cout << setprecision(15) << fixed;
        cout << "  3. The vector[0]                 is " << vector[0] << endl;
        cout << "  4. The vector[dimen-1]           is " << vector[dimen-1] << endl;
        cout << "  5. The real time used ( rank 0 ) is " << 1.0*(t2-t1)/CLOCKS_PER_SEC << " seconds. " << endl;
        cout << "  6. The real time used ( mpi )    is " << (t2_mpi-t1_mpi) << " seconds. "<< endl;
        cout << scientific;
        cout << "  7. The size of the matrix        is " << pow( dimen, 2.0) * 8.0 / pow(1024.0,3.0)
             << " gigabytes. " << endl;
        cout << "  8. We have " << kmax / (t2_mpi-t1_mpi) << " matrix-vector products per second." <<  endl;
        cout << "  9. Enter an integer to exit : ";
        cin  >> sentinel;
    }

    // 21. mpi shut down
    MPI_Finalize( );

    // 22. exiting.
    return 0;
}
