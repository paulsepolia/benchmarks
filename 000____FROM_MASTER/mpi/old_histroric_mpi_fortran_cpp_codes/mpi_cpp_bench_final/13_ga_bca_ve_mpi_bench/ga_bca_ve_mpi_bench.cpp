
// MPI Benchmark 13.

// 1. I allocate ram space for vectors.
//    This is done for every mpi thread.
// 2. I define the vectors in any mpi thread.
// 3. I gather the vectors in master thread.
// 4. I broadcasting all the data of master
//    thread to the rest mpi threads.
// 5. I am using only the mpi function:
//
//    MPI_Init ( ... )
//    MPI_Comm_rank ( ... )
//    MPI_Comm_size ( ... )
//    MPI_Bcast ( ... )
//    MPI_Gather ( ... )
//    MPI_Finalize( ... )

#include "mpi.h"
#include <iostream>
#include <cmath>
#include <ctime>
#include <iomanip>

using namespace std;

// main function
int main( int argc, char** argv )
{
    // 1. mpi start up
    MPI_Init( & argc, & argv );

    // 2. get my process rank
    int my_rank;
    MPI_Comm_rank( MPI_COMM_WORLD, & my_rank );

    // 3. find out how many mpi processes are being used
    int p;
    MPI_Comm_size( MPI_COMM_WORLD, & p );

    // 4. setting the dimension of the vectors
    const long int dimen = 3 * 1024 * static_cast<long int>( pow(10.0, 4.0) );
    long int i,j,k; // looping variables

    // 5. interface
    int sentinel;

    if ( my_rank == 0 ) {
        cout << "  1. Enter an integer to start the mpi benchmark : ";
        cin  >> sentinel;
        cout << "  2. Please wait while the mpi executes ... " << endl;
    }

    // 6. building the vectors
    double * vector_ga = new double [ dimen * p ];
    double * vector = new double [ dimen ];

    for ( long int i = 0; i < dimen; i++ ) {
        vector[ i ] = cos(sin(cos( static_cast<double>(i+my_rank+1) )));
    }

    // 7. timing
    time_t t1;
    if ( my_rank == 0 ) {
        t1 = clock();
    }

    double t1_mpi;
    t1_mpi = MPI_Wtime();

    // 8. gather the vectors in master thread
    const long int kmax = 100;
    for ( k = 1; k <= kmax; k++ ) {
        MPI_Gather( vector, dimen, MPI_DOUBLE,
                    vector_ga, dimen, MPI_DOUBLE,
                    0, MPI_COMM_WORLD );
        if ( k < kmax ) {
            delete [] vector_ga;
            double * vector_ga = new double [ dimen * p ];
        }
    }

    // 9. timing
    time_t t2;
    if ( my_rank == 0 ) {
        t2 = clock();
    }

    double t2_mpi;
    t2_mpi = MPI_Wtime();

    // 10. broadcasting the vector_ga to the rest mpi threads
    MPI_Bcast( vector_ga, dimen * p, MPI_DOUBLE, 0, MPI_COMM_WORLD );

    // 11. put a mpi barrier
    MPI_Barrier( MPI_COMM_WORLD );

    // 12. sentineling - just to observe the ram
    if ( my_rank == 0 ) {
        cout << setprecision(15) << fixed;
        cout << "  3. Total real time used ( rank 0 ) is " << 1.0 * (t2-t1)/ CLOCKS_PER_SEC << " seconds. " <<  endl;
        cout << "  4. Total real time used ( mpi )    is " << (t2_mpi-t1_mpi) << " seconds. " << endl;
        cout << "  5. The gathered vector size        is " << dimen*8/(pow(1024.0,3.0)) << " gigabytes. " << endl;
        cout << scientific;
        cout << "  6. The gathers per second          is " << kmax * p / (t2_mpi-t1_mpi) << endl;
        cout << "  7. Enter an integer to exit : ";
        cin  >> sentinel;
    }

    // 13. mpi shut down
    MPI_Finalize( );

    // 14. exiting.

    return 0;
}
