#!/bin/bash

# 1. compiling

  mpiifort -O3                          \
           -warn all                    \
           -e08                         \
           -assume buffered_io          \
           m_1_type_definitions.f90     \
           driver_read_write_mpi_v5.f90 \
           -o x_intel

# 2. cleaning 

  rm *.mod
