#!/bin/bash

  # 1. compile

    
  mpicxx.openmpi                \
  -O3                           \
  -Wall                         \
  -std=gnu++17                  \
  driver_read_write_mpi_v6.cpp  \
  -o x_gnu_openmpi
