#!/bin/bash

  # 1. compile

  mpiicpc  -O3                          \
           -Wall                        \
           -std=c++11                   \
           driver_read_write_mpi_v6.cpp \
           -o x_intel
