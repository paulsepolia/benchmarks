#!/bin/bash

  # 1. compile

  mpiicpc  -O3                          \
           -Wall                        \
           -std=c++0x                   \
           -static_mpi                  \
           driver_read_write_mpi_v4.cpp \
           -o x_intel
