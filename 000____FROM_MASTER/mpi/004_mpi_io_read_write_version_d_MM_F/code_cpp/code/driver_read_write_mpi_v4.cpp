//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/08/21              //
//===============================//

#include "mpi.h"
#include <iostream>
#include <cmath>
#include <sstream>
#include <cstring>
#include <array>

using namespace std;

// The main function

int main()
{
    //  1. local interface parameters

    string         string_file_base         = "test_file_";
    const long int DIMEN                    = 1 * static_cast<long>(10000) * 10000;
    const char     DATA_REPRESENTATION[128] = "native"; // "internal", "external32"
    const int      I_DO_MAX                 = 2;
    const int      NUM_FILES                = 2;
    const int      IO_MODE                  = MPI::MODE_CREATE |
            MPI::MODE_RDWR   |
            MPI::MODE_DELETE_ON_CLOSE;

    //  2. local variables

    stringstream            ss;
    string                  s_tmp;
    string                  string_file_name;
    MPI::File*              the_file;
    MPI::Offset*            disp;
    MPI::Status             status;
    array<char*, NUM_FILES> files_array;
    int                     my_rank;
    int                     p;
    int                     k;
    double*                 test_array = new double [DIMEN];
    double                  t1_mpi;
    double                  t2_mpi;
    double                  t_mpi_write;
    double                  t_mpi_read;
    double                  total_giga_bytes;
    double                  total_mega_bytes;

    //  3. initializations to avoid
    //     the GNU compiler warnings

    t1_mpi      = static_cast<double>(0.0);
    t2_mpi      = static_cast<double>(0.0);
    t_mpi_write = static_cast<double>(0.0);
    t_mpi_read  = static_cast<double>(0.0);

    //  4. MPI start up

    MPI::Init();

    //  5. get my process rank

    my_rank = MPI::COMM_WORLD.Get_rank();

    //  6. find out how many mpi processes are being used

    p        = MPI::COMM_WORLD.Get_size();
    the_file = new MPI::File[p];
    disp     = new MPI::Offset[p];

    //  7. sanity check

    //  7.a.

    if((NUM_FILES > p) && (my_rank == 0)) {
        cout << " NUM_FILES > p ==> " << NUM_FILES << " > " << p << endl;
        cout << " Choose an integer value less than or equal to p = " << p << endl;
        return(-1);
    }

    MPI::COMM_WORLD.Barrier();

    //  7.b.

    if(((p%NUM_FILES) != 0) && (my_rank == 0)) {
        cout << "p and NUM_FILES must give back module 0" << endl;
        return(-1);
    }

    //  8. initialize the array

    for(long i = 0; i < DIMEN; i++) {
        test_array[i] = sin(static_cast<double>(i+my_rank*p));
    }

    MPI::COMM_WORLD.Barrier();

    //  9. main benchmark starts

    for(int i = 1; i <= I_DO_MAX; i++) {
        // 10. all the processes manualy open the files
        //     to have them in their view

        for (int j = 0; j < NUM_FILES; j++) {
            // 11. create the file name as string

            ss << j;
            ss >> s_tmp;
            string_file_name = string_file_base + s_tmp;

            // 12. reset string stream buffer - must step

            ss.str(string());
            ss.clear();

            // 13. convert the string to character

            files_array[j] = new char[string_file_name.size()+1];

            files_array[j][string_file_name.size()] = 0;

            memcpy(files_array[j], string_file_name.c_str(), string_file_name.size());

            the_file[j] = MPI::File::Open(MPI::COMM_WORLD,
                                          files_array[j],
                                          IO_MODE,
                                          MPI::INFO_NULL);

            // 14. set the MPI file view

            disp[j] = (my_rank+1)%(p/NUM_FILES) * DIMEN * 8; // displacement in bytes

            the_file[j].Set_view(disp[j],
                                 MPI::DOUBLE,
                                 MPI::DOUBLE,
                                 DATA_REPRESENTATION,
                                 MPI::INFO_NULL);
        }

        // 15. timing write starts

        if ((i == 1) && (my_rank == 0)) {
            t1_mpi = MPI::Wtime();
        }

        // 16. write to the file in parallel

        k = my_rank/(p/NUM_FILES)+1; // choose which MPI process to write in which file

        if(((p/NUM_FILES)*(k-1) <= my_rank) && (my_rank < (p/NUM_FILES)*(k))) {
            the_file[k-1].Write(test_array,
                                DIMEN,
                                MPI::DOUBLE,
                                status);

            MPI_File_sync(the_file[k-1]);
        }

        MPI::COMM_WORLD.Barrier();

        // 17. timing write ends

        if ((i == I_DO_MAX) && (my_rank == 0)) {
            t2_mpi = MPI::Wtime();
            t_mpi_write = t2_mpi - t1_mpi;
        }

        MPI::COMM_WORLD.Barrier();

        // 18. timing read starts

        if ((i == 1) && (my_rank == 0)) {
            t1_mpi = MPI::Wtime();
        }

        // 19. read in parallel from the file

        k = my_rank/(p/NUM_FILES) + 1; // choose which MPI process to read from which file

        if(((p/NUM_FILES)*(k-1) <= my_rank) && (my_rank < (p/NUM_FILES)*(k))) {
            the_file[k-1].Read(test_array,
                               DIMEN,
                               MPI::DOUBLE,
                               status);

            MPI_File_sync(the_file[k-1]);
        }

        MPI::COMM_WORLD.Barrier();

        // 20. timing read ends

        if ((i == I_DO_MAX) && (my_rank == 0)) {
            t2_mpi = MPI::Wtime();
            t_mpi_read = t2_mpi - t1_mpi;
        }

        // 21. close manualy the files

        for(int k = 0; k < NUM_FILES; k++) {
            MPI_File_sync(the_file[k]);
            the_file[k].Close();
        }

        MPI::COMM_WORLD.Barrier();

    }

    // 22. report

    if (my_rank == 0) {
        total_giga_bytes = I_DO_MAX*p*static_cast<double>(DIMEN)*8.0/pow(static_cast<double>(1024.0), 3.0);
        total_mega_bytes = total_giga_bytes * static_cast<double>(1024.0);

        cout << endl;
        cout << " ================================================================ " << endl;
        cout << "  Author      : Pavlos G. Galiatsatos                             " << endl;
        cout << "  Description : MPI I/O via 'MPI_File_read' and 'MPI_File_write'  " << endl;
        cout << "  Language    : C++                                               " << endl;
        cout << "  Date        : 2013/08/16                                        " << endl;
        cout << "  Code name   : BENIOE++                                          " << endl;
        cout << " ================================================================ " << endl;
        cout << endl;

        cout << "  1. Total real time used to write is    : " << t_mpi_write << " seconds" << endl;
        cout << "  2. Total real time used to read  is    : " << t_mpi_read  << " seconds" << endl;
        cout << "  3. Total GBytes written are            : " << total_giga_bytes << endl;
        cout << "  4. Total GBytes read are               : " << total_giga_bytes << endl;
        cout << "  5. Speed write (MBytes/sec) is         : " << total_mega_bytes/t_mpi_write << endl;
        cout << "  6. Speed read  (MBytes/sec) is         : " << total_mega_bytes/t_mpi_read << endl;
        cout << "  7. Speed write (MBytes/sec/process) is : " << total_mega_bytes/t_mpi_write/p << endl;
        cout << "  8. Speed read  (MBytes/sec/process) is : " << total_mega_bytes/t_mpi_read/p << endl;
        cout << "  9. Total MPI processes used are        : " << p << endl;
        cout << endl;
    }

    // 23. free up the RAM and finalize MPI

    delete [] test_array;
    delete [] the_file;
    delete [] disp;

    MPI::Finalize();

    return 0;
}

//======//
// FINI //
//======//
