#!/bin/bash

  # 1. compile

  /opt/openmpi/2.0.1/bin/mpicxx \
  -O3                           \
  -Wall                         \
  -std=gnu++17                  \
  driver_read_write_mpi_v4.cpp  \
  -o x_gnu_openmpi
