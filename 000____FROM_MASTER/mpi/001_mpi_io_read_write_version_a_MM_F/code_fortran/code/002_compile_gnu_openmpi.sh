#!/bin/bash

# 1. compiling

  /opt/openmpi/2.0.1/bin/mpif90 \
  -O3                           \
  -Wall                         \
  -std=f2008                    \
  m_1_type_definitions.f90      \
  driver_read_write_mpi_v1.f90  \
  -o x_gnu_openmpi

# 2. cleaning 

  rm *.mod
