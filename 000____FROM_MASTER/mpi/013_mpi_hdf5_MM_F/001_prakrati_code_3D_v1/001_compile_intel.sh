#!/bin/bash

          mpic++  -O3                         \
          -Wall                               \
          -std=c++11                          \
          -I$TACC_HDF5_INC                    \
          cartesian.cpp                       \
          driver_program.cpp                  \
          -Wl,-rpath,$TACC_HDF5_LIB -L$TACC_HDF5_LIB -lhdf5 -lz \
          -lz                                 \
          -o x_intel

