
// Works ok with threads.detach() for writting
// Works ok with threads.join() for cpu and MPI action

// I added file read-synchronization
// CPU time consuming is always done in serial
// but the related to it write statement is detached

// Array space is local to each thread
// I added heavy MPI communication while cpu works 

// Works excellent on T420s
// Works excellent on stampede.tacc.utexas.edu
