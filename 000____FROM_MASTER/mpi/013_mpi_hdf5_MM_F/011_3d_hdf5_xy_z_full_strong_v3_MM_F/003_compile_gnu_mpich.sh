#!/bin/bash

  mpicxx.mpich                    \
  -O3                             \
  -std=gnu++17                    \
  -Wall                           \
  driver_program.cpp              \
  /opt/hdf5/parallel/mpich/1.8.17/lib/libhdf5.a \
  -lz  \
  -ldl \
  -o x_gnu_mpich

