#!/bin/bash

  /opt/openmpi/2.0.1/bin/mpic++   \
  -O3                             \
  -std=gnu++17                    \
  -Wall                           \
  driver_program.cpp              \
  /opt/hdf5/parallel/openmpi/1.8.17/lib/libhdf5.a \
  -lz  \
  -ldl \
  -o x_gnu_openmpi

