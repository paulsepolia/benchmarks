!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/09/19              !
!===============================!

  program driver_program
 
  use m_1_type_definitions
  use mpi

  implicit none

  !=========================!
  !  1a. Start of interface !
  !=========================!

  integer(kind=si), parameter :: matrix_dimen_sparse = 300000_si
  real(kind=dr),    parameter :: sparsity            = 99.40_dr
  integer(kind=si), parameter :: trials              = 100_si

  !=======================!
  !  1b. End of interface !
  !=======================!

  !  2. declaration of the rest variables

  integer(kind=si) :: ierr
  integer(kind=si) :: my_rank
  integer(kind=si) :: p
  integer(kind=si), allocatable, dimension(:) :: row_index_array
  integer(kind=si), allocatable, dimension(:) :: column_index_array
  real(kind=dr), allocatable, dimension(:)    :: ham_elem_array
  integer(kind=di) :: i1
  integer(kind=di) :: i2
  integer(kind=di) :: i3
  integer(kind=si) :: i4
  integer(kind=si) :: irow
  integer(kind=si) :: icolumn
  real(kind=dr), allocatable, dimension(:) :: x
  real(kind=dr), allocatable, dimension(:) :: y
  real(kind=dr), allocatable, dimension(:) :: y_final
  real(kind=dr) :: t1_mpi
  real(kind=dr) :: t2_mpi
  real(kind=dr) :: t1
  real(kind=dr) :: t2
  integer(kind=di) :: non_zero_ham_elem_help
  integer(kind=di) :: non_zero_ham_elem
  integer(kind=di) :: non_zero_local
  integer(kind=si) :: matrix_dimen_dense

  !  3. manipulations

  non_zero_ham_elem_help = &
    int((real(matrix_dimen_sparse,kind=dr)**2)*(100.0_dr-sparsity)/100.0_dr, kind=di)

  matrix_dimen_dense = int(dsqrt(real(non_zero_ham_elem_help, kind=dr)),kind=si)

  !  4. initializations

  t1_mpi = 0.0_dr
  t2_mpi = 0.0_dr
  t1     = 0.0_dr
  t1     = 0.0_dr

  !  5. mpi start up

  call MPI_Init(ierr)

  !  6. get my process rank

  call MPI_Comm_rank(MPI_COMM_WORLD, my_rank, ierr)

  !  7. find out how many mpi processes are being used

  call MPI_Comm_size(MPI_COMM_WORLD, p, ierr)

  !  8. allocating space for the local arrays
  !     to put the sparse matrix data in each mpi thread

  if (mod(matrix_dimen_dense, p) /= 0) then
    matrix_dimen_dense = matrix_dimen_dense + (p-mod(matrix_dimen_dense,p))
  end if

  non_zero_ham_elem = &
    int(real(matrix_dimen_dense,kind=dr) * real(matrix_dimen_dense,kind=dr), kind=di)

  non_zero_local = non_zero_ham_elem/p

  !  9. allocations 

  allocate(row_index_array(1:non_zero_local))
  allocate(column_index_array(1:non_zero_local))
  allocate(ham_elem_array(1:non_zero_local))

  call MPI_Barrier(MPI_COMM_WORLD, ierr)

  ! 10. building the local matrices

  do i1 = 1, matrix_dimen_dense / p
    do i2 = 1, matrix_dimen_dense

      ham_elem_array(i2 + (i1-1) * matrix_dimen_dense) =  & 
      dcos(real((i1 + my_rank * matrix_dimen_dense/p) * i2, kind=dr))

    end do
  end do

  ! 11.

  do i1 = 1, matrix_dimen_dense / p
    do i2 = 1, matrix_dimen_dense

      row_index_array(i2 + (i1-1) * matrix_dimen_dense) = &
        int(i1,kind=si) + my_rank * (matrix_dimen_dense/p)
 
      column_index_array(i2 + (i1-1) * matrix_dimen_dense) = int(i2, kind=si)     

    end do 
  end do

  call MPI_Barrier(MPI_COMM_WORLD, ierr)

  ! 12. building the x vector 

  allocate(x(1:matrix_dimen_sparse))
  
  if (my_rank == 0) then

    do i4 = 1, matrix_dimen_sparse
  
      x(i4) = dcos(dcos(real(i4,kind=dr)))
 
    end do
 
  end if

  call MPI_Barrier(MPI_COMM_WORLD, ierr)

  ! 13. broadcasting the x vector to the rest of the mpi theads

  call MPI_Bcast(x, matrix_dimen_sparse, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)

  call MPI_Barrier(MPI_COMM_WORLD, ierr)

  ! 14. the mpi sparse matrix - vector product 

  allocate(y(1:matrix_dimen_sparse))

  ! 15.

  if (my_rank == 0) then
    allocate(y_final(1:matrix_dimen_sparse))
  end if

  call MPI_Barrier(MPI_COMM_WORLD, ierr)
  
  ! 16.

  if (my_rank == 0) then
    t1_mpi = MPI_Wtime()
    call cpu_time(t1)
  end if

  do i1 = 1, trials ! testing do loop

    y = 0.0_dr
 
    do i3 = 1, non_zero_local
  
      irow = row_index_array(i3)
  
      icolumn = column_index_array(i3)
  
      y(irow) = y(irow) + ham_elem_array(i3) * x(icolumn)
  
!      if (irow.ne.icolumn) y(icolumn) = y(icolumn) + ham_elem_array(i3) * x(irow)
  
    end do 

    call MPI_Barrier(MPI_COMM_WORLD, ierr)

    ! 17.

    call MPI_Reduce(y,                    &
                    y_final,              &
                    matrix_dimen_sparse,  &
                    MPI_DOUBLE_PRECISION, &  
                    MPI_SUM,              &
                    0,                    &
                    MPI_COMM_WORLD,       &
                    ierr)  

    call MPI_Barrier(MPI_COMM_WORLD, ierr)

  end do ! end of testing do loop

  if (my_rank == 0) then
    t2_mpi = MPI_Wtime()
    call cpu_time(t2)
  end if

  ! 18. outputting the results

  if (my_rank == 0) then

    write(*,*) " "
    write(*,*) "  1. matrix dimen as dense        = ", matrix_dimen_dense
    write(*,*) "  2. matrix dimen as sparse       = ", matrix_dimen_sparse
    write(*,*) "  3. sparsity                     = ", sparsity
    write(*,*) "  5. my_rank                      = ", my_rank
    write(*,*) "  6. number of mpi threads        = ", p
    write(*,*) "  7. non_zero_ham_elem            = ", non_zero_ham_elem
    write(*,*) "  8. total real time (mpi)        = ", (t2_mpi-t1_mpi)
    write(*,*) "  9. total real time (fortran)    = ", (t2-t1)
    write(*,*) " 10. smv-mpi per second (mpi)     = ", trials / (t2_mpi-t1_mpi)
    write(*,*) " 11. smv-mpi per second (fortran) = ", trials / (t2-t1)

    do i4 = 1, 1

    write(*,*) " 12. from rank                    = ", my_rank
    write(*,*) " 13. row_index_array              = ", i1, row_index_array(i1)
    write(*,*) " 14. column_index_array           = ", i1, column_index_array(i1)
    write(*,*) " 15. ham_elem_array               = ", i1, ham_elem_array(i1)
    write(*,*) " 16. x(i)                         = ", i1, x(i1)
    write(*,*) " 17. y(i)                         = ", i1, y(i1)
    write(*,*) " 18. y_final(i)                   = ", i1, y_final(i1)

    end do

    write(*,*) ""  

  end if

  call MPI_Barrier(MPI_COMM_WORLD, ierr)

  if (my_rank == 0) then

    do i4 = 1, 10
 
      write(*,*) i4, x(i4), y(i4)

    end do

  end if

  call MPI_Barrier(MPI_COMM_WORLD, ierr)

  if (my_rank == 1) then 

    do i4 = my_rank*(matrix_dimen_dense/p)+1, my_rank*(matrix_dimen_dense/p)+11
 
      write(*,*) i4, x(i4), y(i4)

    end do

  end if

  ! 19. mpi shut down

  call MPI_Finalize(ierr)

  end program driver_program

!======!
! FINI !
!======!
