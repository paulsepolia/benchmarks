#!/bin/bash

# 1. compile

  ifort -O3                      \
        -e08                     \
        -warn all                \
        m_1_type_definitions.f90 \
        m_2_heap_sort_3ar.f90    \
        driver_program.f90       \
        -o x_intel

# 2. clean

  rm *.mod
