//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2014/10/24              //
//===============================//
//===================//
// Title: Vector STL //
//===================//

#include <iostream>
using std::cout;
using std::cin;
using std::endl;

#include <vector>
using std::vector;

#include <ctime>
using std::time;

//======================//
// function declaration //
//======================//

void createVector(int);

//===================//
// the main function //
//===================//

int main()
{
    const int dimen = 5000000;
    const int trials = 500;

    cout << "==============================================================" << endl;
    cout << " " << endl;
    cout << "  1 --> Benchmark" << endl;
    cout << "  2 --> Name: Vector Class STL" << endl;
    cout << "  3 --> Type: Sequential" << endl;
    cout << "  4 --> Compiler Intel C++ 11.1 (Windows/Linux)" << endl;
    cout << "  5 --> Programming language : C++" << endl;
    cout << "  6 --> Exact Real Time (1 x X5160)/Linux       :  132.6 seconds" << endl;
    cout << "  7 --> Exact Real Time (1 x 9140M)/Linux       :   96.3 seconds" << endl;
    cout << "  8 --> Exact Real Time (1 x Opteron 250)/Linux :  221.2 seconds" << endl;
    cout << "  9 --> Please wait... " << endl;

    clock_t t1;
    t1 = clock();

    for (int i = 0; i != trials; ++i) {
        createVector(dimen);
    }

    clock_t t2;
    t2 = clock();

    cout << " 10 --> Exact Real Time = " << static_cast<double>(t2-t1)/CLOCKS_PER_SEC << endl;
    cout << " " << endl;

    return 0;
}

// main function ends

//=====================//
// function definition //
//=====================//

void createVector(int dimen)
{
    vector<int> integers(dimen);  // dimen-element vector <int>

    for(int i = 0; i != dimen; ++i) {
        integers[i] = i;
    }

}

//======//
// FINI //
//======//

