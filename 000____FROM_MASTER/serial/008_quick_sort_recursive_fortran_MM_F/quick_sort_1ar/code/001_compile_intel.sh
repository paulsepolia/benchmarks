#!/bin/bash

# 1. compile

  ifort -O3                        \
        -e08                       \
        -warn all                  \
        -heap-arrays               \
        m_1_type_definitions.f90   \
        m_2_partition_function.f90 \
        m_3_quick_sort.f90         \
        driver_program.f90         \
        -o x_intel

# 2. clean

  rm *.mod
