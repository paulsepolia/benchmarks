!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/09/25              !
!===============================!

  module m_2_partition_function

  use m_1_type_definitions

  implicit none

  contains

  !=======================================!
  ! A. Definition of "partition function" !
  !=======================================!

  function partition_function(array, start_pos, end_pos) result(pivot_pos)

  implicit none

  !  1. interface variables

  integer(kind=di), intent(in) :: start_pos
  integer(kind=di), intent(in) :: end_pos
  real(kind=dr), allocatable, dimension(:), intent(inout) :: array

  !  2. local variables

  integer(kind=di) :: pivot_pos
  real(kind=dr) :: pivot_value
  integer(kind=di) :: pos

  !  3. the pivot element is taken to be the element
  !     at the start of the subrange to be partitioned

  pivot_value = array(start_pos)
  pivot_pos = start_pos

  !  4. rearrange the rest of the array elements to
  !     partition the subrange from statr_pos to end_pos

  do pos = start_pos + 1, end_pos
      
    if (array(pos) < pivot_value) then

      call swap_sub(array(pivot_pos + 1), array(pos))
      call swap_sub(array(pivot_pos), array(pivot_pos + 1))
      pivot_pos = pivot_pos + 1

    end if

  end do

  end function partition_function

  !=================================!
  ! B. Definition of the "swap_sub" !
  !=================================!

  subroutine swap_sub(value_alpha, value_beta)

  implicit none

  real(kind=dr), intent(inout) :: value_alpha
  real(kind=dr), intent(inout) :: value_beta
  real(kind=dr) :: value_tmp

  value_tmp   = value_alpha
  value_alpha = value_beta
  value_beta  = value_tmp

  end subroutine swap_sub
 
  end module m_2_partition_function

!======!
! FINI !
!======!
