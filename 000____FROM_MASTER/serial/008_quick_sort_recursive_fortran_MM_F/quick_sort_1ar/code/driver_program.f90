!===================================!
! Author: Pavlos G. Galiatsatos     !
! Date: 2013/09/25                  !
! Program: The Quick Sort algorithm !
!===================================!

  program driver_program

  use m_1_type_definitions
  use m_2_partition_function
  use m_3_quick_sort

  implicit none

  !=======================!
  !  1a. Interface starts !
  !=======================!

  integer(kind=di), parameter :: ARRAY_SIZE = 300000_di

  !=====================!
  !  1b. Interface ends !
  !=====================!

  !  2. local variables

  real(kind=dr), allocatable, dimension(:) :: array
  integer(kind=di) :: i
  real(kind=dr) :: t1
  real(kind=dr) :: t2

  !  3. allocating the array

  t1 = 0.0_dr
  t2 = 0.0_dr

  allocate(array(1:ARRAY_SIZE))

  !  4. creating the array to be sorted

  do i = 1, ARRAY_SIZE
    array(i) = real(1+ARRAY_SIZE-i, kind=dr)
  end do

  !  5. writing out some unsorted elements

  write(*,*) 
  write(*,*) "  1. The Quick Sort algorithm"
  write(*,*) "  2. Recursive version: slow"
  write(*,*) "  3. The first 5 unsorted elements:"
  write(*,*)
  do i = 1, 5
    write(*,*)  i, array(i)
  end do
  write(*,*)
  write(*,*) "  4. Sorting. Please wait..."

  !  6. sorting starts
 
  call cpu_time(t1)

  call quick_sort(array, 1_di, ARRAY_SIZE)

  call cpu_time(t2)

  write(*,*) "  5. The first sorted elements:"
  write(*,*)
  do i = 1, 5
    write(*,*) i, array(i)
  end do
  write(*,*)
  write(*,*) "  6. Total Real Time: ", (t2-t1)
  write(*,*) "  7. Total sorted elements: ", ARRAY_SIZE
  write(*,*) ""

  end program driver_program

!======!
! FINI !
!======!
