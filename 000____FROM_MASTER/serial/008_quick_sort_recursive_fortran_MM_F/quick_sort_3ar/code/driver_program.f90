!=====================================!
!  Author: Pavlos G. Galiatsatos      !
!  Date: 2013-09-25                   !
!  Program: The Quick Sort Algorithm  !
!=====================================!

  program driver_program

  use m_1_type_definitions
  use m_2_partition_function_3ar
  use m_3_quick_sort_3ar

  implicit none

  integer(kind=di), parameter :: DIMEN = 100000_di
  real(kind=dr), allocatable, dimension(:) :: array_to_sort
  real(kind=dr), allocatable, dimension(:) :: array_stick_a
  real(kind=dr), allocatable, dimension(:) :: array_stick_b
  integer(kind=di) :: i
  real(kind=dr) :: t1
  real(kind=dr) :: t2

  !  1. allocate RAM

  allocate(array_to_sort(1:DIMEN))
  allocate(array_stick_a(1:DIMEN))
  allocate(array_stick_b(1:DIMEN))

  !  2. creating the array to be sorted

  do i = 1, DIMEN

    array_to_sort(i) = real(1+DIMEN-i, kind=dr)
    array_stick_a(i) = real(2+DIMEN-i, kind=dr)
    array_stick_b(i) = real(3+DIMEN-i, kind=dr)   

  end do

  !  3. writing out some unsorted elements

  write(*,*) 
  write(*,*) "  1. The Quick Sort 3ar Sorting Algorithm"
  write(*,*) "  2. Recursive version: slow"
  write(*,*) "  3. The first 5 unsorted elements:"
  write(*,*)
  do i = 1, 5
    write(*,*)  i
    write(*,*)  array_to_sort(i), array_stick_a(i), array_stick_b(i) 
    write(*,*)
  end do
  write(*,*)
  write(*,*) "  4. Sorting. Please wait..."

  !  4. sorting here
 
  call cpu_time(t1)
  
  call quick_sort_3ar(array_to_sort,  &
                      array_stick_a,  &
                      array_stick_b,  & 
                      1_di,           &
                      DIMEN)

 
  call cpu_time(t2) 

  !  5. outputs

  write(*,*) "  5. The first 5 sorted elements:"
  write(*,*)
  do i = 1, 5
    write(*,*) i
    write(*,*) array_to_sort(i), array_stick_a(i), array_stick_b(i)
    write(*,*)
  end do
  write(*,*)
  write(*,*) "  6. Total real time: ", (t2-t1)
  write(*,*) "  7. Total sorted elements: ", DIMEN
  write(*,*)  
 
  end program driver_program

!======!
! FINI !
!======!
