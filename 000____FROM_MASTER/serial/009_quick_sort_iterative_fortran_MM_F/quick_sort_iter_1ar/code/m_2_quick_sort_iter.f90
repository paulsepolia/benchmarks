!=====================================================!
! 1. This is a code part for numerical recipe company !
! 2. There are modifications made by                  !
!    Pavlos G. Galiatsatos                            !
!=====================================================!

  module m_2_quick_sort_iter

  use m_1_type_definitions

  implicit none

  contains

  subroutine quick_sort_iter(arr, n)
 
  !  1. interface variables
   
  integer(kind=di) :: n
  real(kind=dr), allocatable, dimension(:) :: arr

  !  2. local variables 

  integer(kind=di), parameter :: m = 7_di
  integer(kind=di), parameter :: NSTACK = 5000_di
  integer(kind=di) :: i
  integer(kind=di) :: ir
  integer(kind=di) :: j
  integer(kind=di) :: jstack
  integer(kind=di) :: k
  integer(kind=di) :: l
  integer(kind=di), allocatable, dimension(:) :: istack
  real(kind=dr) :: a
  real(kind=dr) :: temp
   
    allocate(istack(1:NSTACK))
 
      jstack=0
      l=1
      ir=n
1     if(ir-l.lt.M)then
        do 12 j=l+1,ir
          a=arr(j)
          do 11 i=j-1,l,-1
            if(arr(i).le.a)goto 2
            arr(i+1)=arr(i)
11        continue
          i=l-1
2         arr(i+1)=a
12      continue
        if(jstack.eq.0)return
        ir=istack(jstack)
        l=istack(jstack-1)
        jstack=jstack-2
      else
        k=(l+ir)/2
        temp=arr(k)
        arr(k)=arr(l+1)
        arr(l+1)=temp
        if(arr(l).gt.arr(ir))then
          temp=arr(l)
          arr(l)=arr(ir)
          arr(ir)=temp
        endif
        if(arr(l+1).gt.arr(ir))then
          temp=arr(l+1)
          arr(l+1)=arr(ir)
          arr(ir)=temp
        endif
        if(arr(l).gt.arr(l+1))then
          temp=arr(l)
          arr(l)=arr(l+1)
          arr(l+1)=temp
        endif
        i=l+1
        j=ir
        a=arr(l+1)
3       continue
          i=i+1
        if(arr(i).lt.a)goto 3
4       continue
          j=j-1
        if(arr(j).gt.a)goto 4
        if(j.lt.i)goto 5
        temp=arr(i)
        arr(i)=arr(j)
        arr(j)=temp
        goto 3
5       arr(l+1)=arr(j)
        arr(j)=a
        jstack=jstack+2
        if(jstack.gt.NSTACK) stop 'NSTACK too small in sort'
        if(ir-i+1.ge.j-l)then
          istack(jstack)=ir
          istack(jstack-1)=i
          ir=j-1
        else
          istack(jstack)=j-1
          istack(jstack-1)=l
          l=i
        endif
      endif
      goto 1

    deallocate(istack)
 
  end subroutine quick_sort_iter

  end module m_2_quick_sort_iter

!======!
! FINI !
!======!
