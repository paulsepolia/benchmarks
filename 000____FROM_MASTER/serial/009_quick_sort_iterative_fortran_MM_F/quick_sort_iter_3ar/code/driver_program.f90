!===================================!
! Author: Pavlos G. Galiatsatos     !
! Date: 2013/09/25                  !
! Program: The Quick Sort algorithm !
!===================================!

  program driver_program

  use m_1_type_definitions
  use m_2_quick_sort_iter_3ar

  implicit none

  !=======================!
  !  1a. Interface starts !
  !=======================!

  integer(kind=di), parameter :: DIMEN = 200000000_di

  !=====================!
  !  1b. Interface ends !
  !=====================!

  real(kind=dr), allocatable, dimension(:) :: array1 ! the array to be sorted
  real(kind=dr), allocatable, dimension(:) :: array2 ! array2 to follow the "array"
  real(kind=dr), allocatable, dimension(:) :: array3 ! array3 to follow the "array"
  integer(kind=di) :: i
  real(kind=dr) :: t1
  real(kind=dr) :: t2

  !  2. allocate RAM

  allocate(array1(1:DIMEN))
  allocate(array2(1:DIMEN))
  allocate(array3(1:DIMEN))

  !  3. create the arrays to be sorted

  do i = 1, DIMEN
    array1(i) = real(1+DIMEN-i, kind=dr)
    array2(i) = real(2+DIMEN-i, kind=dr)
    array3(i) = real(3+DIMEN-i, kind=dr)
  end do

  !  4.  some unsorted elements

  write(*,*) 
  write(*,*) "  1. The Quick Sort algorithm"
  write(*,*) "  2. Iterative version: Fast"
  write(*,*) "  3. The first 5 unsorted elements:"
  write(*,*)
  do i = 1, 5
    write(*,*)  i
    write(*,*)  array1(i), array2(i), array3(i)
    write(*,*)
  end do
  write(*,*)
  write(*,*) "  4. Sorting. Please wait..."

  !  5. main benchmark here
 
  call cpu_time(t1)
  call quick_sort_iter_3ar(array1, array2, array3, DIMEN)
  call cpu_time(t2)

  !  6. some output

  write(*,*) "  5. The first 5 sorted elements:"
  write(*,*)
  do i = 1, 5
    write(*,*) i
    write(*,*) array1(i), array2(i), array3(i) 
    write(*,*)
  end do
  write(*,*)
  write(*,*) "  6. The last 5 sorted elements:"
  write(*,*)
  do i = DIMEN-5, DIMEN
    write(*,*) i
    write(*,*) array1(i), array2(i), array3(i)
    write(*,*)
  end do
  
  write(*,*) "  7. Total real time: ", (t2-t1)
  write(*,*) "  8. Total unsorted elements: ", DIMEN
  write(*,*) 

  end program driver_program

!======!
! FINI !
!======!
