//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2014/10/24              //
//===============================//
//=======================//
// Title: Insertion Sort //
//=======================//

#include <iostream>
using std::cout;
using std::endl;
using std::cin;

#include <cstdlib>
using std::rand;
using std::srand;

#include <ctime>
using std::time;

#include <iomanip>
using std::setw;

#include <ctime>
using std::clock;

//================//
//  main function //
//================//

int main()
{
    const int arraySize = 500000; // size of array a
    int * data = new int [arraySize] ; // the dynamically allocated array
    int insert; // temporary variable to hold element to insert

    cout << "==============================================================" << endl;
    cout << " " << endl;
    cout << "  1 --> Benchmark" << endl;
    cout << "  2 --> Name : Insertion Sort" << endl;
    cout << "  3 --> Type : Sequential" << endl;
    cout << "  4 --> Compiler Intel C++ 11.1 (Windows/Linux)" << endl;
    cout << "  5 --> Programming language : C++" << endl;
    cout << "  6 --> Exact Real Time (1 x X5160)/Linux       : 145.0 seconds" << endl;
    cout << "  7 --> Exact Real Time (1 x 9140M)/Linux       : 192.0 seconds" << endl;
    cout << "  8 --> Exact Real Time (1 x Opteron 250)/Linux : 362.0 seconds" << endl;
    cout << "  9 --> Sorting an array of dimension: " << arraySize << endl;
    cout << " 10 --> Sorting method used is Insertion Sort" << endl;
    cout << " 11 --> Creating the array (always the same)" << endl;

    for(int i = 0; i != arraySize; ++i) {
        data[i] = arraySize-i;
    }

    // insertion sort
    // loop over the element of the array

    clock_t t1;
    t1 = clock();

    cout << " 12 --> Sorting the array" << endl;
    cout << " 13 --> Please wait..." << endl;

    for(int next = 1; next != arraySize; ++next) {
        insert = data[next]; // store the value in the current element

        int moveItem = next;  // initialize location to place the element

        // search for the location in which to put the current element

        while((moveItem > 0) && (data[moveItem-1] > insert)) {
            // shift element one slot to the right
            data[ moveItem ] = data[moveItem-1];
            moveItem--;
        } // end while

        data[ moveItem ] = insert; //place inserted element into the array
    } // end for

    clock_t t2;
    t2 = clock();

    cout << " 14 --> The last five sorted  elements" << endl;
    cout << " " << endl;

    // output sorted array

    for(int i = arraySize-5; i != arraySize; ++i) {
        cout << i << setw(10) << data[i] << endl;
    }

    cout << endl;

    cout << " 15 --> Exact Real Time = "
         << static_cast<double>(t2-t1)/CLOCKS_PER_SEC << " seconds" << endl;

    cout << " " << endl;

    return 0;
}

//======//
// FINI //
//======//

