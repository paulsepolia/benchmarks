#ifndef FUNCTORS_H
#define FUNCTORS_H

//======================//
// FUNCTORS DECLARATION //
//======================//

// functor # 1

template <typename T>
struct Sum_functor {
public:
    Sum_functor();
    void operator()(const T & n);
private:
    T sum;
};

// functor # 2

template<typename T>
struct UniqueNumber_functor {
    T operator () ();
};

// functor # 3

template<typename T>
struct ac_functor {
    T operator()(const T &, const T &);
};

// functor # 4

template <typename T>
struct product_functor {
    T operator()(const T &, const T &);
};

// functor # 5

template <typename T>
struct equal_functor {
    bool operator()(const T &, const T &) const;
};

// functor # 6

template <typename T>
struct isOdd_functor {
    bool operator()(const T &);
};

// functor # 7

template <typename T>
struct lessThanMinusOne_functor {
    bool operator()(const T &);
};

// functor # 8

template <typename T>
struct strComp_functor {
    bool operator()(const T &, const T &);
};

// functor # 9

template <typename T>
struct functor_pp {
    T operator()(T &);
};

// functor # 10

template <typename T>
struct rand_functor {
    int operator()(const T &);
};

// functor # 11

template <typename T>
struct lessThan_functor {
    bool operator()(const T &, const T &) const;
};

// end

#endif // FUNCTORS_H
