#!/bin/bash

  # 1. compile

  g++     -O3                 \
          -Wall               \
          -std=gnu++17        \
		  -fopenmp            \
		  -march=native       \
	      -D_GLIBCXX_PARALLEL \
		  functors.cpp        \
		  functions.cpp       \
          driver_program.cpp  \
          -o x_gnu
