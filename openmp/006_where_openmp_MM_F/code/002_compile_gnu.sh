#!/bin/bash

# 1. compile

  gfortran       -std=f2008               \
                 -Wall                    \
                 -O3                      \
                 -O3                      \
                 -static-libgfortran      \
                 -cpp                     \
                 -fopenmp                 \
                 m_1_type_definitions.f90 \
                 driver_program.f90       \
                 -o x_gnu

# 2. clean

  rm *.mod

# 3. exit
