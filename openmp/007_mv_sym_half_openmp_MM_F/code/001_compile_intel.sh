#!/bin/bash

# 1. compile

  ifort -e08                     \
        -warn all                \
        -O3                      \
        -cpp                     \
        -qopenmp                 \
        -parallel                \
        -par-threshold0          \
        m_1_type_definitions.f90 \
        m_2_mv_sym_half.f90      \
        driver_program.f90       \
        -o x_intel

# 2. clean

  rm *.mod
  rm *.o
