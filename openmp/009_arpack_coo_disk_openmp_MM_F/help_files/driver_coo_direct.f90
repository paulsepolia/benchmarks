!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2011/09/13              !
!===============================!

  program driver_coo_direct
 
  use m_1_type_definitions

  implicit none

  !=======================!
  !  1a. Interface starts !
  !=======================!

  integer(kind=si), parameter   :: matrix_dimen = 1003_si                                 !  1.
  !integer(kind=di)             :: non_zero_ham_elem = ((matrix_dimen**2)+matrix_dimen)/2 !  2.
  integer(kind=di), parameter   :: non_zero_ham_elem = int(matrix_dimen,kind=di)**2       !  2.
  character(len=200), parameter :: matrix_name = "matrix_direct"                          !  3.

  !=====================!
  !  1b. Interface ends !
  !=====================!

  ! 2. declaration of the rest variables

  integer(kind=si), allocatable, dimension(:)  :: row_index_array
  integer(kind=si), allocatable, dimension(:)  :: column_index_array
  real(kind=dr)   , allocatable, dimension(:)  :: ham_elem_array
  integer(kind=di)                             :: i
  real(kind=dr), dimension(3)                  :: dimen_line
  integer(kind=si)                             :: rec_len
  integer(kind=si)                             :: j1
  integer(kind=si)                             :: j2

  !  3a. opening the unit to write the size of the matrix
  !      and the number of the non-zero hamiltonian elements

  inquire(iolength=rec_len) dimen_line

  !  3b.

  open(unit = 11,            &
       form = "unformatted", &
       file = matrix_name,   &
       status = "new",       &
       action = "write",     &
       access = "direct",    &
       recl = rec_len)

  !  3c.

  dimen_line(1) = matrix_dimen
  dimen_line(2) = matrix_dimen
  dimen_line(3) = non_zero_ham_elem

  write(unit=11, rec=1) dimen_line(1), dimen_line(2), dimen_line(3)
   
  !  3d.
 
  close(unit=11, status="keep")

  !  4. allocating space for the local arrays
  !     to put the sparse matrix data in each mpi thread

  allocate(row_index_array(1:non_zero_ham_elem))
  allocate(column_index_array(1:non_zero_ham_elem))
  allocate(ham_elem_array(1:non_zero_ham_elem))


  !  5. create the matrix

  i = 0_di

  do j1 = 1, matrix_dimen
    do j2 = 1, matrix_dimen-j1+1

      i = i + 1_di
      row_index_array(i)    = j1 
      column_index_array(i) = j2
      ham_elem_array(i)     = abs(dcos(real(i,kind=dr)))  ! some random case

      end do
  end do


  !  6. write the matrix data to hard disk

  !  6a. open the unit 

  open(unit = 11,            &
       form = "unformatted", &
       file = matrix_name,   &
       status = "old",       &
       action = "write",     & 
       access = "direct",    &
       recl = rec_len)

  !  6b. write the data 

  do i = 1, non_zero_ham_elem

    dimen_line(1) = row_index_array(i)
    dimen_line(2) = column_index_array(i)
    dimen_line(3) = ham_elem_array(i)

    write(unit=11, rec =i+1) dimen_line(1), &
                             dimen_line(2), &
                             dimen_line(3)

  end do

  !  6c. close the unit and keep the data file

  close( unit=11, status="keep" )

  end program driver_coo_direct

!======!
! FINI !
!======!
