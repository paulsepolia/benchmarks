!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2011/09/23              !
!===============================!

  program driver_program

  use m_1_type_definitions
  use m_2_coo_ram
  use m_3_smv_omp_ram

#ifdef _OPENMP
  use omp_lib
#endif

  implicit none

  !======================!
  ! 1a. Interface starts !
  !======================!

  integer(kind=si), parameter :: DIMEN_SPARSE = 50000_si
  integer(kind=si), parameter :: TRIALS       = 400_si
  real(kind=dr), parameter    :: SPARSITY     = 99.40_dr

  !====================!
  ! 1b. Interface ends !
  !====================!

  ! 2. Local variables

  integer(kind=di) :: non_zero_ham_elem
  integer(kind=si), allocatable, dimension(:) :: row_index_array
  integer(kind=si), allocatable, dimension(:) :: column_index_array
  real(kind=dr)   , allocatable, dimension(:) :: ham_elem_array
  real(kind=dr)   , allocatable, dimension(:) :: x
  real(kind=dr)   , allocatable, dimension(:) :: y
  real(kind=dr)    :: t1
  real(kind=dr)    :: t2  
  integer(kind=si) :: i
  integer(kind=si) :: nt

  ! 3. Getting the number of the OpenMP threads

  !$omp parallel      &
  !$omp default(none) &
  !$omp shared(nt)  

#ifdef _OPENMP
  nt = omp_get_num_threads()
#else
  nt = 1_si
#endif

  !$omp end parallel

  ! 4. Calling the sub which creates the fake coo_sparse matrix

  call matrix_coo_ram(DIMEN_SPARSE,       &
                      non_zero_ham_elem,  &
                      row_index_array,    &
                      column_index_array, &
                      ham_elem_array,     &
                      SPARSITY)


  write(*,*) ""                      
  write(*,*) "  1 -->  Benchmark, OpenMP"
  write(*,*) "  2 -->  Title: Sparse Matrix-Vector product" 
  write(*,*) "  3 -->  matrix_dimen_sparse             --> ", DIMEN_SPARSE
  write(*,*) "  4 -->  non_zero_ham_elem               --> ", non_zero_ham_elem
  write(*,*) "  5 -->  sparsity                        --> ", SPARSITY
  write(*,*) "  6 -->  row_index_array(1)              --> ", row_index_array(1)
  write(*,*) "  7 -->  column_index_array(1)           --> ", column_index_array(1)
  write(*,*) "  8 -->  ham_elem_array(1)               --> ", ham_elem_array(1)
  write(*,*) "  9 -->  RAM needed                      --> ", & 
             (non_zero_ham_elem/1024.0_dr**3)*16, " GBytes "
  write(*,*) " 10 -->  Please wait ... " 

  ! 5. initializing the x vector

  allocate(x(1:DIMEN_SPARSE)) 

  do i = 1, DIMEN_SPARSE

    x(i) = dcos(dcos(real(i,kind=dr)))

  end do

  ! 6. calling the sub sparse matrix vector product

  call cpu_time(t1)

  do i = 1, TRIALS

    call smv_coo_omp_fortran(row_index_array,    & 
                             column_index_array, &
                             ham_elem_array,     &
                             DIMEN_SPARSE,       &
                             non_zero_ham_elem,  &
                             x,                  &
                             y,                  &
                             nt)

  end do

  call cpu_time(t2)

  ! 7. writing out for testing purposes.

  write(*,*) " 11 -->  Number of smv products done     --> ", TRIALS
  write(*,*) " 12 -->  Total CPU time (seconds)        --> ", (t2-t1)
  write(*,*) " 13 -->  SMV products per second         --> ", TRIALS * nt / (t2-t1)
  write(*,*) " 14 -->  Some results are: "
  write(*,*) ""

  do i = 1, 2 

    write(*,*)  i, x(i), y(i)

  end do

  ! 8. deallocations

  deallocate(x)
  deallocate(y)
  deallocate(row_index_array)
  deallocate(column_index_array)
  deallocate(ham_elem_array)

  end program driver_program

!======!
! FINI !
!======!
