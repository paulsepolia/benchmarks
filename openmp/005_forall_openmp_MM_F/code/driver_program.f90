!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/09/22              !
!===============================!

  program driver_program

  use m_1_type_definitions

#ifdef _OPENMP
  use omp_lib
#endif
 
  implicit none
 
  !=======================!
  !  1a. Interface starts !
  !=======================!
 
  integer(kind=si), parameter :: DIMEN  = 15000_si
  integer(kind=si), parameter :: TRIALS = 10_si
  real(kind=dr), parameter    :: CRITE  = 10.0E-5_dr

  !=====================!
  !  1b. Interface ends !
  !=====================!

  !  2. local variables

  real(kind=dr), parameter :: ZERO = 0.0_dr
  real(kind=dr), parameter :: ONE  = 1.0_dr
  real(kind=dr), parameter :: TWO  = 2.0_dr
  real(kind=dr), parameter :: PI   = 3.141592653589793_dr
  real(kind=dr)    :: t1
  real(kind=dr)    :: t2
  integer(kind=si) :: i1
  integer(kind=si) :: i2
  integer(kind=si) :: i3
  real(kind=dr), allocatable, dimension(:,:) :: array 
 
  !  3. allocate RAM
 
  allocate(array(1:DIMEN,1:DIMEN))

  !  4. build the matrix for the forall
 
  call cpu_time(t1)

  !$omp parallel      &
  !$omp default(none) &
  !$omp shared(array)

  !$omp do          &
  !$omp private(i1) &
  !$omp private(i2)
 
  do i1 = 1, DIMEN 
    do i2 = 1, DIMEN
   
      array(i2,i1) = dsin((i2/TWO)*PI)
   
    end do
  end do
 
  !$omp end do
  !$omp end parallel

  call cpu_time(t2)
 
  !  5. some output 
 
  write(*,*) "========================"
  write(*,*) " The 'forall' benchmark "
  write(*,*) "========================"
  write(*,*) " "
  write(*,*) " 1. "
  write(*,*) " "
  write(*,*) " cpu time for initialization = ", (t2-t1)
  write(*,*) " array(1,1) = ", array(1,1)
  write(*,*) " array(2,1) = ", array(2,1)
  write(*,*) " "

  !  6. the "forall" here
 
  call cpu_time(t1)
 
  do i3 = 1, TRIALS

    !$omp parallel workshare
 
    forall(i1=1:DIMEN, i2=1:DIMEN, abs(array(i1,i2)) <= CRITE)
   
      array(i1,i2) = ONE/array(i1,i2)  
 
    end forall
 
    !$omp end parallel workshare 
 
  end do
 
  call cpu_time(t2)

  write(*,*) " "
  write(*,*) " 2. "
  write(*,*) " " 
  write(*,*) " cpu time for 'forall' construct = ", (t2-t1)
  write(*,*) " array(1,1) = ", array(1,1)
  write(*,*) " array(2,1) = ", array(2,1)
  write(*,*) " array(3,1) = ", array(3,1)

  !  7. build the matrix for the hand-made do-loop

  !$omp parallel      &
  !$omp default(none) &
  !$omp shared(array)

  !$omp do          &
  !$omp private(i1) &
  !$omp private(i2)
 
  do i1 = 1, DIMEN
    do i2 = 1, DIMEN
   
      array(i2,i1) = dsin((i2/TWO)*PI)
   
    end do
  end do
 
  !$omp end do
  !$omp end parallel

  !  8.
 
  call cpu_time(t1)
 
  do i3 = 1, TRIALS

    !$omp parallel      &
    !$omp default(none) &
    !$omp shared(array) 

    !$omp do          &
    !$omp private(i1) &
    !$omp private(i2)

    do i1 = 1, DIMEN
      do i2 = 1, DIMEN
   
        if (abs(array(i1,i2)) <= CRITE) array(i1,i2) = ONE/array(i1,i2) 
   
      end do
    end do
  
    !$omp end do
    !$omp end parallel

  end do
 
  call cpu_time(t2)
 
  write(*,*) " "
  write(*,*) " 3. "
  write(*,*) " "
  write(*,*) " cpu time for 'do_loop_hand_made_bad' = ", (t2-t1)
  write(*,*) " array(1,1) = ", array(1,1)
  write(*,*) " array(2,1) = ", array(2,1)
  write(*,*) " array(3,1) = ", array(3,1)

  !  9. build the matrix for the hand-made do loop
 
  !$omp parallel      &
  !$omp default(none) &
  !$omp shared(array)

  !$omp do          &
  !$omp private(i1) &
  !$omp private(i2)

  do i1 = 1, DIMEN
    do i2 = 1, DIMEN
   
      array(i2,i1) = dsin((i2/TWO)*PI)
   
    end do
  end do

  !$omp end do
  !$omp end parallel

  ! 10.

  call cpu_time(t1)

  do i3 = 1, TRIALS
 
    !$omp parallel      &
    !$omp default(none) &
    !$omp shared(array) 
 
    !$omp do          &
    !$omp private(i1) &
    !$omp private(i2)

    do i1 = 1, DIMEN
      do i2 = 1, DIMEN

        if (abs(array(i2,i1)) <= CRITE) array(i2,i1) = ONE/array(i2,i1)

      end do
    end do

    !$omp end do
    !$omp end parallel

  end do

  call cpu_time(t2)

  write(*,*) " "
  write(*,*) " 4. "
  write(*,*) " "
  write(*,*) " cpu time for 'do_loop_hand_made_good' = ", (t2-t1)
  write(*,*) " array(1,1) = ", array(1,1)
  write(*,*) " array(2,1) = ", array(2,1)
  write(*,*) " array(3,1) = ", array(3,1)
  write(*,*) " "
 
  end program driver_program

!======!
! FINI !
!======!
