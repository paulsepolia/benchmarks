#!/bin/bash

# 1. compile

  ifort -e08                     \
        -warn all                \
        -O3                      \
        -cpp                     \
        -qopenmp                 \
        m_1_type_definitions.f90 \
        driver_program.f90       \
        -o x_intel

# 2. clean

  rm *.mod
  rm *.o
