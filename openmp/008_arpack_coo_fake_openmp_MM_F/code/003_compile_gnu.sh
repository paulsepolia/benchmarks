#!/bin/bash

# 1. compile

  gfortran-4.8 -std=f2008                  \
               -Wall                       \
               -Ofast                      \
               -cpp                        \
               -fopenmp                    \
               m_1_type_definitions.f90    \
               m_2_matrix_coo_fake_ram.f90 \
               m_3_arpack_coo_omp_ram.f90  \
               driver_program.f90          \
               /opt/arpack/lib/libarpack_gnu.a \
               -o x_gnu

# 2. clean

  rm *.mod

# 3. exit
