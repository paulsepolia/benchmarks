#!/bin/bash

# 1. compile

  gfortran  -std=f2008                    \
            -Wall                         \
            -O3                           \
            -fopenmp                      \
            m_1_type_definitions.f90      \
            m_2_matrix_function.f90       \
            m_3_write_openmp.f90          \
            m_4_matrix_via_function.f90   \
            m_5_dsyev_pgg.f90             \
            driver_dsyev.f90              \
            -L$MKLPATH                    \
            -I$MKLINCLUDE                 \
            -L$MKLPATH/lmkl_solver_lp64.a \
            -Wl,--start-group             \
            $MKLPATH/libmkl_intel_lp64.a  \
            $MKLPATH/libmkl_gnu_thread.a  \
            $MKLPATH/libmkl_core.a        \
            -Wl,--end-group               \
            -ldl                          \
            -o x_gnu_mkl_i4

# 2. clean

  rm *.mod

# 3. exit 
