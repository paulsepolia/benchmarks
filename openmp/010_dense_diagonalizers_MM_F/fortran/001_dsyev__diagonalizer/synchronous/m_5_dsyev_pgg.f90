!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2014/12/12              !
!===============================!

  module m_5_dsyev_pgg

  use m_1_type_definitions, only: dr, fi

  implicit none

  contains

!===================================================================!
!===================================================================!

  subroutine dsyev_pgg(jobz,  &  !  1
                       uplo,  &  !  2
                       n,     &  !  3
                       a,     &  !  4
                       w,     &  !  5
                       info)     !  6

  implicit none   
  
!====================================================================!
! This is a copy-paste text from LAPACK's site                       !
!====================================================================!
!
!  SUBROUTINE DSYEV( JOBZ, UPLO, N, A, LDA, W, WORK, LWORK, INFO )
!
!  -- LAPACK driver routine (version 3.2) --
!  -- LAPACK is a software package provided by Univ. of Tennessee,    --
!  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
!     November 2006
!
!     .. Scalar Arguments ..
!     CHARACTER          JOBZ, UPLO
!     INTEGER            INFO, LDA, LWORK, N
!     ..
!     .. Array Arguments ..
!     DOUBLE PRECISION   A( LDA, * ), W( * ), WORK( * )
!     ..
!
!  Purpose
!  =======
!
!  DSYEV computes all eigenvalues and, optionally, eigenvectors of a
!  real symmetric matrix A.
!
!  Arguments
!  =========
!
!  JOBZ    (input) CHARACTER*1
!          = 'N':  Compute eigenvalues only;
!          = 'V':  Compute eigenvalues and eigenvectors.
!
!  UPLO    (input) CHARACTER*1
!          = 'U':  Upper triangle of A is stored;
!          = 'L':  Lower triangle of A is stored.
!
!  N       (input) INTEGER
!          The order of the matrix A.  N >= 0.
!
!  A       (input/output) DOUBLE PRECISION array, dimension (LDA, N)
!          On entry, the symmetric matrix A.  If UPLO = 'U', the
!          leading N-by-N upper triangular part of A contains the
!          upper triangular part of the matrix A.  If UPLO = 'L',
!          the leading N-by-N lower triangular part of A contains
!          the lower triangular part of the matrix A.
!          On exit, if JOBZ = 'V', then if INFO = 0, A contains the
!          orthonormal eigenvectors of the matrix A.
!          If JOBZ = 'N', then on exit the lower triangle (if UPLO='L')
!          or the upper triangle (if UPLO='U') of A, including the
!          diagonal, is destroyed.
!
!  LDA     (input) INTEGER
!          The leading dimension of the array A.  LDA >= max(1,N).
!
!  W       (output) DOUBLE PRECISION array, dimension (N)
!          If INFO = 0, the eigenvalues in ascending order.
!
!  WORK    (workspace/output) DOUBLE PRECISION array, dimension (MAX(1,LWORK))
!          On exit, if INFO = 0, WORK(1) returns the optimal LWORK.
!
!  LWORK   (input) INTEGER
!          The length of the array WORK.  LWORK >= max(1,3*N-1).
!          For optimal efficiency, LWORK >= (NB+2)*N,
!          where NB is the blocksize for DSYTRD returned by ILAENV.
!
!          If LWORK = -1, then a workspace query is assumed; the routine
!          only calculates the optimal size of the WORK array, returns
!          this value as the first entry of the WORK array, and no error
!          message related to LWORK is issued by XERBLA.
!
!  INFO    (output) INTEGER
!          = 0:  successful exit
!          < 0:  if INFO = -i, the i-th argument had an illegal value
!          > 0:  if INFO = i, the algorithm failed to converge; i
!                off-diagonal elements of an intermediate tridiagonal
!                form did not converge to zero.
!
!====================================================================!
!====================================================================!

  !  1. Interface variables

  character, intent(in)                                     :: jobz
  character, intent(in)                                     :: uplo
  integer(kind=fi), intent(in)                              :: n
  integer(kind=fi), intent(out)                             :: info
  real(kind=dr), allocatable, dimension(:,:), intent(inout) :: a
  real(kind=dr), allocatable, dimension(:)  , intent(inout) :: w
 
  !  2. Local variables

  integer(kind=fi)                         :: lda
  integer(kind=fi)                         :: lwork
  real(kind=dr), allocatable, dimension(:) :: work
 
  !  3. Local variables initialization

  lda   = n
  lwork = 5_fi*n-1_fi

  !  4. Allocation of workspace

  allocate(work(1:lwork))

  !  5. The main diagonalization step

  call dsyev(jobz,   &  !  1
             uplo,   &  !  2
             n,      &  !  3
             a,      &  !  4
             lda,    &  !  5
             w,      &  !  6
             work,   &  !  7
             lwork,  &  !  8
             info)      !  9

  !  6. Local deallocations

  deallocate(work)

  end subroutine dsyev_pgg

!====================================================================!
!====================================================================!

  end module m_5_dsyev_pgg

! FINI
