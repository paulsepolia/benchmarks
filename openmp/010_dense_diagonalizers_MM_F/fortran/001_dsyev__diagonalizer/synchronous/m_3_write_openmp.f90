!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2014/12/12              !
!===============================!

  module m_3_write_openmp

  use m_1_type_definitions, only: si, di, dr, fi

  implicit none

  contains

!===========================!
! 1 --> matrix_write_openmp !
!===========================!

  subroutine matrix_write_openmp(m,            &  !  1
                                 n,            &  !  2
                                 a,            &  !  3
                                 file_system,  &  !  4
                                 matrix_file,  &  !  5
                                 out_format)      !  6

  implicit none

  !  1. Interface variables

  integer(kind=fi), intent(in)                           :: m           ! matrix rows
  integer(kind=fi), intent(in)                           :: n           ! matrix columns
  real(kind=dr), allocatable, dimension(:,:), intent(in) :: a           ! matrix to write
  character(len=200), intent(in)                         :: file_system ! "lustre_off" or "lustre_on"
  character(len=200), intent(in)                         :: matrix_file ! file name
  character(len=200), intent(in)                         :: out_format  ! "by_column" or "by_row"

  !  2. Local variables

  integer(kind=si)            :: i
  integer(kind=si)            :: j
  integer(kind=si)            :: chunk
  integer(kind=si)            :: low_ba
  integer(kind=si)            :: up_ba
  integer(kind=si)            :: i_am
  integer(kind=si)            :: n_threads
  integer(kind=si)            :: nt
  integer(kind=si)            :: rec_len
  integer(kind=si)            :: omp_get_num_threads
  integer(kind=si)            :: omp_get_thread_num
  real(kind=dr), dimension(1) :: dimen_line
  integer(kind=si), parameter :: URW = 12

  !  3. Write the eigenvectors

  !  3-1. Set the common record length

  inquire(iolength = rec_len) dimen_line

  !  3-2. Open the unit to write

  open(unit = URW,            & 
       form = "unformatted",  &
       file = matrix_file,    &
       status = "replace",    &
       action = "write",      &
       access = "direct",     &
       recl = rec_len)    

  !  3-3. Write the dimensions

  write(unit=URW, rec=1_si) real(m, kind=dr) ! rows
  write(unit=URW, rec=2_si) real(n, kind=dr) ! columns

  !  3-4. Set the file system

  if (file_system == "lustre_off") then

    !$omp parallel       &
    !$omp default(none)  &
    !$omp shared(nt)

    nt = 1_si

    call omp_set_num_threads(nt)

    !$omp end parallel

  else if (file_system == "lustre_on") then
 
    !$omp parallel       &
    !$omp default(none)  &
    !$omp shared(nt)

    nt = omp_get_num_threads()  

    call omp_set_num_threads(nt)

    !$omp end parallel

  end if

  !  3-5. Write now in parallel if "lustre_on" is set
  !       otherwise in serial

  !$omp parallel            &
  !$omp default(none)       &
  !$omp private(i)          &
  !$omp private(j)          &
  !$omp private(chunk)      &
  !$omp private(i_am)       &
  !$omp private(low_ba)     &
  !$omp private(up_ba)      &           
  !$omp shared(n)           &
  !$omp shared(m)           &
  !$omp shared(a)           &
  !$omp shared(n_threads)   &
  !$omp shared(out_format)  &
  !$omp shared(nt)          &
  !$omp num_threads(nt)

  ! a.

  i_am = omp_get_thread_num()
  n_threads = omp_get_num_threads()
  chunk = int(n, kind=si) / n_threads
  low_ba = i_am * chunk + 1_si
  up_ba = (i_am + 1_si) * chunk

  ! b.

  if (i_am == n_threads-1) then
    up_ba = int(n, kind=si)
  end if

  ! c. out_format: "by_column" or "by_row"
  
  if (out_format == "by_column") then
 
    do i = low_ba, up_ba
      do j = 1, int(m, kind=si)

        write(unit=URW, rec=2_di+j+(i-1)*int(n, kind=di))  a(j,i)    

      end do
    end do 

  else if (out_format == "by_row") then

    do i = low_ba, up_ba
      do j = 1, int(m, kind=si)

        write(unit=URW, rec=2_di+i+(j-1)*int(n, kind=di))  a(j,i)    

      end do
    end do 

  end if

  !$omp end parallel

  !  3-6. Close the unit and keep the file

  flush(unit=URW)
  close(unit=URW, status="keep")

  end subroutine matrix_write_openmp

!===========================!
! 2 --> vector_write_openmp !
!===========================!

  subroutine vector_write_openmp(n,            &  !  1
                                 w,            &  !  2
                                 file_system,  &  !  3
                                 vector_file)     !  4

  implicit none

  !  1. Interface variables

  integer(kind=fi), intent(in)                         :: n           ! matrix columns
  real(kind=dr), allocatable, dimension(:), intent(in) :: w           ! vector to write
  character(len=200), intent(in)                       :: file_system ! "lustre_off" or "lustre_on"
  character(len=200), intent(in)                       :: vector_file ! file name

  !  2. Local variables

  integer(kind=si)            :: i
  integer(kind=si)            :: chunk
  integer(kind=si)            :: low_ba
  integer(kind=si)            :: up_ba
  integer(kind=si)            :: i_am
  integer(kind=si)            :: n_threads
  integer(kind=si)            :: nt
  integer(kind=si)            :: rec_len
  integer(kind=si)            :: omp_get_num_threads
  integer(kind=si)            :: omp_get_thread_num
  real(kind=dr), dimension(1) :: dimen_line
  integer(kind=si), parameter :: URW = 12

  !  3. Write the eigenvalues

  !  3-1. Set the common record length

  inquire(iolength = rec_len) dimen_line

  !  3-2. Open the unit to write

  open(unit = URW,            & 
       form = "unformatted",  &
       file = vector_file,    &
       status = "replace",    &
       action = "write",      &
       access = "direct",     &
       recl = rec_len)    

  !  3-3. Write the dimension

  write(unit=URW, rec=1) real(n, kind=dr)

  !  3-4. Set the file system

  if (file_system == "lustre_off") then

    !$omp parallel       &
    !$omp default(none)  &
    !$omp shared(nt)

    nt = 1_si

    call omp_set_num_threads(nt)

    !$omp end parallel

  else if (file_system == "lustre_on") then
 
    !$omp parallel       &
    !$omp default(none)  &
    !$omp shared(nt)

    nt = omp_get_num_threads()  

    call omp_set_num_threads(nt)

    !$omp end parallel

  end if

  !  3-5. Write now in parallel if "lustre_on" is set
  !       otherwise in serial

  !$omp parallel           &
  !$omp default(none)      &
  !$omp private(i)         &
  !$omp private(chunk)     &
  !$omp private(i_am)      &
  !$omp private(low_ba)    &
  !$omp private(up_ba)     &           
  !$omp shared(n)          &
  !$omp shared(w)          &
  !$omp shared(n_threads)  &
  !$omp shared(nt)         &
  !$omp num_threads(nt)

  ! a.

  i_am = omp_get_thread_num()
  n_threads = omp_get_num_threads()
  chunk = int(n, kind=si) / n_threads
  low_ba = i_am * chunk + 1_si
  up_ba = (i_am + 1_si) * chunk

  ! b.

  if (i_am == n_threads-1_si) then
    up_ba = int(n, kind=si)
  end if

  ! c.
 
  do i = low_ba, up_ba

    write(unit=URW, rec=1_si+i)  w(i)    

  end do

  !$omp end parallel

  !  3-6. Close the unit and keep the file

  flush(unit=URW)
  close(unit=URW, status="keep")

  end subroutine vector_write_openmp

!====================================================================!
!====================================================================!

  end module m_3_write_openmp

! FINI
