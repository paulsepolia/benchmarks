#!/bin/bash

# 1. compile

  ifort  -e08                        \
         -O3                         \
         -qopenmp                    \
	     -assume buffered_io         \
         m_1_type_definitions.f90    \
         m_2_matrix_function.f90     \
         m_3_write_openmp.f90        \
         m_4_matrix_via_function.f90 \
         m_5_dsyev_pgg.f90           \
         driver_dsyev.f90            \
         /opt/lapack/lib_2015/liblapack_intel_mp_i8.a \
         /opt/blas/lib_2015/libblas_intel_mp_i8.a     \
         -o x_intel_mp_i8

# 2. clean

  rm *.mod

# 3. exit
