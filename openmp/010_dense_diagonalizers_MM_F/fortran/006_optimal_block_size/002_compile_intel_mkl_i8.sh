#!/bin/bash

# 1. compile

  ifort  -e03                            \
  	     -warn all                       \
         -O3                             \
         -parallel                       \
         -par-threshold0                 \
         m_1_type_definitions.f90        \
         optimal_block_size.f90          \
         -L$MKLPATH                      \
         -I$MKLINCLUDE                   \
         -L$MKLPATH/lmkl_solver_ilp64.a  \
         -Wl,--start-group               \
         $MKLPATH/libmkl_intel_ilp64.a   \
         $MKLPATH/libmkl_intel_thread.a  \
         $MKLPATH/libmkl_core.a          \
         -Wl,--end-group                 \
         -liomp5                         \
         -openmp                         \
         -lpthread                       \
         -lm                             \
         -o x_intel_mkl_i8

# 2. clean

  rm *.mod

# 3. exit 
