!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/02/22              !
!===============================!

  program driver_vector_dense_write

  use m_1_parameters,  only: si, di, dr, len_a
  use m_2_functions,   only: vector_function
  use m_5_write_subs,  only: vector_write_sub
  use m_8_error_check, only: error_check_vector_write

  implicit none

  !  1. Interface variables

  integer(kind=di)                         :: n             ! rows
  real(kind=dr), allocatable, dimension(:) :: w             ! vector
  character(len=len_a)                     :: file_system   ! "lustre_off", "lustre_on"
  character(len=len_a)                     :: file_name     ! file name for matrix
  character(len=len_a)                     :: num_files     ! "one", "many"
  character(len=len_a)                     :: type_of_build ! "via_vector", "via_function"
  integer(kind=si)                         :: trials        ! times to write
  character(len=len_a)                     :: output_yes_no ! "yes", "no"
  character(len=len_a)                     :: stat_open     ! "new", "replace"
  character(len=len_a)                     :: stat_close    ! "keep", "delete"

!===================!
! Drivers interface !
!===================!

  !  2. Initialization of the interface variables

  n             = 40000013_si
  file_name     = "vector_direct"
  file_system   = "lustre_on"
  num_files     = "many"
  type_of_build = "via_vector"
  trials        = 10
  output_yes_no = "yes"
  stat_open     = "replace"
  stat_close    = "keep"

!===========================!
! End of driver's interface !
!===========================!

  !  3. Check for input errors

  call error_check_vector_write(file_system,   & !  1.
                                type_of_build, & !  2.
                                num_files,     & !  3.
                                output_yes_no, & !  4.
                                stat_open,     & !  5.
                                stat_close)      !  6.

  !  4. Vector OpenMP write

  call vector_write_sub(n,               & !  1.
                        w,               & !  2.
                        vector_function, & !  3.
                        file_system,     & !  4.
                        file_name,       & !  5.
                        type_of_build,   & !  6.
                        num_files,       & !  7.
                        trials,          & !  8.
                        output_yes_no,   & !  9.
                        stat_open,       & ! 10.
                        stat_close)        ! 11.

  end program driver_vector_dense_write

!======!
! FINI !
!======!
