!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/02/22              !
!===============================!

  module m_8_error_check

  implicit none

  contains

!=========================================!
! 1. Subroutine: error_check_matrix_write !
!=========================================!

  subroutine error_check_matrix_write(file_system,      & !  1.
                                      type_of_build,    & !  2.
                                      out_format,       & !  3.
                                      num_files,        & !  4.
                                      output_yes_no,    & !  5.
                                      stat_open_write,  & !  6.
                                      stat_close_write)   !  7.

  use m_1_parameters, only: si, len_a,              & 
                            FILE_SYS_LU_ON,         &
                            FILE_SYS_LU_OFF,        & 
                            TYPE_OF_BUILD_MATRIX,   &
                            TYPE_OF_BUILD_FUNCTION, &
                            OUT_FORMAT_COL,         &
                            OUT_FORMAT_ROW,         &
                            NUM_FILES_ONE,          &
                            NUM_FILES_MANY,         &
                            OUTPUT_YES,             &
                            OUTPUT_NO,              &
                            STAT_OPEN_NEW,          &
                            STAT_OPEN_REPLACE,      &
                            STAT_CLOSE_KEEP,        &
                            STAT_CLOSE_DELETE

  implicit none

  !  1. Interface variables 

  character(len=len_a), intent(in) :: file_system
  character(len=len_a), intent(in) :: type_of_build
  character(len=len_a), intent(in) :: out_format
  character(len=len_a), intent(in) :: num_files
  character(len=len_a), intent(in) :: output_yes_no
  character(len=len_a), intent(in) :: stat_open_write
  character(len=len_a), intent(in) :: stat_close_write

  !  2. Local variables

  integer(kind=si) :: error_flag

  !  3. Initialize local variables

  error_flag = 0_si

  !  4. Check for input errors

  !  4-a.

  if (file_system /= FILE_SYS_LU_ON .and. file_system /= FILE_SYS_LU_OFF) then
    write(*,*) ""
    write(*,*) "Error. 'file_system' must be '", trim(FILE_SYS_LU_ON), &
               "' or '", trim(FILE_SYS_LU_OFF), "'"
    write(*,*) "You typed: '", trim(file_system), "'"
    error_flag = 1_si
  end if

  !  4-b.

  if (type_of_build /= TYPE_OF_BUILD_MATRIX .and. type_of_build /= TYPE_OF_BUILD_FUNCTION) then
    write(*,*) ""
    write(*,*) "Error. 'type_of_build' must be '", trim(TYPE_OF_BUILD_MATRIX), &
               "' or '", trim(TYPE_OF_BUILD_FUNCTION), "'"
    write(*,*) "You typed: '", trim(type_of_build), "'"
    error_flag = 1_si
  end if

  !  4-c.

  if (out_format /= OUT_FORMAT_ROW .and. out_format /= OUT_FORMAT_COL) then
    write(*,*) ""
    write(*,*) "Error. 'out_format' must be '", trim(OUT_FORMAT_ROW), &
               "' or '", trim(OUT_FORMAT_COL), "'"
    write(*,*) "You typed: '", trim(out_format), "'"
    error_flag = 1_si
  end if

  !  4-d.

  if (num_files /= NUM_FILES_ONE .and. num_files /= NUM_FILES_MANY) then
    write(*,*) ""
    write(*,*) "Error. 'num_files' must be '", trim(NUM_FILES_ONE), &
               "' or '", trim(NUM_FILES_MANY), "'"
    write(*,*) "You typed: '", trim(num_files), "'"
    error_flag = 1_si
  end if

  !  4-e.

  if (output_yes_no /= OUTPUT_YES .and. output_yes_no /= OUTPUT_NO) then
    write(*,*) ""
    write(*,*) "Error. 'output_yes_no' must be '", trim(OUTPUT_YES), &
               "' or '", trim(OUTPUT_NO), "'"
    write(*,*) "You typed: '", trim(output_yes_no), "'"
    error_flag = 1_si
  end if

  !  4-f.

  if (stat_open_write /= STAT_OPEN_NEW .and. stat_open_write /= STAT_OPEN_REPLACE) then
    write(*,*) ""
    write(*,*) "Error. 'stat_open_write' must be '", trim(STAT_OPEN_NEW), &
               "' or '", trim(STAT_OPEN_REPLACE), "'"
    write(*,*) "You typed: '", trim(stat_open_write), "'"
    error_flag = 1_si
  end if

  !  4-g.

  if (stat_close_write /= STAT_CLOSE_KEEP .and. stat_close_write /= STAT_CLOSE_DELETE) then
    write(*,*) ""
    write(*,*) "Error. 'stat_close_write' must be '", trim(STAT_CLOSE_KEEP), &
               "' or '", trim(STAT_CLOSE_DELETE), "'"
    write(*,*) "You typed: '", trim(stat_close_write), "'"
    error_flag = 1_si
  end if

  !  4-h.

  if (error_flag == 1_si) then
    write(*,*) ""
    write(*,*) "Stop"
    write(*,*) ""
    stop  
  end if

  end subroutine error_check_matrix_write

!========================================!
! 2. Subroutine: error_check_matrix_read !
!========================================!

  subroutine error_check_matrix_read(file_system,     & !  1.
                                     out_format,      & !  2.
                                     output_yes_no,   & !  3.
                                     test_yes_no,     & !  4.
                                     stat_close_read)   !  5.

  use m_1_parameters, only: si, len_a,        & 
                            FILE_SYS_LU_ON,   &
                            FILE_SYS_LU_OFF,  &
                            OUT_FORMAT_COL,   &
                            OUT_FORMAT_ROW,   &
                            OUTPUT_YES,       &
                            OUTPUT_NO,        &
                            TEST_YES,         &
                            TEST_NO,          &
                            STAT_CLOSE_KEEP,  &
                            STAT_CLOSE_DELETE

  implicit none

  !  1. Interface variables 

  character(len=len_a), intent(in) :: file_system
  character(len=len_a), intent(in) :: out_format
  character(len=len_a), intent(in) :: output_yes_no
  character(len=len_a), intent(in) :: test_yes_no
  character(len=len_a), intent(in) :: stat_close_read

  !  2. Local variables

  integer(kind=si) :: error_flag

  !  3. Initialize local variables

  error_flag = 0_si

  !  4. Check for input errors

  !  4-a.

  if (file_system /= FILE_SYS_LU_ON .and. file_system /= FILE_SYS_LU_OFF) then
    write(*,*) ""
    write(*,*) "Error. 'file_system' must be '", trim(FILE_SYS_LU_ON), &
               "' or '", trim(FILE_SYS_LU_OFF), "'"
    write(*,*) "You typed: '", trim(file_system), "'"
    error_flag = 1_si
  end if

  !  4-b.

  if (out_format /= OUT_FORMAT_ROW .and. out_format /= OUT_FORMAT_COL) then
    write(*,*) ""
    write(*,*) "Error. 'out_format' must be '", trim(OUT_FORMAT_ROW), &
               "' or '", trim(OUT_FORMAT_COL), "'"
    write(*,*) "You typed: '", trim(out_format), "'"
    error_flag = 1_si
  end if

  !  4-c.

  if (output_yes_no /= OUTPUT_YES .and. output_yes_no /= OUTPUT_NO) then
    write(*,*) ""
    write(*,*) "Error. 'output_yes_no' must be '", trim(OUTPUT_YES), &
               "' or '", trim(OUTPUT_NO), "'"
    write(*,*) "You typed: '", trim(output_yes_no), "'"
    error_flag = 1_si
  end if

  !  4-d.

  if (test_yes_no /= TEST_YES .and. test_yes_no /= TEST_NO) then
    write(*,*) ""
    write(*,*) "Error. 'test_yes_no' must be '", trim(TEST_YES), &
               "' or '", trim(TEST_NO), "'"
    write(*,*) "You typed: '", trim(test_yes_no), "'"
    error_flag = 1_si
  end if

  !  4-e.

  if (stat_close_read /= STAT_CLOSE_KEEP .and. stat_close_read /= STAT_CLOSE_DELETE) then
    write(*,*) ""
    write(*,*) "Error. 'stat_close_read' must be '", trim(STAT_CLOSE_KEEP), &
               "' or '", trim(STAT_CLOSE_DELETE), "'"
    write(*,*) "You typed: '", trim(stat_close_read), "'"
    error_flag = 1_si
  end if

  !  4-f.

  if (error_flag == 1_si) then
    write(*,*) ""
    write(*,*) "Stop"
    write(*,*) ""
    stop  
  end if

  end subroutine error_check_matrix_read

!=========================================!
! 3. Subroutine: error_check_vector_write !
!=========================================!

  subroutine error_check_vector_write(file_system,      & !  1.
                                      type_of_build,    & !  2.
                                      num_files,        & !  3.
                                      output_yes_no,    & !  4.
                                      stat_open_write,  & !  5.
                                      stat_close_write)   !  6.

  use m_1_parameters, only: si, len_a,              & 
                            FILE_SYS_LU_ON,         &
                            FILE_SYS_LU_OFF,        & 
                            TYPE_OF_BUILD_VECTOR,   &
                            TYPE_OF_BUILD_FUNCTION, &
                            NUM_FILES_ONE,          &
                            NUM_FILES_MANY,         &
                            OUTPUT_YES,             &
                            OUTPUT_NO,              &
                            STAT_OPEN_NEW,          &
                            STAT_OPEN_REPLACE,      &
                            STAT_CLOSE_KEEP,        &
                            STAT_CLOSE_DELETE

  implicit none

  !  1. Interface variables 

  character(len=len_a), intent(in) :: file_system
  character(len=len_a), intent(in) :: type_of_build
  character(len=len_a), intent(in) :: num_files
  character(len=len_a), intent(in) :: output_yes_no
  character(len=len_a), intent(in) :: stat_open_write
  character(len=len_a), intent(in) :: stat_close_write

  !  2. Local variables

  integer(kind=si) :: error_flag

  !  3. Initialize local variables

  error_flag = 0_si

  !  4. Check for input errors

  !  4-a.

  if (file_system /= FILE_SYS_LU_ON .and. file_system /= FILE_SYS_LU_OFF) then
    write(*,*) ""
    write(*,*) "Error. 'file_system' must be '", trim(FILE_SYS_LU_ON), &
               "' or '", trim(FILE_SYS_LU_OFF), "'"
    write(*,*) "You typed: '", trim(file_system), "'"
    error_flag = 1_si
  end if

  !  4-b.

  if (type_of_build /= TYPE_OF_BUILD_VECTOR .and. type_of_build /= TYPE_OF_BUILD_FUNCTION) then
    write(*,*) ""
    write(*,*) "Error. 'type_of_build' must be '", trim(TYPE_OF_BUILD_VECTOR), &
               "' or '", trim(TYPE_OF_BUILD_FUNCTION), "'"
    write(*,*) "You typed: '", trim(type_of_build), "'"
    error_flag = 1_si
  end if

  !  4-c.

  if (num_files /= NUM_FILES_ONE .and. num_files /= NUM_FILES_MANY) then
    write(*,*) ""
    write(*,*) "Error. 'num_files' must be '", trim(NUM_FILES_ONE), &
               "' or '", trim(NUM_FILES_MANY), "'"
    write(*,*) "You typed: '", trim(num_files), "'"
    error_flag = 1_si
  end if

  !  4-d.

  if (output_yes_no /= OUTPUT_YES .and. output_yes_no /= OUTPUT_NO) then
    write(*,*) ""
    write(*,*) "Error. 'output_yes_no' must be '", trim(OUTPUT_YES), &
               "' or '", trim(OUTPUT_NO), "'"
    write(*,*) "You typed: '", trim(output_yes_no), "'"
    error_flag = 1_si
  end if

  !  4-e.

  if (stat_open_write /= STAT_OPEN_NEW .and. stat_open_write /= STAT_OPEN_REPLACE) then
    write(*,*) ""
    write(*,*) "Error. 'stat_open_write' must be '", trim(STAT_OPEN_NEW), &
               "' or '", trim(STAT_OPEN_REPLACE), "'"
    write(*,*) "You typed: '", trim(stat_open_write), "'"
    error_flag = 1_si
  end if

  !  4-f.

  if (stat_close_write /= STAT_CLOSE_KEEP .and. stat_close_write /= STAT_CLOSE_DELETE) then
    write(*,*) ""
    write(*,*) "Error. 'stat_close_write' must be '", trim(STAT_CLOSE_KEEP), &
               "' or '", trim(STAT_CLOSE_DELETE), "'"
    write(*,*) "You typed: '", trim(stat_close_write), "'"
    error_flag = 1_si
  end if

  !  4-g.

  if (error_flag == 1_si) then
    write(*,*) ""
    write(*,*) "Stop"
    write(*,*) ""
    stop  
  end if

  end subroutine error_check_vector_write

!========================================!
! 4. Subroutine: error_check_vector_read !
!========================================!

  subroutine error_check_vector_read(file_system,     & !  1.
                                     output_yes_no,   & !  2.
                                     test_yes_no,     & !  3.
                                     stat_close_read)   !  4.

  use m_1_parameters, only: si, len_a,        & 
                            FILE_SYS_LU_ON,   &
                            FILE_SYS_LU_OFF,  &
                            OUTPUT_YES,       &
                            OUTPUT_NO,        &
                            TEST_YES,         &
                            TEST_NO,          &
                            STAT_CLOSE_KEEP,  &
                            STAT_CLOSE_DELETE

  implicit none

  !  1. Interface variables 

  character(len=len_a), intent(in) :: file_system
  character(len=len_a), intent(in) :: output_yes_no
  character(len=len_a), intent(in) :: test_yes_no
  character(len=len_a), intent(in) :: stat_close_read

  !  2. Local variables

  integer(kind=si) :: error_flag

  !  3. Initialize local variables

  error_flag = 0_si

  !  4. Check for input errors

  !  4-a.

  if (file_system /= FILE_SYS_LU_ON .and. file_system /= FILE_SYS_LU_OFF) then
    write(*,*) ""
    write(*,*) "Error. 'file_system' must be '", trim(FILE_SYS_LU_ON), &
               "' or '", trim(FILE_SYS_LU_OFF), "'"
    write(*,*) "You typed: '", trim(file_system), "'"
    error_flag = 1_si
  end if

  !  4-b.

  if (output_yes_no /= OUTPUT_YES .and. output_yes_no /= OUTPUT_NO) then
    write(*,*) ""
    write(*,*) "Error. 'output_yes_no' must be '", trim(OUTPUT_YES), &
               "' or '", trim(OUTPUT_NO), "'"
    write(*,*) "You typed: '", trim(output_yes_no), "'"
    error_flag = 1_si
  end if

  !  4-c.

  if (test_yes_no /= TEST_YES .and. test_yes_no /= TEST_NO) then
    write(*,*) ""
    write(*,*) "Error. 'test_yes_no' must be '", trim(TEST_YES), &
               "' or '", trim(TEST_NO), "'"
    write(*,*) "You typed: '", trim(test_yes_no), "'"
    error_flag = 1_si
  end if

  !  4-d.

  if (stat_close_read /= STAT_CLOSE_KEEP .and. stat_close_read /= STAT_CLOSE_DELETE) then
    write(*,*) ""
    write(*,*) "Error. 'stat_close_read' must be '", trim(STAT_CLOSE_KEEP), &
               "' or '", trim(STAT_CLOSE_DELETE), "'"
    write(*,*) "You typed: '", trim(stat_close_read), "'"
    error_flag = 1_si
  end if

  !  4-e.

  if (error_flag == 1_si) then
    write(*,*) ""
    write(*,*) "Stop"
    write(*,*) ""
    stop  
  end if

  end subroutine error_check_vector_read

  end module m_8_error_check

!======!
! FINI !
!======!
