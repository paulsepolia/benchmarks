#!/bin/bash

# 1. compile

  gfortran                      \
  -std=f2008                    \
  -Wall                         \
  -O3                           \
  -fopenmp                      \
  -static                       \
  -static-libgfortran           \
  m_1_parameters.f90            \
  m_2_functions.f90             \
  m_3_build_via_function.f90    \
  m_4_write_openmp.f90          \
  m_5_write_subs.f90            \
  m_8_error_check.f90           \
  driver_matrix_dense_write.f90 \
  -liomp5                       \
  -o x_gf_w_mat

# 2. clean

  rm *.mod

# 3. exit
