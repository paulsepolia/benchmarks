!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/02/22              !
!===============================!

  module m_2_functions

  implicit none

  contains

!==============================!
! 1. Function: matrix_function !
!==============================!

  pure function matrix_function(i,j)
  
  use m_1_parameters, only: si, dr

  implicit none

  real(kind=dr)                :: matrix_function
  integer(kind=si), intent(in) :: i
  integer(kind=si), intent(in) :: j
 
    if (abs(i-j) < 50000_si) then
      matrix_function = real(i, kind=dr) + real(j, kind=dr)
    else if (abs(i-j) >= 50000_si) then
      matrix_function = 0.0_dr
    end if

  end function matrix_function

!==============================!
! 2. Function: vector_function !
!==============================!

  pure function vector_function(i)
  
  use m_1_parameters, only: di, dr

  implicit none

  real(kind=dr)                :: vector_function
  integer(kind=di), intent(in) :: i
 
  vector_function = real(i, kind=dr)

  end function vector_function

  end module m_2_functions

!======!
! FINI !
!======!
