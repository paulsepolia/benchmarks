!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/02/22              !
!===============================!

  program driver_matrix_dense_write

  use m_1_parameters,  only: si, dr, len_a
  use m_2_functions,   only: matrix_function
  use m_5_write_subs,  only: matrix_write_sub
  use m_8_error_check, only: error_check_matrix_write

  implicit none

  !  1. Interface variables

  integer(kind=si)                           :: m             ! rows
  integer(kind=si)                           :: n             ! columns
  real(kind=dr), allocatable, dimension(:,:) :: a             ! matrix
  character(len=len_a)                       :: file_system   ! "lustre_off", "lustre_on"
  character(len=len_a)                       :: file_name     ! file name for matrix
  character(len=len_a)                       :: out_format    ! "by_column", "by_row"
  character(len=len_a)                       :: num_files     ! "one", "many"
  character(len=len_a)                       :: type_of_build ! "via_matrix", "via_function"
  integer(kind=si)                           :: trials        ! times to write
  character(len=len_a)                       :: output_yes_no ! "yes", "no"
  character(len=len_a)                       :: stat_open     ! "new", "replace"
  character(len=len_a)                       :: stat_close    ! "keep", "delete"

!===================!
! Drivers interface !
!===================!

  !  2. Initialization of the interface variables

  m             = 7003_si
  n             = m
  file_name     = "matrix_direct"
  file_system   = "lustre_on"
  out_format    = "by_column"
  num_files     = "many"
  type_of_build = "via_function"
  trials        = 10_si
  output_yes_no = "yes"
  stat_open     = "replace"
  stat_close    = "keep"

!===========================!
! End of driver's interface !
!===========================!

  !  3. Check for input errors

  call error_check_matrix_write(file_system,   & !  1.
                                type_of_build, & !  2.
                                out_format,    & !  3.
                                num_files,     & !  4.
                                output_yes_no, & !  5.
                                stat_open,     & !  6.
                                stat_close)      !  7.

  !  4. Matrix OpenMP write

  call matrix_write_sub(m,               & !  1.
                        n,               & !  2.
                        a,               & !  3.
                        matrix_function, & !  4.
                        file_system,     & !  5.
                        file_name,       & !  6.
                        type_of_build,   & !  7.
                        out_format,      & !  8.
                        num_files,       & !  9.
                        trials,          & ! 10.
                        output_yes_no,   & ! 11.
                        stat_open,       & ! 12.
                        stat_close)        ! 13.

  end program driver_matrix_dense_write

!======!
! FINI !
!======!
