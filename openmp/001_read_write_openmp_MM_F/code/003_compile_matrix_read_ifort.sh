#!/bin/bash

# 1. compile

  ifort                        \
  -e03                         \
  -warn all                    \
  -O3                          \
  -qopenmp                     \
  -parallel                    \
  -par-threshold0              \
  -static                      \
  -assume buffered_io          \
  m_1_parameters.f90           \
  m_2_functions.f90            \
  m_3_build_via_function.f90   \
  m_6_read_openmp.f90          \
  m_7_read_subs.f90            \
  m_8_error_check.f90          \
  driver_matrix_dense_read.f90 \
  -o x_ifort_r_mat

# 2. clean

  rm *.mod

# 3. exit
