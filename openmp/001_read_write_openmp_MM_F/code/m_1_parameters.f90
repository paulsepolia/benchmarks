!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/02/22              !
!===============================!

  module m_1_parameters

  implicit none
 
!===========================================!
! 1. Common type definitions and parameters !
!===========================================!

  integer, parameter              :: dr                     = selected_real_kind(p=15,r=300)
  integer, parameter              :: sr                     = selected_real_kind(p=6,r=30)
  integer, parameter              :: di                     = selected_int_kind(18)
  integer, parameter              :: si                     = selected_int_kind(9)
  integer, parameter              :: len_a                  = 300_si
  character(len=len_a), parameter :: FILE_SYS_LU_ON         = "lustre_on"
  character(len=len_a), parameter :: FILE_SYS_LU_OFF        = "lustre_off"
  character(len=len_a), parameter :: OUT_FORMAT_COL         = "by_column"
  character(len=len_a), parameter :: OUT_FORMAT_ROW         = "by_row"
  character(len=len_a), parameter :: NUM_FILES_ONE          = "one"
  character(len=len_a), parameter :: NUM_FILES_MANY         = "many"
  character(len=len_a), parameter :: TYPE_OF_BUILD_MATRIX   = "via_matrix"
  character(len=len_a), parameter :: TYPE_OF_BUILD_FUNCTION = "via_function"
  character(len=len_a), parameter :: TYPE_OF_BUILD_VECTOR   = "via_vector"
  character(len=len_a), parameter :: OUTPUT_YES             = "yes"
  character(len=len_a), parameter :: OUTPUT_NO              = "no"
  character(len=len_a), parameter :: TEST_YES               = "yes"
  character(len=len_a), parameter :: TEST_NO                = "no"
  character(len=len_a), parameter :: STAT_OPEN_NEW          = "new"
  character(len=len_a), parameter :: STAT_OPEN_REPLACE      = "replace"
  character(len=len_a), parameter :: STAT_CLOSE_KEEP        = "keep"
  character(len=len_a), parameter :: STAT_CLOSE_DELETE      = "delete"

  end module m_1_parameters

!======!
! FINI !
!======!
