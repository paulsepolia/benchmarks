!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/02/22              !
!===============================!

  program driver_vector_dense_read_write

  use m_1_parameters,  only: si, di, dr, len_a
  use m_2_functions,   only: vector_function
  use m_5_write_subs,  only: vector_write_sub
  use m_7_read_subs,   only: vector_read_sub
  use m_8_error_check, only: error_check_vector_write, &
                             error_check_vector_read

  implicit none

  !  1. Interface variables

  integer(kind=di)                         :: n                   ! rows
  real(kind=dr), allocatable, dimension(:) :: w                   ! matrix
  character(len=len_a)                     :: file_system_write   ! "lustre_off", "lustre_on"
  character(len=len_a)                     :: file_system_read    ! "luster_off", "lustre_on" 
  character(len=len_a)                     :: file_name_write     ! file name for vector
  character(len=len_a)                     :: file_name_read      ! head of vector file name 
  character(len=len_a)                     :: num_files_write     ! "one", "many"
  integer(kind=si)                         :: num_files_read      ! 1,2,3,..
  character(len=len_a)                     :: type_of_build_write ! "via_vector", "via_function"
  integer(kind=si)                         :: trials_write        ! times to write
  integer(kind=si)                         :: trials_read         ! times to read
  character(len=len_a)                     :: output_yes_no_write ! "yes", "no"
  character(len=len_a)                     :: output_yes_no_read  ! "yes", "no"
  character(len=len_a)                     :: test_yes_no_read    ! "yes", "no"
  character(len=len_a)                     :: stat_open_write     ! "new", "replace"
  character(len=len_a)                     :: stat_close_write    ! "keep", "delete"
  character(len=len_a)                     :: stat_close_read     ! "keep", "delete"

  !  2. Local variables

  integer(kind=si)                :: i
  integer(kind=si), parameter     :: MAX_RW        = 5_si
  character(len=len_a), parameter :: output_driver = "yes"

!===================!
! Drivers interface !
!===================!

  !  2. Initialization of the interface variables

  n                   = 20010003_di
  file_name_write     = "vector_direct"
  file_name_read      = "vector_direct"
  file_system_write   = "lustre_on"
  file_system_read    = "lustre_on"
  num_files_write     = "many"
  num_files_read      = 4
  type_of_build_write = "via_function"
  trials_write        = 3
  trials_read         = 3
  output_yes_no_write = "yes"
  output_yes_no_read  = "yes"
  test_yes_no_read    = "yes"
  stat_open_write     = "replace"
  stat_close_write    = "keep"
  stat_close_read     = "keep"

!===========================!
! End of driver's interface !
!===========================!

  !  3. Check for input errors - write case 

  call error_check_vector_write(file_system_write,   & !  1.
                                type_of_build_write, & !  2.
                                num_files_write,     & !  3.
                                output_yes_no_write, & !  4.
                                stat_open_write,     & !  5.
                                stat_close_write)      !  6.

  !  4. Check for input errors - read case

  call error_check_vector_read(file_system_read,   & !  1.
                               output_yes_no_read, & !  2.
                               test_yes_no_read,   & !  3.
                               stat_close_read)      !  4.

  do i = 1, MAX_RW

    !  5. Counter
 
    if (output_driver == "yes") then
      write(*,*) "<<===================================================>>"
      write(*,*) "    Benchmark Countdown ----------> ", MAX_RW-i+1_si
      write(*,*) "<<===================================================>>"
    end if

    !  6. Vector OpenMP write

    call vector_write_sub(n,                   & !  1.
                          w,                   & !  2.
                          vector_function,     & !  3.
                          file_system_write,   & !  4.
                          file_name_write,     & !  5.
                          type_of_build_write, & !  6.
                          num_files_write,     & !  7.
                          trials_write,        & !  8.
                          output_yes_no_write, & !  9.
                          stat_open_write,     & ! 10.
                          stat_close_write)      ! 11.

    !  7. Vector OpenMP read

    call vector_read_sub(n,                  & !  1.
                         num_files_read,     & !  2.
                         w,                  & !  3.
                         file_system_read,   & !  4.
                         file_name_read,     & !  5.
                         trials_read,        & !  6.
                         output_yes_no_read, & !  7.
                         test_yes_no_read,   & !  8.
                         stat_close_read)      !  9.

    !  8. Local deallocation

    deallocate(w)
 
  end do

  end program driver_vector_dense_read_write

!======!
! FINI !
!======!
