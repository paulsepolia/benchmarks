#!/bin/bash

# 1. compile

  ifort -e03                     \
        -warn all                \
        -O3                      \
        -qopenmp                 \
        -cpp                     \
        m_1_type_definitions.f90 \
        driver_program.f90       \
        -mkl=parallel            \
        -o x_intel_mkl

# 2. clean

  rm *.mod
