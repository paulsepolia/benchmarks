#!/bin/bash

# 1. compile

  gfortran     -std=f2008               \
               -Wall                    \
               -O3                      \
               -cpp                     \
               -fopenmp                 \
               m_1_type_definitions.f90 \
               driver_program.f90       \
               -fexternal-blas          \
               -L/opt/nintel/mkl/lib/intel64                                     \
               -Wl,-R/opt/intel/mkl/lib/intel64  -lmkl_lapack95_lp64             \
               -lmkl_intel_lp64 -lmkl_gnu_thread -lmkl_core -lmkl_sequential -lm \
               -o x_gnu_mkl

# 2. clean

  rm *.mod

# 3. exit
