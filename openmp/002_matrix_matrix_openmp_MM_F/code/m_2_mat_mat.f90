!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/09/20              !
!===============================!

  module m_2_mat_mat
  
  use m_1_type_definitions
#ifdef _OPENMP
  use omp_lib
#endif

  implicit none
  
  contains

  !===================!
  ! 1. fortran_matmat !
  !===================!

  subroutine fortran_matmat(mat1, mat2, matResta, dimen)

  real(kind=dr), allocatable, dimension(:,:), intent(in)    :: mat1
  real(kind=dr), allocatable, dimension(:,:), intent(in)    :: mat2
  real(kind=dr), allocatable, dimension(:,:), intent(inout) :: matResta
  integer(kind=si), intent(in)                              :: dimen 

  !$omp parallel         &
  !$omp default(none)    &
  !$omp shared(matResta) &
  !$omp shared(mat1)     &
  !$omp shared(mat2)     &
  !$omp shared(dimen)
  !$omp workshare
 
  matResta(1:dimen,1:dimen) = &
    matmul(mat1(1:dimen,1:dimen), mat2(1:dimen,1:dimen))
 
  !$omp end workshare
  !$omp end parallel 

  end subroutine fortran_matmat

  !==============!
  ! 2. my_matmat !
  !==============!

  subroutine my_matmat(mat1, mat2, matResta, dimen)

  integer(kind=si), intent(in) :: dimen
  real(kind=dr), allocatable, dimension(:,:), intent(in)    :: mat1
  real(kind=dr), allocatable, dimension(:,:), intent(in)    :: mat2
  real(kind=dr), allocatable, dimension(:,:), intent(inout) :: matResta
  integer(kind=si) :: i
  integer(kind=si) :: j 
  integer(kind=si) :: k
  real(kind=dr)    :: tmp

  !$omp parallel         &
  !$omp default(none)    &
  !$omp shared(dimen)    &
  !$omp shared(mat1)     &
  !$omp shared(mat2)     &
  !$omp shared(matResta)
 

  !$omp do            &
  !$omp private(i)    &
  !$omp private(j)    &
  !$omp private(k)    &
  !$omp private(tmp)

  do i = 1, dimen
    do j = 1, dimen

      tmp = 0.0_dr

      do k = 1, dimen

        tmp = tmp + mat1(j,k) * mat2(i,k)

      end do

      matResta(j,i) = tmp

    end do
  end do

  !$omp end do
  !$omp end parallel

  end subroutine my_matmat

  end module m_2_mat_mat

!======!
! FINI !
!======!
