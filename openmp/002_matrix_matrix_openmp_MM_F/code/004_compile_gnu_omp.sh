#!/bin/bash

# 1. compile

  gfortran     -std=f2008               \
               -Wall                    \
               -O3                      \
               -fopenmp                 \
               -static-libgfortran      \
               -cpp                     \
               m_1_type_definitions.f90 \
               m_2_mat_mat.f90          \
               driver_program.f90       \
               -o x_gnu_omp

# 2. clean

  rm *.mod

# 3. exit
