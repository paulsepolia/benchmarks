!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/09/20              !
!===============================!

  program driver_program

  use m_1_type_definitions
  use m_2_mat_mat
#ifdef _OPENMP
  use omp_lib
#endif

  implicit none

  !=======================!
  !  1a. Interface starts !
  !=======================! 

  integer(kind=si), parameter :: DIMEN = 2000_si

  !=====================!
  !  1b. Interface ends !
  !=====================!

  !  2. local variables

  real(kind=dr), allocatable, dimension(:,:) :: mat1
  real(kind=dr), allocatable, dimension(:,:) :: mat2
  real(kind=dr), allocatable, dimension(:,:) :: matResta
  integer(kind=si) :: i
  integer(kind=si) :: j
  real(kind=dr)    :: t1
  real(kind=dr)    :: t2

  !  3. some output

  write(*,*) ""
  write(*,*) "  1 --> Benchmark type: OpenMP"
  write(*,*) "  2 --> Title: Matrix - Matrix Product"
  write(*,*) "  3 --> RAM needed: ", 3.0*(DIMEN/1024.0_dr**3)*8*DIMEN, " GBytes"
  write(*,*) "  4 --> Please wait ..."

  !  4. allocate

  allocate(mat1(1:DIMEN,1:DIMEN))
  allocate(mat2(1:DIMEN,1:DIMEN))
  allocate(matResta(1:DIMEN,1:DIMEN))

  !  5. build the matrices

  !  5.a

  !$omp parallel      &
  !$omp default(none) &
  !$omp shared(mat1)

  !$omp do         &
  !$omp private(i) &
  !$omp private(j)

  do i = 1, DIMEN
    do j = 1, DIMEN

      mat1(j,i) = dsin(real(i,kind=dr)*real(j,kind=dr))

    end do
  end do

  !$omp end do
  !$omp end parallel

  !  5.b

  !$omp parallel      &
  !$omp default(none) &
  !$omp shared(mat2)  

  !$omp do         &
  !$omp private(i) &
  !$omp private(j)

  do i = 1, DIMEN
    do j = 1, DIMEN

      mat2(j,i) = dcos(real(i,kind=dr)*real(j,kind=dr))

    end do
  end do

  !$omp end do
  !$omp end parallel

  !  6. call the "fortran_matmat" subroutine

  call cpu_time(t1)

  call fortran_matmat(mat1, mat2, matResta, dimen)

  call cpu_time(t2)

  !  7. output the results

  write(*,*) "  5 --> The Results are the following:"
  write(*,*) "  6 --> Total CPU time for matmul (seconds)    = ", (t2-t1) 
  write(*,*) "  7 --> Fortran product: matResta(1,1)         = ", matResta(1,1)
  write(*,*) "  8 --> Fortran product: matResta(2,2)         = ", matResta(2,2)

  !  8. call the hand made matrix-matrix multiplication

  call cpu_Time(t1)

  call my_matmat(mat1, mat2, matResta, DIMEN)

  call cpu_Time(t2)

  !  9. output the results

  write(*,*) "  9 --> Total CPU time for my_matmat (seconds) = ", (t2-t1)
  write(*,*) " 10 --> my_matmat: matResta(1,1)               = ", matResta(1,1)
  write(*,*) " 11 --> my_matmat: matResta(2,2)               = ", matResta(2,2)
  write(*,*) ""

  end program driver_program

!======!
! FINI !
!======!
