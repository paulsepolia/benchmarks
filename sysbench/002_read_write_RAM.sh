#!/bin/bash

COUNTER=0
DIM=1000000
while [  $COUNTER -lt $DIM ]; do

	echo "--------------------------------------------------->>" $COUNTER

	sysbench --test=memory --memory-block-size=1K --memory-scope=global \
			--memory-total-size=10G --memory-oper=read run

	let COUNTER=COUNTER+1
done
