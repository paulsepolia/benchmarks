#!/bin/bash

COUNTER=0
DIM=10000
while [  $COUNTER -lt $DIM ]; do

	echo "--------------------------------------------------->>" $COUNTER

	sysbench --test=fileio --file-total-size=24G --file-test-mode=rndwr --max-time=100 \
			--max-requests=0 --file-block-size=4K --file-num=64 --num-threads=4 run
		
	let COUNTER=COUNTER+1
	rm test_file.*
done
