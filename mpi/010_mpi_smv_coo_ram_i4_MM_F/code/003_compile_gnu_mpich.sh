#!/bin/bash

  # 1. compile

  mpif90.mpich  -O3                      \
                -std=f2008               \
                -Wall                    \
                m_1_type_definitions.f90 \
                driver_program.f90       \
                -o x_gnu_mpich

  # 2. clean

  rm *.mod
