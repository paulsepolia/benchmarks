#!/bin/bash

  # 1. compile

  mpiifort -e03                     \
           -warn all                \
           -O3                      \
           -static_mpi              \
           m_1_type_definitions.f90 \
           driver_program.f90       \
           -o x_intel

  # 2. clean

  rm *.mod
