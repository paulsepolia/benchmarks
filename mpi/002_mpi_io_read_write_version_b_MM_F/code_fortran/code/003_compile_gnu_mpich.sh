#!/bin/bash

# 1. compiling

  mpif90.mpich  -O3                          \
                -Wall                        \
                -std=f2008                   \
                m_1_type_definitions.f90     \
                driver_read_write_mpi_v2.f90 \
                -o x_gnu_mpich

# 2. cleaning 

  rm *.mod
