#!/bin/bash

  # 1. compile

  mpiifort -e08                           \
           -warn all                      \
           -O3                            \
           -static-intel                  \
           -static_mpi                    \
           m_1_type_definitions.f90       \
           m_2_disk_direct_to_ram.f90     \
           m_3_smv_coo_half_mpi.f90       \
           driver_program.f90             \
           /opt/parpack/lib/libparpack_intel_ext.a \
           /opt/parpack/lib/libarpack_intel_ext.a  \
           -mkl=cluster                   \
           -o x_intel_mkl

  # 2. clean

  rm *.mod
