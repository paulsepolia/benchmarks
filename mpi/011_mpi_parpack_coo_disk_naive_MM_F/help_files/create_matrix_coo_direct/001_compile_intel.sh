#!/bin/bash

# 1. compile

  ifort  -e08                     \
         -warn all                \
         -xHost                   \
         -static                  \
         -assume buffered_io      \
         m_1_type_definitions.f90 \
         driver_coo_direct.f90    \
         -o x_intel_coo

# 2. clean

  rm *.mod
