// MPI Benchmark 4.

// 1. An MPI vector-vector product.

// 2. The allocation and building of the 2 vectors
//    is done only in master mpi thread ( rank = 0 ).

// 3. I use the blocking MPI_Send and MPI_Recv commands
//    for broadcasting the elements of each vector
//    to the rest of mpi threads.

// 4. The limitation is that the lenght of the vector
//    must be divisible with p ( number of mpi threads ).

// 5. All the started up mpi threads do the vector-vector product.
//    Each one its own part.

// 6. I use the MPI_Reduce function to collect and sum all
//    the local sums.

// 7. MPI function in use:

//    MPI_Init( ... )
//    MPI_Comm_size( ... )
//    MPI_Comm_rank( ... )
//    MPI_Send( ... )
//    MPI_Recv( ... )
//    MPI_Reduce( ... )

// 8. MPI type declarations in use:

//    MPI_Status

#include "mpi.h"
#include <iostream>
#include <cmath>
#include <ctime>
#include <iomanip>

using namespace std;

// main function
int main( int argc, char** argv )
{
    // 1. mpi start up
    MPI_Init( & argc, & argv );

    // 2. get my process rank
    int my_rank;
    MPI_Comm_rank( MPI_COMM_WORLD, & my_rank );

    // 3. find out how many mpi processes are being used
    int p;
    MPI_Comm_size( MPI_COMM_WORLD, & p );

    // 4. setting the dimension of the vectors
    long int dimen = 1 * 1024 * static_cast<long int>( pow(10.0, 5.0) );
    long int dimen_small = dimen / p;

    // 5. interface
    int sentinel;
    if ( my_rank == 0 ) {
        cout << "  1. Enter an integer to start the mpi benchmark : ";
        cin  >> sentinel;
        cout << "  2. Please wait while the mpi benchmark executes ... " << endl;
    }

    // 6. allocating and building the vectors
    //    vectors for rank 0
    double * vector_1;
    double * vector_2;

    vector_1 = new double [ dimen ];
    vector_2 = new double [ dimen ];

    if ( my_rank == 0 ) {
        for ( long int i = 0; i < dimen; i++ ) {
            vector_1[ i ] = cos(sin(cos( static_cast<double>(i+1) )));
            vector_2[ i ] = sin(cos(sin( static_cast<double>(i+1) )));
        }
    }

    // 7. local vectors for the rest ranks
    double * vector_1_local = new double [ dimen_small ];
    double * vector_2_local = new double [ dimen_small ];

    // 8. sending parts of the vectors
    //    from the master thread to the rest mpi threads

    // 9. some variables
    int tag_1 = 40;
    int tag_2 = 41;
    long int i;
    MPI_Status status;

    // 10. send and receive - vector_1
    if ( my_rank == 0 ) {
        for ( i = 1; i < p; i++ ) {
            MPI_Send( vector_1 + dimen_small * i, dimen_small,
                      MPI_DOUBLE, i, tag_1,
                      MPI_COMM_WORLD );
        }
    } else {
        MPI_Recv( vector_1_local, dimen_small,
                  MPI_DOUBLE, 0, tag_1,
                  MPI_COMM_WORLD, & status );
    }

    // 11. send and receive - vector_2
    if ( my_rank == 0 ) {
        for ( i = 1; i < p; i++ ) {
            MPI_Send( vector_2 + dimen_small * i,
                      dimen_small, MPI_DOUBLE,
                      i, tag_2, MPI_COMM_WORLD );
        }
    } else {
        MPI_Recv( vector_2_local, dimen_small,
                  MPI_DOUBLE, 0, tag_2,
                  MPI_COMM_WORLD, & status );
    }

    // 12. do the dot product.

    // 13. timing
    time_t t1;
    if ( my_rank == 0 ) {
        t1 = clock();
    }

    double t1_mpi;
    t1_mpi = MPI_Wtime();

    // 14. some variables
    double sum;
    sum = 0.0;
    long int k ;
    long int kmax = 1 * static_cast<long int>( pow(10.0, 3.0));

    // 15. main dot product - is done kmax times
    for ( k = 1; k <= kmax; k++ ) {
        double local_sum = 0.0;

        if ( my_rank == 0 ) {
            for ( i = 0; i < dimen_small; i++ ) {
                local_sum = local_sum + vector_1[i] * vector_2[i];
            }
        }

        if ( my_rank != 0 ) {
            for ( i = 0; i < dimen_small; i++ ) {
                local_sum = local_sum + vector_1_local[i] * vector_2_local[i];
            }
        }

        // 16. mpi_reduce
        sum = 0.0;
        MPI_Reduce( & local_sum, & sum, 1, MPI_DOUBLE,
                    MPI_SUM, 0, MPI_COMM_WORLD );
    }

    // 17. timing
    time_t t2;
    if ( my_rank == 0 ) {
        t2 = clock();
    }

    double t2_mpi;
    t2_mpi = MPI_Wtime();

    // 18. the results
    MPI_Barrier( MPI_COMM_WORLD );

    if( my_rank == 0 ) {
        cout << setprecision(15) << fixed;
        cout << "  3. The sum                             is " << sum << endl;
        cout << "  4. The size of each of the two vectors is "
             << dimen * 8.0 / pow(1024.0,3.0) << " gigabytes. " << endl;
        cout << "  5. The real time used ( rank 0 )       is " << 1.0 * (t2-t1)/CLOCKS_PER_SEC << " seconds. " << endl;
        cout << "  6. The real time used ( mpi )          is " << (t2_mpi-t1_mpi) << " seconds. " << endl;
        cout << scientific;
        cout << "  7. We have " << kmax / (t2_mpi-t1_mpi) << " vector-vector products per second." <<  endl;
        cout << "  8. Enter an integer to exit : ";
        cin  >> sentinel;
    }

    // 19. mpi shut down
    MPI_Finalize( );

    // 20. exiting.
    return 0;
}
