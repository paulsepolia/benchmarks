
// MPI Benchmark 1.

// 1. I allocate ram space for a vector.
//    This is done for every mpi thread.
// 2. I define the vector only in mpi thread 0.
//    So I occupy ram only concerning mpi thread 0.
// 3. I broadcast the above ram/vector
//    to any other mpi existing thread.
// 4. I am using only the mpi function:
//
//    MPI_Init ( ... )
//    MPI_Comm_rank ( ... )
//    MPI_Comm_size ( ... )
//    MPI_Bcast ( ... )
//    MPI_Finalize( ... )

#include "mpi.h"
#include <iomanip>
#include <iostream>
#include <cmath>
#include <ctime>

using namespace std;

// main function
int main( int argc, char** argv )
{
    // 1. mpi start up
    MPI_Init( & argc, & argv );

    // 2. get my process rank
    int my_rank;
    MPI_Comm_rank( MPI_COMM_WORLD, & my_rank );

    // 3. find out how many mpi processes are being used
    int p;
    MPI_Comm_size( MPI_COMM_WORLD, & p );

    // 4. setting the dimension of the vectors
    const long int dimen = 2 * 1024 * static_cast<long int>( pow(10.0, 5.0) );

    // 5. interface
    int sentinel;

    if ( my_rank == 0 ) {
        cout << "  1. Enter an integer to start the mpi benchmark : ";
        cin  >> sentinel;
        cout << "  2. Please wait while the mpi executes ... " << endl;
    }

    // 6. building the vector(s)
    double * vector = new double [ dimen ];

    if ( my_rank == 0 ) {
        for ( long int i = 0; i < dimen; i++ ) {
            vector[ i ] = cos(sin(cos( static_cast<double>(i+1) )));
        }
    }

    // 7. timing
    time_t t1;
    if ( my_rank == 0 ) {
        t1 = clock();
    }
    double t1_mpi;
    t1_mpi = MPI_Wtime();

    // 8. looping and broadcasting
    long int k;
    const long int kmax = 5 * static_cast<long int>( pow(10.0, 1.0) );

    for ( k = 1; k <= kmax; k++ ) {
        MPI_Bcast( vector, dimen, MPI_DOUBLE, 0, MPI_COMM_WORLD );
        if ( my_rank != 0 && k != kmax ) {
            delete [] vector;
            double * vector = new double [ dimen ];
        }
    }

    // 9. timing
    time_t t2;
    if ( my_rank == 0 ) {
        t2 = clock();
    }
    double t2_mpi;
    t2_mpi = MPI_Wtime();

    // 10. put a mpi barrier
    MPI_Barrier( MPI_COMM_WORLD );

    if ( my_rank == 1 ) {
        cout << setprecision(15) << fixed;
        cout << " 7-1. The vector(0)       in rank 1  is " << vector[0] << endl;
        cout << " 8-1. The vector(dimen-1) in rank 1  is " << vector[dimen-1] << endl;
    }

    // 11. put a mpi barrier
    MPI_Barrier( MPI_COMM_WORLD );

    if ( my_rank == 2 ) {
        cout << setprecision(15) << fixed;
        cout << " 7-2. The vector(0)       in rank 2  is " << vector[0] << endl;
        cout << " 8-2. The vector(dimen-1) in rank 2  is " << vector[dimen-1] << endl;
    }

    // 12. put a mpi barrier
    MPI_Barrier( MPI_COMM_WORLD );

    // 13. sentineling - just to observe the ram
    if ( my_rank == 0 ) {
        cout << setprecision(15) << fixed;
        cout << "  3. Total real time used ( rank 0 ) is " << 1.0 * (t2-t1)/ CLOCKS_PER_SEC << " seconds. " <<  endl;
        cout << "  4. Total real time used ( mpi )    is " << (t2_mpi-t1_mpi) << " seconds. " << endl;
        cout << "  5. The broadcasted vector size     is " << dimen*8/(pow(1024.0,3.0)) << " gigabytes. " << endl;
        cout << scientific;
        cout << "  6. The broadcasts per second       is "
             << kmax * p / (t2_mpi-t1_mpi) << endl;
        cout << setprecision(15) << fixed;
        cout << "  7-0. The vector(1) in rank 0       is " << vector[0] << endl;
        cout << "  8-0. The vector(dimen-1) in rank 0 is " << vector[dimen-1] << endl;
        cout << "  9. Enter an integer to exit : ";
        cin  >> sentinel;
    }

    // 14. mpi shut down
    MPI_Finalize( );

    // 15. exiting.
    return 0;
}
