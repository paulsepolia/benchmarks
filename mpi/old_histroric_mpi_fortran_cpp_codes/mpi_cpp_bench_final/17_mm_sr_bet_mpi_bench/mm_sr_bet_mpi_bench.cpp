
// MPI Benchmark 17.

// 1. An MPI matrix-matrix product.

// 2. The allocation and building of the two matrices
//    is done only in master mpi thread ( rank = 0 ).

// 3. I use the blocking MPI_Send and MPI_Recv commands
//    for broadcasting parts of the matrix and the whole vector
//    to the rest of mpi threads.

// 4. The limitation is that the number of 1st matrix rows
//    must be divisible by p ( number of mpi threads)
//    and the number of 2nd matrix columns
//    must be divisible by p ( number of mpi threads ).

// 5. All the started up mpi threads do the matrix-matrix product.
//    Each one its own part.

// 6. I do not use the MPI_Reduce function to collect and sum all
//    the local sums.

// 7. MPI function in use :

//    MPI_Init( ... )
//    MPI_Comm_size( ... )
//    MPI_Comm_rank( ... )
//    MPI_Send( ... )
//    MPI_Recv( ... )

// 8. MPI type declarations in use :

//    MPI_Status

#include "mpi.h"
#include <iostream>
#include <cmath>
#include <ctime>
#include <iomanip>

using namespace std;

// main function
int main( int argc, char** argv )
{
    // 1. mpi start up
    MPI_Init( & argc, & argv );

    // 2. get my process rank
    int my_rank;
    MPI_Comm_rank( MPI_COMM_WORLD, & my_rank );

    // 3. find out how many mpi processes are being used
    int p;
    MPI_Comm_size( MPI_COMM_WORLD, & p );

    // 4. setting the dimension of the vectors
    long int dimen = 400 * static_cast<long int>( pow(10.0, 1.0) );
    long int dimen_small = dimen / p;
    long int i,j,k; // common loop variables // ONLY here

    // 5. interface
    int sentinel;

    if ( my_rank == 0 ) {
        cout << "  1. Enter an integer to start the mpi benchmark : ";
        cin  >> sentinel;
        cout << "  2. Please wait while the mpi benchmark executes ... " << endl;
    }

    // 6. allocating of the 1st matrix
    double ** matrix_1 = new double * [ dimen ];
    for ( i = 0; i < dimen; i++ ) {
        matrix_1[i] = new double [ dimen ];
    }

    // 7. definition of 1st matrix for rank 0
    if ( my_rank == 0 ) {
        for( i = 0; i < dimen; i++ ) {
            for ( j = 0; j < dimen; j++ ) {
                matrix_1[i][j] =  cos(sin(cos( i + j + 2 )));
            }
        }
    }

    // 6. setting up the tags
    double * tag = new double [ p ];
    for ( k = 0; k < p; k++ ) {
        tag[k] = k + 10 ;
    }

    MPI_Status status;

    // 7. 1st local matrix for the rest of the mpi threads
    double ** matrix_local_1 = new double * [ dimen ];
    for ( i = 0; i < dimen; i++ ) {
        matrix_local_1[i] = new double [ dimen_small ];
    }

    // 8. send parts of the 1st matrix to the rest of the mpi threads
    if ( my_rank == 0 ) {
        // 9. define the small matrix for rank 0
        for ( j = 0; j < dimen_small; j++ ) {
            for ( i = 0; i < dimen; i++ ) {
                matrix_local_1[i][j] = matrix_1[i][j];
            }
        }
        // 10. define the small matrix for the rest of the mpi threads
        for ( k = 1; k < p; k++ ) {
            for ( i = 0; i < dimen; i++ ) {
                MPI_Send( matrix_1[i] + dimen_small * k, dimen_small, MPI_DOUBLE, k, tag[k], MPI_COMM_WORLD );
            }
        }
    }

    // 11. receive the sent parts of the 1st matrix
    for ( int k = 1; k < p; k++ ) {
        if ( my_rank == k ) {
            for ( i = 0; i < dimen; i++ ) {
                MPI_Recv( matrix_local_1[i], dimen, MPI_DOUBLE, 0, tag[k], MPI_COMM_WORLD, & status );
            }
        }
    }

    // 12. deleting the matrix_1
    for ( i = 0; i < dimen; i++ ) {
        delete [] matrix_1[i];
    }

    delete [] matrix_1;

    // 13. allocating of the 2nd matrix
    double ** matrix_2 = new double * [ dimen ];
    for ( i = 0; i < dimen; i++ ) {
        matrix_2[i] = new double [ dimen ];
    }

    // 14. definition of 2nd matrix for rank 0
    if ( my_rank == 0 ) {
        for( i = 0; i < dimen; i++ ) {
            for ( j = 0; j < dimen; j++ ) {
                matrix_2[i][j] = cos(sin(cos( i + j + 2 )));
            }
        }
    }

    // 15. 2nd local matrix for the rest of the mpi threads
    double ** matrix_local_2 = new double * [ dimen ];
    for ( i = 0; i < dimen; i++ ) {
        matrix_local_2[i] = new double [ dimen_small ];
    }

    // 16. send parts of the 2nd matrix to the rest of the mpi threads
    if ( my_rank == 0 ) {
        // 17. define the small matrix for rank 0
        for ( j = 0; j < dimen_small; j++ ) {
            for ( i = 0; i < dimen; i++ ) {
                matrix_local_2[i][j] = matrix_2[i][j];
            }
        }
        // 18. define the small matrix for the rest of the mpi threads
        for ( k = 1; k < p; k++ ) {
            for ( i = 0; i < dimen; i++ ) {
                MPI_Send( matrix_2[i] + dimen_small * k, dimen_small, MPI_DOUBLE, k, tag[k], MPI_COMM_WORLD );
            }
        }
    }

    // 19. receive the sent parts of the 2nd matrix
    for ( k = 1; k < p; k++ ) {
        if ( my_rank == k ) {
            for ( i = 0; i < dimen; i++ ) {
                MPI_Recv( matrix_local_2[i], dimen, MPI_DOUBLE, 0, tag[k], MPI_COMM_WORLD, & status );
            }
        }
    }

    // 20. deleting the matrix_2
    for ( i = 0; i < dimen; i++ ) {
        delete [] matrix_2[i];
    }

    delete [] matrix_2;

    // 21. timing - part a
    time_t t1;
    if ( my_rank == 0 ) {
        t1 = clock();
    }

    double t1_mpi;
    t1_mpi = MPI_Wtime();

    // 22. the main matrix-matrix product - benchmarking
    double sum_local;
    const long double Lmax = 1 * pow(10.0, 0.0); // benchmark variable
    long double L;

    // 23. allocating of the resultant matrix
    double ** matrix_res = new double * [ dimen ];
    for ( i = 0; i < dimen; i++ ) {
        matrix_res[i] = new double [ dimen ];
    }

    for ( L = 1; L <= Lmax; L++ ) { // 24. benchmarking loop
        for ( j = 0; j < dimen_small; j++ ) {
            for ( i = 0; i < dimen_small; i++ ) {
                sum_local = 0.0;
                for ( k = 0; k < dimen; k++ ) {
                    sum_local = sum_local + matrix_local_1[k][i] * matrix_local_2[k][j];
                    matrix_res[i][j] = sum_local;
                }
            }
        }
    } // 25. benchmarking loop

    // 26. timing - part b
    time_t t2;
    if ( my_rank == 0 ) {
        t2 = clock();
    }

    double t2_mpi;
    t2_mpi = MPI_Wtime();

    // 27. matrix_local_1 and matrix_local_2
    for ( i = 0; i < dimen; i++ ) {
        delete [] matrix_local_1[i];
        delete [] matrix_local_2[i];
    }

    delete [] matrix_local_1;
    delete [] matrix_local_2;

    // 28. the results
    MPI_Barrier( MPI_COMM_WORLD );

    if( my_rank == 0 ) {
        cout << setprecision(8) << fixed;
        cout << "  3. The matrix_result[1][3]       is " << matrix_res[1][3] << endl;
        cout << "  4. The matrix_result[2][1]       is " << matrix_res[2][1] << endl;
        cout << "  5. The matrix_result[1][2]       is " << matrix_res[1][2] << endl;
        cout << "  6. The matrix_result[3][1]       is " << matrix_res[3][1] << endl;
        cout << "  7. The real time used ( rank 0 ) is " << 1.0 * (t2-t1)/CLOCKS_PER_SEC << " seconds. " << endl;
        cout << "  8. The real time used ( mpi )    is " << (t2_mpi-t1_mpi) << " seconds. " << endl;
        cout << "  9. The size of each matrix       is " << pow( dimen, 2.0) * 8.0 / pow(1024.0,3.0)
             << " gigabytes. " << endl;
        cout << scientific;
        cout << " 10. We have " << Lmax / (t2_mpi-t1_mpi) << " matrix-matrix products per second." <<  endl;
        cout << " 11. Enter an integer to exit : ";
        cin  >> sentinel;
    }

    // 29. mpi shut down
    MPI_Finalize( );

    // 30. exiting.
    return 0;
}
