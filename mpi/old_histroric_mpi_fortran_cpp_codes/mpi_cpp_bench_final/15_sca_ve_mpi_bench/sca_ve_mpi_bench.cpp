
// MPI Benchmark 15.

// 1. I allocate ram space for a vector.
//    This is done for every mpi thread.
// 2. I define the vector only in mpi thread 0.
//    So I occupy ram only concerning mpi thread 0.
// 3. I scatter the above ram/vector
//    to any other mpi existing thread.
// 4. I am using only the mpi functions:
//
//    MPI_Init ( ... )
//    MPI_Comm_rank ( ... )
//    MPI_Comm_size ( ... )
//    MPI_Scatter ( ... )
//    MPI_Finalize( ... )

#include "mpi.h"
#include <iostream>
#include <cmath>
#include <ctime>
#include <iomanip>

using namespace std;

// main function
int main( int argc, char** argv )
{
    // 1. mpi start up
    MPI_Init( & argc, & argv );

    // 2. get my process rank
    int my_rank;
    MPI_Comm_rank( MPI_COMM_WORLD, & my_rank );

    // 3. find out how many mpi processes are being used
    int p;
    MPI_Comm_size( MPI_COMM_WORLD, & p );

    // 4. setting the dimension of the vector
    const long int dimen = 4 * 1024 * static_cast<long int>( pow(10.0, 5.0) );

    // 5. interface
    int sentinel;

    if ( my_rank == 0 ) {
        cout << "  1. Enter an integer to start the mpi benchmark : ";
        cin  >> sentinel;
        cout << "  2. Please wait while the mpi executes ... " << endl;
    }

    // 6. building the vector(s)
    double * vector_local = new double [ dimen / p ];
    double * vector = new double [ dimen ];

    if ( my_rank == 0 ) {
        for ( long int i = 0; i < dimen; i++ ) {
            vector[ i ] = cos(sin(cos( static_cast<double>(i+1) )));
        }
    }

    // 7. scatter the vector to the rest of mpi threads

    // 8. timing
    time_t t1;
    if ( my_rank == 0 ) {
        t1 = clock();
    }

    double t1_mpi;
    t1_mpi = MPI_Wtime();

    // 9. looping
    long int k;
    const long int kmax = 2 * static_cast<long int>( pow(10.0, 2.0) );

    for ( k = 1; k <= kmax; k++ ) {
        MPI_Scatter( vector, dimen/p, MPI_DOUBLE,
                     vector_local, dimen/p, MPI_DOUBLE,
                     0, MPI_COMM_WORLD );
        if ( my_rank != 0 ) {
            if ( k != kmax ) {
                delete [] vector_local;
                double * vector_local = new double [ dimen / p ];
            }
        }
    }

    // 10. timing
    time_t t2;
    if ( my_rank == 0 ) {
        t2 = clock();
    }

    double t2_mpi;
    t2_mpi = MPI_Wtime();

    // 11. put a mpi barrier
    MPI_Barrier( MPI_COMM_WORLD );

    // 12. sentineling - just to observe the ram
    if ( my_rank == 0 ) {
        cout << setprecision(15) << fixed;
        cout << "  3. Total real time used ( rank 0 )          is " << 1.0 * (t2-t1)/ CLOCKS_PER_SEC << " seconds. " <<  endl;
        cout << "  4. Total real time used ( mpi )             is " << (t2_mpi-t1_mpi) << " seconds. " << endl;
        cout << "  5. The scattered vector size                is " << dimen*8/(pow(1024.0,3.0)) << " gigabytes. " << endl;
        cout << scientific;
        cout << "  6. The scatters per second                  is "
             << kmax * p / (t2_mpi-t1_mpi) << endl;
        cout << "  7. At rank 0, the vector[ 0 ]               is " << vector[ 0 ] << endl;
        cout << "  8. At rank 0, the vector_local_[ 0 ]        is " << vector_local[ 0 ] << endl;
        cout << "  9. At rank 0, the vector[ dimen/p ]         is " << vector[ dimen / p ] << endl;
        cout << " 10. At rank 0, the vector[ 2*dimen/p ]       is " << vector[ 2 * dimen / p ] << endl;
        cout << " 11. At rank 0, the vector[ dimen - 1 ]       is " << vector[ dimen - 1 ] << endl;
    }

    // 13.
    MPI_Barrier( MPI_COMM_WORLD );

    if ( my_rank == 1 ) {
        cout << setprecision(15) << fixed;
        cout << " 12. At rank " << my_rank << ", the vector_local[ 0 ]         is " << vector_local[0] << endl;
    }

    // 14.
    MPI_Barrier( MPI_COMM_WORLD );

    if ( my_rank == 2 ) {
        cout << setprecision(15) << fixed;
        cout << " 13. At rank " << my_rank << ", the vector_local[ 0 ]         is " << vector_local[0] << endl;
    }

    // 15.
    MPI_Barrier( MPI_COMM_WORLD );

    if ( my_rank == p - 1 ) {
        cout << setprecision(15) << fixed;
        cout << " 14. At rank " << my_rank << ", the vector_local[ dimen/p-1 ] is "
             << vector_local[ dimen / p -1 ] << endl;
    }

    // 16.
    MPI_Barrier( MPI_COMM_WORLD );

    if ( my_rank == 0 ) {
        cout << " 15. Enter an integer to exit : ";
        cin >> sentinel ;
    }

    // 17. mpi shut down
    MPI_Finalize( );

    // 18. exiting.
    return 0;
}
