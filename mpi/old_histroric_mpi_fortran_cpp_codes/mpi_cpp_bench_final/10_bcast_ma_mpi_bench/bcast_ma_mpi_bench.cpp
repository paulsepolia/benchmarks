
// MPI Benchmark 10.

// 1. I allocate ram space for a matrix.
//    This is done for every mpi thread.
// 2. I define the matrix only in mpi thread 0.
//    So I occupy ram only concerning mpi thread 0.
// 3. I broadcast the above ram/matrix
//    to any other mpi existing thread.
// 4. I am using only the mpi function:
//
//    MPI_Init ( ... )
//    MPI_Comm_rank ( ... )
//    MPI_Comm_size ( ... )
//    MPI_Bcast ( ... )
//    MPI_Finalize( ... )

#include "mpi.h"
#include <iostream>
#include <cmath>
#include <ctime>
#include <iomanip>

using namespace std;

// main function
int main( int argc, char** argv )
{
    // 1. mpi start up
    MPI_Init( & argc, & argv );

    // 2. get my process rank
    int my_rank;
    MPI_Comm_rank( MPI_COMM_WORLD, & my_rank );

    // 3. find out how many mpi processes are being used
    int p;
    MPI_Comm_size( MPI_COMM_WORLD, & p );

    // 4. setting the dimension of the vectors
    const long int dimen = 2 * 2 * 4 * static_cast<long int>( pow(10.0, 3.0) );
    long int i,j,k; // common looping variables

    // 5. interface
    int sentinel;
    if ( my_rank == 0 ) {
        cout << "  1. Enter an integer to start the mpi benchmark : ";
        cin  >> sentinel;
        cout << "  2. Please wait while the mpi executes ... " << endl;
    }

    // 6. allocating space and building the matrix
    double ** matrix = new double * [ dimen ];
    for ( i = 0; i < dimen; i++ ) {
        matrix[i] = new double [ dimen ];
    }

    if ( my_rank == 0 ) {
        for ( i = 0; i < dimen; i++ ) {
            for ( j = 0; j < dimen; j++ ) {
                matrix[j][i] = cos(sin(cos( static_cast<double>(i+j+2) )));
            }
        }
    }

    // 7. broadcasting the matrix to the rest mpi threads
    // 8. timing
    time_t t1;
    if ( my_rank == 0 ) {
        t1 = clock();
    }

    double t1_mpi;
    t1_mpi = MPI_Wtime();

    // 9. looping
    const long int kmax = 5 * static_cast<long int>( pow(10.0, 1.0) );

    for ( k = 1; k <= kmax; k++ ) {
        for ( i = 0; i < dimen; i++ ) {
            MPI_Bcast( matrix[i], dimen, MPI_DOUBLE, 0, MPI_COMM_WORLD ); // fast broadcast method and safe
        }

        if ( my_rank != 0 && k != kmax ) {
            for ( i = 0; i < dimen; i++ ) { // free up ram
                delete [] matrix[i];
            }
            delete [] matrix;

            double ** matrix = new double * [ dimen ]; // allocate again ram
            for ( i = 0; i < dimen; i++ ) {
                matrix[i] = new double [ dimen ];
            }
        }
    }

    // 10. timing
    time_t t2;
    if ( my_rank == 0 ) {
        t2 = clock();
    }

    double t2_mpi;
    t2_mpi = MPI_Wtime();

    // 11. sentineling - just to observe the ram
    MPI_Barrier( MPI_COMM_WORLD );

    if ( my_rank == 0 ) {
        cout << setprecision(15) << fixed;
        cout << "  3. rank 0, matrix[0][0]             is " << matrix[0][0] << endl;
        cout << "  4. rank 0, matrix[dimen-1][dimen-1] is " << matrix[dimen-1][dimen-1] << endl;
    }

    MPI_Barrier( MPI_COMM_WORLD );

    if ( my_rank == 1 ) {
        cout << setprecision(15) << fixed;
        cout << "  5. rank 1, matrix[0][0]             is " << matrix[0][0] << endl;
        cout << "  6. rank 1, matrix[dimen-1][dimen-1] is " << matrix[dimen-1][dimen-1] << endl;
    }

    MPI_Barrier( MPI_COMM_WORLD );

    if ( my_rank == 2 ) {
        cout << setprecision(15) << fixed;
        cout << "  7. rank 2, matrix[0][0]             is " << matrix[0][0] << endl;
        cout << "  8. rank 2, matrix[dimen-1][dimen-1] is " << matrix[dimen-1][dimen-1] << endl;
    }

    MPI_Barrier( MPI_COMM_WORLD ) ;

    if ( my_rank == 0 ) {
        cout << setprecision(15) << fixed;
        cout << "  9. Total real time used ( rank 0 ) is " << 1.0 * (t2-t1)/ CLOCKS_PER_SEC << " seconds. "<<  endl;
        cout << " 10. Total real time used ( mpi )    is " << (t2_mpi-t1_mpi) << " seconds. " << endl;
        cout << " 11. The broadcasted matrix size     is " << pow(dimen,2.0)*8/(pow(1024.0,3.0))
             << " gigabytes. " << endl;
        cout << scientific;
        cout << " 12. The broadcasts per second       is " << kmax * p /(t2_mpi-t1_mpi) << endl;
        cout << " 13. Enter an integer to exit : ";
        cin  >> sentinel;
    }

    // 12. mpi shut down
    MPI_Finalize( );

    // 13. exiting.
    return 0;
}
