  program mpi_bench_2

! MPI Benchmark 2. 

! 1. I allocate ram space for the variable: vector(0:dimen-1).

! 2. I allocate ram space for the 2D vector : vector_local(0:p-1)(0:dimen-1).

! 3. I use the blocking mpi routines MPI_Irecv and MPI_Isend 
!    to copy and paste the data of vector(0:dimen-1) to the vector_local(0:p-1)(0:dimen-1).

! 4. The 1D variable vector(0:dimen) is defined only in mpi thread 0.

! 5. The 2D variables vector_loacl(0:p-1)(0:dimen-1) are defined only in mip threads 1 -> p-1.  

! 6. I do this copy and paste many times.

! 7. This benchmarks resables the MPI Benchmark 1 
!    since i do the same but using the MPI_Bcast routine.

! 8. The mpi routines in use are:
!    MPI_Inint( ... )
!    MPI_Comm_rank ( ... )
!    MPI_Comm_size( ... )
!    MPI_Irecv( ... )
!    MPI_Isend( ... )
!    MPI_Wait( ... )

! 9. The mpi type declarations in use are:
!    MPI_Status
!    MPI_Request   
 
  use mpi
  implicit none

  integer*4 :: ierr
  integer*4 :: my_rank
  integer*4 :: p
  integer*8, parameter :: dimen = 2 * (10.0 ** 8)
  integer*4 :: sentinel
  real*8, allocatable, dimension(:) :: vector
  real*8, allocatable, dimension(:,:) :: vector_local
  integer*4 :: i, tag
  integer*4 :: status_my(MPI_STATUS_SIZE), request
  real*8 :: t1, t2, t1_mpi, t2_mpi
  integer*8 :: k, kmax

  ! 1. mpi start up
  call MPI_Init( ierr )

  ! 2. get my process rank
  call MPI_Comm_rank( MPI_COMM_WORLD, my_rank, ierr )

  ! 3. find out how many mpi processes are being used
  call MPI_Comm_size( MPI_COMM_WORLD, p, ierr )

  ! 4. interface
  if ( my_rank == 0 ) then
    write(*,*) "  1. Enter an integer to start the mpi benchmark : "
    read(*,*) sentinel
    write(*,*) "  2. Please wait while the mpi benchmark executes ... "
  end if

  ! 5. allocating and building the vectors
  !    global vector - only for master thread ( my_rank = 0)
  allocate( vector( 1:dimen ) )

  if ( my_rank == 0 ) then  
    do i = 1, dimen
      vector(i) = dcos(dsin(dcos( real(i,kind=8) )))
    end do
  end if

  ! 6. sending parts of the vectors 
  !    from the master thread to the rest mpi threads

  ! 7. the tags and mpi defined data types
  tag = 30

  ! 8. timing
  if ( my_rank == 0 ) then
    call cpu_time(t1)
  end if

  t1_mpi = MPI_Wtime()

  ! 9. some constants
  kmax = 5 * ( 10.0 ** 1.0 ) 

  do k = 1, kmax

    if ( my_rank == 0 ) then       
      call MPI_Isend( vector, dimen, MPI_DOUBLE_PRECISION, 1, tag, MPI_COMM_WORLD, request, ierr )
      call MPI_Wait( request, status_my, ierr )
      call MPI_Irecv( vector, dimen, MPI_DOUBLE_PRECISION, p-1 , tag, MPI_COMM_WORLD, request, ierr )
      call MPI_Wait( request, status_my, ierr )
    end if

    do i = 1, p-2
      if ( my_rank == i ) then
        allocate( vector_local(1:p, 1:dimen ) )
        call MPI_Irecv( vector_local, dimen, MPI_DOUBLE_PRECISION, i-1, tag, MPI_COMM_WORLD, request, ierr )
        call MPI_Wait( request, status_my, ierr )
        call MPI_Isend( vector_local, dimen, MPI_DOUBLE_PRECISION, i+1, tag, MPI_COMM_WORLD, request, ierr )
        call MPI_Wait( request, status_my, ierr )
        deallocate( vector_local )
      end if
   end do
   
   if ( my_rank == p - 1 ) then
     allocate( vector_local( 1:p, 1:dimen) )
     call MPI_Irecv( vector_local, dimen, MPI_DOUBLE_PRECISION, p-2, tag, MPI_COMM_WORLD, request, ierr )
     call MPI_Wait( request,  status_my, ierr )
     call MPI_Isend( vector_local, dimen, MPI_DOUBLE_PRECISION, 0, tag, MPI_COMM_WORLD, request, ierr )  
     call MPI_Wait(  request,  status_my, ierr )
     deallocate( vector_local )
   end if    

 end do
 
  ! 10. timing  
  if ( my_rank == 0 ) then  
    call cpu_time(t2) 
  end if

  t2_mpi = MPI_Wtime( )

  ! 11. the results
  call MPI_Barrier( MPI_COMM_WORLD, ierr )

  if( my_rank == 0 ) then
    write(*,*) "  3. The real time used ( rank 0 ) is ", (t2-t1), " seconds. "
    write(*,*) "  4. The real time used ( mpi )    is ", (t2_mpi-t1_mpi), " seconds. "
    write(*,*) "  5. The size of the vector        is ", dimen * 8 / (1024.0d0 ** 3.0), " gigabytes. "
    write(*,*) "  6. Enter an integer to exit : "
    read(*,*) sentinel
  end if

  ! 12. mpi shut down
  call MPI_Finalize( ierr )

  end program mpi_bench_2
