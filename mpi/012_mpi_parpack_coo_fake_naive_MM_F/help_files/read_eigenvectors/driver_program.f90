!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/09/27              !
!===============================!

   program driver_program

   use m_1_type_definitions

   !=======================!
   !  1a. Interface starts !
   !=======================!
  
   integer(kind=si), parameter :: DIMEN   = 1000_si
   integer(kind=si), parameter :: NUM_VAL = 200_si 

   !=====================!
   !  1b. Interface ends !
   !=====================!

   real(kind=dr), dimension(1) :: dimen_line
   integer(kind=si)            :: rec_len
   real(kind=dr)               :: eig_value
   integer(kind=di)            :: i
   integer(kind=di), parameter :: TOT = DIMEN * NUM_VAL

   inquire(iolength = rec_len) dimen_line

   open(unit=19,              &
        form = "unformatted", &
        access = "direct",    &
        file = "___ve_mpi",   &
        status = "old",       &
        action = "read",      &
        recl = rec_len)

   do i = 1, TOT

     read(unit=19, rec=i) dimen_line(1)
     eig_value = dimen_line(1)
     write(*,*) dabs(eig_value)

   end do  

   end program driver_program

!======!
! FINI !
!======!
