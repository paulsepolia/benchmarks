#include <iostream>
#include <string>

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::to_string;

#include "mpi.h"
#include "/opt/hdf5/parallel/openmpi/1.8.17/include/hdf5.h"

// this example writes data to the HDF5 file
// data conversion is performed during write operation

// the main function

int main()
{
    // init mpi

    MPI::Init();
    int my_rank;
    my_rank = MPI::COMM_WORLD.Get_rank();
    MPI_Comm comm = MPI::COMM_WORLD;

    // local parameters

    const int N1 = 500;
    const int N2 = 500;
    const int N3 = 600;
    const int RANK = 3;
    const string FILE_NAME = "SDS_PARALLEL_3D.h5";
    const char * DATASETNAME = "IntArrayAlpha3D";
    const int I_DO_MAX = 300;

    // local variables

    hid_t file; // file handle
    hid_t  dataset; // dataset handles
    hid_t  datatype; // datatype handle
    hid_t  dataspace; // dataspace handle
    hsize_t  dimsf[RANK]; // dataset dimensions
    herr_t  status;

    for (int iL = 0; iL != I_DO_MAX; ++iL) {

        if (my_rank == 0) {
            cout << "------------------------------------------------------->> " << iL << endl;
        }

        // data to write

        if (my_rank == 0) {
            cout << " --> create the data to write" << endl;
        }

        // data init

        double * data = new double [N1*N2*N3];

        // data and output buffer initialization

        for (int i3 = 0; i3 != N3; ++i3) {
            for (int i2 = 0; i2 != N2; ++i2) {
                for (int i1 = 0; i1 != N1; ++i1) {
                    data[i1+i2*N1+i3*N1*N2] =
                        static_cast<double>(i1+i2*N1+i3*N1*N2);
                }
            }
        }

        MPI::COMM_WORLD.Barrier();

        // create a new file using H5F_ACC_TRUNC access

        if (my_rank == 0) {
            cout << " --> create a parallel HDF5 file" << endl;
        }

        hid_t fapl = H5Pcreate(H5P_FILE_ACCESS);

        H5Pset_fapl_mpio(fapl, comm, MPI::INFO_NULL);

        file = H5Fcreate(FILE_NAME.c_str(),
                         H5F_ACC_TRUNC,
                         H5P_DEFAULT,
                         fapl);

        MPI::COMM_WORLD.Barrier();

        // Describe the size of the array
        // and create the data space for fixed size dataset

        if (my_rank == 0) {
            cout << " --> describe the size of the array" << endl;
            cout << " --> and create the data space for fixed size dataset" << endl;
        }

        dimsf[0] = N1;
        dimsf[1] = N2;
        dimsf[2] = N3;

        dataspace = H5Screate_simple(RANK, dimsf, NULL);

        // synch

        MPI::COMM_WORLD.Barrier();

        // define datatype for the data in the file
        // we will store little endian INT numbers

        if (my_rank == 0) {
            cout << " --> define datatype for the data in the file" << endl;
        }

        datatype = H5Tcopy(H5T_NATIVE_DOUBLE);
        status = H5Tset_order(datatype, H5T_ORDER_LE);

        // synch

        MPI::COMM_WORLD.Barrier();

        if (my_rank == 0) {
            cout << " --> status = " << status << endl;
        }

        // Create a new dataset within the file using defined dataspace and
        // datatype and default dataset creation properties

        if (my_rank == 0) {
            cout << " --> create a new dataset within the file using defined dataspace" << endl;
            cout << " --> and datatype and default dataset creation properties" << endl;
        }

        dataset = H5Dcreate1(file,
                             DATASETNAME,
                             datatype,
                             dataspace,
                             H5P_DEFAULT);

        // synch

        MPI::COMM_WORLD.Barrier();

        // access the dataset

        hid_t dxpl = H5Pcreate(H5P_DATASET_XFER);

        // set data transfer mode

        //H5Pset_dxpl_mpio(dxpl, H5FD_MPIO_INDEPENDENT); // this gives 0.15 GBytes/sec
        H5Pset_dxpl_mpio(dxpl, H5FD_MPIO_COLLECTIVE);     // this is fast 4->5 GBytes/sec

        // synch

        MPI::COMM_WORLD.Barrier();

        // write the data to the dataset using default transfer properties

        if (my_rank == 0) {
            cout << " --> write data to the dataset in parallel" << endl;
        }

        status = H5Dwrite(dataset,
                          H5T_NATIVE_DOUBLE,
                          H5S_ALL,
                          dataspace,
                          dxpl,
                          data);

        // synch

        MPI::COMM_WORLD.Barrier();

        // free up heap

        if (my_rank == 0) {
            cout << " --> delete heap alocated RAM" << endl;
        }

        delete [] data;
    }

    // close units

    if (my_rank == 0) {
        cout << " --> close all the HDF5 related units" << endl;
    }

    H5Sclose(dataspace);
    H5Tclose(datatype);
    H5Dclose(dataset);
    H5Fclose(file);

    MPI::COMM_WORLD.Barrier();

    // MPI finalize

    MPI::Finalize();

    return 0;
}

// end
