#!/bin/bash
#SBATCH -J myMPI         # job name
#SBATCH -o myMPI.o%j     # output and error file name (%j expands to jobID)
#BATCH  -N 2
#SBATCH -n 32            # total number of mpi tasks requested
#SBATCH -p development   # queue (partition) -- normal, development, etc.
#SBATCH -t 2:00:00       # run time (hh:mm:ss) - 1.5 hours

touch x_intel

ibrun ./x_intel          # run the MPI executable named a.out
