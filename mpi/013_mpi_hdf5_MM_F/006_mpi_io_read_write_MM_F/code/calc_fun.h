
#ifndef CALC_FUN_H
#define CALC_FUN_H

// function declaration

double calc_fun(const int &,
                const int &,
                const unsigned long int &);

#endif // CALC_FUN_H

// end
