
#include "calc_fun.h"
#include "mpi.h"

// function definition

void calc_fun(const int & dim,
              const int & comm_b)
{

    for (int i = 0; i != comm_b; ++i) {

        // local containers

        double * vec_loc = new double [dim];
        double * vec_sum = new double [dim];

        // local initializations

        for (int j = 0; j != dim; ++j) {
            vec_loc[j] = j;
            vec_sum[0] = 0;
        }

        MPI::COMM_WORLD.Bcast(vec_loc,
                              dim,
                              MPI_DOUBLE,
                              0);

        // Allreduce

        MPI::COMM_WORLD.Allreduce(vec_loc,
                                  vec_sum,
                                  dim,
                                  MPI_DOUBLE,
                                  MPI_SUM);

        // synch

        MPI::COMM_WORLD.Barrier();

        // delete local heap

        delete [] vec_loc;
        delete [] vec_sum;
    }
}

// end
