
#include <iostream>
#include <stdio.h>
#include <cstring>
#include <assert.h>
#include <string>
#include <cmath>

#include "mpi.h"
#include "hdf5.h"

#include "cartesian.h"

using std::endl;
using std::cout;
using std::string;
using std::cos;

// the main program

int main()
{
    // init MPI

    MPI::Init();
    MPI_Comm comm = MPI_COMM_WORLD;
    int procs = 0;
    MPI_Comm_size(comm, &procs);
    int rank = 0;
    MPI_Comm_rank(comm, &rank);

    // local parameters

    //const hsize_t alignment = 4096;
    const int COEF_A = 20;    // 1
    const int COEF_B = 30;   // 90 // that value to reach 64 Gbytes
    const int IDOMAX = 10;   // 10 // how many times to execute the inner loop

    // local parameters fixed

    const int timeSteps = 100;
    const int DIM = 3;
    const double T0 = 200;
    const double T1 = 0;
    const int props = 1;
    const int L = 10;
    const int layer_offsets[11] = {0, 40, 50, 55, 60, 80, 81, 82, 90, 92, 100};
    const double ages[11] = {200, 200, 190, 180, 100, 10, 3, 2, 1, 0.5, 0.0};

    // local variables

    for (int iKK = 0; iKK != IDOMAX; ++iKK) {

        int iL = 0;

        for (int t = 0; t < timeSteps; ++t) {

            iL = iL+1;
            if (rank == 0) {
                cout << "------------------------------------------------------------------->> "
                     << iL+timeSteps*iKK << endl;
            }

            // local variables

            unsigned long int I = (2000 >= COEF_B*(t+COEF_A)) ? COEF_B*(t+COEF_A) : 2000 ;
            unsigned long int J = (2000 >= COEF_B*(t+COEF_A)) ? COEF_B*(t+COEF_A) : 2000 ;
            string tmp_str = "";
            int procsI;
            int procsJ;

            // execution

            cartesian(procs, &procsI, &procsJ);

            int rankI = rank % procsI;
            int rankJ = rank / procsI;

            // domain decomposition

            unsigned long int I_chunk = (I+procsI-1)/procsI;
            unsigned long int I_local = (rankI < procsI-1) ? I_chunk : (I - (procsI-1) * I_chunk);
            unsigned long int I_offset = I_chunk * rankI;

            unsigned long int J_chunk = (J+procsJ-1)/procsJ;
            unsigned long int J_local = (rankJ < procsJ-1) ? J_chunk : (J - (procsJ-1) * J_chunk);
            unsigned long int J_offset = J_chunk * rankJ;

            // assert

            assert(I_local >= 0);
            assert(J_local >= 0);

            // allocate grids

            unsigned long int SIZE_A = static_cast<double>(I_local)*J_local*layer_offsets[10]*props;

            double * temperature = new double [SIZE_A];

            hid_t fapl = H5Pcreate(H5P_FILE_ACCESS);

            H5Pset_fapl_mpio(fapl, comm, MPI_INFO_NULL);
            //H5Pset_alignment(fapl, 4096, alignment);

            // match chunks with domain decomposition

            hid_t dcpl = H5Pcreate(H5P_DATASET_CREATE);

            hsize_t chunk[DIM] = {layer_offsets[L], J_chunk, I_chunk};

            H5Pset_chunk(dcpl, DIM, chunk);

            // set I/O independent

            hid_t dxpl = H5Pcreate(H5P_DATASET_XFER);

            //H5Pset_dxpl_mpio(dxpl, H5FD_MPIO_INDEPENDENT);
            H5Pset_dxpl_mpio(dxpl, H5FD_MPIO_COLLECTIVE);

            // create file and dataset

            hsize_t globalC[DIM] = {layer_offsets[L], J, I};

            hid_t globalSpaceC = H5Screate_simple(DIM, globalC, NULL);

            tmp_str = "test_data_" +
                      std::to_string(static_cast<long long>(iL)) +
                      ".h5";

            hid_t file = H5Fcreate(tmp_str.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, fapl);

            hid_t datasetC = H5Dcreate2(file,
                                        "temperature",
                                        H5T_NATIVE_DOUBLE,
                                        globalSpaceC,
                                        H5P_DEFAULT,
                                        dcpl,
                                        H5P_DEFAULT);

            // age

            double age = T0 - t * (T0-T1) / (timeSteps - 1);

            // determine layer number and K

            int l = 0;

            //cout << " --> 16" << endl;

            for (int i = 0; i < L; ++i) {
                if (ages[i] < age) {
                    break;
                }
                l = i;
            }

            // assert

            assert(age <= ages[l]);

            // layer offsets

            unsigned long int K = layer_offsets[l] +
                                  static_cast<double>(layer_offsets[l+1]-layer_offsets[l]) *
                                  (ages[l]-age)/(ages[l]-ages[l+1]);

            // fill grids with data

            for (unsigned long int i = 0; i < static_cast<double>(I_local)*J_local*K*props; ++i) {
                temperature[i] = static_cast<double>(i) / age;
            }

            // write file

            hsize_t blockC[DIM] = {K, J_local, I_local};

            hsize_t offset[DIM] = {0, J_offset, I_offset};

            hsize_t localSpaceC = H5Screate_simple(DIM, blockC, NULL);

            H5Sselect_hyperslab(globalSpaceC, H5S_SELECT_SET, offset, NULL, blockC, NULL);

            H5Dwrite(datasetC, H5T_NATIVE_DOUBLE, localSpaceC, globalSpaceC, dxpl, temperature);

            H5Sclose(localSpaceC);

            // close hdf5 related units

            H5Dclose(datasetC);
            H5Fclose(file);
            H5Sclose(globalSpaceC);
            H5Pclose(fapl);
            H5Pclose(dxpl);
            H5Pclose(dcpl);

            // free up heap

            delete [] temperature;

            // remove

            //          if (rank == 0)
            //          {
            //            system("rm test_data_*.h5");
            //          }
        }
    }

    // finalize MPI

    MPI_Finalize();

    return 0;
}

// end
