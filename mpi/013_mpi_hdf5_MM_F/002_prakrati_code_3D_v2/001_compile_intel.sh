#!/bin/bash

  mpiicpc  -O3                            \
		 -std=c++11                     \
           -Wall                          \
           cartesian.cpp                  \
           driver_program.cpp             \
           /opt/hdf5/parallel/v16/lib/libhdf5.a \
		 -lz                            \
           -o x_intel

