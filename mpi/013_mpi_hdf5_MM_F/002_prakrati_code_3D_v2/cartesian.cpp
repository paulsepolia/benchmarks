
// function definition

void cartesian(int P, int * Px, int * Py)
{
    int fitness = 1;
    *Px = P;
    *Py = 1;

    for (int py = 1; py <= P; ++py) {
        if (P % py == 0) {
            int px = P / py;

            int currentFitness = px < py ? px : py;

            if (currentFitness > fitness ) {
                fitness = currentFitness;
                *Px = px;
                *Py = py;
            }
        }
    }
}

// end
