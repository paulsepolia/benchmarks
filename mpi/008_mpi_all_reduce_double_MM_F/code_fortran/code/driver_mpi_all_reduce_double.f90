!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/08/12              !
!===============================!

!======================================================!
! MPI Code                                             !
! Testing the MPI_Allreduce subroutine                 !
!                                                      !
! 1. I allocate ram space for a vector.                !
!    This is done for every mpi thread.                !
!                                                      !
! 2. I define the vector only in mpi thread 0.         !
!    So I occupy ram only concerning mpi thread 0.     !
!                                                      !
! 3. I broadcast the above ram/vector                  !
!    to any other mpi existing thread.                 !
!                                                      !
! 4. I do many MPI_Reduce to find the total sum of the !
!    elements of the vector.                           !
!                                                      !
! 5. I am using only the mpi functions/subroutines:    !
!                                                      !
!    mpi_init ( ... )                                  !
!    mpi_comm_rank ( ... )                             !
!    mpi_comm_size ( ... )                             !
!    mpi_bcast ( ... )                                 !
!    mpi_finalize( ... )                               !
!    mpi_allreduce( ... )                              !
!======================================================!

  program driver_mpi_all_reduce_test

  use mpi
  use m_1_parameters

  implicit none

  !=========================!
  !  1a. Start of interface !
  !=========================!

  integer(kind=si), parameter :: DIMEN    = int(10_si ** 6, kind=si)
  integer(kind=si), parameter :: K_DO_MAX = int(10_si ** 4, kind=si)

  !======================!
  ! 1b. End of interface !
  !======================!

  !  2. variables

  integer(kind=si) :: ierr
  integer(kind=si) :: my_rank
  integer(kind=si) :: p

  integer(kind=di) :: i
  integer(kind=di) :: k
  integer(kind=si) :: comm

  real(kind=dr), allocatable, dimension(:) :: vector
  real(kind=dr), allocatable, dimension(:) :: sum_vector
  real(kind=dr) :: t1
  real(kind=dr) :: t2
  real(kind=dr) :: t1_mpi
  real(kind=dr) :: t2_mpi
  real(kind=dr) :: sum_my

  !  3. mpi start up

  call mpi_init(ierr)

  !  4. get my process rank

  call mpi_comm_rank(MPI_COMM_WORLD, my_rank, ierr)

  comm = MPI_COMM_WORLD

  !  5. find out how many mpi processes are being used

  call mpi_comm_size(comm, p, ierr)

  !  6. interface 

  if (my_rank == 0) then

    write(*,*) " 1. Please wait while the mpi executes ... "

  end if

  call mpi_barrier(comm, ierr)

  !  7. allocating space in each mpi thread
  !     but building the vector only in master mpi thread

  allocate(vector(1:DIMEN))
  allocate(sum_vector(1:DIMEN))

  if (my_rank == 0) then

    do i = 1, dimen

      vector(i) = dcos(real(i, kind=dr))

    end do

  end if

  !  8. broadcasting the vector to the rest mpi threads

  call mpi_bcast(vector,               &
                 DIMEN,                &
                 MPI_DOUBLE_PRECISION, &
                 0,                    &
                 comm,                 &
                 ierr)


  !  9. timing

  if (my_rank == 0) then
    call cpu_time(t1)
  end if
  t1_mpi = mpi_wtime()

  ! 10. initialization 

  sum_vector = 0.0_dr
  sum_my     = 0.0_dr

  ! 11. benchmarking

  do k = 1, K_DO_MAX

    sum_my  = 0.0_dr

    call mpi_allreduce(vector,               & 
                       sum_vector,           &
                       DIMEN,                &
                       MPI_DOUBLE_PRECISION, &
                       MPI_SUM,              & 
                       comm,                 & 
                       ierr)

  end do  

  ! 12. final result

  call mpi_barrier(comm, ierr)

  sum_my = sum(sum_vector) / p

  ! 13. timing

  call cpu_time(t2)
  t2_mpi = mpi_wtime()

  ! 14. outputs 

  do i = 0, p-1

    if (my_rank == i) then

      write(*,*) "  -----> For the mpi thread --> ", i 

      write(*,*) "  2. Total real time used is --> ", (t2-t1), " seconds. "

      write(*,*) "  3. Total real time used (mpi) is --> ", (t2_mpi-t1_mpi), " seconds. "

      write(*,*) "  4. The broadcasted vector size is --> ", &
                 DIMEN * 8.0_dr /(1024.0_dr ** 3.0_dr), " gigabytes. "

      write(*,*) "  5. The all-reductions per second is --> ", K_DO_MAX * p / (t2_mpi-t1_mpi)

      write(*,*) "  6. The sum is --> ", sum_my

    end if

  call mpi_barrier(comm, ierr)

  end do

  ! 15. mpi shut down

  call mpi_finalize(ierr)

  end program driver_mpi_all_reduce_test

!======!
! FINI !
!======!
