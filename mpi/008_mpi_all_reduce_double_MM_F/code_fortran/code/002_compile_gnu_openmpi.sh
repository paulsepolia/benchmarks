#!/bin/bash

  # 1. compile

  mpif90.openmpi  -O3                              \
                  -std=f2003                       \
                  -Wall                            \
                  m_1_parameters.f90               \
                  driver_mpi_all_reduce_double.f90 \
                  -o x_gnu_openmpi

  # 2. clean

  rm *.mod
