!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/08/21              !
!===============================!

  program driver_read_write_mpi_v1

  use mpi
  use m_1_type_definitions

  implicit none
   
  !  1. interface parameters

  character(len=200), parameter :: TEST_FILE  = "test_file"
  integer(kind=di), parameter   :: DIMEN      = 10000_di
  integer(kind=si), parameter   :: I_MAX_DO   = 4_si
  real(kind=dr), parameter      :: ERROR_TEST = 1.0e-15_dr
  logical, parameter            :: RUN_TEST   = .false.

  !  2. local variables

  integer(kind=si)                           :: comm
  integer(kind=si)                           :: ierr
  integer(kind=si)                           :: my_rank
  integer(kind=si)                           :: p
  integer(kind=si)                           :: i1
  integer(kind=si)                           :: i2
  integer(kind=si)                           :: rec_len
  integer(kind=si)                           :: i3
  real(kind=dr), dimension(1)                :: dimen_line
  real(kind=dr), allocatable, dimension(:,:) :: test_matrix
  real(kind=dr)                              :: tmp_var  
  real(kind=dr)                              :: t1_mpi
  real(kind=dr)                              :: t2_mpi
  real(kind=dr)                              :: t_mpi_write
  real(kind=dr)                              :: t_mpi_read
  real(kind=dr)                              :: total_mega_bytes
  real(kind=dr)                              :: total_giga_bytes

  !  3. MPI starts

  t1_mpi      = 0.0_dr
  t2_mpi      = 0.0_dr
  t_mpi_write = 0.0_dr
  t_mpi_read  = 0.0_dr

  call MPI_Init(ierr)

  comm = MPI_COMM_WORLD

  call MPI_Comm_rank(comm, my_rank, ierr)

  call MPI_Comm_size(comm, p, ierr)

  !  4. build matrix local to each MPI process

  allocate(test_matrix(1:DIMEN,1:DIMEN))

  do i1 = 1, DIMEN
    do i2 = 1, DIMEN

      test_matrix(i2, i1) = sin(real(i1+i2, kind=dr))
   
    end do
  end do

  call MPI_Barrier(comm, ierr)

  !  5. setting the common record length

  inquire(iolength=rec_len) dimen_line

  !  6. main benchmark starts

  do i3 = 1, I_MAX_DO

    !  7. each MPI process opens the same unit

    open(unit   = 18,            &
         form   = "unformatted", &
         file   = TEST_FILE,     &
         status = "replace",     &
         action = "readwrite",   &
         access = "direct",      &
         recl   = rec_len)

    call MPI_Barrier(comm, ierr)

    !  8. timing write starts

    if (i3 == 1) then
      t1_mpi = MPI_Wtime()
    end if

    !  9. writting to the hard disk the local matrix

    do i1 = 1, DIMEN
      do i2 = 1, DIMEN

        write(unit=18, &
              rec=i2+(i1-1)*DIMEN+my_rank*(DIMEN**2)) test_matrix(i2, i1)

      end do
    end do

    ! 10. timing write ends

    if (i3 == I_MAX_DO) then
      t2_mpi = MPI_Wtime()
      t_mpi_write = t2_mpi - t1_mpi
    end if

    call MPI_Barrier(comm, ierr)

    ! 11. timing read starts

    if (i3 == 1) then
      t1_mpi = MPI_Wtime()
    end if

    ! 12. reading from the hard disk the stored matrix

    do i1 = 1, DIMEN
      do i2 = 1, DIMEN

        read(unit=18, & 
             rec=i2+(i1-1)*DIMEN+my_rank*(DIMEN**2)) test_matrix(i2, i1)

      end do
    end do

    ! 13. timing read ends

    if (i3 == I_MAX_DO) then
      t2_mpi = MPI_Wtime()
      t_mpi_read = t2_mpi - t1_mpi
    end if

    call MPI_Barrier(comm, ierr)

    ! 14. closing the unit

    close(unit=18, status="keep")

  end do

  !  15. testing

  if (RUN_TEST) then

    do i1 = 1, DIMEN
      do i2 = 1, DIMEN
  
        tmp_var = test_matrix(i2,i1) - sin(real(i1+i2, kind=dr))

        if(abs(tmp_var) > ERROR_TEST) then

          write(*,*)  " From rank ", &
                      my_rank,       &
                      " --> i1 = ",  &
                      i1,            &
                      " --> i2 = ",  &
                      i2,            &
                      abs(tmp_var)
          stop

        end if

      end do
    end do

  end if

  call MPI_Barrier(comm, ierr)

  ! 16. report

  if (my_rank == 0) then

    total_giga_bytes = I_MAX_DO*p*(DIMEN**2)*8/(1024.0_dr)**3
    total_mega_bytes = total_giga_bytes * 1024.0_dr

    write(*,*)
    write(*,*) "=============================================="
    write(*,*) " Author      : Pavlos G. Galiatsatos          "
    write(*,*) " Description : MPI I/O via 'read' and 'write' "
    write(*,*) " Language    : Fortran 2008                   "
    write(*,*) " Date        : 2013/08/21                     "
    write(*,*) " Code name   : BENIOA                         "
    write(*,*) "=============================================="
    write(*,*)

    write(*,*) "  1. Total real time used to write is    : ", t_mpi_write, " seconds"
    write(*,*) "  2. Total real time used to read  is    : ", t_mpi_read , " seconds"
    write(*,*) "  3. Total GBytes written are            : ", total_giga_bytes
    write(*,*) "  4. Total GBytes read are               : ", total_giga_bytes
    write(*,*) "  5. Speed write (MBytes/sec) is         : ", total_mega_bytes/t_mpi_write
    write(*,*) "  6. Speed read  (MBytes/sec) is         : ", total_mega_bytes/t_mpi_read
    write(*,*) "  7. Speed write (MBytes/sec/process) is : ", total_mega_bytes/t_mpi_write/p
    write(*,*) "  8. Speed read  (MBytes/sec/process) is : ", total_mega_bytes/t_mpi_read/p
    write(*,*) "  9. Total MPI processes used are        : ", p
    write(*,*)

  end if

  ! 17. delete test file, free up the RAM and finalize MPI

  if(my_rank == 0) then

    open(unit   = 18,            &
         form   = "unformatted", &
         file   = TEST_FILE,     &
         status = "old",         &
         action = "readwrite",   &
         access = "direct",      &
         recl   = rec_len)

    close(unit = 18, status="delete")

  end if

  deallocate(test_matrix)

  call MPI_Finalize(ierr)

  end program driver_read_write_mpi_v1

!======!
! FINI !
!======!
