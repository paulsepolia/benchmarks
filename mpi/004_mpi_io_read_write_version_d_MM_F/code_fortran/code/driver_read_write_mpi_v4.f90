!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/08/21              !
!===============================!

  program driver_read_write_mpi_v5

  use mpi
  use m_1_type_definitions

  implicit none
   
  !  1. interface parameters

  character(len=200), parameter :: TEST_FILE_BASE      = "test_file_"
  integer(kind=di),   parameter :: DIMEN               = 1_di * 10000_di * 10000_di
  character(len=200), parameter :: DATA_REPRESENTATION = "native" ! "internal", "external32"
  integer(kind=si),   parameter :: I_DO_MAX            = 2_si
  integer(kind=si),   parameter :: NUM_FILES           = 2_si ! must devide evenly the MPI processes
  integer(kind=si),   parameter :: IO_MODE             = MPI_MODE_RDWR   + &
                                                         MPI_MODE_CREATE + &
                                                         MPI_MODE_DELETE_ON_CLOSE

  !  2. local variables

  integer(kind=si)                                         :: comm
  integer(kind=si)                                         :: ierr
  integer(kind=si)                                         :: my_rank
  integer(kind=si)                                         :: p
  integer(kind=si)                                         :: i1
  integer(kind=si)                                         :: i2
  integer(kind=si), dimension(:), allocatable              :: the_file
  integer(kind=MPI_OFFSET_KIND), dimension(:), allocatable :: disp
  real(kind=dr), dimension(:), allocatable                 :: test_array
  real(kind=dr)                                            :: t1_mpi
  real(kind=dr)                                            :: t2_mpi
  real(kind=dr)                                            :: t_mpi_write
  real(kind=dr)                                            :: t_mpi_read
  real(kind=dr)                                            :: total_giga_bytes
  real(kind=dr)                                            :: total_mega_bytes
  character(len=100), parameter                            :: fmt_a = "(I10)"
  character(len=100)                                       :: file_id

  !  3.a. MPI starts

  t1_mpi      = 0.0_dr
  t2_mpi      = 0.0_dr
  t_mpi_write = 0.0_dr
  t_mpi_read  = 0.0_dr

  call MPI_Init(ierr)

  comm = MPI_COMM_WORLD

  call MPI_Comm_rank(comm, my_rank, ierr)

  call MPI_Comm_size(comm, p, ierr)

  !  3.b. test here

  if((NUM_FILES > p) .and. (my_rank == 0)) then
    write(*,*) " NUM_FILES > p ==> ", NUM_FILES, " > ", p 
    write(*,*) " Choose an integer value less than or equal to p = ", p
    stop 
  end if

  call MPI_Barrier(comm, ierr)

  if((mod(p,NUM_FILES) /= 0) .and. (my_rank==0)) then
    write(*,*) "p and NUM_FILES must be give back module 0"
    stop
  end if
  
  !  4. build an array local to each MPI process

  allocate(test_array(1:DIMEN))
  allocate(the_file(1:p))
  allocate(disp(1:p))

  do i1 = 1, DIMEN

    test_array(i1) = sin(real(i1+my_rank*p, kind=dr))
   
  end do

  call MPI_Barrier(comm, ierr)

  !  5. main benchmark starts

  do i1 = 1, I_DO_MAX
      
    !  6. all the processes manual open all the files
    !     to have them in their view
 
    do i2 = 1, NUM_FILES

      write(file_id, fmt_a) i2
      file_id = adjustl(file_id)
      file_id = trim(file_id)

      call MPI_File_open(comm,                                &
                         trim(TEST_FILE_BASE)//trim(file_id), &
                         IO_MODE,                             &
                         MPI_INFO_NULL,                       &
                         the_file(i2),                        &
                         ierr)

      !  7. set the MPI file view

      disp(i2) = mod(my_rank+1, p/NUM_FILES) * DIMEN * 8_si ! displacement in bytes

      call MPI_File_set_view(the_file(i2),         &
                             disp(i2),             &
                             MPI_DOUBLE_PRECISION, &
                             MPI_DOUBLE_PRECISION, &
                             DATA_REPRESENTATION,  &
                             MPI_INFO_NULL,        &
                             ierr)

    end do

    !  8. timing write starts here

    if (i1 == 1) then
      t1_mpi = MPI_Wtime()
    end if

    !  9. select which process writes which file

    i2 = my_rank/(p/NUM_FILES) + 1

    if(((p/NUM_FILES)*(i2-1) <= my_rank) .and. (my_rank < (p/NUM_FILES)*i2)) then

      call MPI_File_write(the_file(i2),         &
                          test_array,           &
                          DIMEN,                &
                          MPI_DOUBLE_PRECISION, &
                          MPI_STATUS_IGNORE,    &
                          ierr)

      call mpi_file_sync(the_file(i2), ierr)

    end if

    ! 10. timing write ends here

    if (i1 == I_DO_MAX) then
      t2_mpi = MPI_Wtime()
      t_mpi_write = t2_mpi - t1_mpi
    end if

    call MPI_Barrier(comm, ierr)

    ! 11. timing read starts here

    if (i1 == 1) then
      t1_mpi = MPI_Wtime()
    end if

    ! 12. select which process reads which file

    i2 = my_rank/(p/NUM_FILES) + 1

    if(((p/NUM_FILES)*(i2-1) <= my_rank) .and. (my_rank < (p/NUM_FILES)*i2)) then

      call MPI_File_read(the_file(i2),         &
                         test_array,           &
                         DIMEN,                &
                         MPI_DOUBLE_PRECISION, &
                         MPI_STATUS_IGNORE,    &
                         ierr) 

      call mpi_file_sync(the_file(i2), ierr)

    end if

    ! 13. timing read ends here

    if (i1 == I_DO_MAX) then
      t2_mpi = MPI_Wtime()
      t_mpi_read = t2_mpi - t1_mpi
    end if

    ! 14. manual close the files and execute the IO_MODE

    do i2 = 1, NUM_FILES

      call MPI_File_close(the_file(i2), ierr)

    end do

  end do

  !  15. report

  if (my_rank == 0) then

    total_giga_bytes = I_DO_MAX*p*DIMEN*8_si/(1024.0_dr)**3
    total_mega_bytes = total_giga_bytes * 1024.0_dr

    write(*,*)
    write(*,*) "================================================================"
    write(*,*) " Author      : Pavlos G. Galiatsatos                            "
    write(*,*) " Description : MPI I/O via 'MPI_File_write' and 'MPI_File_read' "
    write(*,*) " Language    : Fortran 2008                                     "
    write(*,*) " Date        : 2013/08/21                                       "
    write(*,*) " Code name   : BENIOE                                           "
    write(*,*) "================================================================"
    write(*,*)

    write(*,*) "  1. Total real time used to write is    : ", t_mpi_write, " seconds"
    write(*,*) "  2. Total real time used to read  is    : ", t_mpi_read , " seconds"
    write(*,*) "  3. Total GBytes written are            : ", total_giga_bytes
    write(*,*) "  4. Total GBytes read are               : ", total_giga_bytes
    write(*,*) "  5. Speed write (MBytes/sec) is         : ", total_mega_bytes/t_mpi_write
    write(*,*) "  6. Speed read  (MBytes/sec) is         : ", total_mega_bytes/t_mpi_read
    write(*,*) "  7. Speed write (MBytes/sec/process) is : ", total_mega_bytes/t_mpi_write/p
    write(*,*) "  8. Speed read  (MBytes/sec/process) is : ", total_mega_bytes/t_mpi_read/p
    write(*,*) "  9. Total MPI processes used are        : ", p

  end if

  ! 16. clean RAM exit the MPI environment

  deallocate(test_array)
  deallocate(the_file)
  deallocate(disp)
 
  call MPI_Finalize(ierr)

  end program driver_read_write_mpi_v5

!======!
! FINI !
!======!
