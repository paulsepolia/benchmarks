#!/bin/bash

  # 1. compile

  mpicxx.mpich  -O3                          \
                -Wall                        \
                -std=gnu++17                 \
                driver_read_write_mpi_v5.cpp \
                -o x_gnu_mpich
