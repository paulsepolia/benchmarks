//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/08/21              //
//===============================//

#include "mpi.h"
#include <iostream>
#include <cmath>

using namespace std;

// The main function

int main()
{
    //  1. local interface parameters

    const char     TEST_FILE[128]           = "test_file";
    const long int DIMEN                    = 1 * static_cast<long>(1000) * 1000;
    const char     DATA_REPRESENTATION[128] = "native"; // "internal", "external32"
    const int      I_DO_MAX                 = 4;
    const int      RUN_TEST                 = 0;        // 1 == true, 0 == false
    const double   ERROR_TEST               = 10e-12;
    const int      IO_MODE                  = MPI::MODE_CREATE |
            MPI::MODE_RDWR   |
            MPI::MODE_DELETE_ON_CLOSE;

    //  2. local variables

    MPI::File   the_file;
    MPI::Offset disp;
    MPI::Status status;
    int         my_rank;
    int         p;
    double*     test_array = new double [DIMEN];
    double      t1_mpi;
    double      t2_mpi;
    double      t_mpi_write;
    double      t_mpi_read;
    double      total_giga_bytes;
    double      total_mega_bytes;
    double      tmp_var;

    //  3. initializations to avoid
    //     the GNU compiler warnings

    t1_mpi      = 0.0;
    t2_mpi      = 0.0;
    t_mpi_write = 0.0;
    t_mpi_read  = 0.0;

    //  4. MPI start up

    MPI::Init();

    //  5. get my process rank

    my_rank = MPI::COMM_WORLD.Get_rank();

    //  6. find out how many mpi processes are being used

    p = MPI::COMM_WORLD.Get_size();

    //  7. initialize the array

    for(long i = 0; i < DIMEN; i++) {
        test_array[i] = sin(static_cast<double>(i+my_rank*p));
    }

    MPI::COMM_WORLD.Barrier();

    for(int i = 1; i <= I_DO_MAX; i++) {

        //  8. timing write starts

        if (i == 1) {
            t1_mpi = MPI::Wtime();
        }

        //  9. open the file

        the_file = MPI::File::Open(MPI::COMM_WORLD,
                                   TEST_FILE,
                                   IO_MODE,
                                   MPI::INFO_NULL);

        // 10. set the MPI file view

        disp = my_rank * DIMEN * 8; // displacement in bytes

        the_file.Set_view(disp,
                          MPI::DOUBLE,
                          MPI::DOUBLE,
                          DATA_REPRESENTATION,
                          MPI::INFO_NULL);

        // 11. write to the file in parallel

        the_file.Write(test_array,
                       DIMEN,
                       MPI::DOUBLE,
                       status);

        // 12. timing write ends

        if (i == I_DO_MAX) {
            t2_mpi = MPI::Wtime();
            t_mpi_write = t2_mpi - t1_mpi;
        }

        MPI::COMM_WORLD.Barrier();

        // 13. timing read starts

        if (i == 1) {
            t1_mpi = MPI::Wtime();
        }

        // 14. read in parallel from the file

        the_file.Read(test_array,
                      DIMEN,
                      MPI::DOUBLE,
                      status);

        // 15. timing read ends

        if (i == I_DO_MAX) {
            t2_mpi = MPI::Wtime();
            t_mpi_read = t2_mpi - t1_mpi;
        }

        // 16. test

        if (RUN_TEST == 1) {
            for(long i2 = 0; i2 < DIMEN; i2++) {
                tmp_var = test_array[i2] - sin(static_cast<double>(i2+my_rank*p));

                if(abs(tmp_var) > ERROR_TEST) {
                    cout << endl << " From rank " << my_rank <<
                         " --> i2 = "  << i2 << abs(tmp_var) << endl;

                    return (-1);
                }
            }
        }

        // 17. close the file

        the_file.Close();
    }

    // 18. report

    if (my_rank == 0) {
        total_giga_bytes = I_DO_MAX*p*DIMEN*8/pow(static_cast<double>(1024.0), 3.0);
        total_mega_bytes = total_giga_bytes * static_cast<double>(1024.0);

        cout << endl;
        cout << " ================================================================ " << endl;
        cout << "  Author      : Pavlos G. Galiatsatos                             " << endl;
        cout << "  Description : MPI I/O via MPI Read and Write                    " << endl;
        cout << "  Language    : C++                                               " << endl;
        cout << "  Date        : 2013/08/21                                        " << endl;
        cout << "  Code name   : BENIOC++                                          " << endl;
        cout << " ================================================================ " << endl;
        cout << endl;

        cout << "  1. Total real time used to write is    : " << t_mpi_write << " seconds" << endl;
        cout << "  2. Total real time used to read  is    : " << t_mpi_read  << " seconds" << endl;
        cout << "  3. Total GBytes written are            : " << total_giga_bytes << endl;
        cout << "  4. Total GBytes read are               : " << total_giga_bytes << endl;
        cout << "  5. Speed write (MBytes/sec) is         : " << total_mega_bytes/t_mpi_write << endl;
        cout << "  6. Speed read  (MBytes/sec) is         : " << total_mega_bytes/t_mpi_read << endl;
        cout << "  7. Speed write (MBytes/sec/process) is : " << total_mega_bytes/t_mpi_write/p << endl;
        cout << "  8. Speed read  (MBytes/sec/process) is : " << total_mega_bytes/t_mpi_read/p << endl;
        cout << "  9. Total MPI processes used are        : " << p << endl;
        cout << endl;
    }

    // 19. free up the RAM and finalize MPI

    delete [] test_array;

    MPI::Finalize();

    return 0;
}

//======//
// FINI //
//======//
