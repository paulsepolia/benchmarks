!===================================!
! NOT Author: Pavlos G. galiatsatos !
! Date: 2013/09/25                  !
!===================================!

!====================================================!
! this routine is from the Numerical Recipes company !
! is modified to sort the array "ra" and             !
! he rest 2 arrays: "ra2", "ra3" to be sorted        !
! according to "ra"                                  !
!====================================================!

  module m_2_heap_sort_3ar

  use m_1_type_definitions

  contains

      subroutine heap_sort_3ar(n,ra1,ra2,ra3)
      
      implicit none 

      integer(kind=di) :: n
      real(kind=dr), allocatable, dimension(:), intent(inout) :: ra1
      real(kind=dr), allocatable, dimension(:), intent(inout) :: ra2
      real(kind=dr), allocatable, dimension(:), intent(inout) :: ra3
      integer(kind=di) :: i
      integer(kind=di) :: ir
      integer(kind=di) :: j
      integer(kind=di) :: l
      real(kind=dr) :: rra1
      real(kind=dr) :: rra2
      real(kind=dr) :: rra3   

      if (n.lt.2) return
      l=n/2+1
      ir=n
10    continue
        if(l.gt.1)then
          l=l-1
          rra1=ra1(l)
          rra2=ra2(l)
          rra3=ra3(l)
        else
          rra1=ra1(ir)
          rra2=ra2(ir)
          rra3=ra3(ir)
          ra1(ir)=ra1(1)
          ra2(ir)=ra2(1)
          ra3(ir)=ra3(1)
          ir=ir-1
          if(ir.eq.1)then
            ra1(1)=rra1
            ra2(1)=rra2
            ra3(1)=rra3
            return
          endif
        endif
        i=l
        j=l+l
20      if(j.le.ir)then
          if(j.lt.ir)then
            if(ra1(j).lt.ra1(j+1))j=j+1
          endif
          if(rra1.lt.ra1(j))then
            ra1(i)=ra1(j)
            ra2(i)=ra2(j)
            ra3(i)=ra3(j)
            i=j
            j=j+j
          else
            j=ir+1
          endif
        goto 20
        endif
        ra1(i)=rra1
        ra2(i)=rra2
        ra3(i)=rra3
      goto 10

   end subroutine heap_sort_3ar

   end module m_2_heap_sort_3ar

!======!
! FINI !
!======!
