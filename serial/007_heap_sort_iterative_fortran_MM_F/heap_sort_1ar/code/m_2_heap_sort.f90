!===============================!
! Author: Pavlos G. galiatsatos !
! Date: 2013/09/25              !
!===============================

  module m_2_heap_sort

  use m_1_type_definitions

  contains

  subroutine hpsort(n,ra)
 
  implicit none 
    
  integer(kind=di) :: n
  real(kind=dr), allocatable, dimension(:) :: ra
  integer(kind=di) :: i
  integer(kind=di) :: ir
  integer(kind=di) :: j
  integer(kind=di) :: l
  real(kind=dr) :: rra
   
      if (n.lt.2) return
      l=n/2+1
      ir=n
10    continue
        if(l.gt.1)then
          l=l-1
          rra=ra(l)
        else
          rra=ra(ir)
          ra(ir)=ra(1)
          ir=ir-1
          if(ir.eq.1)then
            ra(1)=rra
            return
          endif
        endif
        i=l
        j=l+l
20      if(j.le.ir)then
          if(j.lt.ir)then
            if(ra(j).lt.ra(j+1))j=j+1
          endif
          if(rra.lt.ra(j))then
            ra(i)=ra(j)
            i=j
            j=j+j
          else
            j=ir+1
          endif
        goto 20
        endif
        ra(i)=rra
      goto 10
  end subroutine hpsort

  end module m_2_heap_sort

!======!
! FINI !
!======!
