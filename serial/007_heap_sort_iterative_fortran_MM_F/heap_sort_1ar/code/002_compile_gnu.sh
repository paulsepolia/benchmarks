#!/bin/bash

# 1. compile

  gfortran       -O3                      \
                 -std=f2008               \
                 -Wall                    \
                 m_1_type_definitions.f90 \
                 m_2_heap_sort.f90        \
                 driver_program.f90       \
                 -o x_gnu

# 2. clean

  rm *.mod
