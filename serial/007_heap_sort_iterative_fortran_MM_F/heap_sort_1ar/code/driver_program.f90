!===============================!
! Author: Pavlos G. galiatsatos !
! Date: 2013/09/25              !
!===============================

  program driver_program

  use m_1_type_definitions
  use m_2_heap_sort

  implicit none

  integer(kind=di), parameter :: array_size = 30000000_di
  integer(kind=di) :: i
  real(kind=dr), allocatable, dimension(:) :: array
  real(kind=dr) :: t1
  real(kind=dr) :: t2

  allocate(array(1:array_size))

  do i = 1, array_size
    array(i) = array_size-i+1
  end do

  write(*,*)
  write(*,*) "  1. the heapsort sorting algorithm. "
  write(*,*) "  2. the first 5 unsorted elements: " 
  write(*,*) 

  do i = 1 , 5
    write(*,*) i, array(i)
  end do

  call cpu_time(t1)

  call hpsort(array_size,array)

  call cpu_time(t2)

  write(*,*)
  write(*,*) "  3. the first 5 sorted elements: " 
  write(*,*)

  do i = 1 , 5
    write(*,*) i, array(i)
  end do

  write(*,*)
  write(*,*) "  4. total nummber of unsorted elements: ", array_size
  write(*,*) "  5. total real time: ", (t2-t1) 
  write(*,*)

  end program driver_program

!======!
! FINI !
!======!
