//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2014/10/24              //
//===============================//
//============================//
// Title: Fibonacci Recursive //
//============================//

#include <iostream>
using std::cout;
using std::endl;
using std::cin;

#include <ctime>
using std::time;

//======================//
// function declaration //
//======================//

unsigned long  fibonacci(unsigned long);

//================//
//  main function //
//================//

int main()
{
    int const trials = 47;

    cout << "==============================================================" << endl;
    cout << " " << endl;
    cout << "  1 --> Benchmark" << endl;
    cout << "  2 --> Name : Fibonacci Recursive (Calculate the 47th fibonacci number)" << endl;
    cout << "  3 --> Type : Sequential" << endl;
    cout << "  4 --> Compiler Intel C++ 11.1 (Windows/Linux)" << endl;
    cout << "  5 --> Programming language : C++" << endl;
    cout << "  6 --> Exact Real Time (1 x X5160)/Linux       : 126.8 seconds" << endl;
    cout << "  7 --> Exact Real Time (1 x 9140M)/Linux       : 118.2 seconds" << endl;
    cout << "  8 --> Exact Real Time (1 x Opteron 250)/Linux : 142.0 seconds" << endl;
    cout << "  9 --> Please wait... " << endl;

    // calculate the fibonacci

    clock_t t1;
    t1 = clock();

    long fib_res;

    for (int counter = 0; counter <= trials; counter++) {
        fib_res = fibonacci(counter);
    }

    clock_t t2;
    t2 = clock();

    cout << " 10 --> The results is = " << fib_res << endl;
    cout << " 11 --> Exact Real Time = " << static_cast<double>(t2-t1)/CLOCKS_PER_SEC << endl;
    cout << " " << endl;

    return 0;
}

//=====================//
// function definition //
//=====================//

// recursive method fibonacci

unsigned long fibonacci(unsigned long number)
{
    if ((number == 0) || (number == 1)) { // base cases
        return number;
    } else {
        return (fibonacci(number-1) + fibonacci(number- 2));
    }
} // end function fibonacci

//======//
// FINI //
//======//

