!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/09/25              !
!===============================!

!============================!
! Title: TYPE AND ALLOCATION !
!============================!

 program driver_program

 use m_1_type_definitions
 
 implicit none
 
 ! 1.
 
 type my_alloc_type
   real(kind=dr), allocatable, dimension(:) :: vec
 end type my_alloc_type
 
 ! 2.
 
 type(my_alloc_type), allocatable, dimension(:) :: alloc_type_var
 
 ! 3.
 
 real(kind=dr), allocatable, dimension(:,:) :: mat
 
 ! 4.
 
 integer(kind=si), parameter :: mat_dim = 15000_si
 integer(kind=si), parameter :: trials = 20_si
 real(kind=dr) :: t1
 real(kind=dr) :: t2
 integer(kind=si) :: i1
 integer(kind=si) :: i2
 integer(kind=si) :: i3

 ! 5.

 write(*,*)
 write(*,*) "========================================================"
 write(*,*) " Allocatable type benchmark"
 write(*,*) "========================================================"
 write(*,*)
 
 ! 6.
 
 allocate(alloc_type_var(1:mat_dim))
 
 ! 7.
 
 do i1 = 1, mat_dim

   allocate(alloc_type_var(i1)%vec(1:mat_dim))   

 end do  
 
 ! 8.
 
 call cpu_time(t1)
 
 do i3 = 1, trials
   do i1 = 1, mat_dim
     do i2 = 1, mat_dim

       alloc_type_var(i1)%vec(i2) = real(i1+i2, kind=dr)    

     end do
   end do  
 end do 
 
 call cpu_time(t2)
 
 ! 9.
 
 write(*,*)
 write(*,*) " 1. cpu time for initialization of allocatable type - good = ", (t2-t1)
 write(*,*)
 write(*,*) alloc_type_var(1)%vec(1)
 write(*,*) alloc_type_var(2)%vec(2)
 write(*,*) alloc_type_var(3)%vec(3)
 
 ! 10.
 
 call cpu_time(t1)
 
 do i3 = 1, trials
   do i1 = 1, mat_dim
     do i2 = 1, mat_dim

       alloc_type_var(i2)%vec(i1) = real(i1+i2, kind=dr)    

     end do
   end do  
 end do 
 
 call cpu_time(t2)
 
 ! 11.
 
 write(*,*)
 write(*,*) " 2. cpu time for initialization of allocatable type - bad =  ", (t2-t1)
 write(*,*)
 write(*,*) alloc_type_var(1)%vec(1)
 write(*,*) alloc_type_var(2)%vec(2)
 write(*,*) alloc_type_var(3)%vec(3)

 ! 12.
 
 deallocate(alloc_type_var)
 
 ! 13.
 
 allocate(mat(1:mat_dim,1:mat_dim)) 
 
 ! 14.
 
 call cpu_time(t1)
 
 do i3 = 1, trials
   do i1 = 1, mat_dim
     do i2 = 1, mat_dim

       mat(i2,i1) = real(i1+i2, kind=dr)    

     end do
   end do  
 end do 
 
 call cpu_time(t2)
 
 ! 15.
 
 write(*,*)
 write(*,*) " 3. cpu time for initialization of matrix - good =            ", (t2-t1)
 write(*,*)
 write(*,*) mat(1,1)
 write(*,*) mat(2,2)
 write(*,*) mat(3,2)
 
 ! 16.
 
 call cpu_time(t1)
 
 do i3 = 1, trials
   do i1 = 1, mat_dim
     do i2 = 1, mat_dim

       mat(i1,i2) = real(i1+i2, kind=dr)    

     end do
   end do  
 end do 
 
 call cpu_time(t2)
 
 ! 17.
 
 write(*,*)
 write(*,*) " 4. cpu time for initialization of matrix - bad =           ", (t2-t1)
 write(*,*)
 write(*,*) mat(1,1)
 write(*,*) mat(2,2)
 write(*,*) mat(3,2)
 write(*,*) ""
 
 
 end program driver_program

!======!
! FINI !
!======!
