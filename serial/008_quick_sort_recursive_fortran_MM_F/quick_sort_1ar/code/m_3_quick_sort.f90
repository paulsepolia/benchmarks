!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/09/25              !
!===============================!

  module m_3_quick_sort

  use m_1_type_definitions
  use m_2_partition_function

  implicit none
 
  contains

  recursive subroutine quick_sort(array, start_elem, end_elem)

  implicit none

  real(kind=dr), allocatable, dimension(:), intent(inout) :: array
  integer(kind=di), intent(in) :: start_elem
  integer(kind=di), intent(in) :: end_elem
  integer(kind=di):: pivot_point
     
  ! 1. the main quick sort recursive loop

  if(start_elem < end_elem) then
  
    ! 2. partition the array and get the pivot point
      
    pivot_point = partition_function(array, start_elem, end_elem)
 
    ! 3. sort the portion before the pivot point
      
    call quick_sort(array, start_elem, pivot_point-1)

    ! 4. sort the portion after the pivot point
      
    call quick_sort(array, pivot_point+1, end_elem)

  end if

  end subroutine quick_sort
 
  end module m_3_quick_sort

!======!
! FINI !
!======!
