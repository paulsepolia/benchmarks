//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2014/10/24              //
//===============================//
//=========================//
// Title: Craps Simulation //
//=========================//

#include <iostream>
using std::cout;
using std::endl;
using std::cin;

#include <cstdlib> // contains prototypes for functions srand and rand
using std::rand;
using std::srand;

#include <ctime> // contains prototype for function time
using std::time;

#include <cmath> // contains prototype for function pow
using std::pow;

//=======================//
// functions declaration //
//=======================//

long int rollDice(); // rolls dice, calculates and displays sum

//===============//
// main function //
//===============//

int main()
{
    // enumeration with constants that represent the game status

    enum Status {CONTINUE, WON, LOST}; // all caps in constants

    long winCases = 0;
    long loseCases = 0;
    long const register iterMax = static_cast<long>(pow(10.0,8));

    cout << "==============================================================" << endl;
    cout << "  1 --> Benchmark" << endl;
    cout << "  2 --> Name : Craps Simulation" << endl;
    cout << "  3 --> Type : Sequential" << endl;
    cout << "  4 --> Compiler Intel C++ 11.1 (Windows/Linux)" << endl;
    cout << "  5 --> Programming language : C++" << endl;
    cout << "  6 --> Exact Real Time (1 x X5160)/Linux       : 198.9 seconds" << endl;
    cout << "  7 --> Exact Real Time (1 x 9140M)/Linux       : 429.8 seconds" << endl;
    cout << "  8 --> Exact Real Time (1 x Opteron 250)/Linux : 237.1 seconds" << endl;
    cout << "  9 --> Please wait... " << endl;

    clock_t t1;
    t1 = clock();

    for (long iter = 1; iter <= iterMax; iter++) {
        long myPoint;  // point if no win or loss on first roll
        Status gameStatus; // can contain CONTINUE, WON or LOST

        // randomize random number generator using current time

        srand(static_cast<long>(iter));

        long sumOfDice = rollDice(); // first roll of the dice

        // determine game status and point (if needed) based on first roll

        switch(sumOfDice) {
        case 7:  // win with  7 on first roll
        case 11: // win with 11 on first roll
            gameStatus = WON;
            break;
        case 2:  // lose with  2 on first roll
        case 3:  // lose with  3 on first roll
        case 12: // lose with 12 on first roll
            gameStatus = LOST;
            break;
        default: // did not win or lose, so remember point
            gameStatus = CONTINUE; // game is not over
            myPoint = sumOfDice; // remember the point
            break; // optional at end of switch
        } // end switch

        // while game is not complete

        while(gameStatus == CONTINUE) { // not WON or LOST
            sumOfDice = rollDice(); // roll dice again

            // determine game status

            if (sumOfDice == myPoint) // win by making point
                gameStatus = WON;
            else if (sumOfDice == 7) // lose by rolling 7 before point
                gameStatus = LOST;
        } // end while

        // display won or lost message

        if (gameStatus == WON) {
            winCases = winCases + static_cast<long>(1);
        } else {
            loseCases = loseCases + static_cast<long int>(1);
        }
    } // end big for

    clock_t t2;
    t2 = clock();

    cout << " 10 --> Trials          = " << iterMax << endl;
    cout << " 11 --> Wins            = " << winCases << endl;
    cout << " 12 --> Loses           = " << loseCases << endl;
    cout << " 13 --> Loses/Wins      = " << static_cast<double>(loseCases)/winCases   << endl;
    cout << " 14 --> Exact Real Time = " << static_cast<double>(t2-t1)/CLOCKS_PER_SEC << endl;
    cout << "" << endl;

    return 0; // indicate successful termination

} // end main

//===============================================//
//  roll dice, calculate sum and display results //
//===============================================//

long rollDice()
{
    //pick random die values

    long die1 = 1 + rand()%6; // first die roll
    long die2 = 1 + rand()%6; // second die roll
    long sum = die1 + die2; // compute sum of die values

    return sum;
} // end function rollDice

//======//
// FINI //
//======//

