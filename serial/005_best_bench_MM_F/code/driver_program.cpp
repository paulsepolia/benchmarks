//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2014/10/24              //
//===============================//

//=======================//
// Title: Best Benchmark //
//=======================//

#include <iostream>
#include <cmath>
#include <ctime>
#include <cstdlib>

using std::rand;
using std::endl;
using std::cout;
using std::clock;

//===================//
// the main function //
//===================//

int main()
{
    // inteface

    cout << " ===================================================================== " << endl ;
    cout << endl;
    cout << "  1 --> The Allocations - Initializations - Deletions Benchmark" << endl ;
    cout << endl;
    cout << "  2 --> The Intel 11.1.056 C++, 64 bit. Sequential" << endl ;
    cout << endl ;
    cout << "  3 --> Allocating - Initialiazing - Deleting 1,2,3,...,100 vectors" << endl;
    cout << endl;
    cout << "  4 --> Each vector's length is 1000000" << endl;
    cout << endl;
    cout << "  5 --> Please wait..." << endl;
    cout << endl;
    cout << " ===================================================================== " << endl ;

    const int n_LV = 100;
    const int n_LVE = 1000000;

    // local variables

    time_t t1;
    time_t t2;
    time_t t3;
    time_t t4;
    time_t t5;
    time_t t6;
    time_t t7;
    time_t t8;
    double sum1 = 0.0;
    double sum2 = 0.0;
    double sum3 = 0.0 ;
    long iDEC = 0;
    long iDEL = 0;
    long iINIT = 0;

    t1 = clock();

    for (int i = 1; i != n_LV; ++i) {
        //============================================================= 1.

        t5 = clock();

        double ** LV = new double * [i+1];  // Declaration of Vectors

        for (int kA = 0; kA != i+1; ++kA) { // Creation of  Vectors
            LV[kA] = new double [n_LVE];
            iDEC = iDEC + 1;
        }

        t6 = clock();

        sum2 = sum2 + (t6-t5)/static_cast<double>(CLOCKS_PER_SEC);

        //============================================================== 2.

        t3 = clock();

        for (int kB = 0; kB != i+1; ++kB) { // Initialization of Vectors
            for (int kC = 0; kC != n_LVE; ++kC) {
                LV[kB][kC] = fmod(rand(), 100.0);
                iINIT = iINIT + 1;
            }
        }

        t4 = clock();

        sum1 = sum1 + (t4-t3)/static_cast<double>(CLOCKS_PER_SEC);

        //================================================================ 3.

        t7 = clock();

        for (int kD = 0; kD != i+1; ++kD) {
            delete [] LV[kD];
            iDEL = iDEL + 1;
        }

        delete [] LV;

        t8 = clock();

        sum3 = sum3 + (t8-t7)/double(CLOCKS_PER_SEC);
    }

    t2 = clock();

    cout << endl;
    cout << "  6 --> The Results are the following " << endl;
    cout << "  7 --> Exact Real Time = " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;
    cout << "  8 --> Exact Real Time For Initialization = " << sum1 << endl;
    cout << "  9 --> Exact Real Time For Creation = " << sum2 << endl;
    cout << " 10 --> Exact Real Time For Deletion = " << sum3 << endl;
    cout << endl;

    return 0;
}

//======//
// FINI //
//======//
