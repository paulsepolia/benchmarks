#!/bin/bash

# 1. compiling

  icpc -O3                \
       -std=c++17         \
       -Wall              \
       driver_program.cpp \
       -o x_intel
