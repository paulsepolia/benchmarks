!===================================!
! Author: Pavlos G. Galiatsatos     !
! Date: 2013/09/25                  !
! Program: The Quick Sort Algorithm !
!===================================!

  program driver_program

  use m_1_type_definitions
  use m_2_quick_sort_iter

  implicit none

  !======================!
  ! 1a. Interface starts !
  !======================!

  integer(kind=di), parameter :: DIMEN = 400000000_di

  !====================!
  ! 1b. Interface ends !
  !====================!

  real(kind=dr), allocatable, dimension(:) :: array
  integer(kind=di) :: i
  real(kind=dr) :: t1
  real(kind=dr) :: t2

  !  2. allocate RAM

  allocate(array(1:DIMEN))

  !  3. create the array to be sorted

  do i = 1, DIMEN
    array(i) = real(1+DIMEN-i, kind=dr)
  end do

  !  4. writing out some unsorted elements

  write(*,*) 
  write(*,*) "  1. The Quick Sort algorithm"
  write(*,*) "  2. Iterative version: fast"
  write(*,*) "  3. The first 5 unsorted elements:"
  write(*,*)
  do i = 1, 5
    write(*,*)  i, array(i)
  end do
  write(*,*)
  write(*,*) "  4. Sorting. Please wait..."

  !  5. main benchmark here
 
  call cpu_time(t1)
  
  call quick_sort_iter(array, DIMEN)

  call cpu_time(t2)

  !  6. outputs

  write(*,*) "  5. the first sorted elements: "
  write(*,*)
  do i = 1, 5
    write(*,*) i, array(i)
  end do
  write(*,*)
  write(*,*) "  6. total real time: ", (t2-t1)
  write(*,*) "  7. total sorted elements: ", DIMEN
  write(*,*)

  end program driver_program

!======!
! FINI !
!======!
